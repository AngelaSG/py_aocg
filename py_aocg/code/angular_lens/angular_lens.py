# !/usr/bin/env python3
"""
This module is related to FOCO project: development of Extended of Focus (EDOF) lenses


Articles and books:

*  K Uno  and I. Shimizu "Dual Focus Diﬀractive Optical Element with Extended Depth of Focus" * OPTICAL REVIEW Vol. 21, No. 5 (2014) 668–675
* A. Sabathyan, M. Golbandi "Petal-like zone plate: long depth bifocal diffractive lens and star-like beam generator" Journal of the Optical Society of America A, 35(7) 1243 (20018)
"""

from diffractio import degrees, mm, np, plt
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_math import binarize as func_binarize
from diffractio.utils_math import cart2pol, phase2amplitude
from diffractio.utils_optics import beam_width_2D
from numpy import exp, pi, sqrt

from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.utils.plotters import plot_cost_history


class Angular_lens(Scalar_field_XY):
    """Generates examples for star lenses"""

    def __init__(self, x=None, y=None, wavelength=None, info=""):
        """equal than Scalar_field_XY"""
        super(self.__class__, self).__init__(x, y, wavelength, info)

    def angular_general(self,
                        r0,
                        radius,
                        g,
                        f_ini,
                        f_end,
                        num_periods=2,
                        binarize=False,
                        angle=0 * degrees,
                        mask=True):
        """Transparent lens, from f_ini to f_end
           focal=f_ini+(f_end-f_ini)*theta/(2*sp.pi)

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            g (function): periodic function  2*pi in range (-1,1)
            f_ini (float): focal length of lens
            f_end (float): focal length of lens
            num_periods (int): num loops in a lens
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

        Example:
            lens(r0=(0 * um, 0 * um), radius=(100 * um, 200 * um), focal=(5 * mm, 10 * mm), angle=0 * degrees, mask=True)
        """

        # Vector de onda
        k = 2 * pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        f_mean = (f_end + f_ini) / 2
        f_incr = (f_end - f_ini)
        F = f_mean + f_incr / 2 * g(theta * num_periods)

        self.u = t * exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            self.u = func_binarize(self.u, -np.pi, np.pi)

        return self.u

    def daisy_lens(self,
                   r0,
                   radius,
                   f_ini,
                   f_end,
                   num_periods=2,
                   binarize=False,
                   angle=0 * degrees,
                   mask=True):
        """Transparent lens, from f_ini to f_end
           focal=f_ini+(f_end-f_ini)*theta/(2*sp.pi)

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            f_ini (float): focal length of lens
            f_end (float): focal length of lens
            num_periods (int): num loops in a lens
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

        Example:
            lens(r0=(0 * um, 0 * um), radius=(100 * um, 200 * um), focal=(5 * mm, 10 * mm), angle=0 * degrees, mask=True)
        """
        # si solamente un numero, posiciones y radius son los mismos para ambos
        # Definicion del origen, el radius y la focal

        # Vector de onda
        k = 2 * pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        f_mean = (f_end + f_ini) / 2
        f_incr = (f_end - f_ini)
        F = f_mean + f_incr / 2 * np.sin(theta * num_periods)

        self.u = t * exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            #self.phase2amplitude(matrix=False, new_field=False)
            self.binarize(kind='phase', new_field=False)

        return self.u

    def lotus_lens(self,
                   r0,
                   radius,
                   f_ini,
                   f_end,
                   num_periods=2,
                   binarize=False,
                   angle=0 * degrees,
                   mask=True):

        self.angular_general(r0, radius, triangle_periodic, f_ini, f_end,
                             num_periods, binarize, angle, mask)

    def dual_focus_lens(self,
                        r0,
                        radius,
                        f_ini,
                        f_end,
                        num_periods=2,
                        binarize=False,
                        angle=0 * degrees,
                        mask=True):

        k = 2 * pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        f_mean = (f_end + f_ini) / 2
        f_incr = (f_end - f_ini)
        F = f_mean + f_incr / 2 * np.sign(np.sin(theta * num_periods))

        self.u = t * exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            #self.phase2amplitude(matrix=False, new_field=False)
            self.binarize(kind='phase', new_field=False)

        return self.u

    def lens_fourier(self,
                     r0,
                     radius,
                     focal,
                     delta_f,
                     num_periods=2,
                     coefs=np.array([[0, 1], [1, 1]]),
                     binarize=False,
                     angle=0 * degrees,
                     mask=True,
                     verbose=False):
        """Transparent lens based in fourier series of border

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            focal (float): focal length of lens
            delta_f (float): focal length of lens
            num_periods (int): num loops in a lens
            coefs (np.array): Fourier coefficents
            binarize (bool): If True it is binarized.
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

       References:
             - J.A Gomez-Pedrero et al. "Lente oftálmica refracto-difractiva con profundidad de foco extendida" P202130631 (6 julio e 2021)
        """
        # si solamente un numero, posiciones y radius son los mismos para ambos
        # Definicion del origen, el radius y la focal

        # Vector de onda
        k = 2 * np.pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        shape = 0
        _, num_coefs = coefs.shape

        for i in range(num_coefs):
            if verbose:
                print(i, coefs[:, i])
            shape = shape + coefs[1][i] * np.cos(
                coefs[0][i] * num_periods * theta)

        F = focal + delta_f / 2 * shape

        self.u = t * np.exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            self.binarize(kind='phase')

    def lens_fourier_bn(self,
                        r0,
                        radius,
                        focal,
                        delta_f,
                        num_periods=2,
                        coefs=np.array([[0, 1], [1, 1]]),
                        binarize=False,
                        angle=0 * degrees,
                        theta_0=0 * degrees,
                        mask=True,
                        verbose=False):
        """Transparent lens based in fourier series of border

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            focal (float): focal length of lens
            delta_f (float): focal length of lens
            num_periods (int): num loops in a lens
            coefs (np.array): Fourier coefficents
            binarize (bool): If True it is binarized.
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

       References:
             - J.A Gomez-Pedrero et al. "Lente oftálmica refracto-difractiva con profundidad de foco extendida" P202130631 (6 julio e 2021)
        """
        # si solamente un numero, posiciones y radius son los mismos para ambos
        # Definicion del origen, el radius y la focal

        # Vector de onda
        k = 2 * np.pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)
        print(theta)
        shape = 0
        _, num_coefs = coefs.shape
        for i in range(num_coefs):
            if verbose:
                print(i, coefs[:, i])
            shape = shape + coefs[1][i] * np.cos(coefs[0][i] * num_periods *
                                                 (theta - theta_0)) #+ np.sin(coefs[0][i]*num_periods*theta)

        F = focal + delta_f / 2 * shape

        self.u = t * np.exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            self.u = phase2amplitude(self.u)


    def axilens(self,
                r0,
                radius,
                f_mean,
                f_incr,
                binarize=False,
                angle=0 * degrees,
                mask=True):

        k = 2 * pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        # theta = sp.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        F = f_mean + f_incr / 2 * (r / radius)**2

        self.u = t * exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            #self.phase2amplitude(matrix=False, new_field=False)
            self.binarize(kind='phase', new_field=False)

        return self.u

    def petal_lens(self,
                   r0,
                   radius,
                   focal,
                   N,
                   alpha,
                   binarize=False,
                   angle=0 * degrees,
                   mask=True):
        """Lens according to Sabatyan and Golbandi

        References:
            A. Sabathyan, M. Golbandi "Petal-like zone plate: long depth bifocal diffractive lens and star-like beam generator" Journal of the Optical Society of America A, 35(7) 1243 (20018)


        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            focal (float): focal length
            N (float): petal frequency
            alpha (float): focal length of lens
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

        Example:
            lens(r0=(0 * um, 0 * um), radius=(100 * um, 200 * um), focal=(5 * mm, 10 * mm), angle=0 * degrees, mask=True)
        """

        R = radius
        f = focal

        # k = 2 * pi / self.wavelength

        x0, y0 = r0
        Xrot, Yrot = self.__rotate__(angle)

        r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        self.u = t * np.exp(-1j * pi * (r - alpha * R * np.cos(N * theta))**2 /
                            (self.wavelength * f))
        self.u[t == 0] = 0

        if binarize is True:
            #self.phase2amplitude(matrix=False, new_field=False)
            self.binarize(kind='phase', new_field=False)

        return self.u

    def trifocus_lens(self,
                      r0,
                      radius,
                      f_mean,
                      f_incr,
                      num_periods,
                      binarize=False,
                      angle=0 * degrees,
                      power=3,
                      mask=True):
        """Lens with 3 focuses, defined as sin(N theta)**3

        References:
            Luis Miguel Sanchez Brea

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            f_mean (float): focal length
            f_incr (float): incr focal
            binarize (bool): binarizes
            num_periods (int): number of petals
            angle (float): angle of axis in radians
            power (int): odd number
            mask (bool): if True, mask with size radius

        Example:
            lens(r0=(0 * um, 0 * um), radius=(100 * um, 200 * um), focal=(5 * mm, 10 * mm), angle=0 * degrees, mask=True)
        """

        k = 2 * pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        # r = sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        F = f_mean + f_incr / 2 * np.sin(theta * num_periods)**power

        self.u = t * exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F)))
        self.u[t == 0] = 0

        if binarize is True:
            #self.phase2amplitude(matrix=False, new_field=False)
            self.binarize(kind='phase', new_field=False)

        return self.u

    def dartboard_lens(self,
                       r0,
                       diameter,
                       focal_min,
                       focal_max,
                       num_focals=4,
                       num_sectors=4,
                       has_random_focals=True,
                       angle=0 * degrees):
        """
        """
        focals = np.linspace(focal_min, focal_max, num_focals)
        sectores_aleatorios = np.random.permutation(num_focals)

        t_final = Scalar_mask_XY(x=self.x,
                                 y=self.y,
                                 wavelength=self.wavelength)
        tf = Scalar_mask_XY(x=self.x, y=self.y, wavelength=self.wavelength)
        mask = Scalar_mask_XY(x=self.x, y=self.y, wavelength=self.wavelength)

        [rho, theta] = cart2pol(t_final.X - r0[0], t_final.Y - r0[1])

        theta = theta + np.pi

        delta_angle = 2 * pi / (num_focals * num_sectors)
        delta_f = 2 * pi / (num_sectors)

        if has_random_focals is True:
            fs_ = focals[sectores_aleatorios]
        else:
            fs_ = focals

        for i_focal, focal in enumerate(fs_):
            mask = Scalar_mask_XY(x=self.x,
                                  y=self.y,
                                  wavelength=self.wavelength)

            tf.fresnel_lens(r0=r0,
                            radius=diameter / 2,
                            focal=focal,
                            kind='amplitude',
                            phase=0,
                            angle=angle)

            for i_sectors in range(num_sectors):
                ang_0 = i_focal * delta_angle + i_sectors * delta_f
                ang_1 = i_focal * delta_angle + i_sectors * delta_f + delta_angle

                ix = (theta > ang_0) & (theta <= ang_1)
                mask.u[ix] = 1

            t_final.u = t_final.u + mask.u * tf.u
        self.u = t_final.u

    def dartboard_lens_weighted(self,
                                r0,
                                diameter,
                                focals,
                                num_sectors=4,
                                has_random_focals=True):
        """
        """

        num_focals = len(focals)

        t_final = Scalar_mask_XY(x=self.x,
                                 y=self.y,
                                 wavelength=self.wavelength)
        tf = Scalar_mask_XY(x=self.x, y=self.y, wavelength=self.wavelength)
        mask = Scalar_mask_XY(x=self.x, y=self.y, wavelength=self.wavelength)

        [rho, theta] = cart2pol(t_final.X - r0[0], t_final.Y - r0[1])
        theta = theta + np.pi
        delta_angle = 2 * np.pi / (num_focals * num_sectors)
        delta_f = 2 * np.pi / (num_sectors)

        if has_random_focals is True:
            random_sectors = np.random.permutation(num_focals)
            fs_ = focals[random_sectors]
        else:
            fs_ = focals

        for i_focal, focal in enumerate(fs_):
            mask = Scalar_mask_XY(x=self.x,
                                  y=self.y,
                                  wavelength=self.wavelength)

            tf.fresnel_lens(r0=r0,
                            radius=diameter / 2,
                            focal=focal,
                            kind='amplitude',
                            phase=0)

            for i_sectors in range(num_sectors):
                ang_0 = i_focal * delta_angle + i_sectors * delta_f
                ang_1 = i_focal * delta_angle + i_sectors * delta_f + delta_angle

                ix = (theta > ang_0) & (theta <= ang_1)
                mask.u[ix] = 1

            t_final.u = t_final.u + mask.u * tf.u
            t_final.u[t_final.u > 1] = 1
        self.u = t_final.u
        return self

    def lens_axi_fourier(self,
                         r0,
                         radius,
                         focal,
                         delta_f,
                         num_periods=2,
                         coefs_radius=np.array([[0, 1], [1, 1]]),
                         coefs_fourier=np.array([[0, 1], [1, 1]]),
                         binarize=False,
                         angle=0 * degrees,
                         mask=True,
                         verbose=False):
        """Transparent lens based in fourier series of border

        Parameters:
            r0 (float, float): (x0,y0) - center of lens
            radius (float, float): radius of lens mask
            focal (float): focal length of lens
            delta_f (float): focal length of lens
            num_periods (int): num loops in a lens
            coefs_radius (np.array): radial series coefficients
            coefs_fourier (np.array): Fourier coefficients
            binarize (bool): If True it is binarized.
            angle (float): angle of axis in radians
            mask (bool): if True, mask with size radius

       References:
             - J.A Gomez-Pedrero et al. "Lente oftálmica refracto-difractiva con profundidad de foco extendida" P202130631 (6 julio e 2021)
        """
        # si solamente un numero, posiciones y radius son los mismos para ambos
        # Definicion del origen, el radius y la focal

        # Vector de onda
        k = 2 * np.pi / self.wavelength

        x0, y0 = r0
        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle)

        R = np.sqrt((Xrot - x0)**2 + (Yrot - y0)**2)
        theta = np.arctan2((Yrot - y0), (Xrot - x0))

        # Definicion de la amplitude y la phase
        if mask is True:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = np.ones_like(self.X)

        shape = 0
        _, num_coefs = coefs_fourier.shape
        for i in range(num_coefs):
            if verbose:
                print(i, coefs_fourier[:, i])
            shape = shape + coefs_fourier[1][i] * np.cos(
                coefs_fourier[0][i] * num_periods * theta)

        F1 = focal + delta_f / 2 * shape

        shape_r = 0
        _, num_coefs = coefs_radius.shape
        for i in range(num_coefs):
            if verbose:
                print(i, coefs_radius[:, i])
            shape_r = shape_r + coefs_radius[1][i] * (
                R / radius)**coefs_radius[0][i]

        F2 = shape_r

        self.u = t * np.exp(-1.j * k * ((Xrot**2 + Yrot**2) / (2 * F1 * F2)))
        self.u[t == 0] = 0

        if binarize is True:
            self.binarize(kind='phase')

        return self  # necesario para usar la función como dentro de la clase

    def lens_cylindrical_daisy(self,
                               x0,
                               focal,
                               Df,
                               period,
                               refractive_index=1.5,
                               radius=0,
                               angle=0 * degrees):
        """Daysy cylindrical lens, without paraxial approximation. The focal distance and the refraction index are used for the definition.  When the refraction index decreases, the radius of curvature decrases and less paraxial.
        Now, only one focal.
      """

        k = 2 * np.pi / self.wavelength

        r0 = (x0, 0)

        Xrot, Yrot = self.__rotate__(angle, r0)

        if radius > 0:
            amplitude = Scalar_mask_XY(self.x, self.y, self.wavelength)
            amplitude.circle(r0, radius, angle)
            t = amplitude.u
        else:
            t = 1

        self.u = t * np.exp(
            -1j * k * Xrot**2 /
            (2 * (focal + 2 * Df * np.sin(2 * np.pi * Yrot / period))))
        self.u[t == 0] = 0

    def lens_cylindrical_fourier(self,
                                 x0,
                                 focal,
                                 delta_f,
                                 period,
                                 coefs_fourier=np.array([[0, 1], [1, 1]]),
                                 binarize=False,
                                 angle=0 * degrees):
        """Transparent cylindrical lens based on fourier series of border

        Parameters:
            x0 (float): center of lens
            focal (float): focal length of lens
            delta_f (float): focal length of lens
            period (float): period of the series
            coefs_fourier (np.array): Fourier coefficients
            binarize (bool): If True it is binarized.
            angle (float): angle of axis in radians
        """

        # Vector de onda
        k = 2 * np.pi / self.wavelength

        r0 = x0, 0

        # rotation de la lens
        Xrot, Yrot = self.__rotate__(angle, r0)

        shape = 0
        _, num_coefs = coefs_fourier.shape
        for i in range(num_coefs):
            shape = shape + coefs_fourier[1][i] * np.cos(
                coefs_fourier[0][i] * Yrot / period)

        F = focal + delta_f / 2 * shape

        self.u = np.exp(-1.j * k * ((Xrot**2) / (2 * F)))

        if binarize is True:
            self.binarize(kind='phase')

    def compute_beam_width(self,
                           u_xy,
                           z0,
                           num_processors=16,
                           has_draw=True,
                           verbose=False):
        """computes beam width for scalar_field_XY. Uses Rayleigh-Sommerfeld Approach

        Parameters:
            u_xy (scalar_field_XY): field (u0*t) at a z=0 plane
            z0 (numpy_array): position z0
            num_processors (int): num processors for computation
            has_draw (bool): if True draws
            verbose (bool): if True returns data
        """

        widths = np.zeros_like(z0, dtype=float)

        X, Y = u_xy.X, u_xy.Y
        for iz, zi in enumerate(z0):
            uz = np.squeeze(self.u[:, :, iz])
            dx, dy, principal_axis, moments = beam_width_2D(uz, X, Y)
            print("{:2.2f} ".format(dx))
            widths[iz] = dx

        if has_draw is True:
            plt.figure(figsize=(12, 6))
            plt.plot(z0 / mm, widths / 2, 'k')
            plt.plot(z0 / mm, -widths / 2, 'k')

        return widths


def triangle_periodic(theta):
    # https://en.wikipedia.org/wiki/Triangle_wave
    p = 2 * np.pi
    return 4 * np.abs(theta / p - np.floor(theta / p + 0.5)) - 1


num_zones = 50


def focal_distribution_cubic(num_zones,
                             focal,
                             df,
                             lambda1,
                             lambda2,
                             has_drawing=False):

    x = np.linspace(0, 1, num_zones)
    lineal = (focal - df / 2) + df * x
    cuadratico = (focal - df / 2) + df * x**2
    cubico = focal + 2**2 * df * (x - 0.5)**3
    all_ = (1 - lambda1 -
            lambda2) * lineal + lambda1 * cuadratico + lambda2 * cubico

    if has_drawing:
        plt.figure(figsize=(12, 5))
        plt.subplot(1, 2, 1)
        plt.plot(x * num_zones, lineal, 'rx', label='f$_{1,i}$')
        plt.plot(x * num_zones, cuadratico, 'go', label='f$_{2,i}$')
        plt.plot(x * num_zones, cubico, 'b^', label='f$_{3,i}$')
        plt.xlim(0, num_zones)
        plt.ylim(focal - df / 2, focal + df / 2)
        plt.xlabel('i', fontsize=16)
        plt.ylabel('f$_{α,i}$', fontsize=16)
        plt.title('(a)', fontsize=22)
        plt.legend()

        plt.subplot(1, 2, 2)
        texto = '$f_i$ : $k_1 ={}$,  $k_2 ={}$'.format(lambda1, lambda2)
        plt.plot(x * num_zones, lineal, 'rx', label='f$_{1,i}$')
        plt.plot(x * num_zones, all_, 'ko', label=texto)
        plt.xlim(0, num_zones)
        plt.ylim(focal - df / 2, focal + df / 2)
        plt.xlabel('i', fontsize=16)
        # plt.ylabel('f$_{i}$', fontsize=16)
        plt.title('(b)', fontsize=22)
        plt.legend()

    return all_


class Optimization_lens():
    """Optimization parameters of angular lenses with Particle Swarm Optimization
    """

    def __init__(self, t0, focal, delta_f):
        """
        Initialize the class

        Args:
            t0 (Angular_lens): Angular lens to optimize
            focal (float): Focal length.
            delta_f (float): DOF length. 
        """
        #self._object = None
        self.focal = focal
        self.delta_f = delta_f
        self.t0 = t0
        self.diameter = 2*t0.x[-1]

    def intensity_theoretical(self,z):

        """
        Objective intensity of the elongated beam.

        Args:
            z (np.ndarray): z_out of the CZT algorithm.

        Returns:
            intensity (np.ndarray): Objetive intensity 
        """

        intensity = np.zeros(len(z))
        beta = 0.25

        z1 = self.focal/mm - self.delta_f/(2*mm)
        z2 = self.focal/mm + self.delta_f/(2*mm)
        for i, elem in enumerate(z):
            if elem<(self.focal-self.delta_f/2):
                intensity[i] = 1/(1+(beta*(elem/mm-z1))**2)
            elif elem>(self.focal+self.delta_f/2):
                intensity[i] = 1/(1+(beta*(elem/mm-z2))**2)        
            else:
                intensity[i] = 1    
        return intensity


    def width_theoretical(self,z):

        """Objective width of the elongated beam.

        Args:
            z (np.ndarray): z_out of the CZT algorithm.

        Returns:
            width (np.ndarray): Objetive intensity 
        """

        width = np.zeros(len(z))   
        # beta=1/25
        beta=1
        z1 = self.focal/mm - self.delta_f/(2*mm)
        z2 = self.focal/mm + self.delta_f/(2*mm)
        for i,elem in enumerate(z):
            if elem<(self.focal-self.delta_f/2):
                width[i] = 1-beta*(elem/mm-z1)       
            elif elem>(self.focal+self.delta_f/2):
                width[i] = 1+beta*(elem/mm-z2)   
            else:
                width[i] = 1      

        return width

    def anchura(self,z, w0, w1, delta=2, has_draw=True):

        """
        to do: Unificar con la función width_theoretical. Hacen lo mismo pero con fórmulas diferentes.
        """
        
        z_range = z[-1]-z[0]
        width_0 =  np.abs((z-380000) / z_range)**delta

        width = w0 + np.abs(((z-380000)  / z_range))**delta * (w1 - w0) / width_0.max()

        if has_draw:
            plt.figure()
            plt.plot(z,width,'k',lw=2)
            plt.xlim(z[0],z[-1])
            #plt.ylim(0, 80)

        return width

    def fourier_lens_an(self,an,u0,z_out,x_out,num_periods):
        """Merit function to optimize the coefficients of a Fourier lens."""

        n = np.arange(1,len(an)+1,1)
        coefs = np.array([n,an])
        self.t0.lens_fourier(r0=(0,0), radius=self.diameter/2,focal=self.focal, delta_f=self.delta_f,num_periods=num_periods, coefs=coefs)
        u1 = self.t0*u0
        u_czt_xz = u1.CZT(z_out, x_out, yout=np.array([0.,]))

        width = u_czt_xz.beam_widths(kind='FWHM1D',has_draw=(False,False))
        w = width[0]/width[0].min()

        pos_a = np.where(z_out<self.focal - self.delta_f/2)[0]
        pos_b = np.where(z_out>self.focal + self.delta_f/2)[0]
        intensidad_maxima=(u_czt_xz.intensity()).max(axis=0)
        intensidad_maxima = intensidad_maxima/intensidad_maxima.max()
        It = self.intensity_theoretical(z_out)
        wt = self.width_theoretical(z_out)
    
       
        merit = np.sqrt((np.sum(intensidad_maxima[pos_a.max():pos_b.min()]-It[pos_a.max():pos_b.min()])**2)) + np.sqrt(np.sum((w[pos_a.max():pos_b.min()]-wt[pos_a.max():pos_b.min()])**2))

        return merit

    def fourier_lens_k(self,k,u0,z_out,x_out,num_periods,w0,w1,delta,n_coef):
        """ Merit function to optimize k1 and k2 of a Fourier lens. """

        n = np.arange(1,n_coef,1)
        coef_lineal,coef_cuadratico,coef_cubico = np.zeros(len(n)),np.zeros(len(n)),np.zeros(len(n))

        for i,elem in enumerate(n):
            coef_cuadratico[i] = 8/(np.pi*elem)**2
            if elem%2 != 0:
                coef_lineal[i] =  8/(np.pi*elem)**2
                coef_cubico[i] = -6/(np.pi*elem**4)+3*np.pi/(4*elem**2)

        an   = (1-k[0]-k[1])*coef_lineal + k[0]*coef_cuadratico + k[1]*coef_cubico

        coefs = np.array([n,an])
       
        self.t0.lens_fourier(r0=(0,0), radius = self.diameter /2, focal=self.focal, 
                        delta_f=self.delta_f, num_periods=num_periods, coefs=coefs)
        u1 = self.t0*u0
        u_czt_xz = u1.CZT(z_out, x_out, yout=np.array([0.,]))
        width = u_czt_xz.beam_widths(kind='FWHM1D',has_draw=(False,False))
        DOF = width[0]

        pos_a = np.where(z_out<self.focal - self.delta_f/2)[0]
        pos_b = np.where(z_out>self.focal + self.delta_f/2)[0]
        width_function = self.anchura(z_out, w0=w0, w1=w1,delta=delta, has_draw=False)
        merit = np.sqrt(((width_function[pos_a.max():pos_b.min()]
                        -DOF[pos_a.max():pos_b.min()])**2).sum()
                        /len(DOF[pos_a.max():pos_b.min()]))
        return merit


    def opt_PSO(self,p,fun,args):
        """General function to optimize paramterts using PSO.

        Args:
            p (np.ndarray): Parameters to be optimized.
            fun (function): Merit function.
            args (tuple): Arguments of the merit function.

        Returns:
            merit (float): minimun value reached of the merit function.
        """
        num_particles = p.shape[0]  # number of particles
        merit = [fun(p[i,:],*args) for i in range(num_particles)]

        return merit


    def opt_fourier_k(self,n_particles,options,bounds,p0,args,draw_cost=False):
        """PSO function to optimize the parameters k1 and k2 of a Fourier lens.

        Args:
            n_particles (int): Number of particles of the PSO process.
            options (dict): Hyperparameters of the PSO process. 
            bounds (np.ndarray,np.ndarray): Minimum and maximum limits.
            p0 (np.ndarray): Initial positions
            args (tuple): Arguments of the merit function.
            draw_cost (bool): If draw_cost, the value of the merit function 
            is shown over the iterations.

        Returns:
            cost, transitions (float, np.ndarray): cost function value, optimized parameters.
        """

        optimizer = GlobalBestPSO(n_particles=n_particles, dimensions=len(bounds[0]), 
                                options=options, bounds=bounds,init_pos=p0)

        cost, transitions = optimizer.optimize(self.opt_PSO, iters=10, fun=self.fourier_lens_k, args=args)

        if draw_cost:
            plot_cost_history(optimizer.cost_history)

        return cost,transitions


    def opt_fourier_an(self,n_particles,options,bounds,p0,args,draw_cost=False):

        """PSO function to optimize the coeficients of a Fourier lens.

        Args:
            n_particles (int): Number of particles of the PSO process.
            options (dict): Hyperparameters of the PSO process. 
            bounds (np.ndarray,np.ndarray): Minimum and maximum limits.
            p0 (np.ndarray): Initial positions
            args (tuple): Arguments of the merit function.
            draw_cost (bool): If draw_cost, the value of the merit function 
            is shown over the iterations.

        Returns:
            cost, transitions (float, np.ndarray): cost function value, optimized parameters.
        """

        optimizer = GlobalBestPSO(n_particles=n_particles, dimensions=len(bounds[0]), 
                                options=options, bounds=bounds,init_pos=p0)

        cost, transitions_final = optimizer.optimize(self.opt_PSO, iters=15, fun=self.fourier_lens_an, args=args)

        if draw_cost:
            plot_cost_history(optimizer.cost_history)

        return cost,transitions_final

