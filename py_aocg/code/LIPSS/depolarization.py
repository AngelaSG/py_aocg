# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module is related to LIPSS depolarization."""


from scipy.io import loadmat, savemat
import datetime

from matplotlib import rcParams


from diffractio import np, plt, sp
from diffractio import degrees, mm, um


from diffractio.utils_optics import roughness_1D, roughness_2D
from diffractio.utils_math import find_local_extrema
from scipy.optimize import curve_fit

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes

from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

# from py_lab.utils import S_4
import glob

rcParams['figure.figsize'] = (20, 4)


Tetraedro = np.array([[1, 1, 0, 0], [1, -1/3, np.sqrt(8)/3, 0], [1, -1/3, -np.sqrt(2)/3, np.sqrt(6)/3], [1, -1/3, -np.sqrt(2)/3, -np.sqrt(6)/3]]).T

S_4 = Stokes().from_matrix(Tetraedro)#.analysis.filter_physical_conditions(tol=0)


class Generate_rough_surface(object):
    """Generates Rough profiles of LIPSS"""

    def __init__(self, x=None, profile=None, info=""):
        """equal than Scalar_field_XY"""
        self.x = x
        if profile is not None:
            self.profile = profile
        else:
            self.profile = np.zeros_like(x)
        self.profile_clean = np.zeros_like(x)
        self.profile_roughness = np.zeros_like(x)

    def draw_profile(self, kind='profile', title=''):
        """_summary_

        Args:
            x (_type_): _description_
            p (_type_): _description_
            title (str, optional): _description_. Defaults to ''.
        """
        if kind == 'profile':
            y = self.profile
        elif kind == 'clean':
            y = self.profile_clean
        elif kind == 'roughness':
            y = self.profile_roughness

        plt.plot(self.x, y, linewidth=1.5, c='blue', ls='-')
        plt.xlabel('x ($\mu$m)')
        plt.ylabel('h ($\mu$m)')
        plt.xlim(self.x[0], self.x[-1])
        plt.grid(True)
        plt.title(title)
        plt.tight_layout()

    def load_profile(self, filename, scale='meters', has_draw=False):
        """Loadss a txt

        Args:
            filename (_type_): _description_
            scale (str, optional): _description_. Defaults to 'meters'.
            has_draw (bool, optional): _description_. Defaults to False.
        """
        data = np.loadtxt(filename)
        if scale == 'meters':
            factor = 1e6
        elif scale == 'microns':
            factor = 1

        self.x = data[:, 0]*factor
        self.profile = data[:, 1]*factor

        if has_draw == True:
            self.draw_profile()

    def compute_variogram(self, ipoints, has_draw=False):
        """compute the experimental variogram

        Args:
            ipoints (np.array integers): compute at points where i_points = np.arange(integer, integer, integer)

        Returns:
            _type_: _description_
        """

        variogram = np.zeros_like(ipoints, dtype=float)
        num_points = np.zeros_like(ipoints, dtype=int)

        num_data = len(self.x)
        num_h = ipoints.size

        y = self.profile

        for i in np.arange(0, num_h):
            hi = ipoints[i]
            VAR = 0
            n_data = 0
            for j in np.arange(1, hi + 1):
                positions = np.arange(j, num_data, hi, dtype='int')
                values = y[positions]
                diffs = np.diff(values)**2
                VAR = VAR + diffs.sum()
                n_data = n_data + diffs.size

            if n_data > 0:
                variogram[i] = VAR / (2 * n_data)
            else:
                variogram[i] = -1
            num_points[i] = n_data

        # Remove points with no data
        i_remove = np.argwhere(num_points == 0)
        # data = np.delete(data, i_remove)
        variogram = np.delete(variogram, i_remove)
        num_points = np.delete(num_points, i_remove)
        ipoints = np.delete(ipoints, i_remove)

        incr_h = self.x[1] - self.x[0]
        h = ipoints * incr_h
        variogram = variogram
        num_h = num_points
        i_pos = ipoints
        # self.imax = len(self.h)

        if has_draw:
            plt.figure()
            plt.plot(h, np.sqrt(variogram))
            plt.xlabel('h')
            plt.ylabel('$\gamma^{1/2}$')

            plt.ylim(0)
            plt.xlim(h[0], h[-1])

        return h, variogram, num_h, i_pos

    def profile_parameters(self, ipoints=None, has_draw=True, verbose=True):
        """_summary_

        Args:
            x (_type_): _description_
            p (_type_): _description_
            i_points (_type_, optional): _description_. Defaults to None.
            has_draw (bool, optional): _description_. Defaults to True.
            verbose (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        if ipoints is None:
            length_variogram = int(len(self.x)/2)
            delta_h = 1
            num_puntos = int(length_variogram / (self.x[1]-self.x[0]))
            print(num_puntos)
            ipoints = np.arange(0, num_puntos, delta_h)

        std_1d = np.std(self.profile)
        lc = 0

        h, variogram, num_h, i_pos = self.compute_variogram(ipoints)
        x_minima_frac, y_minima_frac, i_pos_integ = find_local_extrema('maxima', variogram, h)
        lc = y_minima_frac[0]/2  # TODO: estoy es temporal
        print(lc, x_minima_frac, np.sqrt(y_minima_frac), i_pos_integ)

        if verbose:
            print("s_corr = {:2.2f} um".format(std_1d))
            print("noise = {:2.4f} um".format(np.sqrt(variogram[0])))
            print("lc = {:2.4f} um".format(lc))
            print(h[i_pos_integ], np.sqrt(variogram[i_pos_integ]))

        if has_draw:
            plt.figure()
            plt.plot(h, np.sqrt(variogram))
            plt.xlabel('h')
            plt.ylabel('$\gamma^{1/2}$')

            plt.ylim(0)
            plt.xlim(h[0], h[-1])

        return std_1d, lc, h, variogram

    # def profile_supergaussian_periodical(self,x,  num_periods, x0, h1,sigma1,beta1,h2,sigma2,beta2, has_draw=True):
    #     """_summary_

    #     Args:
    #         x (_type_): _description_
    #         num_periods (_type_): _description_
    #         x0 (_type_): _description_
    #         h1 (_type_): _description_
    #         sigma1 (_type_): _description_
    #         beta1 (_type_): _description_
    #         h2 (_type_): _description_
    #         sigma2 (_type_): _description_
    #         beta2 (_type_): _description_
    #         has_draw (bool, optional): _description_. Defaults to True.

    #     Returns:
    #         _type_: _description_
    #     """
    #     if num_periods > 1:
    #             x_new = np.linspace(x[0], x[-1], int(len(x)/num_periods))
    #     else:
    #             x_new = x

    #     supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
    #     supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
    #     y = supergauss1 - supergauss2

    #     if num_periods>1:
    #             y_new = np.tile(y,num_periods)
    #     else:
    #             y_new = y

    #     if has_draw:
    #         self.draw_profile('profile')

    #     return y_new

    # def profile_supergaussian(self, period,  num_periods, x0, h1,sigma1,beta1,h2,sigma2,beta2, lc_noise=1000000, std_noise=0, has_draw=True):
    #     """_summary_

    #     Args:
    #         x (_type_): _description_
    #         period (_type_): _description_
    #         num_periods (_type_): _description_
    #         x0 (_type_): _description_
    #         h1 (_type_): _description_
    #         sigma1 (_type_): _description_
    #         beta1 (_type_): _description_
    #         h2 (_type_): _description_
    #         sigma2 (_type_): _description_
    #         beta2 (_type_): _description_
    #         lc_noise (int, optional): _description_. Defaults to 1000000.
    #         std_noise (int, optional): _description_. Defaults to 0.
    #         has_draw (bool, optional): _description_. Defaults to True.

    #     Returns:
    #         _type_: _description_
    #     """

    #     profile_clean = np.zeros_like(self.x)

    #     data_types = (float, int)
    #     if num_periods == 1:
    #         profile = profile_supergaussian_periodical(self.x,  num_periods, x0, h1,sigma1,beta1,h2,sigma2,beta2, has_draw=False)
    #     else:
    #         if isinstance(x0, data_types):
    #             x0 = x0*np.ones(num_periods)
    #         if isinstance(h1, data_types):
    #             h1 = h1*np.ones(num_periods)
    #         if isinstance(sigma1, data_types):
    #             sigma1 = sigma1*np.ones(num_periods)
    #         if isinstance(beta1, data_types):
    #             beta1 = beta1*np.ones(num_periods)
    #         if isinstance(h2, data_types):
    #             h2 = h2*np.ones(num_periods)
    #         if isinstance(sigma2, data_types):
    #             sigma2 = sigma2*np.ones(num_periods)
    #         if isinstance(beta2, data_types):
    #             beta2 = beta2*np.ones(num_periods)

    #         for i in range(num_periods):
    #             x_center = x[0]+ i*period + x0[i] + period/2
    #             supergauss1 = h1[i]*np.exp(-(np.abs(self.x-x_center)/sigma1[i])**beta1[i])
    #             supergauss2 = h2[i]*np.exp(-(np.abs(self.x-x_center)/sigma2[i])**beta2[i])
    #             y = supergauss1 - supergauss2

    #             profile_clean = profile_clean + y

    #     if std_noise > 0:
    #         profile_noise = roughness_convolution(self.x, lc_noise, std_noise, has_draw=False)
    #     else:
    #         profile_noise = np.zeros_like(self.x)

    #     profile = profile_clean + profile_noise
    #     profile = profile - profile.min()
    #     profile_clean = profile_clean - profile_clean.min()

    #     if has_draw:
    #         self.draw_profile('profile')

    #     self.profile=profile
    #     self.profile_clean=profile_clean
    #     self.profile_noise = profile_noise

    #     return profile, profile_clean, profile_noise

    def roughness_convolution(self, lc_noise, std_noise, has_draw=False):
        """"_summary_"

        Args:
            x (_type_): _description_
            lc_noise (_type_): _description_
            std_noise (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        profile = roughness_1D(self.x, t=lc_noise, s=std_noise, kind='normal')
        std_1d = np.std(profile)

        # std_measured, lc, h, variogram = profile_parameters(x,profile, verbose=False, has_draw=True)
        self.profile = profile

        if has_draw:
            self.draw_profile(kind='profile')

            plt.plot([self.x[0], self.x[-1]], [std_1d, std_1d], 'r-.')
            plt.plot([self.x[0], self.x[-1]], [-std_1d, -std_1d], 'r-.')

        return profile

    def roughness_Fourier(self, num_orders, spectrum=None, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            num_orders (_type_): _description_
            spectrum (_type_, optional): _description_. Defaults to None.
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_ods > 1:
                x_new = np.linspace(x[0], x[-1], int(len(x)/num_periods))
        else:
                x_new = x

        supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
        supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
        y = supergauss1 - supergauss2

        if num_periods>1:
                y_new = np.tile(y,num_periods)
        else:
                y_new = y

        if has_draw:
                plt.figure()
                plt.plot(x,y_new, linewidth=1.5, c='blue', ls='-')
                plt.xlabel(r'x ($\mu$m)')
                plt.ylabel(r'h  ($\mu$m)')
                plt.xlim(x[0], x[-1])
                plt.grid(True)
                plt.show()

        return y_new
        """

        orders = np.arange(0, num_orders+1)
        periodo = (self.x[-1]-self.x[0])

        media_g = 0
        std_g = 1
        media_u = 0
        std_u = 1
        amplitudes_random = np.random.normal(media_g, std_g, len(orders))
        phases_random = np.random.normal(media_u, std_u, len(orders))
        if spectrum is None:
            spectrum = np.ones_like(orders)

        profile = np.zeros(len(self.x))
        for order in orders:
            pp_jj = spectrum[order] * amplitudes_random[order] * np.cos(2 * np.pi * order * self.x/periodo + phases_random[order])
            profile = profile + pp_jj

        self.profile = profile
        # std_1d, lc, h, variogram = profile_parameters(x,profile, verbose=True)
        std_1d = np.std(profile)

        if has_draw:
            self.draw_profile(kind='profile')

            plt.plot([self.x[0], self.x[-1]], [std_1d, std_1d], 'r-.')
            plt.plot([self.x[0], self.x[-1]], [-std_1d, -std_1d], 'r-.')

        return profile

    def profile_supergaussian_periodical(self, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        if num_periods > 1:
            x_new = np.linspace(self.x[0], self.x[-1], int(len(self.x)/num_periods))
        else:
            x_new = self.x

        supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
        supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
        y = supergauss1 - supergauss2

        if num_periods > 1:
            y_new = np.tile(y, num_periods)
        else:
            y_new = y

        if has_draw:
            plt.figure()
            plt.plot(self.x, y_new, linewidth=1.5, c='blue', ls='-')
            plt.xlabel(r'x ($\mu$m)')
            plt.ylabel(r'h  ($\mu$m)')
            plt.xlim(self.x[0], self.x[-1])
            plt.grid(True)
            plt.show()

        self.profile = y_new
        return y_new

    def profile_supergaussian(self, period, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, lc_noise=1000000, std_noise=0, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            period (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            lc_noise (int, optional): _description_. Defaults to 1000000.
            std_noise (int, optional): _description_. Defaults to 0.
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """

        profile_clean = np.zeros_like(self.x)

        data_types = (float, int)
        if num_periods == 1:
            profile_clean = self.profile_supergaussian_periodical(num_periods, x0, h1, sigma1, beta1,
                                                                  h2, sigma2, beta2, has_draw=False)
        else:
            if isinstance(x0, data_types):
                x0 = x0*np.ones(num_periods)
            if isinstance(h1, data_types):
                h1 = h1*np.ones(num_periods)
            if isinstance(sigma1, data_types):
                sigma1 = sigma1*np.ones(num_periods)
            if isinstance(beta1, data_types):
                beta1 = beta1*np.ones(num_periods)
            if isinstance(h2, data_types):
                h2 = h2*np.ones(num_periods)
            if isinstance(sigma2, data_types):
                sigma2 = sigma2*np.ones(num_periods)
            if isinstance(beta2, data_types):
                beta2 = beta2*np.ones(num_periods)

            for i in range(num_periods):
                x_center = self.x[0] + i*period + x0[i] + period/2
                supergauss1 = h1[i]*np.exp(-(np.abs(self.x-x_center)/sigma1[i])**beta1[i])
                supergauss2 = h2[i]*np.exp(-(np.abs(self.x-x_center)/sigma2[i])**beta2[i])
                y = supergauss1 - supergauss2

                profile_clean = profile_clean + y

        if std_noise > 0:
            profile_noise = self.roughness_convolution(lc_noise, std_noise, has_draw=False)
        else:
            profile_noise = np.zeros_like(self.x)

        profile = profile_clean+profile_noise
        profile = profile - profile.min()

        self.profile = profile
        self.profile_clean = profile_clean
        self.profile_roughness = profile_noise

        if has_draw:
            self.draw_profile(kind='profile')

        return profile, profile_noise, profile_clean

    def __profile_cosb_periodical(self, num_periods, period, GH, FF, beta, x0,
                                  lc_noise=1e4, std_noise=0, has_draw=True):
        """
        __profile_cosb_periodical _summary_

        _extended_summary_

        Args:
            num_periods (_type_): _description_
            period (_type_): _description_
            GH (_type_): _description_
            FF (_type_): _description_
            beta (_type_): _description_
            x0 (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.
        """
        if num_periods > 1:
            x_new = np.linspace(self.x[0], self.x[-1], int(len(self.x)/num_periods))
        else:
            x_new = self.x

        y = GH*(1-2*(0.5*(1+np.cos(2*np.pi*(x_new-x0-2*FF*period/2)/(2*FF*period)))**beta))
        y[y < 0] = 0
        y[x_new < x0 - 2*FF*period/2] = 0
        y[x_new > x0 + 2*FF*period/2] = 0

        if num_periods > 1:
            y_new = np.tile(y, num_periods)
        else:
            y_new = y

        if has_draw:
            plt.figure()
            plt.plot(self.x, y_new, linewidth=1.5, c='blue', ls='-')
            plt.xlabel(r'x ($\mu$m)')
            plt.ylabel(r'h  ($\mu$m)')
            plt.xlim(self.x[0], self.x[-1])
            plt.grid(True)
            plt.show()

        self.profile_clean = y_new
        self.profile = y_new

    def profile_cosb(self, num_periods, period, GH, FF, beta, x0, lc_noise=1e4, std_noise=0, has_draw=True):
        """
        profile_cosb _summary_

        _extended_summary_

        Args:
            num_periods (_type_): _description_
            period (_type_): _description_
            GH (_type_): _description_
            FF (_type_): _description_
            beta (_type_): _description_
            x0 (_type_): _description_
            lc_noise (int, optional): _description_. Defaults to 1000000.
            std_noise (int, optional): _description_. Defaults to 0.
            has_draw (bool, optional): _description_. Defaults to True.
        """

        self.profile = np.zeros_like(self.x)
        self.profile_clean = np.zeros_like(self.x)
        self.profile_roughness = np.zeros_like(self.x)

        data_types = (float, int)
        if num_periods == 1:
            x_center = 0
            print(x_center)
            self.__profile_cosb_periodical(1, period, GH, FF, beta, x_center, has_draw=False)
            profile_clean = self.profile_clean
        else:
            if isinstance(x0, data_types):
                x0 = x0*np.ones(num_periods+1)
            if isinstance(GH, data_types):
                GH = GH*np.ones(num_periods+1)
            if isinstance(beta, data_types):
                beta = beta*np.ones(num_periods+1)
            if isinstance(FF, data_types):
                FF = FF*np.ones(num_periods+1)
            profile_clean = np.zeros_like(self.x)

            for i in range(num_periods+1):
                x_center = self.x[0] + i*period + x0[i]
                self.__profile_cosb_periodical(1, period, GH[i], FF[i],
                                               beta[i], x_center, has_draw=False)
                y = self.profile

                profile_clean = profile_clean + y

        if std_noise > 0:
            profile_noise = self.roughness_convolution(lc_noise, std_noise, has_draw=False)
        else:
            profile_noise = np.zeros_like(self.x)

        profile = profile_clean+profile_noise
        profile = profile - profile.min()

        self.profile = profile
        self.profile_clean = profile_clean
        self.profile_roughness = profile_noise

        if has_draw:
            self.draw_profile(kind='profile')

        # return profile, profile_noise, profile_clean

    def save_profile(self, filename, kind='profile', add_name='', params_supergaussian=None):
        """_summary_

        Args:
            filename (_type_): _description_
            x (_type_): _description_
            y (_type_): _description_
            params_supergaussian (_type_, optional): _description_. Defaults to None.
        """
        if kind == 'profile':
            y = self.profile
        elif kind == 'clean':
            y = self.profile_clean
        elif kind == 'roughness':
            y = self.profile_roughness

        data = (np.vstack(((1e-6*self.x).transpose(), (1e-6*y).transpose())).transpose())
        np.savetxt(filename+add_name, data, fmt='%1.4e', delimiter='\t')

        if params_supergaussian is not None:
            save_dictionary(params_supergaussian,
                            filename[:-4]+'.mat',
                            add_name=add_name,
                            description='',
                            verbose=False)
            save_dictionary(params_supergaussian,
                            filename[:-4]+'.npz',
                            add_name=add_name,
                            description='',
                            verbose=False)


class Generate_rough_surface_2D(object):
    """Generates Rough profiles of LIPSS"""

    def __init__(self, x=None, y=None, profile=None, info=""):
        """equal than Scalar_field_XY"""
        self.x = x
        self.y = y

        self.X, self.Y = np.meshgrid(self.x, self.y)

        if profile is not None:
            self.profile = profile
        else:
            self.profile = np.zeros_like(self.X)
        self.profile_clean = np.zeros_like(self.X)
        self.profile_roughness = np.zeros_like(self.X)

    def draw_profile(self, kind='profile', title=''):
        """_summary_

        Args:
            x (_type_): _description_
            p (_type_): _description_
            title (str, optional): _description_. Defaults to ''.
        """
        if kind == 'profile':
            z = self.profile
        elif kind == 'clean':
            z = self.profile_clean
        elif kind == 'roughness':
            z = self.profile_roughness
        else:
            print("bad name: 'profile', 'clean', 'roughness")

        plt.figure(figsize=(4, 4))
        plt.imshow(z, extent=[self.x[0], self.x[-1], self.y[0], self.y[-1]], cmap='gray')
        plt.xlabel('x ($\mu$m)')
        plt.ylabel('y ($\mu$m)')
        plt.title(title)
        plt.tight_layout()

    def load_profile(self, filename, scale='meters', has_draw=False):
        """Loadss a txt

        Args:
            filename (_type_): _description_
            scale (str, optional): _description_. Defaults to 'meters'.
            has_draw (bool, optional): _description_. Defaults to False.
        """
        data = np.loadtxt(filename)
        if scale == 'meters':
            factor = 1e6
        elif scale == 'microns':
            factor = 1

        self.x = data[:, 0]*factor
        self.y = data[:, 1]*factor
        self.profile = data[:, 2]*factor

        if has_draw == True:
            self.draw_profile()

    # def compute_variogram(self, ipoints, has_draw=False):
    #     """compute the experimental variogram

    #     Args:
    #         ipoints (np.array integers): compute at points where i_points = np.arange(integer, integer, integer)

    #     Returns:
    #         _type_: _description_
    #     """

    #     variogram = np.zeros_like(ipoints, dtype=float)
    #     num_points = np.zeros_like(ipoints, dtype=int)

    #     num_data = len(self.x)
    #     num_h = ipoints.size

    #     y = self.profile

    #     for i in np.arange(0, num_h):
    #         hi = ipoints[i]
    #         VAR = 0
    #         n_data = 0
    #         for j in np.arange(1, hi + 1):
    #             positions = np.arange(j, num_data, hi, dtype='int')
    #             values = y[positions]
    #             diffs = np.diff(values)**2
    #             VAR = VAR + diffs.sum()
    #             n_data = n_data + diffs.size

    #         if n_data > 0:
    #             variogram[i] = VAR / (2 * n_data)
    #         else:
    #             variogram[i] = -1
    #         num_points[i] = n_data

    #     # Remove points with no data
    #     i_remove = np.argwhere(num_points == 0)
    #     # data = np.delete(data, i_remove)
    #     variogram = np.delete(variogram, i_remove)
    #     num_points = np.delete(num_points, i_remove)
    #     ipoints = np.delete(ipoints, i_remove)

    #     incr_h = self.x[1] - self.x[0]
    #     h = ipoints * incr_h
    #     variogram = variogram
    #     num_h = num_points
    #     i_pos = ipoints
    #     # self.imax = len(self.h)

    #     if has_draw:
    #         plt.figure()
    #         plt.plot(h,np.sqrt(variogram))
    #         plt.xlabel('h')
    #         plt.ylabel('$\gamma^{1/2}$')

    #         plt.ylim(0)
    #         plt.xlim(h[0], h[-1])

    #     return h, variogram, num_h, i_pos

    # def profile_parameters(self, ipoints=None, has_draw=True, verbose=True):
    #     """_summary_

    #     Args:
    #         x (_type_): _description_
    #         p (_type_): _description_
    #         i_points (_type_, optional): _description_. Defaults to None.
    #         has_draw (bool, optional): _description_. Defaults to True.
    #         verbose (bool, optional): _description_. Defaults to True.

    #     Returns:
    #         _type_: _description_
    #     """
    #     if ipoints is None:
    #         length_variogram = int(len(self.x)/2)
    #         delta_h = 1
    #         num_puntos = int(length_variogram / (self.x[1]-self.x[0]))
    #         print(num_puntos)
    #         ipoints = np.arange(0, num_puntos, delta_h)

    #     std_1d = np.std(self.profile)
    #     lc=0

    #     h, variogram, num_h, i_pos = self.compute_variogram(ipoints)
    #     x_minima_frac, y_minima_frac, i_pos_integ = find_local_extrema('maxima', variogram, h)
    #     lc = y_minima_frac[0]/2 #TODO: estoy es temporal
    #     print(lc, x_minima_frac, np.sqrt(y_minima_frac), i_pos_integ)

    #     if verbose:
    #         print("s_corr = {:2.2f} um".format(std_1d))
    #         print("noise = {:2.4f} um".format(np.sqrt(variogram[0])))
    #         print("lc = {:2.4f} um".format(lc))
    #         print(h[i_pos_integ], np.sqrt(variogram[i_pos_integ]))

    #     if has_draw:
    #         plt.figure()
    #         plt.plot(h,np.sqrt(variogram))
    #         plt.xlabel('h')
    #         plt.ylabel('$\gamma^{1/2}$')

    #         plt.ylim(0)
    #         plt.xlim(h[0], h[-1])

    #     return std_1d, lc, h, variogram

    def roughness_Fourier(self, num_orders, spectrum=None, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            num_orders (_type_): _description_
            spectrum (_type_, optional): _description_. Defaults to None.
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_ods > 1:
                x_new = np.linspace(x[0], x[-1], int(len(x)/num_periods))
        else:
                x_new = x

        supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
        supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
        y = supergauss1 - supergauss2

        if num_periods>1:
                y_new = np.tile(y,num_periods)
        else:
                y_new = y

        if has_draw:
                plt.figure()
                plt.plot(x,y_new, linewidth=1.5, c='blue', ls='-')
                plt.xlabel(r'x ($\mu$m)')
                plt.ylabel(r'h  ($\mu$m)')
                plt.xlim(x[0], x[-1])
                plt.grid(True)
                plt.show()

        return y_new
        """

        orders = np.arange(0, num_orders+1)
        periodo_x = (self.x[-1]-self.y[0])
        periodo_y = (self.X[-1]-self.y[0])

        media_g = 0
        std_g = 1
        media_u = 0
        std_u = 1
        amplitudes_random = np.random.normal(media_g, std_g, len(orders))
        phases_random_x = np.random.normal(media_u, std_u, len(orders))
        phases_random_y = np.random.normal(media_u, std_u, len(orders))
        if spectrum is None:
            spectrum = np.ones_like(orders)

        profile = np.zeros(len(x))
        for order in orders:
            p1 = np.cos(2 * np.pi * order * self.X/periodo_x + phases_random_x[order])
            p2 = np.cos(2 * np.pi * order * self.Y/periodo_y + phases_random_y[order])
            pp_jj = spectrum[order] * amplitudes_random[order] * p1 * p2
            profile = profile + pp_jj

        if has_draw:
            self.draw_profile(kind='profile')

        return profile

    def roughness_convolution(self, lc_noise, std_noise, has_draw=False):
        """"_summary_"

        Args:
            lc_noise (_type_): _description_
            std_noise (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        profile = roughness_2D(self.x, self.y, t=lc_noise, s=std_noise)
        std_1d = np.std(profile)

        # std_measured, lc, h, variogram = profile_parameters(x,profile, verbose=False, has_draw=True)

        self.profile_roughness = profile

        if has_draw:
            self.draw_profile(kind='roughness')

        return profile

    def profile_supergaussian_periodical(self, x, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        if num_periods > 1:
            x_new = np.linspace(x[0], x[-1], int(len(x)/num_periods))
        else:
            x_new = x

        supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
        supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
        y = supergauss1 - supergauss2

        if num_periods > 1:
            y_new = np.tile(y, num_periods)
        else:
            y_new = y

        if has_draw:
            self.draw_profile('profile')

        return y_new

    def profile_supergaussian(self, period, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, lc_noise=1000000, std_noise=0, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            period (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            lc_noise (int, optional): _description_. Defaults to 1000000.
            std_noise (int, optional): _description_. Defaults to 0.
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """

        profile_clean = np.zeros_like(self.x)

        data_types = (float, int)
        if num_periods == 1:
            profile = profile_supergaussian_periodical(self.x, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, has_draw=False)
        else:
            if isinstance(x0, data_types):
                x0 = x0*np.ones(num_periods)
            if isinstance(h1, data_types):
                h1 = h1*np.ones(num_periods)
            if isinstance(sigma1, data_types):
                sigma1 = sigma1*np.ones(num_periods)
            if isinstance(beta1, data_types):
                beta1 = beta1*np.ones(num_periods)
            if isinstance(h2, data_types):
                h2 = h2*np.ones(num_periods)
            if isinstance(sigma2, data_types):
                sigma2 = sigma2*np.ones(num_periods)
            if isinstance(beta2, data_types):
                beta2 = beta2*np.ones(num_periods)

            for i in range(num_periods):
                x_center = x[0] + i*period + x0[i] + period/2
                supergauss1 = h1[i]*np.exp(-(np.abs(self.x-x_center)/sigma1[i])**beta1[i])
                supergauss2 = h2[i]*np.exp(-(np.abs(self.x-x_center)/sigma2[i])**beta2[i])
                y = supergauss1 - supergauss2

                profile_clean = profile_clean + y

        if std_noise > 0:
            profile_noise = roughness_convolution_2D(self.x, self.y, lc_noise, std_noise, has_draw=False)
        else:
            profile_noise = np.zeros_like(self.x)

        profile = profile_clean + profile_noise
        profile = profile - profile.min()
        profile_clean = profile_clean - profile_clean.min()

        if has_draw:
            self.draw_profile('profile')
        self.profile = profile
        self.profile_clean = profile_clean
        self.profile_noise = profile_noise

        return profile, profile_clean, profile_noise

    def profile_supergaussian_periodical(self, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        if num_periods > 1:
            x_new = np.linspace(self.x[0], self.x[-1], int(len(self.x)/num_periods))
        else:
            x_new = self.x

        supergauss1 = h1*np.exp(-(np.abs(x_new-x0)/sigma1)**np.abs(beta1))
        supergauss2 = h2*np.exp(-(np.abs(x_new-x0)/sigma2)**np.abs(beta2))
        y = supergauss1 - supergauss2

        if num_periods > 1:
            y_new = np.tile(y, num_periods)
        else:
            y_new = y

        if has_draw:
            plt.figure()
            plt.plot(self.x, y_new, linewidth=1.5, c='blue', ls='-')
            plt.xlabel(r'x ($\mu$m)')
            plt.ylabel(r'h  ($\mu$m)')
            plt.xlim(self.x[0], self.x[-1])
            plt.grid(True)
            plt.show()

        self.profile = y_new
        return y_new

    def profile_supergaussian(self, period, num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, lc_noise=1000000, std_noise=0, has_draw=True):
        """_summary_

        Args:
            x (_type_): _description_
            period (_type_): _description_
            num_periods (_type_): _description_
            x0 (_type_): _description_
            h1 (_type_): _description_
            sigma1 (_type_): _description_
            beta1 (_type_): _description_
            h2 (_type_): _description_
            sigma2 (_type_): _description_
            beta2 (_type_): _description_
            lc_noise (int, optional): _description_. Defaults to 1000000.
            std_noise (int, optional): _description_. Defaults to 0.
            has_draw (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """

        profile_clean = np.zeros_like(self.x)

        data_types = (float, int)
        if num_periods == 1:
            profile_clean = self.profile_supergaussian_periodical(num_periods, x0, h1, sigma1, beta1, h2, sigma2, beta2, has_draw=False)
        else:
            if isinstance(x0, data_types):
                x0 = x0*np.ones(num_periods)
            if isinstance(h1, data_types):
                h1 = h1*np.ones(num_periods)
            if isinstance(sigma1, data_types):
                sigma1 = sigma1*np.ones(num_periods)
            if isinstance(beta1, data_types):
                beta1 = beta1*np.ones(num_periods)
            if isinstance(h2, data_types):
                h2 = h2*np.ones(num_periods)
            if isinstance(sigma2, data_types):
                sigma2 = sigma2*np.ones(num_periods)
            if isinstance(beta2, data_types):
                beta2 = beta2*np.ones(num_periods)

            for i in range(num_periods):
                x_center = self.x[0] + i*period + x0[i] + period/2
                supergauss1 = h1[i]*np.exp(-(np.abs(self.x-x_center)/sigma1[i])**beta1[i])
                supergauss2 = h2[i]*np.exp(-(np.abs(self.x-x_center)/sigma2[i])**beta2[i])
                y = supergauss1 - supergauss2

                profile_clean = profile_clean + y

        if std_noise > 0:
            profile_noise = self.roughness_convolution(lc_noise, std_noise, has_draw=False)
        else:
            profile_noise = np.zeros_like(self.x)

        profile = profile_clean+profile_noise
        profile = profile - profile.min()

        self.profile = profile
        self.profile_clean = profile_clean
        self.profile_roughness = profile_noise

        if has_draw:
            self.draw_profile(kind='profile')

        return profile, profile_noise, profile_clean

    def save_profile(self, filename, kind='profile', add_name='', params_supergaussian=None):
        """_summary_

        Args:
            filename (_type_): _description_
            x (_type_): _description_
            y (_type_): _description_
            params_supergaussian (_type_, optional): _description_. Defaults to None.
        """
        if kind == 'profile':
            z = self.profile
        elif kind == 'clean':
            z = self.profile_clean
        elif kind == 'roughness':
            z = self.profile_roughness

        z = z - z.min()

        p1 = 1e-6*self.X.flatten()
        p2 = 1e-6*self.Y.flatten()
        p3 = 1e-6*z.flatten()

        data = np.vstack((p1, p2, p3)).transpose()

        np.savetxt(filename+add_name+'.txt', data, fmt='%1.4e', delimiter='\t')

        if params_supergaussian is not None:
            save_dictionary(params_supergaussian,
                            filename+'.mat',
                            add_name=add_name,
                            description='',
                            verbose=False)
            save_dictionary(params_supergaussian,
                            filename+'.npz',
                            add_name=add_name,
                            description='',
                            verbose=False)


def fitting(x, y, deg, label, color):

    p = np.polyfit(x, y, deg=deg)
    y_fit = np.polyval(p, x)
    
    print("{}: {}".format(label,p))

    plt.plot(x, y_fit, label=label, color=color)
    plt.plot(x, y, color+'.', ms=8)
    plt.xlabel("std($x_0$)")
    plt.xlim(x[0], x[-1])
    plt.ylabel("value")
    plt.grid()
    plt.legend()

def rand_parameters(num_data, media, std, kind='normal', is_positive=True):
    """_summary_

    Args:
        num_data (_type_): _description_
        media (_type_): _description_
        std (_type_): _description_
        kind (str, optional): _description_. Defaults to 'normal'.
        is_positive (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """
    if kind == 'normal':
        values = np.random.normal(media, std, num_data)
    elif kind == 'uniform':
        values = np.random.uniform(media-std, media+std, num_data)
    if is_positive:
        values = np.abs(values)

    return values


def save_dictionary(dictionary,
                    filename,
                    add_name='',
                    description='',
                    verbose=False):
    """Common save data function to be used in all the modules.
    The methods included are: npz, matlab

    Parameters:
        filename(str): filename
        add_name = (str): sufix to the name, if 'date' includes a date
        description(str): text to be stored in the dictionary to save.
        verbose(bool): If verbose prints filename.

    Returns:
        (str): filename. 
    """

    now = datetime.datetime.now()
    date = now.strftime("%Y-%m-%d_%H_%M_%S")

    if add_name == 'date':
        add_name = "_" + date
    extension = filename.split('.')[-1]
    file = filename.split('.')[0]
    final_filename = file + add_name + '.' + extension

    if verbose:
        print(final_filename)

    dictionary['date'] = date
    dictionary['description'] = description

    if extension == 'npz':
        np.savez(file=final_filename, dict=dictionary)

    elif extension == 'mat':
        savemat(final_filename, dictionary)

    return final_filename


# __________________________________________________________________________________________

def replace_complex_sign(filename, folder=None, verbose=False):
    """_summary_

    Args:
        filename (_type_): _description_
        folder (_type_): _description_
    """
    # folder = 'results2'
    # filename = 'Profile_00-E0x_1_E0y_0.txt'

    if folder is None:
        name = filename
    else:
        name = folder+"/"+filename

    if verbose:
        print(name)

    # First read the contents of file in a variable and replace what you want using .replace() function
    x = open(name)
    s = x.read().replace("i", "j")
    x.close()

    # Now open the file in write mode and write the string with replaced
    x = open(name, "w")
    x.write(s)
    x.close


def batch_replace_complex_sign(folder, extension=".txt", verbose=True):
    """_summary_

    Args:
        folder (_type_): _description_
        extension (str, optional): _description_. Defaults to ".txt".
        verbose (bool, optional): _description_. Defaults to True.
    """

    files_txt = sorted(glob.glob(folder+"/*"+extension))
    num_files = len(files_txt)

    if verbose:
        print("num files = {}".format(num_files), end='\r')

    for i, file in enumerate(files_txt):
        replace_complex_sign(filename=file, folder=None, verbose=False)
        if verbose:
            print("{}/{}: {}".format(i, num_files, file), end='\r')


def stokes_average(s, has_draw=True, verbose=True):
    """_summary_

    Args:
        s (_type_): _description_
        has_draw (bool, optional): _description_. Defaults to True.
        verbose (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """

    s0 = s.M[0, :].mean()
    s1 = s.M[1, :].mean()
    s2 = s.M[2, :].mean()
    s3 = s.M[3, :].mean()

    if has_draw:
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(12, 3))
        ax1.plot(s.M[0, :], 'k', lw=2)
        ax1.set_title("s$_0$")
        ax1.set_ylim(0, 1)

        ax2.plot(s.M[1, :], 'r', lw=2)
        ax2.set_title("s$_1$")
        ax2.set_ylim(-1, 1)

        ax3.plot(s.M[2, :], 'g', lw=2)
        ax3.set_title("s$_2$")
        ax3.set_ylim(-1, 1)

        ax4.plot(s.M[3, :], 'b', lw=2)
        ax4.set_title("s$_3$")
        ax4.set_ylim(-1, 1)

        plt.tight_layout()

    if verbose:
        print(s0, s1, s2, s3)

    s_mean = Stokes("average").from_components((s0, s1, s2, s3))

    return s_mean


def load_Ex_Ey(file_txt, has_draw=False, verbose=True):
    """_summary_

    Args:
        file_txt (_type_): _description_
        has_draw (bool, optional): _description_. Defaults to False.
        verbose (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """
    # cargar archivo

    E_all = np.loadtxt(file_txt, comments='%', dtype=complex)

    # determinar campos
    temp_x = E_all[:, 0].astype(float)
    temp_z = E_all[:, 1].astype(float)
    Ex = E_all[:, 2]
    Ey = E_all[:, 3]

    Ex = Ex.conjugate()
    Ey = Ey.conjugate()

    # Reshaping x, z, fields
    x, x_indices = np.unique(temp_x, return_inverse=True)
    z, z_indices = np.unique(temp_z, return_inverse=True)

    x = x * 1e6
    z = z * 1e6

    x_size = len(x)
    z_size = len(z)

    Ex = Ex.reshape((z_size, x_size))
    Ey = Ey.reshape((z_size, x_size))

    # Ex = Ex.conjugate()
    # Ey = Ey.conjugate()

    Intensity = np.abs(Ex)**2 + np.abs(Ey)**2

    if verbose:
        print("size = ({},{})".format(x_size, z_size))

    if has_draw:
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(12, 12))
        ax1.imshow(np.real(Ex), cmap='seismic', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax1.set_title("E$_x$(x,z)")
        ax1.set_xlabel("x [$\mu$m]")
        ax1.set_ylabel("z [$\mu$m]")

        ax2.imshow(np.real(Ey), cmap='seismic', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax2.set_title("E$_y$(x,z)")
        ax2.set_xlabel("x [$\mu$m]")

        Intensity[0, 0] = 0
        Intensity[0, 1] = 1
        ax3.imshow(Intensity, cmap='hot', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax3.set_title("I(x,z)")
        ax3.set_xlabel("x [$\mu$m]")
        plt.tight_layout()

        fig, ax4 = plt.subplots(1, 1, figsize=(12, 4))

        # ax4.plot(z, Intensity[:, 0], 'k', label='I')
        ax4.plot(z, np.real(Ex[:, 0]), 'r', label='E$_x$')
        ax4.plot(z, np.real(Ey[:, 0]), 'b', label='E$_y$')
        ax4.set_title("profiles")
        ax4.set_xlabel("z [$\mu$m]")
        ax4.set_xlim(z[0], z[-1])
        ax4.set_ylabel("E$_x$, E$_y$ [arb. u.]")
        ax4.grid('on')

        ax4.legend()

    return x, z, Ex, Ey


def load_Ex_Ey_3D(file_txt, has_draw=False, verbose=True):
    """_summary_

    Args:
        file_txt (list): _description_
        has_draw (bool, optional): _description_. Defaults to False.
        verbose (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """
    # cargar archivo

    E1 = np.loadtxt(file_txt[0], comments='%', dtype=complex)
    temp_x = E1[:, 0].astype(float)
    temp_y = E1[:, 1].astype(float)
    temp_z = E1[:, 2].astype(float)
    Ex = E1[:, 3]

    E2 = np.loadtxt(file_txt[1], comments='%', dtype=complex)
    Ey = E2[:, 3]

    Ex = Ex.conjugate()
    Ey = Ey.conjugate()

    # Reshaping x, z, fields
    x, x_indices = np.unique(temp_x, return_inverse=True)
    y, y_indices = np.unique(temp_y, return_inverse=True)
    z, z_indices = np.unique(temp_z, return_inverse=True)

    x = x * 1e6
    y = y * 1e6
    z = z * 1e6

    x_size = len(x)
    y_size = len(y)
    z_size = len(z)

    Ex = Ex.reshape((z_size, x_size, y_size))
    Ey = Ey.reshape((z_size, x_size, y_size))

    # Ex = Ex.conjugate()
    # Ey = Ey.conjugate()

    Intensity = np.abs(Ex)**2 + np.abs(Ey)**2

    if verbose:
        print("size = (x: {}, y: {}, z: {})".format(x_size, y_size, z_size))

    if has_draw:
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(12, 3))
        ax1.imshow(np.real(Ex[:, 0, :]), cmap='seismic', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax1.set_title("E$_x$(x,z)")

        ax2.imshow(np.real(Ey[:, 0, :]), cmap='seismic', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax2.set_title("E$_y$(x,z)")

        ax3.imshow(Intensity[:, 0, :], cmap='hot', origin='lower', extent=(x[0], x[-1], z[0], z[-1]))
        ax3.set_title("I(x,z)")

        ax4.plot(z, Intensity[:, 0, 0], 'k', label='I')
        ax4.plot(z, np.real(Ex[:, 0, 0]), 'r', label='E$_x$')
        ax4.plot(z, np.real(Ey[:, 0, 0]), 'b', label='E$_y$')
        ax4.set_title("profiles")
        ax4.legend()

        plt.tight_layout()

    return x, y, z, Ex, Ey




def compute_Mueller_from_Stokes(Sini, Sfin):
    """Calcula la matriz de Mueller a partir de los Stokes incidentes (Sini) y finales (Sfin).

    Cuando incidimos con un estado $S_{ini}$, el estado a la salida es $S_{fin} = m * S_{ini}$.

    Por lo tanto, si colocamos todos los diferentes Stokes en unas matrices 4x4 $s_{ini}$ y $s_{fin}$:

    $s_{fin} = m * s_{ini}$. Entonces  $m = s_{fin} * s_{ini}^{-1}$.

    Args:
        Sini (_type_): _description_
        Sfin (_type_): _description_

    Returns:
        _type_: _description_
    """
    # Crear matrices de Stokes como filas
    sini = np.zeros((4, 4))
    sfin = np.zeros((4, 4))
    for ind, S in enumerate(Sini):
        sini[:, ind] = S.M[:, 0]
    for ind, S in enumerate(Sfin):
        sfin[:, ind] = S.M[:, 0]
    # Calcular matriz de Mueller
    m = sfin @ np.linalg.inv(sini)
    M = Mueller().from_matrix(m)
    return M


def compute_Mueller_from_files(name, folder='', slice_pixels='', has_draw=False, verbose=False):
    """_summary_

    Args:
        name (_type_): _description_
        slice_pixels (_type_): _description_
        folder (str, optional): _description_. Defaults to ''.
        has_draw (bool, optional): _description_. Defaults to False.
        verbose (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    if folder == '':
        files_txt = glob.glob(name+"*.txt")
    else:
        files_txt = glob.glob(folder+"/*"+name+"*.txt")

    files_txt = sorted(files_txt)

    matrix_out = np.zeros((4, 4), dtype=float)

    s = Stokes()

    for i, file in enumerate(files_txt):
        if verbose:
            print("{}/{}: {}".format(i, len(files_txt), file), end="\r")

        x, z, Ex, Ey = load_Ex_Ey(file, verbose=False, has_draw=False)

        if slice_pixels != '':
            i_z0, i_z1 = slice_pixels

            iz = slice(i_z0, i_z1)
            s.from_distribution(Ex[iz, :].transpose(), Ey[iz, :].transpose())
        else:
            s.from_distribution(Ex[:, :].transpose(), Ey[:, :].transpose())

        s_mean = stokes_average(s, has_draw=has_draw, verbose=False)
        # print(s_mean)

        matrix_out[:, i] = s_mean.M.T

    S_out = Stokes().from_matrix(matrix_out)  # .analysis.filter_physical_conditions(tol=0)

    Mcalc = compute_Mueller_from_Stokes(S_4, S_out)

    if verbose:
        print(Mcalc)

    return Mcalc, S_out


def compute_Mueller_from_files_3D(name, folder='', slice_pixels='', has_draw=False, verbose=False):
    """_summary_

    Args:
        name (_type_): _description_
        slice_pixels (_type_): _description_
        folder (str, optional): _description_. Defaults to ''.
        has_draw (bool, optional): _description_. Defaults to False.
        verbose (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    if folder == '':
        files_txt = glob.glob(name+"*.txt")
    else:
        files_txt = glob.glob(folder+"/*"+name+"*.txt")

    files_txt = sorted(files_txt)

    matrix_out = np.zeros((4, 4), dtype=float)

    s = Stokes()

    num_cases = int(len(files_txt)/2)

    for i in range(num_cases):
        file_Ex = files_txt[i]
        file_Ey = files_txt[i+4]
        print("{}/{}: {} + {}".format(i, num_cases, file_Ex, file_Ey), end="\r")

        x, y, z, Ex, Ey = load_Ex_Ey_3D((file_Ex, file_Ey), verbose=True, has_draw=True)

        if slice_pixels != '':
            i_z0, i_z1 = slice_pixels

            iz = slice(i_z0, i_z1)

            Ex_new = Ex[iz, :, :]
            Ey_new = Ex[iz, :, :]

            Ex_new = Ex_new.reshape(Ex_new.shape[0], -1)
            Ey_new = Ey_new.reshape(Ey_new.shape[0], -1)

            s.from_distribution(Ex_new.transpose(), Ex_new.transpose())
        else:

            Ex_new = Ex.reshape(Ex.shape[0], -1)
            Ey_new = Ey.reshape(Ey.shape[0], -1)

            s.from_distribution(Ex_new.transpose(), Ex_new.transpose())

        s_mean = stokes_average(s, has_draw=has_draw, verbose=False)
        # print(s_mean)

        matrix_out[:, i] = s_mean.M.T

    S_out = Stokes().from_matrix(matrix_out)  # .analysis.filter_physical_conditions(tol=0)

    Mcalc = compute_Mueller_from_Stokes(S_4, S_out)

    if verbose:
        print(Mcalc)

    return Mcalc, S_out
