# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Top-level package for AOCG/UCM laboratory."""
"""
py_aocg: laboratory AOCG
======================================================================================================

Contents
--------
py_aocg is the development package for AOCG laboratory
"""


import datetime
import multiprocessing

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from matplotlib import cm, rcParams


__author__ = """M. Verónica Pastor Villarrubia"""
__email__ = 'veronica.pastor@ucm.es'
__version__ = '0.0.1'

um = 1.
mm = 1000. * um
nm = um / 1000.
degrees = np.pi / 180.
s = 1.
seconds = 1.

eps = 1e-6
no_date = True
number_types = (int, float, complex, np.int32, np.float64)
