# !/usr/bin/env python3
"""
Explicacion del módulo


"""

import copyreg
import time
import types

from . import np, sp, plt, um, nm, mm, degrees
from .utils_multiprocessing import (_pickle_method, _unpickle_method,
                                    execute_multiprocessing)

copyreg.pickle(types.MethodType, _pickle_method, _unpickle_method)


class Optimizacion(object):
    """Class for unidimensional scalar fields.

    Parameters:
        x (numpy.array): linear array with equidistant positions.
            The number of data is preferibly :math:`2^n` .

    Attributes:
        self.x (numpy.array): Linear array with equidistant positions.
            The number of data is preferibly :math:`2^n`.
    """
    def __init__(self):
        self.x = 0

    def __str__(self):
        """Represents main data of the atributes."""

        print(" - info:       {}".format(self.x))

        return ""

    def prueba(self):
        print("estamos en prueba")
        x = np.linspace(0, 1, 11)
        plt.plot(x, x**2)
        return 1.5
