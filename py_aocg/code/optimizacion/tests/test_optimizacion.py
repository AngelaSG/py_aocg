# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""tests for module"""

import copyreg
import datetime
import os
import sys
import types

import numpy as np
from py_aocg import eps, no_date
from py_aocg.optimizacion import Optimizacion
from py_aocg.utils_multiprocessing import _pickle_method, _unpickle_method
from py_aocg.utils_tests import comparison

copyreg.pickle(types.MethodType, _pickle_method, _unpickle_method)

if no_date is True:
    date = '0'
else:
    now = datetime.datetime.now()
    date = now.strftime("%Y-%m-%d_%H")

path_base = "tests_results"
path_class = "optimizacion"

newpath = "{}_{}/{}/".format(path_base, date, path_class)

if not os.path.exists(newpath):
    os.makedirs(newpath)


class Test_Scalar_fields_X(object):
    def test_print(self):
        func_name = sys._getframe().f_code.co_name
        filename = '{}{}'.format(newpath, func_name)

        o1 = Optimizacion()
        o1.prueba()
        proposal = o1.x
        solution = 0

        assert comparison(proposal, solution, eps), func_name

    """
    def test_clear_field(self):
        func_name = sys._getframe().f_code.co_name
        filename = '{}{}'.format(newpath, func_name)

        x = np.linspace(-50 * um, 50 * um, 128)
        wavelength = 1 * um
        t1 = Scalar_field_X(x, wavelength)
        t1.u = np.ones_like(x, dtype=complex)

        t1.save_data(filename=filename + '.npz')

        solution = np.zeros_like(t1.u)
        t1.clear_field()
        proposal = t1.u
        assert comparison(proposal, solution, eps), func_name

    def test_save_load(self):
        "
        Tests save in savez and other functions
        "
        func_name = sys._getframe().f_code.co_name
        filename = '{}{}.'.format(newpath, func_name)

        x = np.linspace(-500 * um, 500 * um, 512)
        wavelength = .5 * um

        t1 = Scalar_field_X(x, wavelength)
        t1.u = np.sin(x**2 / 5000)
        t1.draw()

        save_figure_test(newpath, func_name, add_name='_saved')
        t1.save_data(filename=filename + '.npz')

        t2 = Scalar_field_X(x, wavelength)
        t2.load_data(filename=filename + '.npz')
        t2.draw()
        save_figure_test(newpath, func_name, add_name='_loaded')

        assert True
    """
