# !/usr/bin/env python3
"""
Functions to generate dxf mask files
@Authors:
    Luis Miguel Sánchez Brea
"""

import os
import sys
import pathlib
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt

import ezdxf
from ezdxf import recover
from ezdxf.addons.drawing import RenderContext, Frontend
from ezdxf.addons.drawing.matplotlib import MatplotlibBackend

degrees = 1
mm=1.

um=mm/1000.
nm = um/1000.
inches = 25.4*mm


class DXF_mask_contour():
    """DXF_file
    """

    def __init__(self, version="R2000", filename: str = None, dpi: int = 600,
                 cutting_dict: dict[bool, str, float | list[float]] = {}):
        """
        __init__ Starts DXF mask

        Args:
            version (str, optional): _description_. Defaults to "R2000".
            filename (str, optional): _description_. Defaults to None.
            dpi (int, optional): _description_. Defaults to 600.
            cutting_dict (dict[bool, str, float  |  list[float]], optional): _description_. Defaults to {}.
                type (str): '', 'circular' or 'rectangular'
                size (float): 'circular =  diameter, rectangular = width,height
        """
        self.doc = ezdxf.new(version)
        self.msp = self.doc.modelspace()
        
        self.filename = filename
        self.dpi = dpi
        self.version = version
        self.cutting_dict = cutting_dict
                
        self.total_size = 4*inches, 4*inches
        self.num_masks = (4, 4)
        self.line_size = 1*um
        self.mask_size = (self.total_size)/np.array(self.num_masks)

    def load_dxf(self, filename, has_draw=False):
        # Safe loading procedure (requires ezdxf v0.14):
        try:
            doc, auditor = recover.readfile(filename)
            self.doc = doc
            self.msp = doc.modelspace()
        except IOError:
            print('Not a DXF file or a generic I/O error.')
            sys.exit(1)
        except ezdxf.DXFStructureError:
            print('Invalid or corrupted DXF file.')
            sys.exit(2)

        if not auditor.has_errors and has_draw is True:
            self.draw()

    def draw(self, has_draw: bool = True):
        """Draws dxf file.

        Args:
            filename (str): draw a saved dxf file
        """
        filename = self.filename + ".png"

        draw(self.msp, self.doc, has_draw=has_draw, filename=filename, 
             dpi=self.dpi)


    def save(self, filename: str = '', verbose:bool = False):
        """Saves dxf_file

        Args:
            filename (str): filename
        """
        
        if filename == '':
            filename = self.filename
            
        
        self.doc.saveas(filename+'.dxf')
        if verbose:
            print("filename = "+filename+'.dxf')

    def cutting(self, has_save: bool = True, has_png: bool = True, 
                has_draw: bool = True):
        
        if self.cutting_dict['type']!= '':
            doc_cutting = ezdxf.new(self.version)
            msp_cutting = doc_cutting.modelspace()
            
        if self.cutting_dict['type'] == 'circular':
            msp_cutting.add_circle(center=self.cutting_dict['r0'], 
                                   radius=self.cutting_dict['size']/2)
        elif self.cutting_dict['type'] == 'rectangular':
            self.rectangle(r0 = self.cutting_dict['r0'], 
                                  size = self.cutting_dict['size'], 
                                  origin='0')

        if has_save is True:
            doc_cutting.saveas(self.filename+'_cutting.dxf')

        if has_png is True:
            filename = self.filename + '_cutting.png'
        else:
            filename = ''

        if has_draw is True:  
            draw(msp_cutting, doc_cutting, has_draw=has_draw,
                filename=filename, dpi=self.dpi)

    def draw_all(self, has_save: bool = True, has_png: bool = True, 
                 has_draw: bool = True):
        

        if self.cutting_dict['type'] == 'circular':
            self.msp.add_circle(center=self.cutting_dict['r0'], 
                                   radius=self.cutting_dict['size']/2)
        elif self.cutting_dict['type'] == 'rectangular':
            self.rectangle(r0 = self.cutting_dict['r0'], 
                                  size = self.cutting_dict['size'], 
                                  origin='0')

        if has_save is True:
            self.doc.saveas(self.filename+'_all.dxf')

        filename = self.filename + '_all.png'

        if has_draw is True:  
            draw(self.msp, self.doc, has_draw=has_draw, filename=filename, 
                    dpi=self.dpi)
    
    def rectangle(self, r0: list[float], size: list[float], angle: float = 0*degrees,  origin: str = '0' ,
                  rotation_point: list[float] | None = None):
        """
        rectangle _summary_

        _extended_summary_

        Args:
            r0 (list[float]): _description_
            size (list[float]): _description_
            angle (_type_, optional): _description_. Defaults to 0*degrees.
            origin (str, optional): _description_. Defaults to '0'.
            rotation_point (_type_, optional): _description_. Defaults to None.
        """
        x0, y0 = r0
        sx, sy = size
        
        r0 = np.array(r0)
        
        
        if origin == '0':
            init_point = np.array([0,0])
        elif origin == 'center':
            init_point = np.array([-sx/2, -sy/2])

        if rotation_point is None:
            rotation_point = np.array([sx/2, sy/2])
            
        pnr = [(0,0), (sx,0), (sx,sy), (0,sy)]

            
        point_1 = rotate(pnr[0], rotation_point, angle)
        point_2 = rotate(pnr[1], rotation_point, angle)
        point_3 = rotate(pnr[2], rotation_point, angle)
        point_4 = rotate(pnr[3], rotation_point, angle)
        
        points = np.array([point_1, point_2, point_3, point_4])
        points = points + r0 + init_point

        self.msp.add_lwpolyline(points, close=True)
        

        
    def double_slit(self, r0: list[float], width: float, height: float, separation: float, origin: str = '0'):
        """_summary_

        Args:
            r0 (list[float]): _description_
            width (float): _description_
            height (float): _description_
            separation (float): _description_
            origin (str, optional): _description_. Defaults to '0'.
        """
        x0, y0 = r0
        
        rect1_x = x0 - separation/2 - width/2
        rect1_y = -height/2

        rect2_x = x0 + separation/2 - width/2
        rect2_y = -height/2
        
        r1 = (rect1_x, rect1_y)
        r2 = (rect2_x, rect2_y)

        size = (width, height)
        
        self.rectangle(r0=r1, size=size, origin=origin)
        self.rectangle(r0=r2, size=size, origin=origin)
        

    def photon_sieve(self, r0, XY, radii, size_margin = 50*um, has_background = False):
        """Generates dxf file for a photon sieve. Data are in microns.
            The photon sieve is defined only with the contours

        Args:
            r0 (np.array): array with position of photon sieve.
            XY (np.array): array with XY positions of holes.
            radii (anp.array): array with radii of holes.
            filename (str): filename for dxf file.
            drawEdge_mask (bool/str): if 'Square' draw a square margin, if 'Circular' draw a circular margin. False in other case.
            size_margin (float): 50 um by default.
            has_background (bool): False by default.
        """
        # doc = ezdxf.new("R2010", setup=True)
        # doc.header['$INSUNITS'] = 13 # for microns unit
        # self.doc.header['$INSUNITS'] = 13 # for microns unit ¡¡¡AL PARECER NO FURULA ÉSTO!!!
        
        # Voy a poner todo en mm (Los datos entrada tienen que ser en um)

        XY_mm = XY*1e-3 
        r0_mm = r0*1e-3
        radii_mm =  radii*1e-3
        size_margin_mm = size_margin*1e-3


        for i in range(len(radii_mm)):
            self.msp.add_circle(center = r0_mm+XY_mm[i], radius = radii_mm[i])

   

def addpoint(dx: float, dy: float, points: list):
    """_summary_

    Args:
        p0 (list[float, float]): previous point (x,y)
        dx (float): increment in x
        dy (float): increment in y
        points (list): list with points

    Returns:
        points: list with added point
    """
    p0 = points[-1]
    p1 = (p0[0]+dx, p0[1] + dy)
    points.append(p1)

    
    return points
    

def rotate(point: list[float], rotation_point: list[float], angle: float):
    """
    rotate _summary_

    _extended_summary_

    Args:
        point (list[float]): _description_
        rotation_point (list[float]): _description_
        angle (float): _description_

    Returns:
        _type_: _description_
    """
    
    x = point[0]-rotation_point[0]
    y = point[1]-rotation_point[1]

    x_new = rotation_point[0] + x*np.cos(angle) - y*np.sin(angle)
    y_new = rotation_point[1] + x*np.sin(angle) + y*np.cos(angle)
    

    
    return (x_new, y_new)


def draw(msp, doc, has_draw: bool = True, filename: str = "", dpi=600):
    """Draws dxf file.

    Args:
        filename (str): draw a saved dxf file
    """

    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1])
    ctx = RenderContext(doc)
    out = MatplotlibBackend(ax)
    Frontend(ctx, out).draw_layout(msp, finalize=True)
    if filename != "":
        print(filename)
        fig.savefig(filename, dpi=dpi)
    if has_draw is False:
        plt.close() 