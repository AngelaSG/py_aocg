# !/usr/bin/env python3
"""
Functions to generate dxf mask files
@Authors:
    Luis Miguel Sánchez Brea
"""

import os
import sys
import pathlib
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt

import ezdxf
from ezdxf import recover
from ezdxf.addons.drawing import RenderContext, Frontend
from ezdxf.addons.drawing.matplotlib import MatplotlibBackend

degrees = 1
mm=1.

um=mm/1000.
nm = um/1000.
inches = 25.4*mm


class DXF_mask_hatch():
    """DXF_file
    """

    def __init__(self, version="R2000", filename: str = None, dpi: int = 600,
                 cutting_dict: dict[bool, str, float | list[float]] = {}, hatch_style: int = 0, num_layers: int = 1):
        """
        __init__ Starts DXF mask

        Args:
            version (str, optional): _description_. Defaults to "R2000".
            filename (str, optional): _description_. Defaults to None.
            dpi (int, optional): _description_. Defaults to 600.
            cutting_dict (dict[bool, str, float  |  list[float]], optional): _description_. Defaults to {}.
                type (str): '', 'circular' or 'rectangular'
                size (float): 'circular =  diameter, rectangular = width,height
            num_layers (int): Total layers of the dxf file.
        """

        self.dwg = ezdxf.new(version)

        for i in range(num_layers):
            name = 'Layer{}'.format(i+1)
            self.dwg.layers.add(name=name,color=1+i)

        self.msp = self.dwg.modelspace()
        # self.hatch = self.msp.add_hatch(dxfattribs=dxfattribs)
        # self.edge_path = self.hatch.paths.add_edge_path()
        
        dxfattribs={
                "hatch_style": hatch_style,
        }        
        
                    
        self.dxfattribs = dxfattribs
        self.filename = filename
        self.dpi = dpi
        self.version = version
        self.cutting_dict = cutting_dict
                
        self.total_size = 4*inches, 4*inches
        self.num_masks = (4, 4)
        self.line_size = 1*um
        self.mask_size = (self.total_size)/np.array(self.num_masks)

    def load_dxf(self, filename, has_draw=False):
        # Safe loading procedure (requires ezdxf v0.14):
        try:
            dwg, auditor = recover.readfile(filename)
            self.dwg = dwg
            self.msp = dwg.modelspace()
        except IOError:
            print('Not a DXF file or a generic I/O error.')
            sys.exit(1)
        except ezdxf.DXFStructureError:
            print('Invalid or corrupted DXF file.')
            sys.exit(2)

        if not auditor.has_errors and has_draw is True:
            self.draw()

    def draw(self, has_draw: bool = True):
        """Draws dxf file.

        Args:
            filename (str): draw a saved dxf file
        """
        filename = self.filename + ".png"

        draw(self.msp, self.dwg, has_draw=has_draw, filename=filename, 
             dpi=self.dpi)

    def save(self, filename: str = '', verbose: bool = False):
        """Saves dxf_file

        Args:
            filename (str): filename
        """

        if filename == '':
            filename = self.filename

        self.dwg.saveas(filename+'.dxf')
        if verbose:
            print("filename = "+filename+'.dxf')

    def cutting(self, has_save: bool = True, has_png: bool = True, 
                has_draw: bool = True):

        if self.cutting_dict['type']!= '':
            dwg_cutting = ezdxf.new(self.version)
            msp_cutting = dwg_cutting.modelspace()

        if self.cutting_dict['type'] == 'circular':
            msp_cutting.add_circle(center=self.cutting_dict['r0'], 
                                   radius=self.cutting_dict['size']/2)
        elif self.cutting_dict['type'] == 'rectangular':
            self.rectangle(r0 = self.cutting_dict['r0'], 
                                  size = self.cutting_dict['size'], 
                                  origin='0')

        if has_save is True:
            dwg_cutting.saveas(self.filename+'_cutting.dxf')

        if has_png is True:
            filename = self.filename + '_cutting.png'
        else:
            filename = ''

        if has_draw is True:  
            draw(msp_cutting, dwg_cutting, has_draw=has_draw,
                filename=filename, dpi=self.dpi)



    def background(self, r0, size, angle=0., origin='0', rotation_point=None):
        
        self.rectangle(r0=r0, size=size, angle=angle, origin=origin, 
                       rotation_point=rotation_point)
        # self.hatch.paths.add_polyline_path(
        #     [(x0, y0), (diameter, 0),
        #         (diameter, diameter), (0, diameter)],
        #     is_closed=True,
        #     flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
        #     # The second path has to set flag: 16 = outermost
        #     )

    def edge_rectangle(self, total_size, num_masks, line_size):
        """_summary_ befor edges

        Args:
            total_size (float, float): Size of mask
            num_masks (int, int): number of masks
            line_size (float): size line

        Returns:
            (np.array): pos_x - positions x
            (np.array): pos_y - positions y
            (int): delta_x - size x of mask
            (int): delta_y - size y of mask
        """

        self.total_size = total_size
        self.num_masks = num_masks
        self.line_size = line_size

        num_masks_x, num_masks_y = num_masks
        total_size_x, total_size_y = total_size
        delta_x, delta_y = total_size_x/num_masks_x, total_size_y/num_masks_y

        pos_x = np.arange(0, total_size_x, delta_x, dtype=float)
        pos_y = np.arange(0, total_size_y, delta_y, dtype=float)

        # by default a solid fill hatch with fill color=7 (white/black)
        hatch = self.msp.add_hatch(
            color=7,
            dxfattribs={
                "hatch_style": ezdxf.const.HATCH_STYLE_NESTED,
                # 0 = nested: ezdxf.const.HATCH_STYLE_NESTED
                # 1 = outer: ezdxf.const.HATCH_STYLE_OUTERMOST
                # 2 = ignore: ezdxf.const.HATCH_STYLE_IGNORE
            },
        )

        # The first path has to set flag: 1 = external
        # flag const.BOUNDARY_PATH_POLYLINE is added (OR) automatically

        line_outer = np.array([(0, 0),
                               (total_size_x, 0),
                               (total_size_x, total_size_y),
                               (0, total_size_y)])

        line_inner = np.array([(line_size, line_size),
                               (total_size_x-line_size, line_size),
                               (total_size_x-line_size, total_size_y-line_size),
                               (line_size, total_size_y-line_size)])

        hatch.paths.add_polyline_path(
            line_outer,
            is_closed=True,
            flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
            # The second path has to set flag: 16 = outermost
        )

        hatch.paths.add_polyline_path(
            line_inner,
            is_closed=True,
            flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST,
        )

        for xi in pos_x[1:]:
            x_init = xi
            x_end = xi + line_size
            y_init = 0
            y_end = total_size_y

            line = np.array([(x_init, y_init),
                            (x_end, y_init),
                            (x_end, y_end),
                            (x_init, y_end)])

            hatch.paths.add_polyline_path(
                line,
                is_closed=True,
                # flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
                # The second path has to set flag: 16 = outermost
            )

        for yi in pos_y[1:]:
            x_init = 0
            x_end = total_size_x
            y_init = yi
            y_end = yi + line_size

            line = np.array([(x_init, y_init),
                            (x_end, y_init),
                            (x_end, y_end),
                            (x_init, y_end)])

            hatch.paths.add_polyline_path(
                line,
                is_closed=True,
                # flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
                # The second path has to set flag: 16 = outermost
            )
        return pos_x, pos_y, delta_x, delta_y


    def draw_all(self, has_save: bool = True, has_png: bool = True, 
                 has_draw: bool = True):
        

        if self.cutting_dict['type'] == 'circular':
            self.msp.add_circle(center=self.cutting_dict['r0'], 
                                   radius=self.cutting_dict['size']/2)
        elif self.cutting_dict['type'] == 'rectangular':
            self.rectangle(r0 = self.cutting_dict['r0'], 
                                  size = self.cutting_dict['size'], 
                                  origin='0')

        if has_save is True:
            self.dwg.saveas(self.filename+'_all.dxf')

        filename = self.filename + '_all.png'

        if has_draw is True:  
            draw(self.msp, self.dwg, has_draw=has_draw, filename=filename, 
                    dpi=self.dpi)
    
    def rectangle(self, r0: list[float], size: list[float], angle=0*degrees,  origin='0',layer=1, rotation_point=None):
        
        x0, y0 = r0
        sx, sy = size
        
        r0 = np.array(r0)
        
        
        if origin == '0':
            init_point = np.array([0,0])
        elif origin == 'center':
            init_point = np.array([-sx/2, -sy/2])

        if rotation_point is None:
            rotation_point = np.array([sx/2, sy/2])
            
        pnr = [(0,0), (sx,0), (sx,sy), (0,sy)]

            
        point_1 = rotate(pnr[0], rotation_point, angle)
        point_2 = rotate(pnr[1], rotation_point, angle)
        point_3 = rotate(pnr[2], rotation_point, angle)
        point_4 = rotate(pnr[3], rotation_point, angle)
        
        points = np.array([point_1, point_2, point_3, point_4])
        points = points + r0 + init_point

        capa = 'Layer{}'.format(layer)
        self.dxfattribs['layer'] = capa
        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.hatch.paths.add_polyline_path(points, is_closed=True)
        

    def circle(self, r0: list[float], radii: float | list[float], angle=0*degrees,layer=1):
        """
        circle _summary_

        _extended_summary_

        Args:
            r0 (list[float]): _description_
            radii (float | list[float]): _description_
            angle (_type_, optional): _description_. Defaults to 0*degrees.
        """

        if isinstance(radii, (float, int)):
            radii = (radii, radii)
        
        radii = np.array(radii)    
        major_axis= (radii.max(), radii.max()*np.sin(angle))
                
        ratio = radii.min()/radii.max()

        capa = 'Layer{}'.format(layer)
        self.dxfattribs['layer'] = capa
        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.edge_path = self.hatch.paths.add_edge_path()
        self.edge_path.add_ellipse(r0, major_axis=major_axis, ratio=ratio)  # , flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL)


    def polyline(self, points: list[float, float, float]):
    
    
        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.hatch.paths.add_polyline_path(points, is_closed=True)
        
    
    def spline(self, points: list[float, float]):
        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.edge_path = self.hatch.paths.add_edge_path()
        self.edge_path.add_spline(control_points=points)
        
        
        
    def double_slit(self, r0: list[float], width: float, height: float, separation: float, origin: str = '0'):
        """_summary_

        Args:
            r0 (list[float]): _description_
            width (float): _description_
            height (float): _description_
            separation (float): _description_
            origin (str, optional): _description_. Defaults to '0'.
        """
        x0, y0 = r0
        
        rect1_x = x0 - separation/2 - width/2
        rect1_y = -height/2

        rect2_x = x0 + separation/2 - width/2
        rect2_y = -height/2
        
        r1 = (rect1_x, rect1_y)
        r2 = (rect2_x, rect2_y)

        size = (width, height)
        
        self.rectangle(r0=r1, size=size, origin=origin)
        self.rectangle(r0=r2, size=size, origin=origin)
        


    def fresnel_zonal_plate(self, r0, focal, wavelength, diameter, kind, background=True):
        """Generates a fresnel_zonal_plate

        Args:
            r0 (float, float): (x,y position of border)
            focal (float): focal
            wavelength (float): wavelength
            diameter (float): diameter
            kind (str): # + centro en negro, - centro en blanco
        """

        # kind = '-' # + centro en negro, - centro en blanco
        x0, y0 = r0

        num_radii = int(diameter**2/(4*wavelength*focal))

        ns = np.arange(0, num_radii)
        radii = np.sqrt(wavelength*focal*ns+0.25*ns**2*wavelength**2)

        num_radii = len(radii)

        if (num_radii % 2) == 0:
            even = True
        else:
            even = False

        if kind == '+' and even is False:
            radii = radii[0:-1]
        elif kind == '-' and even is True:
            radii = radii[0:-1]

        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.edge_path = self.hatch.paths.add_edge_path()
        for radius in radii[:]:
            self.edge_path.add_ellipse((diameter/2+x0, diameter/2+y0),
                                  major_axis=(radius, 0), ratio=1)


        
    def photon_sieve(self, r0, XY, radii, has_background=False):
        """Generates dxf file for a photon sieve. Data are in microns.

        Args:
            XY (np.array): array with XY positions of holes.
            radii (anp.array): array with radii of holes.
            size (float, float): size of mask
            filename (str): filename for dxf file.
        """
        self.hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
        self.edge_path = self.hatch.paths.add_edge_path()
        for i, xy in enumerate(XY):
            self.edge_path.add_ellipse(xy, major_axis=(radii[i], 0), ratio=1) 
            

    def VDOE(self, r0, num_sectors, focal, diameter, wavelength, delta_r):
        """Mask for vector DOE: VDOE.

        Args:
            r0 (float, float): position of center
            num_sectors (int): num_sectors
            focal (float): focal
            diameter (float): diameter
            wavelength (float): wavelength
            delta_r (float): delta_r

        TODO: square or circle at the background
        """

        x0, y0 = r0

        xmin = 1e10
        xmax = -1e10

        diameter1lens = diameter/3

        for i in range(num_sectors):
            angle = 2*np.pi*i/num_sectors
            x = delta_r * np.cos(angle)
            y = delta_r * np.sin(angle)

            ri = (x0 + self.mask_size[0]/2 + x, y0 + self.mask_size[1]/2 + y)

            self.fresnel_zonal_plate(r0=ri, focal=focal, wavelength=wavelength,
                                     diameter=diameter1lens, kind='+', background=False)

            xmin = min(xmin, x0+x-diameter/2)
            xmax = max(xmax, x0+x+diameter/2)
        return xmin, xmax

    def edof_daisy(self, r0, focal, delta_f, diameter, num_petals, wavelength, num_pixels=512, has_background=True, has_draw=False):
        """_summary_

        Args:
            r0 (_type_): _description_
            focal (_type_): _description_
            delta_f (_type_): _description_
            diameter (_type_): _description_
            num_petals (_type_): _description_
            wavelength (_type_): _description_
            num_pixels (int, optional): _description_. Defaults to 512.
            has_draw (bool): If True draws, otherwise does nothing.
        """

        x0, y0 = r0

        num_radii = int(diameter**2/(4*wavelength*focal))

        ns = np.arange(0, num_radii)

        if num_radii/2 != int(num_radii/2):
            ns = ns[0:-1]

        angle = np.linspace(0, 2*np.pi, num_pixels)
        Angle, Ns = np.meshgrid(angle, ns)

        Focal = focal + delta_f/2 * np.cos(Angle*num_petals)
        Radii = np.sqrt(wavelength*Focal*Ns+0.25*Ns**2*wavelength**2)
        radii = np.sqrt(wavelength*focal*ns+0.25*ns**2*wavelength**2)

        if has_draw:
            plt.figure()
            plt.axis('equal')

        for i in ns[::2]:
            ri1 = Radii[i, :]
            ri2 = Radii[i+1, :]
            xi1 = x0 + self.mask_size[0]/2 + ri1 * np.cos(angle)
            yi1 = y0 + self.mask_size[1]/2 + ri1 * np.sin(angle)
            xi2 = x0 + self.mask_size[0]/2 + ri2 * np.cos(angle)
            yi2 = y0 + self.mask_size[1]/2 + ri2 * np.sin(angle)

            if has_draw:
                plt.plot(xi1, yi1, 'r')
                plt.plot(xi2, yi2, 'b')

            points_1 = np.vstack((xi1, yi1)).transpose()
            points_2 = np.vstack((xi2, yi2)).transpose()

    
            # The first path has to set flag: 1 = external
            # flag const.BOUNDARY_PATH_POLYLINE is added (OR) automatically
            hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
            hatch.paths.add_polyline_path(points_1,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST,
                                          )
            hatch.paths.add_polyline_path(points_2,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
                                          # The second path has to set flag: 16 = outermost
                                          )
        return radii

    def edof_lotus(self, r0, focal, delta_f, diameter, num_petals, wavelength, num_pixels=512, has_draw=False):
        """_summary_

        Args:
            r0 (_type_): _description_
            focal (_type_): _description_
            delta_f (_type_): _description_
            diameter (_type_): _description_
            num_petals (_type_): _description_
            wavelength (_type_): _description_
            num_pixels (int, optional): _description_. Defaults to 512.
            has_draw (bool): If True draws, otherwise does nothing.
        """

        x0, y0 = r0

        num_radii = int(diameter**2/(4*wavelength*focal))

        ns = np.arange(0, num_radii)

        if num_radii/2 != int(num_radii/2):
            ns = ns[0:-1]

        angle = np.linspace(0, 2*np.pi, num_pixels)
        Angle, Ns = np.meshgrid(angle, ns)

        Focal = 4 * np.abs(Angle*num_petals / (2 * np.pi) - np.floor(Angle*num_petals / (2 * np.pi) + 0.5)) - 1

        Focal = focal - delta_f/2 * Focal
        Radii = np.sqrt(wavelength*Focal*Ns+0.25*Ns**2*wavelength**2)
        radii = np.sqrt(wavelength*focal*ns+0.25*ns**2*wavelength**2)

        if has_draw:
            plt.figure()
            plt.axis('equal')

        for i in ns[::2]:
            ri1 = Radii[i, :]
            ri2 = Radii[i+1, :]
            xi1 = x0 + self.mask_size[0]/2 + ri1 * np.cos(angle)
            yi1 = y0 + self.mask_size[1]/2 + ri1 * np.sin(angle)
            xi2 = x0 + self.mask_size[0]/2 + ri2 * np.cos(angle)
            yi2 = y0 + self.mask_size[1]/2 + ri2 * np.sin(angle)

            if has_draw:
                plt.plot(xi1, yi1, 'r')
                plt.plot(xi2, yi2, 'b')

            points_1 = np.vstack((xi1, yi1)).transpose()
            points_2 = np.vstack((xi2, yi2)).transpose()

            # by default a solid fill hatch with fill color=7 (white/black)
            hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)

            # The first path has to set flag: 1 = external
            # flag const.BOUNDARY_PATH_POLYLINE is added (OR) automatically
            hatch.paths.add_polyline_path(points_1,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST,
                                          )
            hatch.paths.add_polyline_path(points_2,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
                                          # The second path has to set flag: 16 = outermost
                                          )
        return radii

    def edof_fourier(self, r0, focal, delta_f, coefs, diameter, num_petals, wavelength, num_pixels=512, has_draw=False):
        """_summary_

        Args:
            r0 (_type_): _description_
            focal (_type_): _description_
            delta_f (_type_): _description_
            coefs (np.array): Fourier coefficents
            diameter (_type_): _description_
            num_petals (_type_): _description_
            wavelength (_type_): _description_
            num_pixels (int, optional): _description_. Defaults to 512.
            has_draw (bool): If True draws, otherwise does nothing.
        """

        x0, y0 = r0

        num_radii = int(diameter**2/(4*wavelength*focal))

        ns = np.arange(0, num_radii)

        if num_radii/2 != int(num_radii/2):
            ns = ns[0:-1]

        shape = 0
        num_coefs, _ = coefs.shape

        angle = np.linspace(0, 2*np.pi, num_pixels)
        Angle, Ns = np.meshgrid(angle, ns)

        for i in range(num_coefs):
            shape = shape + coefs[i][1] * np.cos(
                coefs[i][0] * Angle * num_petals)

        Focal = focal + delta_f / 2 * shape

        Radii = np.sqrt(wavelength*Focal*Ns+0.25*Ns**2*wavelength**2)
        radii = np.sqrt(wavelength*focal*ns+0.25*ns**2*wavelength**2)

        if has_draw:
            plt.figure()
            plt.axis('equal')

        for i in ns[::2]:
            ri1 = Radii[i, :]
            ri2 = Radii[i+1, :]
            xi1 = x0 + self.mask_size[0]/2 + ri1 * np.cos(angle)
            yi1 = y0 + self.mask_size[1]/2 + ri1 * np.sin(angle)
            xi2 = x0 + self.mask_size[0]/2 + ri2 * np.cos(angle)
            yi2 = y0 + self.mask_size[1]/2 + ri2 * np.sin(angle)

            if has_draw:
                plt.plot(xi1, yi1, 'r')
                plt.plot(xi2, yi2, 'b')

            points_1 = np.vstack((xi1, yi1)).transpose()
            points_2 = np.vstack((xi2, yi2)).transpose()

            # by default a solid fill hatch with fill color=7 (white/black)
            hatch = self.msp.add_hatch(dxfattribs=self.dxfattribs)
            # The first path has to set flag: 1 = external
            # flag const.BOUNDARY_PATH_POLYLINE is added (OR) automatically
            hatch.paths.add_polyline_path(points_1,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST,
                                          )
            hatch.paths.add_polyline_path(points_2,
                                          is_closed=True,
                                          flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
                                          # The second path has to set flag: 16 = outermost
                                          )
        return radii

def addpoint(dx: float, dy: float, points: list):
    """_summary_

    Args:
        p0 (list[float, float]): previous point (x,y)
        dx (float): increment in x
        dy (float): increment in y
        points (list): list with points

    Returns:
        points: list with added point
    """
    p0 = points[-1]
    p1 = (p0[0]+dx, p0[1] + dy)
    points.append(p1)

    
    return points
    

def rotate(point: list[float], rotation_point: list[float], angle: float):
    """
    rotate _summary_

    _extended_summary_

    Args:
        point (list[float]): _description_
        rotation_point (list[float]): _description_
        angle (float): _description_

    Returns:
        _type_: _description_
    """
    
    x = point[0]-rotation_point[0]
    y = point[1]-rotation_point[1]

    x_new = rotation_point[0] + x*np.cos(angle) - y*np.sin(angle)
    y_new = rotation_point[1] + x*np.sin(angle) + y*np.cos(angle)
    

    
    return (x_new, y_new)


def draw(msp, dwg, has_draw: bool = True, filename: str = "", dpi=600):
    """Draws dxf file.

    Args:
        filename (str): draw a saved dxf file
    """

    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1])
    ctx = RenderContext(dwg)
    out = MatplotlibBackend(ax)
    Frontend(ctx, out).draw_layout(msp, finalize=True)
    if filename != "":
        print(filename)
        fig.savefig(filename, dpi=dpi)
    if has_draw is False:
        plt.close() 