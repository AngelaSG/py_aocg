# !/usr/bin/env python3
"""
py_aocg/dxf: DXF masks for CLUR laser and ISOM litography

It requires ezdxf module for the generation of .dxf files
    - pip install ezdxf
https://ezdxf.readthedocs.io/en/stable/

to generate pdf:
    - pip install pymupdf

"""

