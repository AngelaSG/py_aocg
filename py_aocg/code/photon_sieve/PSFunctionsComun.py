
# PSFunctionsComun.py

import numpy as np
import io
import PIL
import matplotlib as mpl

from types import NoneType
from PIL import Image
from tqdm import trange # progress bar
from copy import deepcopy
from scipy.interpolate import interp1d
from scipy.signal import find_peaks, peak_widths
from scipy.optimize import curve_fit, minimize
from scipy.signal import savgol_filter
from skimage.transform import rotate
from scipy import interpolate
from py_lab.camera import Camera
from diffractio import degrees, mm, plt, sp, um, nm
from diffractio.scalar_masks_XY import Scalar_mask_XY

### Pruebecitas para ver si se carga el fichero

print('PSFunctionsComun imported')

def IntensityRegion_v2(HolesMask, f0 = 100*mm, lamb = 0.6328*um, NFresnelMax = 9, plots = False, prints = False):
    """
    Figure of merit for Minimize. Return de Irradiance min (- Imax). 

        Parameters:
            HolesMask: Mask of pinholes whose propagation will be minimize in a region with size of Airy radius.
            f0: focal distance in mm, for default is 100 mm.
            lamb: scalar value of wavelength in um, for default is 0.633 um.
            N: maximun order of Fresnel Zone in the future Photon Sieve.
            npx: tuple (npx,npy) o scalar value npx .Number of pixels in x,y axis. For default is 512 in both axis.
            plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.
        
        Returns:
            I_min: Magnitude to minimize.'I_min = -I_max'. 
            
        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """    
    NFresnelMaxSup = NFresnelMax+1
    # lamb = HolesMask.wavelength
    #npx = len(HolesMask.x)
    #npy = len(HolesMask.y) # realmente no hace falta porque de moemento van a ser cuadradas
    
    RAiry = 0.61*np.sqrt(f0*lamb/NFresnelMaxSup) # 0.305*np.sqrt(f0*lamb/NFresnelMaxSup) # um. El 0.305 surge de 1.22/(2*2)
    AiryMask = Scalar_mask_XY(HolesMask.x,HolesMask.y,lamb) 
    AiryMask.circle(r0=(0,0),radius = RAiry)
    
    PropHoleMask = HolesMask.CZT(z=f0, xout = HolesMask.x, yout=HolesMask.y, verbose=False)
    psOverlap = AiryMask.u * PropHoleMask.intensity()
    
    IntRegIrrad = psOverlap.sum()
    
    if prints == True:
        print('IntRegIrrad =', IntRegIrrad)
        
        
    if plots == True:
        AiryMask.draw(title="AiryMask")#, has_colorbar='vertical')
        HolesMask.draw(title="HolesMask")#, has_colorbar='vertical')
        PropHoleMask.draw(kind = 'intensity', logarithm = 1e-2, has_colorbar='horizontal')
        
    I_min = -IntRegIrrad
    
    if prints == True:
        print('I_min =', I_min)
        
    return I_min

def FZP(f0 = 100* mm, lamb = 0.633* um, NFresnelMax = 9, prints = False):
    """
    Returns the radius of Fresnel Zones and their centers.

    Parameters:
        f0 (float): focal distance in mm, by default is 100 mm.
        lamb (float): scalar value of wavelength in um, by default is 0.633 um.
        NFresnelMax (int): maximun order of Fresnel Zone in the future Photon Sieve.
        prints (bool): Show prints. By default is False.

    Returns:
        r_Zn (float): radii of Fresnel Zones.
        r_n (float): radii of Fresnel Zones in the middle.
  
    """
    r_Zn = []
    r_n = []
    Nsup = NFresnelMax+1
    for i in range(1,Nsup+2):
        r_Zn += [np.sqrt(i*lamb*f0)]
    
    r_n += [r_Zn[0]]

    for i in range(1,Nsup+1):
        r_n += [0.5*(r_Zn[i-1]+r_Zn[i])]
    
    
    if prints == True:
        print(r_Zn)
        print(r_n)

    return r_Zn, r_n

def AjParameter(DiffractioMask, f0, propagation = False, contributions = False, plots = False, prints = False):
    """
    Returns the parameter Aj, which give information about how similar is the mask respect to Fresnel Zone Plate.

    Parameters:
        DiffractioMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask
        f0 (float): focal distance in mm, by default is 100 mm.
        propagation (bool): if you want to get the intensity and field, set True, for only Aj parameter set False. By default is False.
        contributions (bool): if True returns the positives contributions mask.
        plots (bool): Show plots. By default is False.
        prints (bool): Show prints. By default is False.

    Returns:
        if propagation = True
            AjNorm (float): Aj parameter normalized to positive FZP. It`s the rate of open area compared to open area of the positive FZP.
            I_Aj (float): Intensity that contributes constructively in the focal plane.
            I_DiffractioMask (float): Intensity of mask in the focal plane.
  
    """
    lamb = DiffractioMask.wavelength
    xmax = np.max(DiffractioMask.x)
    NFresnelMax = int(np.floor(xmax**2/(lamb*f0)))

    FPZMask_ideal = Scalar_mask_XY(x = DiffractioMask.x, y =DiffractioMask.y, wavelength = lamb)
    FPZMask_ideal.fresnel_lens(r0 = (0,0), focal = f0, radius = xmax)

    
    if prints == True:
        print('FPZMask_ideal.u: ', FPZMask_ideal.u)


    if plots == True:
        FPZMask_ideal.draw(title='FPZMask_ideal', has_colorbar = 'horizontal')

    ComparationMask = Scalar_mask_XY(x = FPZMask_ideal.x, y = FPZMask_ideal.y, wavelength = lamb)
    
    ComparationMask.u = FPZMask_ideal.u*np.conjugate(DiffractioMask.u) # el conjugado aquí no tiene por qué involucrarse
    ComparationMaskNorm = FPZMask_ideal.u*np.conjugate(FPZMask_ideal.u) # el conjugado aquí no tiene por qué involucrarse


    if plots == True:
        ComparationMask.draw(title='DiffractioMask', has_colorbar = 'horizontal')
    
    Aj = np.sum(np.sum(ComparationMask.u, axis = 0), axis = 0) 

    Anorm = np.sum(np.sum(ComparationMaskNorm, axis = 0), axis = 0) 

    AjNorm = np.abs(Aj/Anorm)

    if propagation == True and contributions == False:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Aj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)

        return (AjNorm, I_Aj, I_DiffractioMask)

    elif propagation == True and contributions == True:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Aj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        return (AjNorm, I_Aj, I_DiffractioMask, ComparationMask)

    elif propagation == False and contributions == True:
        return (AjNorm, ComparationMask)

    else:
        return (AjNorm)

def BjParameter(DiffractioMask, f0, propagation = False,  contributions = False, plots = False, prints = False):
    """
    Returns the parameter Bj, which give information about how different is the mask respect to Fresnel Zone Plate.

    Parameters:
        DiffractioMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask
        f0 (float): focal distance in mm, by default is 100 mm.
        propagation (bool): if you want to get the intensity and field, set True, for only Bj parameter set False. By default is False.
        contributions (bool): if True returns the positives contributions mask.
        plots (bool): Show plots. By default is False.
        prints (bool): Show prints. By default is False.

    Returns:
        if propagation = True
            BjNorm (float): Bj parameter nomarlized to negative FZP. It`s the rate of open area compared to open area of the negative FZP.
            I_Bj (float): Intensity that contributes destructively in the focal plane.
            I_DiffractioMask (float): Intensity of mask in the focal plane.
  
    """
    lamb = DiffractioMask.wavelength
    xmax = np.max(DiffractioMask.x)
    NFresnelMax = int(np.floor(xmax**2/(lamb*f0)))

    FPZMask_ideal = Scalar_mask_XY(x = DiffractioMask.x, y =DiffractioMask.y, wavelength = lamb)
    FPZMask_ideal.fresnel_lens(r0 = (0,0), focal = f0, radius = xmax, phase = 2*np.pi )
    FPZMask_ideal.inverse_amplitude()
    # FPZMask_ideal.u = np.abs(FPZMask_ideal.u)

    marginMask = Scalar_mask_XY(DiffractioMask.x, DiffractioMask.y, DiffractioMask.wavelength)
    marginMask.circle(r0 = (0,0), radius = max(DiffractioMask.x))

    FPZMask_ideal.u = FPZMask_ideal.u * np.conjugate(marginMask.u)

    FPZMask_ideal.u = np.abs(FPZMask_ideal.u)

    if prints == True:
        print('FPZMask_ideal.u: ', FPZMask_ideal.u)

    if plots == True:
        FPZMask_ideal.draw(title = 'Inverse FPZMask_ideal', has_colorbar='horizontal')
   

    ComparationMask = Scalar_mask_XY(x = FPZMask_ideal.x, y = FPZMask_ideal.y, wavelength = lamb)
    
    ComparationMask.u = FPZMask_ideal.u * np.conjugate(DiffractioMask.u)
    ComparationMaskNorm = FPZMask_ideal.u * np.conjugate(FPZMask_ideal.u)


    if plots == True:
        ComparationMask.draw(title = 'DiffractioMask', has_colorbar='horizontal')
    
    Bj = np.sum(np.sum(ComparationMask.u, axis = 0), axis = 0) 

    Bnorm = np.sum(np.sum(ComparationMaskNorm, axis = 0), axis = 0) 

    BjNorm = np.abs(Bj/Bnorm)

    if propagation == True and contributions == False:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Bj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        return (BjNorm, I_Bj, I_DiffractioMask)
    
    elif propagation == True and contributions == True:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Bj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        return (BjNorm, I_Bj, I_DiffractioMask, ComparationMask)

    elif propagation == False and contributions == True:
        return (BjNorm, ComparationMask)

    else:
        return(BjNorm)

def ideal_photon_sieve(maskEmpty, xcenter, ycenter, RHoles):
    """
    Return an ideal photon sieve. The holes are placed in the odd Fresnel Zones.

    Inputs:

    xcenter (list): Each element is an array that contains x coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(x11, x12, ..., x1j1), nd.array_FZ3(x31, x32, ..., x1j3), ..., nd.array_FZN(xN1, xN2, ..., xNjN)]
    ycenter (list): Each element is an array that contains y coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(y11, y12, ..., y1j1), nd.array_FZ3(y31, y32, ..., y1j3), ..., nd.array_FZN(yN1, yN2, ..., yNjN)]
    RHoles (list): radii of Holes in each Fresnel Zone

    Outputs:
    
    modify input maskEmpty ideal photon sieve mask.
    """
    x0 = maskEmpty.x
    y0 = maskEmpty.y
    lamb = maskEmpty.wavelength

    #ps0=Scalar_mask_XY(x0,y0,lamb) # Hay que meter la longitud de onda en um
    t_circle=Scalar_mask_XY(x0,y0,lamb)
    #ps=Scalar_mask_XY(x0,y0,lamb)   

    #t = []

    for n, R in enumerate(RHoles):
        t = trange(len(xcenter[n]))
        for tm,i,j in zip(t, xcenter[n], ycenter[n]):
            #print(i)
            t_circle.circle(r0 = (i,j), radius = R)

            #t += [t_circle]
            #t_circle.draw()
            maskEmpty.u += t_circle.u
    
    return None

def IdealPSParameters(f0 = 100*mm, wavelength = 0.633*um, g = 20*um, plots = False):
    """
    Returns the necessary parameters for ideal_photon_sieve() function.

    Inputs:

    f0 (float): the focal of the photon sieve. By default is 100 mm.
    wavelength (float): the wavelength of the light source. By default is 0.633 um.
    g (float): the safety parameter for drilled holes. By default is 20 um.
    plots (bool): if True show plots. By default is False.

    Outputs:
    
    xcenter (list): Each element is an array that contains x coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(x11, x12, ..., x1j1), nd.array_FZ3(x31, x32, ..., x1j3), ..., nd.array_FZN(xN1, xN2, ..., xNjN)]
    ycenter (list): Each element is an array that contains y coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(y11, y12, ..., y1j1), nd.array_FZ3(y31, y32, ..., y1j3), ..., nd.array_FZN(yN1, yN2, ..., yNjN)]
    RHoles (list): radii of Holes in each Fresnel Zone

    """
    
    NFresnelMax = int(np.floor(wavelength*f0/(16*g**2)))
    r_Zn, r_n = FZP(f0, wavelength, NFresnelMax) #OJO, por construcción da dos zonas de más en r_Zn
    r_Zn_arr = np.array(r_Zn)
    r_n_arr = np.array(r_n)
    phi_n_arr = r_Zn_arr[1:]-r_Zn_arr[:-1]
    phi_n_arr = np.insert(phi_n_arr, 0, 2*r_Zn_arr[0])
    j_n = np.floor(2*np.pi*r_n_arr[1:]/(phi_n_arr[1:]+2*g))
    j_n = np.insert(j_n, 0, 1)

    alpha_i_n = {}
    for i,j in enumerate(j_n):
        #print("i: ",i);
        #print("j:",j);
        alpha_i_n[str(i+1)] = []
        for k in range(int(j)):
            alpha_i_n[str(i+1)]+=[k*2*np.pi/j]
            #print("k: ",k)

    xcenter = [[np.array(0)]]
    ycenter = [[np.array(0)]]
    RHoles = [r_n[0]]

    for r, i in enumerate(alpha_i_n.values()):
        if r%2 == 0 and r !=0:
            #i_arr = np.array(i)
            r_aux = r_n_arr[r]
            xcenter += [r_aux*np.cos(i)]
            ycenter += [r_aux*np.sin(i)]
            RHoles += [phi_n_arr[r]/2]
            #print(i_arr.shape)
            #print(r_aux)

    if plots == True:
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(1,1,1)
        for FZ in range(len(xcenter)):
            ax1.plot(xcenter[FZ], ycenter[FZ], '.') 
    return xcenter, ycenter, RHoles

def GetResultsPS(PSMask, f0 = 100*mm, g = 20*um, plots = False, prints = False):
    """
    Return values for comparations with others photon sieves.

    Inputs:

    PSMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): photon sieve mask.
    f0 (float): focal length of the photon sieve.
    g (float): the safety parameter for drilled holes. By default is 20 um.
    plots (bool): if True show plots. By default is False.
    prints (bool): = if True show prints. By default is False.

    Outputs:

    I_max (float): Intensity calculated with IntensityRegion_v2() function.
    OpenArea (float): percentage of surface area of holes in the mask.
    Ajnorm (float): parameter Aj normalized.
    Bjnorm (float): parameter Bj normalized.
    """
    
    lamb = PSMask.wavelength
    NFresnelMax = int(np.floor(lamb*f0/(16*g**2)))
    
    # Intensity

    I_min = IntensityRegion_v2(HolesMask = PSMask, f0 = f0, NFresnelMax = NFresnelMax, plots=plots)
    I_max = -I_min
    
    if prints == True:
        print('Intensity (U. A.): {:.2f}'.format(I_max))
    
    # Open area (area drilled)

    # ind1 = np.where(np.sqrt(PSMask.u*np.transpose(PSMask.u))>0)
    ind1 = np.where(np.sqrt(PSMask.u*np.conjugate(PSMask.u))>0)
    OpenArea = sum(np.abs(PSMask.u[ind1]))/np.sum(np.ones_like(np.abs(PSMask.u)))
    
    if prints == True:
        print('OpenArea: {:.2f}%'.format(OpenArea*100))
    
    # Aj parameter

    Ajnorm = AjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Aj: {:.2f}'.format(Ajnorm))

    # Bj parameter

    Bjnorm = BjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Bj: {:.2f}'.format(Bjnorm))
    
    return I_max, OpenArea, Ajnorm, Bjnorm

def GetResultsPS_v2(PSMask, f0 = 100*mm, g = 20*um, NFresnelMax = 20, plots = False, prints = False):
    """
    Return values for comparations with others photon sieves.

    Inputs:

    PSMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): photon sieve mask.
    f0 (float): focal length of the photon sieve.
    g (float): the safety parameter for drilled holes. By default is 20 um.
    NFresnelMax (int): 
    plots (bool): if True show plots. By default is False.
    prints (bool): = if True show prints. By default is False.

    Outputs:

    I_max (float): Intensity calculated with IntensityRegion_v2() function.
    OpenArea (float): percentage of surface area of holes in the CIRCULAR mask.
    Ajnorm (float): parameter Aj normalized.
    Bjnorm (float): parameter Bj normalized.
    """
    
    lamb = PSMask.wavelength
    # NFresnelMax = int(np.floor(lamb*f0/(16*g**2)))
    
    # Intensity

    I_min = IntensityRegion_v2(HolesMask = PSMask, f0 = f0, NFresnelMax = NFresnelMax, plots=plots)
    I_max = -I_min
    
    if prints == True:
        print('Intensity (U. A.): {:.2f}'.format(I_max))
    
    # Open area (area drilled)

    Rmask = max(PSMask.x)
    maskCompArea = Scalar_mask_XY(PSMask.x, PSMask.y, PSMask.wavelength)
    maskCompArea.circle(r0 = (0,0), radius = Rmask)
    
    if plots == True:
        maskCompArea.draw(title = 'maskCompArea')

    OAPSMask = deepcopy(PSMask)
    OAPSMask.u = maskCompArea.u * OAPSMask.u

    # ind1 = np.where(np.sqrt(OAPSMask.u*np.transpose(OAPSMask.u))>0)
    ind1 = np.where(np.sqrt(OAPSMask.u*np.conjugate(OAPSMask.u))>0)
    OpenArea = np.sum(np.abs(OAPSMask.u[ind1]))/np.sum(np.abs(maskCompArea.u)) # AQUÍ ME HE QUEDADO RETOCANDO
    # print('np.abs(OAPSMask.u[ind1]): ', np.sum(np.abs(OAPSMask.u[ind1])))
    # print('np.sum(np.abs(maskCompArea.u): ', np.sum(np.abs(maskCompArea.u)))
    # print('OpenArea: ', OpenArea)
    # print('I_max: ', I_max)
        
    if prints == True:
        print('OpenArea: {:.2f}%'.format(OpenArea*100))
    
    # Aj parameter

    Ajnorm = AjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Aj: {:.2f}'.format(Ajnorm))

    # Bj parameter

    Bjnorm = BjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Bj: {:.2f}'.format(Bjnorm))
    
    return I_max, OpenArea, Ajnorm, Bjnorm

def GetResultsPS_v3(PSMask, f0 = 100*mm, g = 20*um, NFresnelMax = 20, D = 3.5*mm, plots = False, prints = False):
    """
    Return values for comparations with others photon sieves.

    Inputs:

    PSMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): photon sieve mask.
    f0 (float): focal length of the photon sieve.
    g (float): the safety parameter for drilled holes. By default is 20 um.
    NFresnelMax (int): 
    plots (bool): if True show plots. By default is False.
    prints (bool): = if True show prints. By default is False.

    Outputs:

    I_max (float): Intensity calculated with IntensityRegion_v2() function.
    OpenArea (float): percentage of surface area of holes in the CIRCULAR mask.
    Ajnorm (float): parameter Aj normalized.
    Bjnorm (float): parameter Bj normalized.
    """
    
    lamb = PSMask.wavelength
    # NFresnelMax = int(np.floor(lamb*f0/(16*g**2)))
    
    # Intensity

    I_min = IntensityRegion_v2(HolesMask = PSMask, f0 = f0, NFresnelMax = NFresnelMax, plots=plots)
    I_max = -I_min
    
    if prints == True:
        print('Intensity (U. A.): {:.2f}'.format(I_max))
    
    # Open area (area drilled)

    Rmask = D/2
    maskCompArea = Scalar_mask_XY(PSMask.x, PSMask.y, PSMask.wavelength)
    maskCompArea.circle(r0 = (0,0), radius = Rmask)
    
    if plots == True:
        maskCompArea.draw(title = 'maskCompArea')

    OAPSMask = deepcopy(PSMask)
    OAPSMask.u = maskCompArea.u * OAPSMask.u

    # ind1 = np.where(np.sqrt(OAPSMask.u*np.transpose(OAPSMask.u))>0)
    ind1 = np.where(np.sqrt(OAPSMask.u*np.conjugate(OAPSMask.u))>0)
    OpenArea = np.sum(np.abs(OAPSMask.u[ind1]))/np.sum(np.abs(maskCompArea.u)) # AQUÍ ME HE QUEDADO RETOCANDO
    # print('np.abs(OAPSMask.u[ind1]): ', np.sum(np.abs(OAPSMask.u[ind1])))
    # print('np.sum(np.abs(maskCompArea.u): ', np.sum(np.abs(maskCompArea.u)))
    # print('OpenArea: ', OpenArea)
    # print('I_max: ', I_max)
        
    if prints == True:
        print('OpenArea: {:.2f}%'.format(OpenArea*100))
    
    # Aj parameter

    Ajnorm = AjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Aj: {:.2f}'.format(Ajnorm))

    # Bj parameter

    Bjnorm = BjParameter(DiffractioMask = PSMask, f0 = f0)

    if prints == True:
        print('Bj: {:.2f}'.format(Bjnorm))
    
    return I_max, OpenArea, Ajnorm, Bjnorm

def IsManufacturable(mask, CoordHoles, RHoles, g = 20*um):
    """
    Return arrays non-empty if it exits overlap.

    Inputs:

    mask (diffractio.scalar_masks_XY.Scalar_mask_XY object): photon sieve mask.
    CoordHoles (ndarray): 2-dimensional array that store x,y coordinates of each hole.
    RHoles (array): 1-dimensional array that store the radius of each hole (including the first Fresnel zone hole).
    g (float): the safety parameter for drilled holes. By default is 20 um.

    Outputs:

    HolesOverlap1 (array): contains holes that overlap with HolesOverlap2's holes.
    HolesOverlap2 (array): contains holes that overlap with HolesOverlap1's holes.
    dc (array): contains center distance between olesOverlap1's holes and olesOverlap2's holes.
    dm (array): contains minimun center distance that it should exit between olesOverlap1's holes and olesOverlap2's holes.
    indHole1 (array): index HolesOverlap1`s in CoordHoles and RHoles.
    indHole2 (array): index HolesOverlap2`s in CoordHoles and RHoles.
    """
    
    # Verificación de que el agujero no solapa con el resto

    NumberOfHoles = len(RHoles)
    #verificationOverlap = True
    HolesOverlap1 = []
    indHole1 = []
    indHole2 = []
    HolesOverlap2 = []
    dc = []
    dm = []

    for NH in range(NumberOfHoles):

        xcNHf, ycNHf = CoordHoles[NH]
        RNewHole = RHoles[NH]
        d_centers = np.sqrt((xcNHf-CoordHoles[NH+1:,0])**2+(ycNHf-CoordHoles[NH+1:,1])**2)
        dmin = RNewHole+RHoles[NH+1:]+2*g
    
        #print('NH = {}, CoordHoles[NH] = {} y  RHoles[NH] = {}: '.format(NH, CoordHoles[NH], RHoles[NH]))
        # print('d_centers = ', d_centers)
        # print('dmin = ', dmin)
        # print('(d_centers < RNewHole+RHoles[NH+1:]+2*g).any(): ', (d_centers < RNewHole+RHoles[NH+1:]+2*g).any())
    
        if (d_centers < dmin).any() == True:
        
            mask1 = Scalar_mask_XY(mask.x, mask.y, wavelength = mask.wavelength)
            mask2 = Scalar_mask_XY(mask.x, mask.y, wavelength = mask.wavelength)
            mask3 = Scalar_mask_XY(mask.x, mask.y, wavelength = mask.wavelength)
            mask1.circle(r0 = (xcNHf,ycNHf), radius=RNewHole)
            #mask1.draw()
            # print('d_centers = ', d_centers)
            # print('dmin = ', dmin)
            # print('(d_centers < RNewHole+RHoles[NH+1:]+2*g): ', (d_centers < dmin))

            prob1 = (d_centers < dmin)*CoordHoles[NH+1:,0]
            prob2 = (d_centers < dmin)*CoordHoles[NH+1:,1]
            prob3 = (d_centers < dmin)*RHoles[NH+1:]
            xx = list(np.extract(prob1, CoordHoles[NH+1:,0]))
            yy = list(np.extract(prob2, CoordHoles[NH+1:,1]))
            RR = list(np.extract(prob3, RHoles[NH+1:]))
    
            mask2.circle(r0 = (xx[0],yy[0]), radius=RR[0])
            #mask2.draw()
    
            mask3.u = mask1.u + mask2.u
    
            # mask3.draw()
    
            HolesOverlap1 += [[xcNHf, ycNHf, RNewHole]]
            HolesOverlap2 += [xx + yy + RR]
            # print('xx[0]: ', xx[0])
            # print('yy[0]: ', yy[0])
            # print('RR[0]: ', RR[0])
            # print('dc: ', np.sqrt((xcNHf-xx[0])**2+(ycNHf-yy[0]**2)))
            # print('dm: ', RNewHole+RR[0]+2*g)
            # print('(xcNHf-xx[0])**2: ', (xcNHf-xx[0])**2)
            # print('(ycNHf-yy[0]**2): ', (ycNHf-yy[0]**2))
            indHole1 += [NH]
            indHole2Aux, = np.where(CoordHoles[:,0] == xx[0])
            # print(indHole2Aux)
            indHole2 += [indHole2Aux[0]]
            #print('indhole2: ', indHole2)
            dc += [np.sqrt((xcNHf-xx[0])**2+(ycNHf-yy[0])**2)]
            dm += [RNewHole+RR[0]+2*g]

    return HolesOverlap1, HolesOverlap2, dc, dm, indHole1, indHole2

def RemakeMask(CoordHoles, RHoles, limits, pxs, wavelength, plots = False, prints = False):
    """
    Generates a previusly designed mask. 

    Inputs:

    CoordHoles (ndarray): 2-dimensional array that store x,y coordinates of each hole.
    RHoles (array): 1-dimensional array that store the radius of each hole (including the first Fresnel zone hole).
    limits (like array): limits of size of mask. limits = [xmin xmax ymin ymax].
    pxs (int or tupple of int): number of pixels in x-axis and y-axis (tupple). If isn't tupple the mask assumed square.
    wavelength (float): wavelength in nm.
    plots (bool): if true, show plots.

    Outputs:

    mask2 (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask generated.
    """
    if isinstance(pxs,tuple):
        npxx = pxs[0]
        npxy = pxs[1]
        # print('npxx: ', npxx)
        # print('npxy: ', npxy)
        
    else:
        npxx = pxs
        npxy = npxx
        # print('npx: ', npxx)

    xmin = limits[0]
    xmax = limits[1]
    ymin =  limits[2]
    ymax =  limits[3]

    mask1 = Scalar_mask_XY(np.linspace(xmin, xmax, npxx), np.linspace(ymin, ymax, npxy), wavelength)
    mask2 = Scalar_mask_XY(np.linspace(xmin, xmax, npxx), np.linspace(ymin, ymax, npxy), wavelength)

    for xypos, RH in zip(CoordHoles, RHoles):

        mask1.circle(r0=xypos, radius=RH)
        #mask1.draw()
        mask2.u =  mask2.u + mask1.u

    mask2.u = abs(mask2.u) # No sé por qué pero si no se vuelve complex

    if prints == True:
        print(mask2.u)

    if plots == True:
        mask2.draw()

    return mask2

def RemakeMask_inverse(CoordHoles, RHoles, limits, pxs, wavelength, D, plots = False, prints = False):
    """
    Generates a previusly designed mask. 

    Inputs:

    CoordHoles (ndarray): 2-dimensional array that store x,y coordinates of each hole.
    RHoles (array): 1-dimensional array that store the radius of each hole (including the first Fresnel zone hole).
    limits (like array): limits of size of mask. limits = [xmin xmax ymin ymax].
    pxs (int or tupple of int): number of pixels in x-axis and y-axis (tupple). If isn't tupple the mask assumed square.
    wavelength (float): wavelength in nm.
    plots (bool): if true, show plots.

    Outputs:

    mask2 (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask generated.
    """
    if isinstance(pxs,tuple):
        npxx = pxs[0]
        npxy = pxs[1]
        # print('npxx: ', npxx)
        # print('npxy: ', npxy)
        
    else:
        npxx = pxs
        npxy = npxx
        # print('npx: ', npxx)

    xmin = limits[0]
    xmax = limits[1]
    ymin =  limits[2]
    ymax =  limits[3]

    mask1 = Scalar_mask_XY(np.linspace(xmin, xmax, npxx), np.linspace(ymin, ymax, npxy), wavelength)
    mask2 = Scalar_mask_XY(np.linspace(xmin, xmax, npxx), np.linspace(ymin, ymax, npxy), wavelength)
    mask1.circle(r0=(0,0), radius=D/2)

    mask2.u =  mask2.u + mask1.u
    # mask2.draw()

    for xypos, RH in zip(CoordHoles, RHoles):

        mask1.circle(r0=xypos, radius=RH)
        mask1.inverse_amplitude()
        # mask1.draw()
        mask2.u =  mask2.u * mask1.u
        # mask2.draw()

    mask2.u = abs(mask2.u) # No sé por qué pero si no se vuelve complex

    if prints == True:
        print(mask2.u)

    if plots == True:
        mask2.draw()

    return mask2

# ÚTILES ESTADISTICA

# Para ver qué número de bins es adecuado (obtenido de https://www.analyticslane.com/2022/02/25/calcular-el-numero-optimo-de-bins-para-un-histograma/):

def sturges(data):
    """
    Return the optimal number of bins of a data set by Sturges rule.

    Inputs:
    data (array): data set.

    Outputs:
    num_bins (int): optimal number of bins of a data set.
    
    """
    num_data = len(data)
    num_bins = int(np.log2(num_data)) + 1
    return num_bins

def freedman_diaconis(data):
    """
    Return the optimal number of bins of a data set by Freedman-Diaconis rule.

    Inputs:
    data (array): data set.

    Outputs:
    num_bins (int): optimal number of bins of a data set.
    
    """
    num_data = len(data)
    irq = np.percentile(data, 75) - np.percentile(data, 25)
    bin_width = 2 * irq / np.power(num_data, 1/3)
    num_bins = int((np.max(data) -  np.min(data)) / bin_width)  + 1
    return num_bins
    
def GetOptimalBins(data):
    """
    Return the optimal number of bins of a data set by taking the maximun between Sturges rule and Freedman-Diaconis rule.

    Inputs:
    data (array): data set.

    Outputs:
    Bins (int): optimal number of bins of a data set.
    
    """
    BinsSturges = sturges(data)
    BinsFreedman = freedman_diaconis(data)
    Bins = max(BinsSturges, BinsFreedman)

    return Bins

# ÚTILES PARA EL SLM

def MaskPS2SLM(radii, alpha, Radii, widthMaskPS, highMaskPS, resolutionSLM = np.array([1024, 768]), pixelSizeSLM = np.array([19, 19])*um, lamb = 0.633 *um, Draw = False):

    """
    Return a mask of photon sieve adapted to SLM. 

        Parameters:
            radii (array): np.array([r1, r2, ..., rN]). Radial position of holes.
            alpha (array): np.array([alpha1, alpha2, ..., alphaN]). Angular position of holes.
            Radii (array): np.array([R1, R2, ..., RN]). Radius of holes.
            widthMaskPS (float): width of initial mask in um.
            highMaskPS (float):  high of initial mask in um.
            resolutionSLM (array): np.array([npx, npy]). Number of pixels in axis x and y. Default is np.array([1024, 768]).
            pixelSizeSLM (array): np.array([lpx, lpy]). SLM pixel height and width. Default is np.array([19, 19])*um.
            Draw (bool): Draw the adapted mask. Default False.
        
        Returns:
            MaskSLM (diffractio.scalar_masks_XY.Scalar_mask_XY object): It's the mask of photon Sieve adapted.

            
        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """    
    
    SizeSLM = resolutionSLM*pixelSizeSLM
    Mw = SizeSLM[0]/widthMaskPS
    Mh = SizeSLM[1]/highMaskPS
    M = min(Mw,Mh)
    
    
    x = radii*np.cos(alpha)*M
    y = radii*np.sin(alpha)*M
    R = Radii*M   
    #print(M)

    x0SLM = np.linspace(-int(SizeSLM[0]/2), int(SizeSLM[0]/2), resolutionSLM[0])
    y0SLM = np.linspace(-int(SizeSLM[1]/2), int(SizeSLM[1]/2), resolutionSLM[1])

    MaskSLM = Scalar_mask_XY(x0SLM, y0SLM, lamb)
    Mask = Scalar_mask_XY(x0SLM, y0SLM, lamb)
    
    for xi,yi,Ri in zip(x,y,R):
    
        Mask.circle(r0=(xi,yi), radius=Ri)
        MaskSLM.u = MaskSLM.u + Mask.u
   

    if Draw == True:
        MaskSLM.draw()
    
    return MaskSLM, M

# ANÁLISIS DE LAS MÁSCARAS

def DOFMask(fieldXZ, c = 0.5, prints = False, plots = False, normalize = False):
    """
    Return the depth of focus (DOF) of the mask field. 

        Parameters:
            fieldXZ (diffractio.scalar_fields_XZ.Scalar_field_XZ object): propagation of the mask.
            c (float): heigth ratio used for beam width calculus. it's 0.5 by default.
            prints: show prints.
            plots: show plots.
        
        Returns:
            dof (float): DOF in um.
    
    """    

    
    Ix0 = fieldXZ.profile_longitudinal(kind='intensity', x0=0, draw=None)
    Ix0 = Ix0*1e2
    
    maxI = max(Ix0)
    
    HalfI = c*maxI
    
    ind = np.where(Ix0>=HalfI)

    dof = fieldXZ.z[ind][-1]-fieldXZ.z[ind][0]

    if normalize == True:
        Ix0 = Ix0/max(Ix0)
        maxI = max(Ix0)
        HalfI = c*maxI

    if prints == True:
        print(Ix0)
        print(maxI)
        print(HalfI)
        print(ind)   

    if plots == True:
        fig = plt.figure()
        ax=fig.add_subplot()
        ax.plot(fieldXZ.z*1e-3,Ix0)
        ax.plot(fieldXZ.z[ind]*1e-3,np.ones(Ix0[ind].shape)*HalfI,':r')
        ax.plot(fieldXZ.z[ind][0]*1e-3,HalfI,'*r')
        ax.plot(fieldXZ.z[ind][-1]*1e-3,HalfI,'*r')
        ax.set_xlabel('z (mm)', fontsize=17)
        ax.set_ylabel('$I_z$ (A. U.)', fontsize=15)
        ax.grid(True)

    print(dof)

    return dof

def SizeBeamMask(fieldXZ, c = 0.5, f0 = 100*mm, prints = False, plots = False, N = None):

    """
    Return a width beam. 

        Parameters:
            fieldXZ (diffractio.scalar_fields_XZ.Scalar_field_XZ object): propagation of the mask.
            c (float): heigth ratio used for beam width calculus. it's 0.5 by default.
            f0 (float): focus value for the mask.
            prints: show prints.
            plots: show plots.
            N (int): id number for mask.
        
        Returns:
            widthx (float): width beam in um.
    
    """ 

    Ix0 = fieldXZ.profile_transversal(z0=f0, draw=plots)
    # print(Ix0)
    maxI = max(Ix0)
    
    HalfI = c*maxI
    
    ind = np.where(Ix0>=HalfI)

    widthx = fieldXZ.x[ind][-1]-fieldXZ.x[ind][0]

    if prints == True:
        print(Ix0)
        print(maxI)
        print(HalfI)
        print(ind)   

    if plots == True:
        plt.plot(fieldXZ.x[ind],np.ones(Ix0[ind].shape)*HalfI,'.r')
        plt.plot(fieldXZ.x[ind][0],HalfI,'+r')
        plt.plot(fieldXZ.x[ind][-1],HalfI,'+r')
        if N != None:
            plt.title('Mask {}'.format(N))

    #print(fod)
    return widthx

def BeamWidth(FieldXY, plots=False, N = None):

    """
    Return a width beam. 

        Parameters:
            fieldXY (diffractio.scalar_fields_XY.Scalar_field_XY object): propagation of the mask in XZ plane.
            plots: show plots.
            N (int): id number for mask.
        
        Returns:
            widthx (float): width beam in um.
            widthy (float): width beam in um.

        Example Code:
            u = MaskDiffractio.CZT(z=f0)
            widthx, widthy = BeamWidth(u, plots=True)
    
    """

    # La matriz con la propagación en el plano focal es "Mask.u". Vamos a obtener la anchura del foco en las direcciones x e y siguiendo el criterio de FWHM, 
    # que básicamente se trata de ver la anchura a la mitad del valor máximo.
    
    npxx = len(FieldXY.x)
    npxy = len(FieldXY.y)

    if (npxx%2 == 0) == True:
        hnpx = int(np.floor(npxx*0.5))
        DirX = FieldXY.u[hnpx:(hnpx+1),:] # Por ejemplo, si npx = 256, tomo los píxeles 127 y 128 en eje y fijos y hago el barrido en todo el eje x.
    else:
        hnpx = int(np.floor(npxx*0.5))
        DirX = FieldXY.u[hnpx+1,:] # Por ejemplo, si npx = 257, tomo el píxel 129 (128+1) en eje y fijo y hago el barrido en todo el eje x.

    if (npxy%2 == 0) == True:
        hnpy = int(np.floor(npxy*0.5))
        DirY = FieldXY.u[hnpy:(hnpy+1),:] # Por ejemplo, si npy = 256, tomo los píxeles 127 y 128 en eje x fijos y hago el barrido en todo el eje y.
    else:
        hnpy = int(np.floor(npxy*0.5))
        DirY = FieldXY.u[hnpy+1,:] # Por ejemplo, si npx = 257, tomo el píxel 129 (128+1) en eje x fijo y hago el barrido en todo el eje y.
    
    DirXMean = np.mean(DirX, axis=0)
    DirYMean = np.mean(DirY, axis=0)

    Ix = DirXMean*np.conjugate(DirXMean)
    Iy = DirYMean*np.conjugate(DirYMean)

    # Veamos ahora la anchura

    # Eje x

    Maxx = np.max(Ix)
    HalfMaxx = 0.5*Maxx

    fx = interp1d(FieldXY.x, Ix) # interpolo para que sea más exacto
    xx = np.linspace(-200,200,1000)
    IxInt = fx(xx)

    OverValuesX = np.where(IxInt>=HalfMaxx)
    widthx = xx[OverValuesX[0][-1]]-xx[OverValuesX[0][0]]

    # Eje y

    Maxy = np.max(Iy)
    HalfMaxy = 0.5*Maxy

    fy = interp1d(FieldXY.y, Iy) # interpolo para que sea más exacto
    yy = np.linspace(-200,200,10000) #Para nosotros de momento me funcionan con estos valores... podemos hacer algo para que se automatice también.
    IyInt = fy(yy)

    OverValuesY = np.where(IyInt>=HalfMaxy)
    widthy = yy[OverValuesY[0][-1]]-yy[OverValuesY[0][0]]

    #print('anchura en x: {} um\n'.format(widthx))
    #print('anchura en y: {} um\n'.format(widthy))

    if plots == True: 
        fig2,ax2=plt.subplots(2,2)

        ax2[0,0].plot(FieldXY.x, Ix, '.-')
        ax2[0,0].set_xlabel('x($\mu$ m)')
        ax2[0,0].set_ylabel('$I_x$ (U. A.)')
        ax2[1,0].plot(FieldXY.y, Iy, '.-')
        ax2[1,0].set_xlabel('y($\mu$ m)')
        ax2[1,0].set_ylabel('$I_y$ (U. A.)')
        ax2[0,0].grid(True)
        ax2[1,0].grid(True)
        ax2[0,1].plot(FieldXY.x, Ix, '.-')
        ax2[0,1].plot(xx, IxInt, '.')
        ax2[0,1].set_xlabel('x($\mu$ m)')
        ax2[0,1].set_ylabel('$I_x$ (U. A.)')
        ax2[1,1].plot(FieldXY.y, Iy, '.-')
        ax2[1,1].plot(yy, IyInt, '.')
        ax2[1,1].set_xlabel('y($\mu$ m)')
        ax2[1,1].set_ylabel('$I_y$ (U. A.)')
        ax2[0,1].grid(True)
        ax2[1,1].grid(True)

        if N != None:
            plt.title('Mask {}'.format(N))

        fig2.subplots_adjust(wspace=0.3 ,hspace=0.5)
        fig2.suptitle('Interpolation')

    return widthx, widthy

def BeamWidth_v2(FieldXY, plots=False, N = None, normalize = False):

    """
    Return a width beam. 

        Parameters:
            fieldXY (diffractio.scalar_fields_XY.Scalar_field_XY object): propagation of the mask in XZ plane.
            plots: show plots.
            N (int): id number for mask.
        
        Returns:
            widthx (float): width beam in um.
            widthy (float): width beam in um.

        Example Code:
            u = MaskDiffractio.CZT(z=f0)
            widthx, widthy = BeamWidth(u, plots=True)
    
    """

    # La matriz con la propagación en el plano focal es "Mask.u". Vamos a obtener la anchura del foco en las direcciones x e y siguiendo el criterio de FWHM, 
    # que básicamente se trata de ver la anchura a la mitad del valor máximo.
    npxx = len(FieldXY.x)
    npxy = len(FieldXY.y)

    if (npxx%2 == 0) == True:
        hnpx = int(np.floor(npxx*0.5))
        DirX = FieldXY.u[hnpx:(hnpx+1),:] # Por ejemplo, si npx = 256, tomo los píxeles 127 y 128 en eje y fijos y hago el barrido en todo el eje x.
    else:
        hnpx = int(np.floor(npxx*0.5))
        DirX = FieldXY.u[hnpx+1,:] # Por ejemplo, si npx = 257, tomo el píxel 129 (128+1) en eje y fijo y hago el barrido en todo el eje x.

    if (npxy%2 == 0) == True:
        hnpy = int(np.floor(npxy*0.5))
        DirY = FieldXY.u[hnpy:(hnpy+1),:] # Por ejemplo, si npy = 256, tomo los píxeles 127 y 128 en eje x fijos y hago el barrido en todo el eje y.
    else:
        hnpy = int(np.floor(npxy*0.5))
        DirY = FieldXY.u[hnpy+1,:] # Por ejemplo, si npx = 257, tomo el píxel 129 (128+1) en eje x fijo y hago el barrido en todo el eje y.
    
    DirXMean = np.mean(DirX, axis=0)
    DirYMean = np.mean(DirY, axis=0)

    Ix = np.abs(DirXMean*np.conjugate(DirXMean))
    Iy = np.abs(DirYMean*np.conjugate(DirYMean))

    if normalize == True:
        Ix = Ix/np.max(Ix)
        Iy = Iy/np.max(Iy)

    # Veamos ahora la anchura

    # Eje x

    fx = interp1d(FieldXY.x, Ix) # interpolo para que sea más exacto
    xx = np.linspace(-200,200,1000)
    IxInt = fx(xx)

    Maxx = np.max(IxInt)
    HalfMaxx = 0.5*Maxx

    OverValuesX, = np.where(IxInt>=HalfMaxx)
    widthx = xx[OverValuesX[-1]]-xx[OverValuesX[0]]

    print('xx[OverValuesX[0]]: ', xx[OverValuesX[0]])

    if plots == True: 
        
        fig2 = plt.figure()
        ax2 = fig2.add_subplot()
        ax2.plot(xx, IxInt)
        ax2.plot(xx[OverValuesX],np.ones_like(xx[OverValuesX])*HalfMaxx, ':g')
        ax2.plot(xx[OverValuesX][0],HalfMaxx, '*g')
        ax2.plot(xx[OverValuesX][-1],HalfMaxx, '*g')
        ax2.set_ylabel('$I_x$ (A. U.)', fontsize=15)
        ax2.set_xlabel('$x$ ($\mu m$)', fontsize=15)
        ax2.tick_params(axis='both', which='major', labelsize=12)
        ax2.text(x=xx[OverValuesX[0]]*3.3, y=HalfMaxx*1.1, s='$width_x$ = {:.2f} $\mu m$'.format(widthx), fontsize=15)
        ax2.grid()
        if isinstance(N, int):
            ax2.set_title('Mask {}'.format(N), fontsize=15)

    # Eje y

    Maxy = np.max(Iy)
    HalfMaxy = 0.5*Maxy

    fy = interp1d(FieldXY.y, Iy) # interpolo para que sea más exacto
    yy = np.linspace(-200,200,10000) #Para nosotros de momento me funcionan con estos valores... podemos hacer algo para que se automatice también.
    IyInt = fy(yy)

    OverValuesY, = np.where(IyInt>=HalfMaxy)
    widthy = yy[OverValuesY[-1]]-yy[OverValuesY[0]]

    if plots == True: 
        
        fig3 = plt.figure()
        ax3 = fig3.add_subplot()
        ax3.plot(yy, IyInt)
        ax3.plot(yy[OverValuesY],np.ones_like(yy[OverValuesY])*HalfMaxy, ':c')
        ax3.plot(yy[OverValuesY][0],HalfMaxy, '*c')
        ax3.plot(yy[OverValuesY][-1],HalfMaxy, '*c')
        ax3.set_ylabel('$I_y$ (A. U.)', fontsize=15)
        ax3.tick_params(axis='both', which='major', labelsize=12)
        ax3.set_xlabel('$y$ ($\mu m$)', fontsize=15)
        ax3.text(x=yy[OverValuesY[0]]*3.3, y=HalfMaxy*1.1, s='$width_y$ = {:.2f} $\mu m$'.format(widthy), fontsize=15)
        ax3.grid(True)
        if isinstance(N, int):
            ax3.set_title('Mask {}'.format(N), fontsize=15)
        
    if plots == False:
        return widthx, widthy
    
    return widthx, widthy, ax2, ax3

def FWHM_FromImage_old(profile, ax, prints = True):
    """
    Return a width beam used FWHF. 

        Parameters:
            profile (np.ndarray): profile intensity obtained from photo.
            ax (np.ndarray): correponding axis profile.
        
        Returns:
            width (float): width beam in um.

        Example Code:
            u = MaskDiffractio.CZT(z=f0)
            widthx, widthy = BeamWidth(u, plots=True)
    
    """
    Imax = np.max(profile)
    HalfImax = 0.5*Imax
    Iover = np.where(profile > HalfImax,profile, 0)
    indNonNull, = np.where(Iover != 0)
    width = ax[indNonNull[-1]]-ax[indNonNull[0]]
    
    
    w = np.ones_like(Iover[indNonNull])

    return width

def FWHM_FromImage(profile, ax, plots = False):
    
    """
    Return a width beam used FWHF. 

        Parameters:
            profile (np.ndarray): profile intensity obtained from photo.
            ax (np.ndarray): correponding axis profile.
        
        Returns:
            width (float): width beam in um.

        Example Code:
            u = MaskDiffractio.CZT(z=f0)
            widthx, widthy = BeamWidth(u, plots=True)
    
    """
    Imax = np.max(profile)
    PosMax, = np.where(profile == Imax)
    # print('PosMax before: ',PosMax )
    # print('np.shape(PosMax)[1] before: ',np.shape(PosMax)[0])
    if np.shape(PosMax)[0] != 1:
        idx = int(np.ceil(np.shape(PosMax)[0]/2))
        # print('idx: ',idx )
        PosMax = PosMax[idx-1]
    else:
        PosMax = PosMax[0]
        # print('PosMax: ~~ ',PosMax )

        pass
    # print('PosMax after: ',PosMax )

    sl = ax[PosMax] - 100
    sr = ax[PosMax] + 100
    # print('sl: ', sl)
    # print('sr: ', sr)
    indSel, = np.where((ax>sl)*(ax<sr))
    ax_mod = ax[indSel]
    profile_mod = profile[indSel]
    
    HalfImax = 0.5*Imax

    Iover = np.where(profile_mod >= HalfImax,profile_mod, 0)

    indNonNull, = np.where(Iover != 0)
    # print('indNonNull: ', indNonNull)
    # print('len(indNonNull): ', len(indNonNull))
    
    if indNonNull.shape[0] != 0:
        width_um = ax_mod[indNonNull[-1]]-ax_mod[indNonNull[0]]
        width_um_inv = ax_mod[indNonNull[-1]-1]-ax_mod[indNonNull[0]+1]
        width_mean = 0.5*(width_um + width_um_inv)  
    else:
        return None

    if plots == True:

        w = np.ones_like(Iover[indNonNull])
        w2 = np.ones_like(np.concatenate(([indNonNull[0]-1],indNonNull, [indNonNull[0]+1])))

        width_arr = HalfImax*w
        width_arr2 = HalfImax*w2

        font = {'family': 'sans-serif','color': 'k', 'weight': 'normal','size': 20}

        fig1_Prof = plt.figure(figsize=[15,10])
        ax1_Prof = fig1_Prof.add_subplot(2,2,1)
        ax1_Prof.plot(ax_mod,profile_mod,'.-')

        ax1_Prof.plot(ax_mod[indNonNull[0]-1],width_arr2[0],'g+')
        ax1_Prof.plot(ax_mod[indNonNull[-1]+1],width_arr2[-1],'g+')

        # print('indNonNull: ', indNonNull)
        # print('len(ax_mod[indNonNull[0]-1:indNonNull[-1]+2]): ', len(ax_mod[indNonNull[0]-1:indNonNull[-1]+2]))
        # print('indNonNull[0]-1: ', indNonNull[0]-1)
        # print('indNonNull[-1]+2: ', indNonNull[-1]+2)
        # print('len(width_arr2): ', len(width_arr2))

        # line1, = ax1_Prof.plot(ax_mod[indNonNull[0]-1:indNonNull[-1]+2],width_arr2,'g.', label = 'Overflow width')
        line1, = ax1_Prof.plot(width_arr2,'g.', label = 'Overflow width')

        ax1_Prof.plot(ax_mod[indNonNull][0],width_arr[0],'r+')
        ax1_Prof.plot(ax_mod[indNonNull][-1],width_arr[-1],'r+')
        line2, = ax1_Prof.plot(ax_mod[indNonNull],width_arr,'r', label = 'Default width')

        ax1_Prof.set_xlabel(xlabel = r'x ($\mu m$)', fontdict=font)
        ax1_Prof.set_ylabel(ylabel = 'Intensity (A. U.)', fontdict=font)

        ax1_Prof.xaxis.set_tick_params(labelsize=13)
        ax1_Prof.yaxis.set_tick_params(labelsize=13)

        ax1_Prof.legend(handles=[line1, line2])
        ax1_Prof.grid(True)
    return width_mean

def FWHMGaussian_FromImage(profile, ax, field = None, plots = False, fit = False):
    """
    Return a beamwidth from profiles got from Image. For the calculus used FWHF. 

        Parameters:
            profile (np.ndarray): profile intensity obtained from photo.
            ax (np.ndarray): correponding axis profile.
            field (diffractio.scalar_fields_XY.Scalar_field_XY or None): Scalar fields XY object of Diffractio.
            plots (bool): If True, show plots.
            fit (np.ndarray): profile intensity obtained from gaussian fitting.
        
        Returns:
            widthFWHM (float): beamwidth in um.
    
    """
    # Voy a probar haciendo un ajuste gaussiano

    # Defino la función gaussiana

    def GaussianFit(z, a, b, c):
        return a*np.exp(-(z-b)**2/(2*c**2))
    
    fitparam, _ = curve_fit(f = GaussianFit, xdata = ax, ydata = profile, p0 = (1,0,100))
    _, mu, sigma = fitparam

    widthFWHM = 2*np.abs(sigma)*np.sqrt(2*np.log(2))

    if plots:

        fig1 = plt.figure(figsize = (15,5))
        ax1 = fig1.add_subplot(1,2,1)

        im1 = ax1.plot(ax,profile)
        ax1.grid(True)
        ax1.set_title('Profile', fontdict = {'fontweight': 'bold'})
            
        ax2 = fig1.add_subplot(1,2,2)

        im2 = ax2.plot(ax,profile,alpha=0.7)
        im3 = ax2.plot(ax,GaussianFit(ax,*fitparam),'--r')
        im4 = ax2.hlines(y = np.max(GaussianFit(ax,*fitparam))/2, xmin = mu-widthFWHM/2, xmax = mu+widthFWHM/2)
        ax2.grid(True)
        ax2.set_title('Gaussian Fit. FWHM width = {0:.2f} um'.format(widthFWHM), fontdict = {'fontweight': 'bold'})


        if field is not None:
        
            fig3 = plt.figure()
            ax3 = fig3.add_subplot()
            im3 = ax3.imshow(X = np.abs(field.u))
    if fit == True:
        
        return widthFWHM, GaussianFit(ax,*fitparam)
    
    else:

        return widthFWHM

def PreProcessingProfile(profile, ax, plots = True):
    """
    Return a modified profile. This modifications is based on decreasing the predominant values of the profile that doesn`t belong to the focus.

        Parameters:
            profile (np.ndarray): original profile intensity.
            ax (np.ndarray): correponding axis profile.
            plots (bool): If True, show plots.
        
        Returns:
            widthFWHM (float): profile_mod
    
    """
    w = savgol_filter(profile, 101, 2)
    
    if plots:

        fig1 = plt.figure(figsize = (15,5))
        ax1_1 = fig1.add_subplot(1,2,1)
        ax1_1.plot(ax, profile, 'b')
        ax1_1.set_xlabel('axis um})')
        ax1_1.set_ylabel('I ($\mb{A. U.})')
        
        ax1_2 = fig1.add_subplot(1,2,2)
        ax1_2.plot(ax, w, 'b')
        ax1_2.set_xlabel('axis um)')
        ax1_2.set_ylabel('I ($\mb{A. U.})')
    
    peaks,_= find_peaks(x = w, rel_height=0.5)
    inv_peaks,_= find_peaks(x = -w, rel_height=0.5)

    if plots:
        
        fig2 = plt.figure(figsize = (12,5))
        ax2_1 = fig2.add_subplot()
        ax2_1.plot(ax[inv_peaks], w[inv_peaks], '.--r')
        ax2_1.plot(ax[peaks], w[peaks], '.--m')
        ax2_1.plot(ax, w, 'b', alpha = 0.5)

    ind_I_w_max, = np.where(np.max(w[peaks]) == w)
    ind_I_w_max_izq = inv_peaks[(inv_peaks<ind_I_w_max)][-1]
    ind_I_w_max_dch = inv_peaks[(inv_peaks>ind_I_w_max)][0]
    
    profile_mod = deepcopy(profile)

    profile_mod[ax<ax[ind_I_w_max_izq]] = profile[ax<ax[ind_I_w_max_izq]]/8
    profile_mod[ax>ax[ind_I_w_max_dch]] = profile[ax>ax[ind_I_w_max_dch]]/8

    return profile_mod
            
            
def ProfilesAnalisisFromImages(Images, prints = False, camNameOrPixelSize = "uEye_SLM"):
    """
    Return intensity profiles and widths of the beam's images. 

        Parameters:
            Images (Dict): intensity profiles obtained from photo.
            prints (bool): show prints.
            camNameOrPixelSize (py_lab.camera.Camera object or tuple): necessary data for imaging processing.
        
        Returns:
            Profiles_x (Dict): intensity profiles in x-axis.
            Profiles_y (Dict): intensity profiles in y-axis.
            Widths_x (Dict): widths in x-axis.
            Widths_y (Dict): widths in y-axis.
    """
    if isinstance(camNameOrPixelSize, str):
        cam = Camera(name = camNameOrPixelSize)

        cam_resolution = cam.resolution
        cam_pixel_size = cam.pixel_size*mm
    else: 
        cam_resolution = Images[0].shape
        cam_pixel_size = np.array(camNameOrPixelSize)

    XYplane = cam_resolution*cam_pixel_size
    x_ax = np.arange(-XYplane[0]/2,XYplane[0]/2,cam_pixel_size[0])
    y_ax = np.arange(-XYplane[1]/2,XYplane[1]/2,cam_pixel_size[1])

    # Obtención de los máximos de intensidad en cada uno de los planos XY que recorren el eje z (images)
    
    indMax = []
    indRow  = []
    indCol = []

    for ph in Images:
        indMax += [np.where(ph==np.max(ph))]
        indRow += [indMax[-1][0]]
        indCol += [indMax[-1][1]]

        if prints:
            print(indMax[-1])

    Widths_x = {}
    Profiles_x = {}
    Widths_y = {}
    Profiles_y = {}

    axx = x_ax
    axy = y_ax

    # Extracción de los perfiles de intensidad y anchuras en el eje x

    for n,i in enumerate(indRow):
    
        # print(n)
        Widths_x['Image {}'.format(n+1)] = []
        Profiles_x['Image {}'.format(n+1)] = []
        # print(len(i))
        for j in i:
            # print(j)
            profile = Images[n][j,:]
            Widths_x['Image {}'.format(n+1)] += [FWHM_FromImage(profile,axx, plots=False)]
            Profiles_x['Image {}'.format(n+1)] += [profile]

    # Extracción de los perfiles de intensidad y anchuras en el eje y

    for n,i in enumerate(indCol):
    
        # print(n)
        Widths_y['Image {}'.format(n+1)] = []
        Profiles_y['Image {}'.format(n+1)] = []
        # print(len(i))
        for j in i:
            # print(j)
            profile = Images[n][:,j]
            Widths_y['Image {}'.format(n+1)] += [FWHM_FromImage(profile,axy, plots=False)]
            Profiles_y['Image {}'.format(n+1)] += [profile]
    
    return Profiles_x, Profiles_y, Widths_x, Widths_y

def PlotProfile(Dictprofile, ax, camNameOrPixelSize = "uEye_SLM"):

    """
    Return a plot of a beam's intensity profile. The direction 'x' or 'y' is selected for the user. 

        Parameters:
            Dictprofile (np.ndarray): profile intensity obtained from photo.
            ax (str): 'x' or 'y'. Correponding axis profile.
            camNameOrPixelSize (py_lab.camera.Camera object): necessary data for imaging processing.
        
        Returns:
            Plots of profile.

        Example Code:
            u = MaskDiffractio.CZT(z=f0)
            widthx, widthy = BeamWidth(u, plots=True)
    """
    if isinstance(camNameOrPixelSize,'str'):
        cam = Camera(name = camNameOrPixelSize)
    
        if ax == 'x':
            camAx = cam.x
        elif ax == 'y':
            camAx = cam.y
    
    else:

        if ax == 'x':
            camAx = camNameOrPixelSize[0]
        elif ax == 'y':
            camAx = camNameOrPixelSize[1]
    
    for t,i in zip(Dictprofile.keys(),Dictprofile.values()): 
    
        nfig = len(i)
        fig = plt.figure(figsize=(20,10))
    
        if nfig > 1:

            for pfig,j in enumerate(i):
    
                if (nfig%2 == 0):
                    colfig = nfig//2
                elif (nfig%2 == 1): 
                    colfig = nfig//2+1

                axis = fig.add_subplot(2,colfig,pfig+1)
                axis.set_title(t+'. Profile {}'.format(pfig+1))
                axis.plot(camAx,j)
                axis.set_xlabel(ax+' ($\mu m$)')

        
        if nfig == 1:

            axis = fig.add_subplot()
            axis.set_title(t+'. Profile 1')
            axis.plot(camAx,i[0])
            axis.set_xlabel(ax+' ($\mu m$)')

def DOF_experimental(zPos, photo, cam, f0 = 100*mm, wavelength = 0.6328*um,  plots = False):

    """
    Return a DOF of mask and plots (optional). 

        Parameters:
            zPos (np.ndarray): position over z axis in mm. 
            photo (np.ndarray): photos of XYplane along z axis.
            plots (bool): if true, plots are showed.
        
        Returns:
            DOF calculated with FWHM criterion.
            f0_estimated

    """

    Imax_Z = []

    for i in photo:
        I = ImaxCentreIm(i, wavelength, f0, cam , plots= False)
        Imax_Z += [I]
    
    Imax_Z = np.array(Imax_Z, dtype="object")

    f = interpolate.interp1d(zPos, Imax_Z)

    zPos_interp = np.arange(zPos[0],zPos[-1], 0.1)
    Imax_Z_interp = f(zPos_interp)

    HalfImax_Z = (np.max(Imax_Z_interp)-np.min(Imax_Z_interp))*0.5
    # print('HalfImax_Z: ', HalfImax_Z)

    peaks, _ = find_peaks(Imax_Z_interp, height=HalfImax_Z+np.min(Imax_Z_interp))
    peak_widths_z = peak_widths(Imax_Z_interp,peaks)
    z_step = zPos_interp[1]-zPos_interp[0]

    # print('peaks: ', peaks)
    # print('z_step: ', z_step)
    # print('peak_widths_z: ', peak_widths_z)

    ind_Imax = np.where(peak_widths_z[0] == np.max(peak_widths_z[0]))

    pIzq = peak_widths_z[2][ind_Imax]*z_step+zPos_interp[0]
    pDch = peak_widths_z[3][ind_Imax]*z_step+zPos_interp[0]
    f0_estimated_arr = np.abs((pDch+pIzq)/2)
    f0_estimated = f0_estimated_arr[0]

    # print('pIzq: ', pIzq)
    # print('pDch: ', pDch)

    ind_Imax = np.where(peak_widths_z[0] == np.max(peak_widths_z[0]))

    DOF = (peak_widths_z[0][ind_Imax]*z_step)[0]

    if plots == True:

        font = {'family': 'serif','color': 'k','weight': 'normal','size': 20}
        font2 = {'family': 'serif','weight': 'normal','size': 20}
    
        fig = plt.figure(figsize=[15,5])
        ax = fig.add_subplot(1,2,1)
        ax2 = fig.add_subplot(1,2,2)

        ax.plot(zPos_interp, Imax_Z_interp, '.-', zPos,Imax_Z,'o')
        ax.plot(pDch,peak_widths_z[1][ind_Imax], 'ro')
        ax.plot(pIzq,peak_widths_z[1][ind_Imax], 'ro')
        ax.hlines(peak_widths_z[1][ind_Imax], pIzq, pDch, color="red")

        ax.set_xlabel('z (mm)',fontdict=font)
        ax.set_ylabel('Grey level',fontdict=font)
        ax.grid()

        ax2.plot(zPos_interp, Imax_Z_interp/np.max(Imax_Z_interp),'.-', zPos,Imax_Z/np.max(Imax_Z),'o')
        ax2.plot(pDch,peak_widths_z[1][ind_Imax]/np.max(Imax_Z), 'ro')
        ax2.plot(pIzq,peak_widths_z[1][ind_Imax]/np.max(Imax_Z), 'ro')
        ax2.hlines(peak_widths_z[1][ind_Imax]/np.max(Imax_Z), pIzq, pDch, color="red")

        ax2.set_xlabel('z (mm)',fontdict=font)
        ax2.set_ylabel('$I_{max}$ (arb. unit)',fontdict=font)
        ax2.grid()

        # fig.suptitle('DOF = {:.2f} mm'.format(DOF), fontdict=font)
        fig.suptitle('DOF = {:.2f} mm'.format(DOF), fontproperties=font2)

    return DOF, f0_estimated

def ImaxCentreIm(Im, lamb, f0, cam, plots= False):
    
    """
    Return maximum intensity measured in the centre of the Image.

        Parameters:
            Im (np.ndarray): image of XYplane along z axis.
            lamb (float): the wavelength of the light source.
            f0 (float): focus value for the mask.
            cam (py_lab.camera.Camera object): necessary data for imaging processing, or dimensions of mask.
            plots (bool): if true, plots are showed.
        
        Returns:
            Imax measured.
    """

    #rx, ry = cam.resolution
    if isinstance(cam,tuple):
        px, py = cam
    else:
        px, py = cam.pixel_size*1e3 # tamaño pixel en um

    if plots == True:
        fig = plt.figure()
        ax = fig.add_subplot()
        ax.imshow(Im)
        ax.set_axis_off()

    indx, indy = np.where(np.max(Im) == Im)
    
    RCenReg = np.sqrt(lamb*f0)*um 

    # print(indx)

    x1 = int(indx[0]-RCenReg//px)
    x2 = int(indx[0]+RCenReg//px)
    y1 = int(indy[0]-RCenReg//py)
    y2 = int(indy[0]+RCenReg//py)

    if plots == True:
        fig2 = plt.figure()
        ax2 = fig2.add_subplot()
        ax2.imshow(Im[x1:x2, y1:y2])
        ax2.set_axis_off()
        ax2.set_title('ZOOM')

    if np.shape(Im[x1:x2, y1:y2])[0] == 0:
        
        Imax = np.array([np.max(Im)])
    else:
        Imax = np.max(Im[x1:x2, y1:y2])
    
    return Imax

def PropImageExp(PSmask, camObj, wavelength, f0OrRang, ZPlane, deltaZ = 1*mm, xlim = (-800,900), ylim = (-900,800), plots = False):
   
    """
    Return maximum intensity measured in the centre of the Image.

        Parameters:
            PSmask (np.ndarray): photo of mask that will be propagated.
            camObj (py_lab.camera.Camera object): necessary data for imaging processing.
            wavelength (float): the wavelength of the light source.
            f0OrRang (float or tuple): focus value for the mask or interval [a,b].
            ZPlane (int): number of Z-planes.
            deltaZ (float): only used if f0OrRang is not tuple or np.ndarray
            xlim (tuple): limit in x axis for the ZOOM.
            ylim (tuple): limit in x axis for the ZOOM.
            plots (bool): if true, plots are showed.
        
        Returns:
                uPS1_list (list): fields calculated for Z-planes.
                z (np.ndarray): axis z.
    
    """

    pxxCam = camObj.x
    pxyCam = camObj.y

    PS = Scalar_mask_XY(x = pxxCam, y = pxyCam, wavelength = wavelength)
    PS.u =  PSmask

    if isinstance(f0OrRang, tuple):
        z = np.linspace(f0OrRang[0],f0OrRang[1], ZPlane)
    elif isinstance(f0OrRang, np.ndarray):
        z = f0OrRang
    else:
        z = np.linspace(f0OrRang-ZPlane//2*deltaZ,f0OrRang+ZPlane//2*deltaZ, ZPlane)

    uPS_list = []

    for i in z:

        uPS_list += [PS.CZT(z = float(i))]

    if plots == True:
        fig = plt.figure()
        ax = fig.add_subplot(1,2,1)
        ax2 = fig.add_subplot(1,2,2)

        p1 = ax.imshow(PS.u,  extent=[PS.x[0],PS.x[-1],PS.y[0],PS.y[-1]])
        fig.colorbar(p1, location = 'bottom', orientation = 'horizontal', ax = ax)

        p2 = ax2.imshow(PS.u)
        fig.colorbar(p2, location = 'bottom', orientation = 'horizontal', ax = ax2)
        
        ax.set_xlim((-800,900))
        ax.set_ylim((-900,800))

        for n,i in enumerate(uPS_list):
            i.draw()
            plt.xlim((-800,900))
            plt.ylim((-900,800))
            plt.title(r'z={0:.2f} $mm$'.format(z[n]*1e-3))

    return uPS_list, z

def propXZ(mask, zprop, plots = False, DeltaTicksz = 2*mm, DeltaTicksx = 50*um):
    
    """
    Return maximum intensity measured in the centre of the Image.

        Parameters:
            PSmask (diffractio.scalar_masks_XY.Scalar_mask_XY object): photon sieve mask.
            zprop (np.ndarray): z axis in um.
            plots (bool): if true, plots are showed.
            DeltaTicksz (float): step in z axis in um.
            DeltaTicksx (float): step in x axis un um.
        
        Returns:
                IXZ (np.ndarray): Irradiance along z axis for y = 0 (2D). 
                uXZ (np.ndarray): Field along z axis for y = 0 (2D).
                IZ (np.ndarray): Irradiance along z axis for y = 0, middle x (1D).
                uZ (np.ndarray): Field along z axis for y = 0, middle x (1D).
    
    """

    u = mask.CZT(z = zprop)
    nx, ny, nz = u.u.shape

    uXZ = u.u[:, ny//2, :]
    IXZ = np.abs(uXZ*np.conjugate(uXZ))

    uZ = uXZ[nx//2,:]
    IZ = IXZ[nx//2,:]

    if plots:

        fig1 = plt.figure() 
        ax1 = fig1.add_subplot()
        
        ax1.imshow(IXZ, origin='lower')
        ax1.set_ylim(ny//2-50, ny//2+50)
        ax1.set_title('$CZT: \ \lambda = {:.3f}$'.format(mask.wavelength))
        labelz = ['{:.2f}'.format(i) for i in np.arange(np.min(zprop/1000),np.max(zprop/1000),DeltaTicksz/1000)]
        # print(nx//2-50)
        # print(nx//2+50)
        labelx = ['{:.2f}'.format(i) for i in np.arange(u.x[nx//2-50],u.x[nx//2+50],DeltaTicksx)]
        # print(labelx)
        # print(labelz)
        # plt.ylim(u.x[nx]//2-50, u.x[nx]//2+50)
        ax1.set_xticks(ticks=np.linspace(0,nz-1,len(labelz)), labels = labelz)
        ax1.set_yticks(ticks=np.linspace(nx//2-50,nx//2+50,len(labelx)), labels = labelx)
        ax1.set_xlabel(xlabel = '$z (mm)$')
        ax1.set_ylabel(ylabel = '$x (\mu m)$')

        fig2 = plt.figure() 
        ax2 = fig2.add_subplot()

        ax2.plot(zprop/1000, IZ/np.max(IZ),'.-')
        ax2.set_xlabel('$z \ (mm)$')
        ax2.set_ylabel('$I \ (A.U.)$')
        indx, = np.where(IZ/np.max(IZ) == np.max(IZ/np.max(IZ)))
        # print(indx)
        # print(zprop[indx])
        ax2.axvline(x = zprop[indx]/1000,color='magenta', linestyle='--')
        # ax.axvline(x=np.sqrt(3*f0*wavelength)*len(x[x>0])/np.max(x[x>0]), color='magenta', linestyle='--', label = r'\textbf{FZ3 boundary}')

    return IXZ, uXZ, IZ, uZ

# ANÁLISIS FOCO

def PreProcessingProfile2D(images, linf = 50, lsup = 55):
    """
    Clean the image background of the propagation plane z.

    inputs: 
    - images (numpy.ndarray): image of the propagation plane z.
    - linf (float/int): lower irrandiance cut-off value.
    - lsup (float/int): upper irrandiance cut-off value.

    outputs:
    - photoExt_mod (photoExt_mod): final imagen
        
    """
    photoExt_mod = []

    Nz, _, _ = images.shape

    if 2*(Nz//2) < Nz:
        threshold_I = np.concatenate([np.linspace(linf,lsup,Nz//2),np.linspace(lsup,linf,Nz//2+1)])
    else:
        threshold_I = np.concatenate([np.linspace(linf,lsup,Nz//2),np.linspace(lsup,linf,Nz//2)])

    for n, image in enumerate(images):
        photoExt_mod += [(image>threshold_I[n])*image]


    return photoExt_mod

def widthsGaussian2D(image, x, y, p0 = (1,0,0,100,100), plots = False):
    """
    Return the parameters and errors of the 2D Gaussian fit.

    inputs: 
    - image (numpy.ndarray): data to fit.
    - x (numpy.ndarray): x-axis.
    - y (numpy.ndarray): y-axis.
    - p0 (array_like): initial suggestion. Default p0 = (1,0,0,100,100).
    - plots (bool): if True, show plots.

    outputs: 
    - popt (numpy.ndarray): fitted parameters.
    - perr (numpy.ndarray): standard deviation errors on the parameters

    """
    def Gaussian2D(X, A, x0, y0, sigmax, sigmay):
        x, y = X
        return A*np.exp(-((x-x0)**2/(2*sigmax**2)+(y-y0)**2/(2*sigmay**2)))

    X, Y = np.meshgrid(x, y)
    Z = image # np.invert(mask)*photoExt[n]

    # Aplanar las matrices para curve_fit
    X_data = np.vstack([X.ravel(), Y.ravel()])
    Z_data = Z.ravel()

    # Ajustar el modelo a los datos
    popt, pcov = curve_fit(Gaussian2D, X_data, Z_data, p0 = p0)


    # Obtener los parámetros ajustados
    # A, x0, y0, sigmax, sigmay = popt

    # incertidumbres de los parámetros ajustados
    perr = np.sqrt(np.diag(pcov))
    # delta_A, delta_x0, delta_y0, delta_sigmax, delta_sigmay = perr

    # Crear una superficie ajustada usando los parámetros óptimos
    Z_fit = Gaussian2D((X, Y), *popt)

    if plots:
        
        # Visualizar los datos y el ajuste
        fig = plt.figure(figsize=(12, 6))

        # Mostrar los datos originales
        ax1 = fig.add_subplot(121, projection='3d')
        ax1.plot_surface(X, Y, Z, cmap='turbo', edgecolor='none')
        ax1.set_title('Datos Originales')

        # Mostrar la superficie ajustada
        ax2 = fig.add_subplot(122, projection='3d')
        ax2.plot_surface(X, Y, Z, cmap='viridis', edgecolor='none')
        ax2.plot_surface(X, Y, Z_fit, cmap='turbo', edgecolor='none', alpha =0.7)
        ax2.set_title('Superficie Ajustada')

        plt.show()
    
    return popt, perr

def CalcAngElipse(x, y):

    h0, k0 = np.mean(x), np.mean(y)
    a0 = (np.max(x) - np.min(x)) / 2
    b0 = (np.max(y) - np.min(y)) / 2
    params_iniciales = [h0, k0, a0, b0]
    
    def error(params):
        h, k, a, b = params
        return np.sum(((x - h)**2 / a**2 + (y - k)**2 / b**2 - 1)**2)
    
    resultado = minimize(error, params_iniciales)
    _, _, a, b = resultado.x
    
    # Calcular el ángulo de la elipse
    angulo = np.arctan2(b, a)  # Ángulo de los ejes principales
    return np.degrees(angulo)

def SelectedPointsPhoto(imagen, umbral_percentil=90):

    umbral = np.percentile(imagen, umbral_percentil)  # Puntos más brillantes
    y, x = np.where(imagen > umbral)  # Coordenadas de puntos relevantes
    return x, y

def AngRotBeamDetect(imagenes):

    todos_x, todos_y = [], []

    # Extraer puntos relevantes de todas las imágenes
    for im in imagenes:
        # imagen = io.imread(ruta, as_gray=True)  # Cargar imagen en escala de grises
        x, y = SelectedPointsPhoto(im)
        todos_x.extend(x)
        todos_y.extend(y)
    
    # Convertir a arrays
    todos_x = np.array(todos_x)
    todos_y = np.array(todos_y)
    
    # Calcular el ángulo único de la elipse ajustada a todos los puntos
    ang = CalcAngElipse(todos_x, todos_y)
    
    return ang
            
# PS SECTORIZED

def ParamPalotes(D, wavelength, f0, p0):

    """
    Return parameters for the future generation of PS Sectorized. 

        Inputs:
            D (float): PS diameter.
            wavelength (np.ndarray): PS wavelength.
            f0 (float): PS focal 
            p0 (float,int): the number of initial subdivision. 
        
        Returns:
            xc_arr (np.ndarray): x coordinate of the center of each rectangle/palote.
            yc_arr (np.ndarray): y coordinate of the center of each rectangle/palote.
            l_arr (np.ndarray): length of each rectangle/palote.
            thetaT_arr (np.ndarray): angles between rectangles/palotes.

        Example Code:

            D = 4*mm
            lamb = 0.6328*um
            f0 = 125*mm
            p0 = 20

            xc, yc, l, theta = PSFC.ParamPalotes(D, lamb, f0, p0)
    
    """

    rmax = D/2
    r1 = np.sqrt(wavelength*f0)

    M = int(np.ceil(np.log10(rmax/r1)/np.log10(2)-1))

    p = [int(p0)]
    rho = [r1]
    theta = [2*np.pi/p0]

    for k in range(1,M+1):
        p += [int(2**k*p0)]
        theta += [2*np.pi/p[-1]]
        # rho += [2**(k+1)*r1]
        rho += [2**k*r1]

    xc = []
    yc = []
    l = []

    print('p: ',p)
    # print('theta: ',np.array(theta)*180/np.pi)
    # print('rmax: ',rmax)
    print('rho: ',rho)

    thetaT = []

    for n,i in enumerate(p):
        
        for j in range(i):
            
            rc = (rmax+rho[n])/2
            # print('rc: ',rc)
            
            thetaT += [j*theta[n]] 
            xc += [rc*np.cos(thetaT[-1])]
            yc += [rc*np.sin(thetaT[-1])]
            l += [rmax-rho[n]]
    
    xc_arr = np.array(xc)
    yc_arr = np.array(yc)
    l_arr = np.array(l)
    thetaT_arr = np.array(thetaT)


    return xc_arr, yc_arr, l_arr, thetaT_arr

def FresnelSectorized(slm_obj, xc, yc, l, theta, thickness, D, f0 = 100*mm, wavelength = 0.6328 * um, plots = False, npx = 512, npy = 512):
    
    """
    Return masks of PS sectorized and only rectangles/palotes. 

        Inputs:
            slm_obj (py_lab.slm.SLM object): slm data
            xc (np.ndarray): x coordinate of the center of each rectangle/palote.
            yc (np.ndarray): y coordinate of the center of each rectangle/palote.
            l (np.ndarray): length of each rectangle/palote.
            theta (np.ndarray): angles between rectangles/palotes.
            thickness (float): width of the rectangles/palotes.
            D (float): PS diameter.
            f0 (float): PS focal. 
            wavelength (float): PS wavelength.
            plots (bool): draw figures.
        
        Returns:
            tAux (diffractio.scalar_masks_XY.Scalar_mask_XY): PS sectorized mask.
            OnlyPalotes (diffractio.scalar_masks_XY.Scalar_mask_XY): rectangles/palotes mask.

        Example Code:

            D = 4*mm
            lamb = 0.6328*um
            f0 = 125*mm
            p0 = 20

            xc, yc, l, theta = PSFC.ParamPalotes(D, lamb, f0, p0)
            
            slm = SLM(name='HoloEyePluto')
            thickness = 16*um

            PS, OnlyPalotes = PSFC.FresnelSectorized(slm, xc, yc, l, theta, thickness, D, f0, lamb, plots = True)
    
    """

    rmax = D/2
    
    if isinstance(slm_obj,tuple):
        x = np.linspace(-slm_obj[0],slm_obj[0],npx)
        y = np.linspace(-slm_obj[1],slm_obj[1],npy)
    else:
        x = slm_obj.x
        y = slm_obj.y

    t1 = Scalar_mask_XY(x, y, wavelength)
    tAux = deepcopy(t1)
    tAux.inverse_amplitude()

    for n in range(len(xc)):

        t1.square(r0=(xc[n], yc[n]), size=(l[n], thickness), angle=theta[n])
        t1.inverse_amplitude()
        tAux.u = tAux.u * t1.u

    Fresnel = Scalar_mask_XY(x, y, wavelength)
    Fresnel.fresnel_lens(r0 = (0,0), focal = f0, kind='amplitude', radius = rmax)

    Fresnel.draw()

    OnlyPalotes = deepcopy(tAux)
    tAux.u = tAux.u * Fresnel.u
    tAux.u = np.abs(tAux.u)

    if plots == True:

        fig3 = plt.figure(figsize=(10,10))
        ax3 = fig3.add_subplot()

        xmax = max(tAux.x)
        ymax = max(tAux.y)
        xmin = min(tAux.x)
        ymin = min(tAux.y)

        cmap = mpl.cm.gray
        norm = mpl.colors.Normalize(vmin=0, vmax=1)

        ax3.imshow(np.abs(tAux.u),cmap=cmap, origin='lower', extent=([xmin,xmax,ymin,ymax]), norm = norm)
        ax3.set_title('PS Sectorized')

    return tAux, OnlyPalotes

def InveMask(mask, D, OnlyPalotes = None ,plots = False):

    """
    Return inverted mask.

        Inputs:
            mask (diffractio.scalar_masks_XY.Scalar_mask_XY): PS mask to be inverted.
            D (float): PS diameter.
            OnlyPalotes (diffractio.scalar_masks_XY.Scalar_mask_XY): rectangles/palotes mask.
            plots (bool): draw figures. 
        
        Returns:
            m1 (diffractio.scalar_masks_XY.Scalar_mask_XY): inverted mask.

        Example Code:

            D = 4*mm

            m1 = PSFC.InveMask(mask, D, OnlyPalotes ,plots = False)
    
    """

    m1 = deepcopy(mask)
    m2 = deepcopy(mask)

    m2.circle(r0=(0,0), radius = D/2)
    m1.inverse_amplitude()
    if isinstance(OnlyPalotes,type(mask)):
        m1.u = m1.u * m2.u * OnlyPalotes.u
    else:
        m1.u = m1.u * m2.u
        
    m1.u = np.abs(m1.u)

    if plots == True:

        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot()

        xmax = max(m1.x)
        ymax = max(m1.y)
        xmin = min(m1.x)
        ymin = min(m1.y)

        cmap = mpl.cm.gray
        norm = mpl.colors.Normalize(vmin=0, vmax=1)

        ax.imshow(m1.u,cmap = cmap, origin='lower', extent=([xmin,xmax,ymin,ymax]), norm = norm)
        ax.set_title('PS Sectorized inverse')
    
    return m1

# ÚTILES PARA GIF

def fig2img(fig):
    """Convert a Matplotlib figure to a PIL Image and return it"""
    buf = io.BytesIO()
    fig.savefig(buf, bbox_inches='tight', pad_inches = 0)
    buf.seek(0)
    img = Image.open(buf)
    return img