# FMinPSFunctions.py

import matplotlib.pyplot as plt
from IPython.display import Image
from scipy import optimize
from itertools import permutations

import numpy as np
from copy import copy
import datetime
import os
from tqdm import trange # progress bar

from py_aocg.photon_sieve import PSFunctionsComun

from py_aocg.photon_sieve.PSFunctionsComun import IntensityRegion_v2
from diffractio import degrees, mm, plt, sp, um, nm
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_math import fft_convolution2d
from diffractio.utils_drawing import draw_several_fields



print('FMinPSFunctions imported')

def MakingHoles_v2(AntMask, x, plots = False, prints = False):
    """Return a mask of photon sieve.

        Parameters:
            AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY. Mask where the hole will be made.
            x: np.array([r, alpha, R]).
            plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.

        Returns:
            HolesMask: diffractio.scalar_masks_XY.Scalar_mask_XY. The mask after making holes.

        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """

    r = x[0]
    alpha = x[1]
    R_opt = x[2]

    # Obtengo coordenadas cartesianes
    x_coord = r*np.cos(alpha)
    y_coord = r*np.sin(alpha)

    # Creamos nueva máscara que solaparemos a la máscara anterior
    HolesMask = Scalar_mask_XY(AntMask.x,AntMask.y,AntMask.wavelength)

    # Añadimos un agujero en la posición deseada
    HolesMask.circle(r0 = (x_coord,y_coord), radius = R_opt)

    if plots == True and prints == True:
        print('x_coord: ',x_coord)

    if plots == True:
        AntMask.draw(title="AntMask",has_colorbar='vertical')
        HolesMask.draw(title="HolesMask",has_colorbar='vertical')

    HolesMask.u = AntMask.u + HolesMask.u

    if plots == True:
        HolesMask.draw(title="HolesMask tras solapamiento",has_colorbar='vertical')

    return HolesMask


def widenMod_v2(Mask, SpreadRadius, new_field = True, binarize = True, plots = False):
        """
        Tiny modify of method widen include in Diffractio. Widens a mask using a convolution of a certain radius.

        Parameters:
            Mask: diffractio.scalar_masks_XY.Scalar_mask_XY. Mask is widen an amount SpreadRadius.
            SpreadRadius (float): radius of convolution
            new_field (bool): returns a new XY field
            binarize (bool): binarizes result.
            plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.

        Returns:
            filt:


        """

        filt = Scalar_mask_XY(Mask.x, Mask.y, Mask.wavelength)
        filt.circle(r0=(0 * um, 0 * um),
                      radius=(SpreadRadius, SpreadRadius),
                      angle=0 * degrees)

        image = np.abs(Mask.u)
        filtrado = np.abs(filt.u) / np.abs(filt.u.sum())

        covolved_image = fft_convolution2d(image, filtrado)
        average = covolved_image.max()*0.00005 #0.005%... así probando #0.01 # 1% del máximo de la convolución (sugerencia de Luismi) Lo he hecho más restrictivo, 0.5% del máximo.

        if binarize is True:
            covolved_image[covolved_image > average] = 1
            covolved_image[covolved_image <= average] = 0

        if new_field is True:
            filt.u = covolved_image
            if plots == True:
                Mask.draw(title = 'Before Widen')
                filt.draw(title = 'After Widen')
            return filt
        else:
            MaskBefore = copy(Mask)
            Mask.u = covolved_image
            if plots == True:
                MaskBefore.draw(title = 'Before Widen')
                Mask.draw(title = 'After Widen')

def RestricVerify_v2(x, AntMask, CoordHoles, RHoles, g, File):
    """
        Verify if the mask cumply with the restriction impost.

        Parameters:
            x: angular coordinates and size of new hole: np.array([r, alpha, R]).
            AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY. Mask is widen an amount SpreadRadius.
            CoordHoles (float): cartesian coordinates of the holes made previously done.
            RHoles (bool): radius of the holes made previously done.
            g: safety belt of holes.
            File: indicate the file that is being written.

        Returns:
            verification (bool): True or false if the mask made is checked.

        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """
    # A veces Fmin prueba con valores negativos de R_opt... ESO NO PODEMOS PERMITIRLO, así que...

    #print('FileLogs object?: ', type(File))
    NumberOfHoles = len(RHoles)

    xmax = np.max(AntMask.x)
    ymax = np.max(AntMask.y)
    xmin = np.min(AntMask.x)
    ymin = np.min(AntMask.y)

    rNewHole = x[0]
    alphaNewHole = x[1]
    RNewHole = x[2]

    # A veces Fmin prueba con valores negativos de R_opt... ESO NO PODEMOS PERMITIRLO, así que...

    if RNewHole < 0:
        
        File.write('RNewHole is negative! \n')
        verification = False
        return verification

    else:

        xcNHf = rNewHole*np.cos(alphaNewHole)
        ycNHf = rNewHole*np.sin(alphaNewHole)

        # Verificación de que el agujero no se sale de la máscara

        verificationLimitMask = True

        if abs(xcNHf) + RNewHole > xmax - g:

            srtAlertVerLimX = 'verificationLimitMask = False -> abs(xcNHf) + RNewHole > xmax - g: {} >= {}'. format(abs(xcNHf) + RNewHole, xmax - g)
            File.write(srtAlertVerLimX+'\n')
            #print(srtAlertVerLimX)
            verificationLimitMask = False

        elif abs(ycNHf) + RNewHole > ymax - g:

            srtAlertVerLimY = 'verificationLimitMask = False -> abs(ycNHf) + RNewHole > ymax - g: {} >= {}'. format(abs(ycNHf) + RNewHole, ymax - g)
            File.write(srtAlertVerLimY+'\n')
            #print(srtAlertVerLimY)
            verificationLimitMask = False

        # Verificación de que el agujero no solapa con el resto

        verificationOverlap = True

        for NH in range(NumberOfHoles):

                d_centers = np.sqrt((xcNHf-CoordHoles[NH,0])**2+(ycNHf-CoordHoles[NH,1])**2)

                if d_centers < RNewHole+RHoles[NH]+g :

                    strAlertVerOverl = 'verificationOverlap = False -> RNewHole+RHoles[{}]+g > d_centers: {} > {}'. format(NH,RNewHole+RHoles[NH]+g, d_centers)
                    File.write(strAlertVerOverl+'\n')
                    #print(strAlertVerOverl)
                    verificationOverlap = False
                    break

                else:

                    continue

        verification = verificationLimitMask and verificationOverlap

        return verification

def RestricVerify_v3(x, AntMask, CoordHoles, RHoles, g, Rmin, File):
    """
        Verify if the mask cumply with the restriction impost.

        Parameters:
            x: angular coordinates and size of new hole: np.array([r, alpha, R]).
            AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY. Mask is widen an amount SpreadRadius.
            CoordHoles (float): cartesian coordinates of the holes made previously done.
            RHoles (bool): radius of the holes made previously done.
            g: safety belt of holes.
            File: indicate the file that is being written.

        Returns:
            verification (bool): True or false if the mask made is checked.

        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """
    # A veces Fmin prueba con valores negativos de R_opt... ESO NO PODEMOS PERMITIRLO, así que...

    #print('FileLogs object?: ', type(File))
    NumberOfHoles = len(RHoles)

    xmax = np.max(AntMask.x)
    ymax = np.max(AntMask.y)

    rNewHole = x[0]
    alphaNewHole = x[1]
    RNewHole = x[2]

    # A veces Fmin prueba con valores negativos de R_opt... ESO NO PODEMOS PERMITIRLO, así que...

    if (RNewHole < Rmin) or (rNewHole < RHoles[0]):
        
        File.write('RNewHole is not valid! \n')
        verification = False
        return verification

    else:

        xcNHf = rNewHole*np.cos(alphaNewHole)
        ycNHf = rNewHole*np.sin(alphaNewHole)

        # Verificación de que el agujero no se sale de la máscara

        verificationLimitMask = True

        if abs(xcNHf) + RNewHole > xmax - g:

            srtAlertVerLimX = 'verificationLimitMask = False -> abs(xcNHf) + RNewHole > xmax - g: {} >= {}'. format(abs(xcNHf) + RNewHole, xmax - g)
            File.write(srtAlertVerLimX+'\n')
            #print(srtAlertVerLimX)
            verificationLimitMask = False

        elif abs(ycNHf) + RNewHole > ymax - g:

            srtAlertVerLimY = 'verificationLimitMask = False -> abs(ycNHf) + RNewHole > ymax - g: {} >= {}'. format(abs(ycNHf) + RNewHole, ymax - g)
            File.write(srtAlertVerLimY+'\n')
            #print(srtAlertVerLimY)
            verificationLimitMask = False

        # Verificación de que el agujero no solapa con el resto

        verificationOverlap = True

        for NH in range(NumberOfHoles):

            d_centers = np.sqrt((xcNHf-CoordHoles[NH,0])**2+(ycNHf-CoordHoles[NH,1])**2)

            if d_centers < RNewHole+RHoles[NH]+2*g:

                strAlertVerOverl = 'verificationOverlap = False -> RNewHole+RHoles[{}]+g > d_centers: {} > {}'. format(NH,RNewHole+RHoles[NH]+2*g, d_centers)
                File.write(strAlertVerOverl+'\n')
                #print(strAlertVerOverl)
                verificationOverlap = False
                break

            #else:

                #continue

        verification = verificationLimitMask and verificationOverlap

        return verification

def FigMerit_Fmin_v3(x, AntMask, CoordHoles, RHoles, fileLogs ,f0 = 100*mm, NFresnelMax = 9, g = 20* um, Rmin = 2.5* um, plots = False, prints = False):
    """

    Verify if the mask cumply with the restriction impost.

    Parameters:
        x:
        AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY. Mask is widen an amount SpreadRadius.
        SpreadRadius (float): radius of convolution
        new_field (bool): returns a new XY field
        binarize (bool): binarizes result.
        plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.

    Returns:
        verification:

    Request:
        import numpy as np
        from diffractio import degrees, mm, plt, sp, um, np, nm
        from diffractio.scalar_masks_XY import Scalar_mask_XY


    """
    r = x[0]
    alpha = x[1]
    R_opt = x[2]

    #print('FigMerit: x = ', x)

    xcNHf = r*np.cos(alpha)
    ycNHf = r*np.sin(alpha)

    # Creación de máscara con nuevo agujero

    MaskNewHole = Scalar_mask_XY(AntMask.x, AntMask.y, AntMask.wavelength)
    AntMaskAux = Scalar_mask_XY(AntMask.x, AntMask.y, AntMask.wavelength)

    # Verificación de que la máscara es válida

    verification = RestricVerify_v2(x, AntMask, CoordHoles, RHoles, g, fileLogs)

    MaskNewHole.circle(r0 = (xcNHf, ycNHf), radius = R_opt)
    AntMaskAux.u = AntMask.u + MaskNewHole.u

    if verification == True:

        # Como la máscara es válida hay que realizar el cálculo de la irradiancia sobre la máscara con el nuevo agujero.

        Imin = IntensityRegion_v2(AntMaskAux, f0, NFresnelMax, plots = False, prints = False) # propagacion de la máscara MaskNewHole

    else:
        Liminf = int(IntensityRegion_v2(AntMaskAux, f0, NFresnelMax, plots = False, prints = False))
        Imin = np.random.rand()*Liminf


    AntMaskAux.clear_field
    MaskNewHole.clear_field

    return Imin

def PointsAvailables(R_opt, AntMask, SpreadRadius, g = 20*um , new_field = True, binarize = True, plots = False, prints = False):
    """
     Gets the coordinates suceptibles to be the new position of the next hole.

    Parameters:
        R_opt: radii of the new hole.
        AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY.
        SpreadRadius (float): radius of convolution.
        new_field (bool): returns a new XY field.
        binarize (bool): binarizes result.
        plots: False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.


    Returns:
        CartProdAvailables: array with the availables coordinates for the new hole.
        AntMaskAux: diffractio.scalar_masks_XY.Scalar_mask_XY.

    Request:
        import numpy as np
        from diffractio import degrees, mm, plt, sp, um, np, nm
        from diffractio.scalar_masks_XY import Scalar_mask_XY

    """
    # Restricciónes para el nuevo agujero

    x0 = AntMask.x
    y0 = AntMask.y
    lamb = AntMask.wavelength
    xmax = np.max(x0)
    ymax = np.max(y0)
    xmin = np.min(x0)
    ymin = np.min(y0)
    lmax = max(xmax, ymax)
    lmin = min(xmin, ymin)

    Npxs_x = len(x0)
    Npxs_y = len(y0)
    stepx = (xmax-xmin)/(Npxs_x-1)
    stepy = (ymax-ymin)/(Npxs_y-1)
    stepMax = max(stepx,stepy)

    AntMaskAux = Scalar_mask_XY(x0, y0, lamb)
    AntMaskAuxWiden = Scalar_mask_XY(x0, y0, lamb)
    mcirc = Scalar_mask_XY(x0,y0,lamb) # máscara que va a delimitar los puntos del borde, pues los agujeros no deben salirse de la máscara.

    # (1) Engrosamos la máscara para asegurarnos que el agujero no solapa con el resto de pinholes

    AntMaskAuxWiden = widenMod_v2(AntMask, SpreadRadius, new_field = new_field, binarize = binarize, plots = False) # engrosamiento + 3*stepMax R_opt + g + 2*stepMax

    # (2) Imponemos restricción en el borde de la máscara para que el pinhole quede siempre dentro de ésta

    size = lmax-lmin-2*(R_opt+g)
    mcirc.circle(r0 = (0,0), radius = size/2, angle = 0)
    mcirc.inverse_amplitude()

    # Para obtener los valores x,y permitidos para el del nuevo agujero debemos imponer las restricciones anteriores simultáneamente

    AntMaskAux.u = AntMaskAuxWiden.u + mcirc.u

    if plots == True:
        mcirc.draw(title = 'Margins')
        AntMaskAux.draw(title = 'AntMaskAux', has_colorbar = 'vertical')

    # Se genera el array que almacena las posiciones x,y compatibles del nuevo agujero

    maskFlatten = AntMaskAux.u.flatten()

    IndexPointsMajor1 = np.where(maskFlatten > 1+0j) # detecto que valores son mayores a 1 en la máscara
    IndexPointsAvailables = np.where(maskFlatten == 0+0j) # detecto que valores son iguales a 0 en las máscara

    indM1 = IndexPointsMajor1[0]
    indE0 = IndexPointsAvailables[0]

    maskFlatten[indM1] = 1+0j # Algunos valores de la máscara que surge de solapar la cuadrada de la inicial, salen con valor 2. Aquí lo corrijo. No es relevante para lo que hago, creo.

    CartProd = np.array(np.meshgrid(x0,y0)).T.reshape(-1,2)
    CartProd = np.array([CartProd[:,1],CartProd[:,0]]).T

    if prints == True:
        print(CartProd.shape)
        print('len(CartProd): ',len(CartProd))

    CartProdAvailables = CartProd[indE0]

    return CartProdAvailables, AntMaskAux

def PointsAvailablesFmin2(AntMask, SpreadRadius, rpos, R_opt, g = 20*um, new_field = True, binarize = True, plots = False, prints = False):
    """

     Gets the coordinates suceptibles to be the new position of the next hole.

    Parameters:
        AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY.
        SpreadRadius (float): radius of convolution.
        rpos (float): initial r_suggested
        R_opt: radii of the new hole.
        new_field (bool): returns a new XY field.
        binarize (bool): binarizes result.
        plots: False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.

    Returns:
        CartProdAvailables: array with the availables coordinates for the new hole.
        AntMaskAux: diffractio.scalar_masks_XY.Scalar_mask_XY.

    Request:
        import numpy as np
        from diffractio import degrees, mm, plt, sp, um, np, nm
        from diffractio.scalar_masks_XY import Scalar_mask_XY

    """
    # (1) Engrosamos la máscara para asegurarnos que el agujero no solapa con el resto de pinholes

    x0 = AntMask.x
    y0 = AntMask.y
    X = AntMask.X
    Y = AntMask.Y
    lamb = AntMask.wavelength
    xmax = np.max(x0)
    ymax = np.max(y0)
    xmin = np.min(x0)
    ymin = np.min(y0)
    lmax = max(xmax, ymax)
    lmin = min(xmin, ymin)

    AntMaskAux = Scalar_mask_XY(x0, y0, lamb)
    AntMaskAuxWiden = Scalar_mask_XY(x0, y0, lamb)
    mcirc = Scalar_mask_XY(x0,y0,lamb) # máscara que va a delimitar los puntos del borde, pues los agujeros no deben salirse de la máscara.
    mring = Scalar_mask_XY(x0,y0,lamb) 
 
    AntMaskAuxWiden = widenMod_v2(AntMask, SpreadRadius, new_field = new_field, binarize = binarize, plots = False) # engrosamiento + 3*stepMax R_opt + g + 2*stepMax
    
    # (2) Imponemos restricción en el borde de la máscara para que el pinhole quede siempre dentro de ésta

    size = lmax-lmin-2*(R_opt+g)
    mcirc.circle(r0 = (0,0), radius = size/2, angle = 0)
    mcirc.inverse_amplitude()

    mring.ring(r0 = (0,0), radius1 = rpos-g, radius2 = rpos+R_opt+g)
    mring.inverse_amplitude()
    
    # Para obtener los valores x,y permitidos para el del nuevo agujero debemos imponer las restricciones anteriores simultáneamente

    AntMaskAux.u = AntMaskAuxWiden.u + mcirc.u + mring.u

    if plots == prints:
        mcirc.draw(title = 'Margins')
        mring.draw(title = 'Coordinates availables widen')
        AntMaskAux.draw(title = 'AntMaskAux', has_colorbar = 'vertical')
        
    rstep = np.sqrt((X[0,1]-X[0,0])**2+(Y[1,0]-Y[0,0])**2)

    mring.clear_field()
    mring.ring(r0 = (0,0), radius1 = rpos-0.5*rstep, radius2 = rpos+0.5*rstep)
    mring.inverse_amplitude()

    mring.u = mring.u + AntMaskAux.u

    if plots == prints:
        mring.draw(title = 'Positions avalaibles', has_colorbar = 'vertical')

    index = np.where(mring.u == 0+0j)

    XAvailables = copy(X)
    YAvailables = copy(Y)
    XAvailables = XAvailables[index]
    YAvailables = YAvailables[index]
    
    # Se genera el array que almacena las posiciones x,y compatibles del nuevo agujero

    CartProdAvailables = np.vstack((XAvailables, YAvailables))

    return CartProdAvailables, AntMaskAux


def ChooseCenterNH(PointsAvailables, file, *args):

    """
    Choose the coordinates inside the list obtained (array) by PointsAvailables.

    Parameters:
        PointsAvailables:
        file: indicate the file that is being written.
        *args -> AntMask, R_opt, g:
            AntMask: diffractio.scalar_masks_XY.Scalar_mask_XY.
            R_opt (float): size (radii) of new hole.
            g (float): safety distance, 20 um.
            PreviusCartProdAvailables (bool): Indicate if the positions array has created before. False by default.
            CartProdAvailables
            AntMaskAux




    Returns:
        xcNH, ycNH: coordinates chosen for the new hole.
        R_opt: size (radii) of new hole.
        CartProdAvailables: updated array with the availables coordinates for the next hole.
        NewInd: updated array with the number of coordinates for the next hole.
        AntMaskAux: diffractio.scalar_masks_XY.Scalar_mask_XY.
        Error: indicates if there was any error in the process.

    Request:
        import numpy as np
        from diffractio import degrees, mm, plt, sp, um, np, nm
        from diffractio.scalar_masks_XY import Scalar_mask_XY


    """
    #print('args: ', args)

    if len(args)>3:
        PreviusCartProdAvailables = args[3]
    else:
        PreviusCartProdAvailables = False

    AntMask, R_opt, g = args[:3]

    if PreviusCartProdAvailables == False:
        CartProdAvailables, AntMaskAux = PointsAvailables(R_opt, AntMask, R_opt+2*g)
    else:
        CartProdAvailables = args[4]
        AntMaskAux = args[5]
    
    Error = False
    indMax = len(CartProdAvailables)

    while indMax == 0:
        R_opt = R_opt*0.5 #Reduzco el tamaño del agujero un 50%
        if R_opt < 2.5:
            Error = True
            break

        CartProdAvailables, AntMaskAux = PointsAvailables(R_opt, AntMask, R_opt+2*g)

        indMax = len(CartProdAvailables)
    
    file.write('EN FUNCIÓN CHOOSECENTERNH: ERROR: {} \n'.format(Error))
    print('EN FUNCIÓN CHOOSECENTERNH: ERROR: ', Error)

    # Para que al hacer el solapamiento los valores no excedan 1+j (máscara binaria)
    tinyfix = np.where(AntMaskAux.u> 1+0j)
    AntMaskAux.u[tinyfix] = 1+0j

    if Error == False:
        if CartProdAvailables.shape[0] == 1:
            NewInd = 0
            NewCoord = CartProdAvailables[NewInd]
        else:
            NewInd = np.random.randint(0, indMax)
            NewCoord = CartProdAvailables[NewInd]
            print('En ChooseCenterNH np.shape(CartProdAvailables) antes: ', np.shape(CartProdAvailables))
            print('Borramos una fila de CartProdAvailables.')
            CartProdAvailables = np.delete(CartProdAvailables, NewInd, axis = 0)
            print('En ChooseCenterNH np.shape(CartProdAvailables) después: ', np.shape(CartProdAvailables))

        xcNH = NewCoord[0]
        ycNH = NewCoord[1]

        return xcNH, ycNH, R_opt, CartProdAvailables, NewInd, AntMaskAux, Error
    else:
        NewInd = 0
        xcNH = 0
        ycNH = 0
        return xcNH, ycNH, R_opt, CartProdAvailables, NewInd, AntMaskAux, Error

def RmaxF2(NHoles, lamb, f0, Nzones, B, C):
    n1, n2 =  Nzones
    A = (np.sqrt(n2)-np.sqrt(n1))*np.sqrt(lamb*f0)
    x = np.array(range(NHoles))
    f = A*np.exp(-B*x)+C
    return f

# Funciones para obtener plots de interés ###############################################################

## Intensidad vs. Nº Holes

def IvsNHoles(Dirfile, NameFile, RHoles, IntAcu, Output = 'all'):

    N = len(RHoles)
    NHolesAcu = np.linspace(1, N, N)

    I_arr = np.array(IntAcu)
    I_arr = -I_arr

    fig1,ax1 = plt.subplots()
    ax1.plot(NHolesAcu, I_arr, '.-')
    ax1.grid()
    ax1.set_xlabel('Nº Holes')
    ax1.set_ylabel('Intensity (U. A.)')
    print('Dirfile + \\ + NameFile in IvsNHoles: ', Dirfile + '\\' + NameFile)
    fig1.savefig(Dirfile + '\\' + NameFile, transparent = True, bbox_inches = 'tight')
    plt.close(fig1)

    if Output == 'all':
        return I_arr, NHolesAcu
    elif Output == 'I':
        return I_arr
    elif Output == 'NHolesAcu':
        return NHolesAcu
    else:
        return None

## Área libre vs. Nº Holes

def AreaRatiovsNHoles(Dirfile, NameFile, AntMask, RHoles, Output = 'all'):

    N = len(RHoles)
    NHolesAcu = np.linspace(1, N, N)

    x0 = AntMask.x
    y0 = AntMask.y
    xmax = np.max(x0)
    ymax = np.max(y0)
    xmin = np.min(x0)
    ymin = np.min(y0)
    lx = xmax-xmin
    ly = ymax-ymin

    AreaIndividual = np.pi*RHoles**2
    AreaAcu = []
    A = 0

    for i in range(N):
        A += AreaIndividual[i]
        AreaAcu += [A]

    AreaAcu_arr = np.array(AreaAcu)
    AMask = lx*ly
    AMask_arr = np.ones(AreaAcu_arr.shape)*AMask

    ARatio = AreaAcu/AMask*100

    fig2,ax2 = plt.subplots()
    ax2.plot(NHolesAcu, ARatio, '.-')
    ax2.grid()
    ax2.set_xlabel('Nº Holes')
    ax2.set_ylabel('Area ratio (%)')
    fig2.savefig(Dirfile + '\\' + NameFile, transparent = True, bbox_inches = 'tight')
    plt.close(fig2)

    if Output == 'all':
        return ARatio, NHolesAcu
    elif Output == 'ARatio':
        return ARatio
    elif Output == 'NHolesAcu':
        return NHolesAcu
    else:
        return None

## Intensidad vs. Área libre

def IvsAreaRatio(Dirfile, NameFile, I, RatioA):

    fig3,ax3 = plt.subplots()
    ax3.plot(RatioA, I, '.-')
    ax3.grid()
    ax3.set_ylabel('Intensity (U. A.)')
    ax3.set_xlabel('Area ratio (%)')
    fig3.savefig(Dirfile + '\\' + NameFile, transparent = True, bbox_inches = 'tight')
    plt.close(fig3)

def SavedWork(tf, N, AntMask, MaskAcu, SolOptTOTAL, CoordHoles, RHoles, CartProdAvailables, IntAcu, dt): # tf, N

    pathcwd = os.getcwd()
    today = datetime.date.today()
    pathDirFolder = pathcwd + '\\{}_PS'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'))

    nameFolder = '{}_PS-N{}'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), N)
    pathNewFolder = pathDirFolder +'\\'+nameFolder

    # creamos carpeta

    try:
        os.mkdir(pathNewFolder)
    except OSError:
        print ("Creation of the directory %s failed" % pathNewFolder)
    #else:
    #    print ("Successfully created the directory %s " % pathNewFolder)

    # Gráficos de interés con los datos obtenidos

    NameFile01 = 'IvsNHoles_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)
    NameFile02 = 'ARatiovsNHoles_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)
    NameFile03 = 'IvsAratio_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)

    IFinal = IvsNHoles(pathNewFolder, NameFile01, RHoles, IntAcu, Output = 'I')
    Aratio = AreaRatiovsNHoles(pathNewFolder, NameFile02, AntMask, RHoles, Output = 'ARatio')
    IvsAreaRatio(pathNewFolder, NameFile03, IFinal, Aratio)

    # Imagen png máscara final

    AntMask.draw()[0].savefig(pathNewFolder+'\\AntMaskFinal.png') # agregado el 28 Marzo 22, comprobar si funciona...

    # guardamos ahí la información

    nameFile1 = 'VarOfComp_{}_N{}.npz'.format(today.strftime('%Y%m%d'), N)
    np.savez(pathNewFolder+'\\'+ nameFile1, AntMaskSaved = AntMask, MaskAcuSaved = MaskAcu, SolOptTOTALSaved = SolOptTOTAL, CoordHolesSaved = CoordHoles, RHolesSaved = RHoles, CartProdAvailablesSaved = CartProdAvailables, IntAcuSaved = IntAcu, dtSaved = dt)

def WriteData(N, tf, dt, DictHead, IntAcu, SolOptTOTAL): #meter argumento con nº iter

    Numbers = np.arange(1,N+1)

    pathcwd = os.getcwd()
    today = datetime.date.today()
    pathDirFolder = pathcwd + '\\{}_PS'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'))

    nameFolder = '{}_PS-N{}'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), N)
    pathNewFolder = pathDirFolder +'\\'+ nameFolder
    nameFile = pathNewFolder+'\\PlaneTextData_{}_N{}.txt'.format(today.strftime('%Y%m%d'), N)

    #print('nameFolder: ',nameFolder)
    #print('pathNewFolder: ', pathNewFolder)
    #print('nameFile: ', nameFile)

    # Escribimos txt

    file = open(nameFile, "w", encoding="utf-8")


    #DictHead = {'lamb': lamb, 'f0': f0, 'npxx': 256, 'npxy': 256, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N-1]}
    strHead = repr(DictHead)
    strHead = strHead.replace('array','')
    strHead = strHead.replace('(','')
    strHead = strHead.replace(')','')
    file.write("DATOS CÁLCULO " + str(tf) + ". NUMBER OF HOLES MADE: {}. COMPUTATION TIME: {} \n".format(N-1, dt))
    file.write(strHead + "\n")
    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Intensity (U. A.) [(including the first Fresnel zone hole)]\n")

    for num in Numbers:
        strI = repr(IntAcu[num-1])
        file.write(strI+ "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Final positions of Holes made\n")

    for num in Numbers[:-1]:
        strP = repr(SolOptTOTAL['Hole {}'.format(num)][0])
        strP = strP.replace('array','')
        strP = strP.replace('(','')
        strP = strP.replace(')','')

        file.write(strP+ "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")

    for num in Numbers[:-1]:

        str0 = 'Hole {}'.format(int(num)) # añadir num iters
        str1 = repr(SolOptTOTAL['Hole {}'.format(int(num))][5])
        str1 = str1.replace('array','')
        str1 = str1.replace('(','')
        str1 = str1.replace(')','')
        #print(num)
        #print(str1+'\n')
        file.write(str0 + "\n")
        file.write(str1 + "\n")

    file.close()

def SavedWork_v2(tf, N, AntMask, SolOptTOTAL, CoordHoles, RHoles, CartProdAvailables, IntAcu, dt): # tf, N

    pathcwd = os.getcwd()
    today = datetime.date.today()
    pathDirFolder = pathcwd + '\\{}_PS'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'))

    nameFolder = '{}_PS-N{}'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), N)
    pathNewFolder = pathDirFolder +'\\'+nameFolder

    # creamos carpeta

    try:
        os.mkdir(pathNewFolder)
    except OSError:
        print ("Creation of the directory %s failed" % pathNewFolder)
    #else:
    #    print ("Successfully created the directory %s " % pathNewFolder)

    # Gráficos de interés con los datos obtenidos

    NameFile01 = 'IvsNHoles_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)
    NameFile02 = 'ARatiovsNHoles_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)
    NameFile03 = 'IvsAratio_{}_N{}.png'.format(today.strftime('%Y%m%d'), N)

    IFinal = IvsNHoles(pathNewFolder, NameFile01, RHoles, IntAcu, Output = 'I')
    Aratio = AreaRatiovsNHoles(pathNewFolder, NameFile02, AntMask, RHoles, Output = 'ARatio')
    IvsAreaRatio(pathNewFolder, NameFile03, IFinal, Aratio)

    # Imagen png máscara final

    AntMask.draw(title= 'Final Mask')[0].savefig(pathNewFolder+'\\AntMaskFinal.png') # agregado el 28 Marzo 22, comprobar si funciona...

    # guardamos ahí la información

    nameFile1 = 'VarOfComp_{}_N{}.npz'.format(today.strftime('%Y%m%d'), N)
    np.savez(pathNewFolder+'\\'+ nameFile1, AntMaskSaved = AntMask, SolOptTOTALSaved = SolOptTOTAL, CoordHolesSaved = CoordHoles, RHolesSaved = RHoles, CartProdAvailablesSaved = CartProdAvailables, IntAcuSaved = IntAcu, dtSaved = dt)

def WriteData_v2(N, tf, dt, DictHead, IntAcu, SolOptTOTAL): #meter argumento con nº iter
    print('ENTRANDO EN WRITEDATA_V2')
    Numbers = np.arange(1,N+1)

    pathcwd = os.getcwd()
    today = datetime.date.today()
    pathDirFolder = pathcwd + '\\{}_PS'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'))

    nameFolder = '{}_PS-N{}'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), N)
    pathNewFolder = pathDirFolder +'\\'+ nameFolder
    nameFile = pathNewFolder+'\\PlaneTextData_{}_N{}.txt'.format(today.strftime('%Y%m%d'), N)

    print('pathcwd: ', pathcwd)
    print('nameFolder: ',nameFolder)
    print('pathNewFolder: ', pathNewFolder)
    print('nameFile: ', nameFile)

    # Escribimos txt

    file = open(nameFile, "w", encoding="utf-8")


    #DictHead = {'lamb': lamb, 'f0': f0, 'npxx': 256, 'npxy': 256, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N-1]}
    strHead = repr(DictHead)
    strHead = strHead.replace('array','')
    strHead = strHead.replace('(','')
    strHead = strHead.replace(')','')
    file.write("DATOS CÁLCULO " + str(tf) + ". NUMBER OF HOLES MADE: {}. COMPUTATION TIME: {} \n".format(N-1, dt))
    file.write(strHead + "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Intensity (U. A.) [(including the first Fresnel zone hole)]\n")

    for num in Numbers:
        strI = repr(IntAcu[num-1])
        file.write(strI+ "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Final positions of Holes made\n")

    for num in Numbers[:-1]:
        strP = repr(SolOptTOTAL['Hole {}'.format(num)][0])
        strP = strP.replace('array','')
        strP = strP.replace('(','')
        strP = strP.replace(')','')

        file.write(strP+ "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")

    for num in Numbers[:-1]:

        str0 = 'Hole {}'.format(int(num)) # añadir num iters
        str1 = repr(SolOptTOTAL['Hole {}'.format(int(num))][5])
        str1 = str1.replace('array','')
        str1 = str1.replace('(','')
        str1 = str1.replace(')','')
        #print(num)
        #print(str1+'\n')
        file.write(str0 + "\n")
        file.write(str1 + "\n")

    file.close()

# Diferentes versiones de PS_Optimization ###########################################################################################################################3

def PS_Optimization_v5(AntMask, NHoles, RmaxF, RmFargs, f0 = 100* mm, NFresnelMax = 9, g = 20, Rmin = 2.5, plots = False, prints = False):
    """
        Tiny modify of method widen include in Diffractio. Widens a mask using a convolution of a certain radius.

        Parameters:
            AntMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): Mask where each hole will be made one by one.
            NHoles (int): Number of holes that will be made.
            RmaxF (function): It`s a function that determinate the maximun value of radius of the new hole per iteration.
            RmFargs (tuple): This variable is a set of parameters for input the function RmaxF
            f0 (float): focal of diffractive element. By default is 100 mm.
            NFresnelMax (int): It`s maximun number of Fresnel Zones expected for calculate of intensity in the Airy Region, it was calculated for the photon Sieve more simple and 9 zones were obtained. Thus by default is 9.
            g (float o int): value of "safety belt" of holes.
            Rmin (float): minimun value manufacturable of holes.
            plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.

        Returns:

            AntMask (diffractio.scalar_masks_XY.Scalar_mask_XY): Final mask with the NHoles made.
            MaskAcu (dictionary): Store all mask made in each iteration.
            SolOptTOTAL (dictionary): Store all optimizations for each hole
            CoordHoles (ndarray): 2-dimensional array that store x,y coordinates of each hole.
            RHoles (array): 1-dimensional array that store the radius of each hole (including the first Fresnel zone hole).
            CartProdAvailables (ndarray): array where the allowed positions for the new hole has been stored.
            IntAcu (list): Store the intensity values found for the mask in each iteration.
            dt (class 'datetime.timedelta' object): Time of computation.


    """
    ####### PREPARO PARA ESCRIBIR LOGS ##############################

    pathcwd = os.getcwd()
    today = datetime.date.today()
    tname = datetime.datetime.now()

    nameFolder = '{}_PS'.format(today.strftime('%Y%m%d')+'-'+tname.strftime('%Hh%Mmin%Ss'))
    pathNewFolder = pathcwd+'\\'+nameFolder
    nameFile = pathNewFolder+'\\logsData_{}.txt'.format(today.strftime('%Y%m%d'))

    # creamos carpeta

    try:
        os.mkdir(pathNewFolder)
    except OSError:
        print ("Creation of the directory %s failed" % pathNewFolder)
    else:
        print ("Successfully created the directory %s " % pathNewFolder)

    print('nameFolder: ',nameFolder)
    print('pathNewFolder: ', pathNewFolder)
    print('nameFile: ', nameFile)

    # Abrimos fichero logs

    fileLogs = open(nameFile, "w", encoding="utf-8")

    ##################################################################
    t0 = datetime.datetime.now()
    x0 = AntMask.x
    y0 = AntMask.y
    lamb = AntMask.wavelength
    R1FZ = np.sqrt(lamb*f0)
    Rmax = RmaxF(*RmFargs) # Obtendré un array de salida con NHoles elementos. En cada iteración (puesta de agujero) escoge un valor
    #Rmax = R1FZ*(1/np.sqrt(2)) # RZ1F/np.sqrt(2) Lo voy a modificar para que falle antes
    xmax = np.max(x0)
    ymax = np.max(y0)
    xmin = np.min(x0)
    ymin = np.min(y0)
    lx = xmax - xmin
    ly = ymax - ymin
    lmax = max(xmax, ymax)
    lmin = min(xmin, ymin)

    if prints == True:
        print('f0: ', f0)
        print('lamb: ', lamb)
        print('Rmax: ', Rmax)
        print('lmax: ', lmax)
        print('lmin: ', lmin)

    # Inicializa las máscaras que serán utilizadas en el proceso

    NewHoleMask = Scalar_mask_XY(x0, y0, lamb) # Ésta será la máscara que utilizaremos para poner el nuevo agujero.
    AntMaskAux = Scalar_mask_XY(x0, y0, lamb)
    #mSquare = Scalar_mask_XY(x0,y0,lamb) # máscara que va a delimitar los puntos del borde, pues los agujeros no deben salirse de la máscara.

    # Se inicializan variables que serán devueltas por la función

    SolOptTOTAL = {}
    MaskAcu = {}
    IntAcu = []
    CoordHoles = []
    RHoles = []


    # Partimos de que en la máscara hay un primer agujero fijo que es el que ocupa la primera zona semiperiódica de Fresnel

    IntAcu += [IntensityRegion_v2(AntMask, f0, NFresnelMax, plots = False, prints = False)]
    CoordHoles += [[0,0]]
    RHoles += [R1FZ]
    #NumberOfHoles = 1;

    # Voy a transformar las listas en array porque prefiero trabajar con ese tipo de variable

    CoordHoles = np.array(CoordHoles)
    RHoles = np.array(RHoles)

    #print('CoordHoles: ', CoordHoles)
    #print('RHoles: ', RHoles)

    # Vamos a realizar un bucle for para cada uno de los distintos agujeros que se indican como input (NHoles)

    for pb, i in zip(trange(NHoles),range(NHoles)):
        #print('Hole nº {} de {}'.format(i+1, NHoles))
        fileLogs.write('Hole nº {} alerts: \n'.format(i+1))

        # Generamos un valor aleatorio del tamaño del nuevo agujero que está comprendido entre el radio mínimo fabricable (2.5 um) y el radio de la primera zona semiperiódicade Fresnel

        R_opt = np.random.rand()*Rmax[i] + Rmin

        # Obtenemos la coordenadas del centro del nuevo agujero y otros parámetro útiles
        argsPointsAvailables = (AntMask, R_opt, g)
        #print('argsPointsAvailables: ', argsPointsAvailables)

        #print('R_opt: ',R_opt)



        #argsPointsAvailables = (AntMask, R_opt, g)
        xcNH, ycNH, R_opt, CartProdAvailables, NewInd, AntMaskAux, Error = ChooseCenterNH(PointsAvailables, fileLogs, *argsPointsAvailables)

        if Error == True:

            tf = datetime.datetime.now()
            dt = tf-t0
            N = len(RHoles)

            # Creamos directorio y guardamos ahí toda la información

            Outputs = (AntMask, MaskAcu, SolOptTOTAL, CoordHoles, RHoles, CartProdAvailables, IntAcu, dt)
            SavedWork(tname, N, *Outputs)

            # Guardamos en ficheros .txt

            npxx = len(AntMask.x)
            npxy = len(AntMask.y)

            DictHead = {'lamb': lamb, 'f0': f0, 'npxx': npxx, 'npxy': npxy, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N]}
            WriteData(N, tname, dt, DictHead, IntAcu, SolOptTOTAL)

            print("There aren`t space for more holes.")
            fileLogs.write("There aren`t space for more holes.\n")

            fileLogs.close()

            return Outputs
            #raise ValueError("There aren`t space for more holes.")


        # Eliminamos de la lista las coordenadas que vamos a probar. La función te devuelve el vector de TODAS las coordenadas posibles, y el índice de las escogidas aleatoriamente (xcNH, ycNH)

        CartProdAvailablesAfterNH = np.delete(CartProdAvailables, NewInd, axis=0)

        # Pasamos de las coordenadas cartesianas a polares y almacenamos en array x, que será el conjunto de variables a optimizar por Fmin

        r = np.sqrt(xcNH**2+ycNH**2)
        alpha = np.arctan2(ycNH, xcNH)
        x = [r, alpha, R_opt]

        #AntMaskAux.draw(title = ' rNH = {}, alphaNH = {}, RNH = {}'.format(r, alpha, R_opt))
        #plt.plot(xcNH, ycNH, 'r+')
        #plt.plot(CoordHoles[:,0], CoordHoles[:,1], 'y+')

        #print('Antes Fmin y de la primera verificación: x = ',x)
        verificationInit = RestricVerify_v2(x, AntMask, CoordHoles, RHoles, g, fileLogs)
        #print('Antes Fmin y DESPUÉS de la primera verificación: x = ',x)
        #print('Antes Fmin: verification = ', verificationInit)
        fileLogs.write('Antes Fmin y DESPUÉS de la primera verificación: x = {} \n'.format(x))
        fileLogs.write('Antes Fmin: verification = {} \n'.format(verificationInit))
        fileLogs.write('COMIENZA FMIN \n')

        # argsFigMerit contiene los argumentos necesarios para FigMerit_Fmin_v3. Ésta utiliza RestricVerify(x, AntMask, CoordHoles, RHoles, g) y IntensityRegion_v2(HolesMask, f0 = 10, NFresnelMax = 9, plots = False, prints = False),
        # por lo tanto incluye todo lo necesario para poder llamarlas.

        argsFigMerit = (AntMask, CoordHoles, RHoles, fileLogs, f0, NFresnelMax, g, Rmin, False, False) # lamb = 0.633*um, f0 = 100*mm, npx = 512, N = 9, g = 20* um, Rmin = 2.5* um
        SolOpt = optimize.fmin(FigMerit_Fmin_v3, x, argsFigMerit, maxiter = 100, maxfun = None,full_output = True, disp = False, retall = True)

        # Nos aseguramos de que el valor obtenido de la optimización cumple las condiciones:

        verificationFinal = RestricVerify_v2(SolOpt[0], AntMask, CoordHoles, RHoles, g, fileLogs)

        while SolOpt[4] != 0 or verificationFinal == False:
            fileLogs.write('Fmin no encontró solución óptima. \n')
            #print('Fmin no encontró solución óptima')
            xcNH, ycNH, R_opt, CartProdAvailables, NewInd, AntMaskAux, Error = ChooseCenterNH(PointsAvailables, fileLogs, *argsPointsAvailables)
            r = np.sqrt(xcNH**2+ycNH**2)
            alpha = np.arctan2(ycNH, xcNH)
            x = [r, alpha, R_opt]
            SolOpt = optimize.fmin(FigMerit_Fmin_v3, x, argsFigMerit, maxiter = 50, maxfun = None,full_output = True, disp = False, retall = True)

            # Nos aseguramos de que el valor obtenido de la optimización cumple las condiciones:

            verificationFinal = RestricVerify_v2(SolOpt[0], AntMask, CoordHoles, RHoles, g, fileLogs)

            if Error == True:

                tf = datetime.datetime.now()
                dt = tf-t0
                N = len(RHoles)

                # Creamos directorio y guardamos ahí toda la información

                Outputs = (AntMask, MaskAcu, SolOptTOTAL, CoordHoles, RHoles, CartProdAvailables, IntAcu, dt)
                SavedWork(tname, N, *Outputs)

                # Guardamos en ficheros .txt

                npxx = len(AntMask.x)
                npxy = len(AntMask.y)

                DictHead = {'lamb': lamb, 'f0': f0, 'npxx': npxx, 'npxy': npxy, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N]}
                WriteData(N, tname, dt, DictHead, IntAcu, SolOptTOTAL)

                fileLogs.write("There aren`t space for more holes.\n")
                print("There aren`t space for more holes.")

                fileLogs.close()

                return Outputs
                #raise ValueError("There aren`t space for more holes.")


        x = SolOpt[0]

        fileLogs.write('TERMINÓ FMIN \n')
        #print('TERMINÓ FMIN')

        xcNHf = x[0]*np.cos(x[1])
        ycNHf = x[0]*np.sin(x[1])

        #AntMaskAux.draw(title = ' rNH = {}, alphaNH = {}, RNH = {}'.format(x[0], x[1], x[2]))
        #plt.plot(xcNH, ycNH, 'c+')
        #plt.plot(xcNHf, ycNHf, 'r+')
        #plt.plot(CoordHoles[:,0], CoordHoles[:,1], 'y+')

        if plots == True:
            AntMaskAux.draw(title = ' rNH = {}, alphaNH = {}, RNH = {}'.format(x[0], x[1], x[2]))
            plt.plot(xcNH, ycNH, 'c+')
            plt.plot(xcNHf, ycNHf, 'r+')
            plt.plot(CoordHoles[:,0], CoordHoles[:,1], 'y+')

        # Guardamos la posición y el tamaño finales

        CoordHoles = np.append(CoordHoles,[[xcNHf, ycNHf]], axis = 0)
        RHoles = np.append(RHoles, x[2])


        NewHoleMask.clear_field()
        NewHoleMask.circle(r0 = (xcNHf,ycNHf), radius = x[2])
        #NewHoleMask.draw(title='Comprobation NewHoleMask in iter {}'.format(i+1))

        AntMask = MakingHoles_v2(AntMask, x, plots = False, prints = False)
        CartProdAvailablesAfterNH = []

        #AntMask.draw(title='AntMask final en el Hole nº {}'.format(i+1))
        #draw_several_fields([AntMaskAux, NewHoleMask, AntMask])

        if plots == True:
            AntMask.draw(title='AntMask final en el Hole nº {}'.format(i+1))

        MaskAcu['MaskHolesN{}'.format(i+1)] = AntMask

        NewHoleMask.clear_field
        AntMaskAux.clear_field
        #mSquare.clear_field

        SolOptTOTAL['Hole {}'.format(i+1)] = SolOpt
        #AntMask.draw(title = 'AntMask antes de calcular I')
        IntAcu += [IntensityRegion_v2(AntMask, f0, NFresnelMax, plots = False, prints = False)]
    #print('Intensidad obtenida en el return Fmin: ', SolOptTOTAL['Hole '+str(i+1)][1])
    #print('Intensidad obtenida por IntensityRegion_v2: ', IntAcu[-1])
    # Será uno nuevo

    fileLogs.write('THE END ;)\n')
    #print('THE END ;)')

    tf = datetime.datetime.now()
    dt = tf-t0
    N = len(RHoles)

    # Creamos directorio y guardamos ahí toda la información

    # Guardamos gráficas y variables

    Outputs = (AntMask, MaskAcu, SolOptTOTAL, CoordHoles, RHoles, CartProdAvailables, IntAcu, dt)
    SavedWork(tname, N, *Outputs)

    # Guardamos en ficheros .txt

    npxx = len(AntMask.x)
    npxy = len(AntMask.y)

    DictHead = {'lamb': lamb, 'f0': f0, 'npxx': npxx, 'npxy': npxy, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N]}
    WriteData(N, tname, dt, DictHead, IntAcu, SolOptTOTAL)

    fileLogs.close()

    return Outputs

def RmaxF2(NHoles, lamb, f0, Nzones, B, C):
    n1, n2 =  Nzones
    A = (np.sqrt(n2)-np.sqrt(n1))*np.sqrt(lamb*f0)
    x = np.array(range(NHoles))
    f = A*np.exp(-B*x)+C
    return f