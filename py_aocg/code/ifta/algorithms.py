# !/usr/bin/env python3

from copy import deepcopy

from diffractio import mm, np, plt, degrees
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.scalar_sources_XY import Scalar_source_XY
from diffractio.vector_sources_XY import Vector_source_XY

from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

from .parameters import RMSE, make_binary, standard_deviation, compute_parameters, proportion_factor


def distances_scalar(DOE, Es_posible):
    """Computes the distance of a given $u = A exp(i \phi)$ to a number of possible $u_f = A_f exp(i \phi_f)$ fields
    Arguments:
        DOE (np.array): Amplitudes array (complex)
        Es_posible (np.array): Amplitudes possible (complex)

    returns:
        DOE_new (np.array, complex): New DOE with fields substitued to the minor distance
        i_positions (np.array, int): Table with the substitutions.
        distances_min (np.array, float): Distances between the fields and the possible fields


     Cuatro ejes:
    - 0: Corresponde con el campo saliente del SLM_system. (256)
    - 1: Dimesion x del DOE
    - 2: Dimension y del DOE
    - 3: Representa parte real e imaginaria de los campos.

    # Dimensiones de las variables:
    (Toammos como ejemplo las dimensiones del SLM Pluto)
    x1: (1080,1920,2)
    x2: (256,2)
    distance: (256,1080,1920,2)
    distances_min: (256,437,2)
    """

    DOE_r = np.real(DOE)
    DOE_i = np.imag(DOE)
    
    Ef_r = np.real(Es_posible)
    Ef_i = np.imag(Es_posible)
    x1 = np.stack((DOE_r, DOE_i), axis=2)
    x2 = np.stack((Ef_r, Ef_i), axis=1)

    distance = (x1[np.newaxis, :, :, :] - x2[:, np.newaxis, np.newaxis, :])**2
    distances = np.sqrt(distance.sum(axis=3))
    distances_min = (distances.min(axis=0)).mean()
    i_mins = (distances == distances.min(axis=0))  #Array de booleanos.
    i_positions = np.argmax(i_mins, axis=0)
    
    DOE_new = np.zeros_like(i_positions, dtype=complex)
    for i, E_posible in enumerate(Es_posible):
        DOE_new[i_positions == i] = E_posible
    
    

    # return DOE_new, i_positions, distances
    return DOE_new, i_positions, distances_min


def distances_scalar_circle(DOE, Es_posible):
    """Computes the distance of a given $u = A exp(i \phi)$ to a number of possible $u_f = A_f exp(i \phi_f)$ fields
    Arguments:
        DOE (np.array): Amplitudes array (complex)
        Es_posible (np.array): Amplitudes possible (complex)

    returns:
        DOE_new (np.array, complex): New DOE with fields substitued to the minor distance
        i_positions (np.array, int): Table with the substitutions.
        distances_min (np.array, float): Distances between the fields and the possible fields


    # Tres ejes:
    - 0: Corresponde con el campo saliente del SLM_system.
    - 1: Corrresponde el espacio de fases.
    - 2: Representa parte real e imaginaria de los campos.

    # Dimensiones de las variables:
    x1: (437,2)
    x2: (256,2)
    distances_min: (256,437,2)
    """

    DOE_r = np.real(DOE)
    DOE_i = np.imag(DOE)
    Ef_r = np.real(Es_posible)
    Ef_i = np.imag(Es_posible)

    x1 = np.stack((DOE_r, DOE_i), axis=1)  # Espacio de fases
    x2 = np.stack((Ef_r, Ef_i), axis=1)  # SLM_system
    distance = (x1[np.newaxis, :, :] - x2[:, np.newaxis, :])**2
    distances = np.sqrt(distance.sum(axis=2))
    #print(distances.shape)
    distances_min = (distances.min(axis=0)).mean()
    i_mins = (distances == distances.min(axis=0))
    i_positions = np.argmax(i_mins, axis=0)
    DOE_new = np.zeros_like(i_positions, dtype=complex)
    for i, E_posible in enumerate(Es_posible):
        DOE_new[i_positions == i] = E_posible

    # return DOE_new, i_positions, distances
    return DOE_new, i_positions, distances_min


def distances_vector(DOEx, DOEy, Esx_posible, Esy_posible):
    """Computes the distance of a given $u = A exp(i \phi)$ to a number of possible $u_f = A_f exp(i \phi_f)$ fields
    Arguments:
        DOEx (np.array): Amplitudes array (complex)
        DOEy (np.array): Amplitudes array (complex)
        Esx_posible (np.array): Amplitudes possible (complex)
        Esy_posible (np.array): Amplitudes possible (complex)

    returns:
        DOE_new_x (np.array, complex): New DOE with fields substitued to the minor distance
        DOE_new_y (np.array, complex): New DOE with fields substitued to the minor distance
        i_positions (np.array, int): Table with the substitutions.
        distances (np.array, float): Distances between the fields and the possible fields
    """
    DOEx_r = np.real(DOEx)
    DOEx_i = np.imag(DOEx)
    DOEy_r = np.real(DOEy)
    DOEy_i = np.imag(DOEy)

    Efx_r = np.real(Esx_posible)
    Efx_i = np.imag(Esx_posible)
    Efy_r = np.real(Esy_posible)
    Efy_i = np.imag(Esy_posible)

    x1 = np.stack((DOEx_r, DOEx_i, DOEy_r, DOEy_i), axis=2)
    x2 = np.stack((Efx_r, Efx_i, Efy_r, Efy_i), axis=1)
    distance = (x1[np.newaxis, :, :, :] - x2[:, np.newaxis, np.newaxis, :])**2
    distances = np.sqrt(distance.sum(axis=3))
    distances_min = (distances.min(axis=0)).mean()

    i_mins = (distances == distances.min(axis=0))
    i_positions = np.argmax(i_mins, axis=0)
    DOE_new_x = np.zeros_like(i_positions, dtype=complex)
    DOE_new_y = np.zeros_like(i_positions, dtype=complex)
    for i, Ex_posible in enumerate(Esx_posible):
        DOE_new_x[i_positions == i] = Ex_posible
        DOE_new_y[i_positions == i] = Esy_posible[i]

    return DOE_new_x, DOE_new_y, i_positions, distances_min


def GS_scalar_far(source,
                  target,
                  kind,
                  is_binary,
                  num_steps,
                  mask_wyrowsky=1,
                  phase=np.pi,
                  rmse='accurate',
                  has_draw=False,
                  verbose=False):
    """Gerbech-Saxton algorithm for the far field.

    Arguments:
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        kind (str): 'phase' or 'amplitude'
        is_binary (bool): False or True
        num_steps (int): number of steps in the algorithm
        phase (float): phase for binary masks
        rmse (str): 'fast' or 'accurate'
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    x = target.x
    y = target.y

    num_x = len(x)
    num_y = len(y)

    wavelength = target.wavelength

    if source is None:
        source = 1.

    u_target_abs = np.fft.fftshift(np.abs(target.u))
    u_target_abs = np.fft.fftshift(target.u)

    I_target = np.abs(u_target_abs)**2

    mean_target = (I_target.max() + I_target.min()) / 2

    if isinstance(mask_wyrowsky, np.ndarray):
        wyrowsky_abs = np.fft.fftshift(np.abs(mask_wyrowsky))
        wyrowsky_abs = np.fft.fftshift((mask_wyrowsky))
        # wyrowsky_abs = np.abs(mask_wyrowsky)
    else:
        wyrowsky_abs = 1.

    I_target = np.abs(u_target_abs)**2

    far_field_u = u_target_abs * \
        np.exp(1j * 2 * phase * np.random.rand(num_y, num_x))

    for i in range(num_steps):
        DOEu = np.fft.ifft2(far_field_u)
        
        if kind == 'amplitude':
            mask = np.abs(DOEu)
            mask = mask / mask.max()
            if is_binary:
                mask = make_binary(mask, 0, 1, 0.5)
            DOEu = mask

        elif kind == 'phase':
            mask = np.angle(DOEu)
            if is_binary:
                mask = make_binary(mask, -phase / 2, phase / 2, 0)

            DOEu = np.exp(1j * mask)

        field_z = np.fft.fft2(DOEu)
        I_field = np.abs(field_z)**2

        if rmse == 'fast':
            error, factor = RMSE(I_field, I_target, fast=False)
        elif rmse == 'accurate':
            error, factor = RMSE(I_field, I_target, fast=False)

        # far_field_u = u_target_abs * np.exp(1j * np.angle(field_z))

        # 230620 - quito de momento
        i_wyrowsky = wyrowsky_abs > mean_target
        far_field_u[i_wyrowsky] = u_target_abs[i_wyrowsky] * \
            np.exp(1j * np.angle(field_z[i_wyrowsky]))

        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

    mask = np.fft.fftshift(mask)
    if kind == 'phase':
        mask = (mask + np.pi) / (2 * np.pi)
    elif kind == 'amplitude':
        mask = mask / mask.max()
    
    
    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    DOE = Scalar_mask_XY(x, y, wavelength)
    DOE.u = DOEu
    # plt.figure()
    # plt.imshow(np.angle(DOE.u))


    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)

    if verbose:
        field_final = DOE.fft(new_field=True, remove0=False)
        compute_parameters(field_final.intensity(), I_target)

    return DOE, mask_final, errors


def GS_scalar_far_pluto(source,
                  target,
                  kind,
                  is_binary,
                  num_steps,
                  mask_wyrowsky=1,
                  phase=np.pi,
                  rmse='accurate',
                  has_draw=False,
                  verbose=False):
    """Gerbech-Saxton algorithm for the far field.

    Arguments:
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        kind (str): 'phase' or 'amplitude'
        is_binary (bool): False or True
        num_steps (int): number of steps in the algorithm
        phase (float): phase for binary masks
        rmse (str): 'fast' or 'accurate'
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    #Dimensiones del target cuadrado (espacio de frecuencias/ espacio de ks)
    x = target.x
    y = target.y
    num_x = len(x)
    num_y = len(y)
    wavelength = target.wavelength

    #Dimensiones del DOE = Dimensiones del SLM (rectangular)
    pixel_size = 8
    size_x = num_x*pixel_size
    size_y = num_y*pixel_size
    x_DOE=np.linspace(-size_x/2, size_x/2,num_x) #256
    y_DOE=np.linspace(-size_y/2,size_y/2,num_y) #128

    DOE = Scalar_mask_XY(x_DOE,y_DOE, wavelength)
    DOE_final = Scalar_mask_XY(x_DOE,y_DOE, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    I_target = target.intensity()
    u_target_abs = np.abs(np.fft.fftshift((target.u)))

    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_y, num_x))

    I_target_shifted = u_target_abs**2
    if source is None:
        source = 1.

    if isinstance(mask_wyrowsky, np.ndarray):
        wyrowsky_abs = np.fft.fftshift(np.abs(mask_wyrowsky))
        # wyrowsky_abs = np.abs(mask_wyrowsky)
    else:
        wyrowsky_abs = 1.

    I_target = u_target_abs**2

    far_field_u = u_target_abs * \
        np.exp(1j * 2 * phase * np.random.rand(num_y, num_x))

    for i in range(num_steps):
        DOEu = np.fft.ifft2(far_field_u)

        if kind == 'amplitude':
            mask = np.abs(DOEu)
            mask = mask / mask.max()
            if is_binary:
                mask = make_binary(mask, 0, 1, 0.5)
            DOEu = mask

        elif kind == 'phase':
            mask = np.angle(DOEu)
            if is_binary:
                mask = make_binary(mask, -phase / 2, phase / 2, 0)

            DOEu = np.exp(1j * mask)

        field_z = np.fft.fft2(DOEu)
        I_field = np.abs(field_z)**2

        if rmse == 'fast':
            error, factor = RMSE(I_field, I_target, fast=False)
        elif rmse == 'accurate':
            error, factor = RMSE(I_field, I_target, fast=False)

        far_field_u = u_target_abs * np.exp(1j * np.angle(field_z))

        #i_wyrowsky = wyrowsky_abs > mean_target
        #far_field_u[i_wyrowsky] = I_target[i_wyrowsky] * \
        #    np.exp(1j * np.angle(field_z[i_wyrowsky]))

        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

    mask = np.fft.fftshift(mask)
    if kind == 'phase':
        mask = (mask + np.pi) / (2 * np.pi)
    elif kind == 'amplitude':
        mask = mask / mask.max()

    mask_final = Scalar_mask_XY(x_DOE, y_DOE, wavelength)
    mask_final.u = mask

    DOE.u = DOEu

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)

    if verbose:
        field_final = DOE.fft(new_field=True, remove0=False)
        compute_parameters(field_final.intensity(), I_target)

    return DOE, mask_final, errors


def VIFTA_far(target,num_steps,Ein,phi_0='random',has_draw=True):
    """""
    Vectorial Iterative Fourier Transform Algorithm - far field

    target(Vector_source_XY): Objective.
    num_steps(int): Number of iterations.
    Ein (Vector_source_XY): Incident light.
    phi_0 (string): Phase seed. Default: 'random'.
    has_draw (bool): If True, draws the errors. Default: True.

    Returns:
    VDOE (Vector_source_XY): Vectorial DOE.
    errors (np.ndarray): Errors of the algorithm.

    Ref: "Vectorial Holograms with a Dielectric Metasurface: Ultimate Polarization Pattern Generation". Ehsan Arbabi, et al. ACS Photnics. https://pubs.acs.org/doi/10.1021/acsphotonics.9b00678 
    """""

    x = target.x
    y = target.y
    wavelength = target.wavelength

    Ex_t_shift = np.fft.fftshift(target.Ex)
    Ey_t_shift = np.fft.fftshift(target.Ey)

    #Amplitud del target
    Ex_target = np.abs(Ex_t_shift)
    Ey_target = np.abs(Ey_t_shift)

    #Parámetros de Stokes del target para calcular el error del algoritmo.
    S0_t, S1_t, S2_t, S3_t = target.get('stokes')
    S0_t, S1_t, S2_t, S3_t = np.fft.fftshift(S0_t),np.fft.fftshift(S1_t),np.fft.fftshift(S2_t),np.fft.fftshift(S3_t)

    
    global_phase = np.angle(Ey_t_shift) - np.angle(Ex_t_shift)
    
    if phi_0 == 'random':
        phi_x = np.random.rand(len(x), len(x))*2*np.pi
        phi_y = np.random.rand(len(y),len(y))*2*np.pi
    elif phi_0 == 'plane':
        phi_x = np.ones((len(x), len(x)))*2*np.pi
        phi_y = np.ones((len(y),len(y)))*2*np.pi
    elif phi_0 == 'paper':
         phi_x = 0
         phi_y = global_phase

    #Inicializamos el campo con la amplitud del target y la fase random/plana. 
    Ex = Ex_target*np.exp(1j*phi_x)
    Ey = Ey_target*np.exp(1j*phi_y)

    errors = np.zeros(num_steps)
    error_min = 1e2
    for i in range(num_steps):
        
        #Espacio objeto
        DOE_Ex = np.fft.ifft2(Ex)
        DOE_Ey = np.fft.ifft2(Ey)
        
        phase_x = np.angle(DOE_Ex)
        phase_y = np.angle(DOE_Ey)

        Ax_in  = np.abs(Ein.Ex)
        Ay_in = np.abs(Ein.Ey)

        #Restricción de fase en el espacio del DOE
        DOE_Ex = Ax_in*np.exp(1j*phase_x)
        DOE_Ey = Ay_in*np.exp(1j*phase_y)

        I_DOE = np.sqrt(np.abs(DOE_Ex)**2 + np.abs(DOE_Ey)**2) #intensidad nula, el DOE es de fase.
        DOE_Ex = DOE_Ex / I_DOE
        DOE_Ey = DOE_Ey / I_DOE

        if i%2 == 0:
            Ex = np.fft.fft2(DOE_Ex)
            Ey = np.fft.fft2(DOE_Ey)
            
            result = Vector_source_XY(target.x,target.y,target.wavelength)
            result.Ex = Ex
            result.Ey = Ey
            result.normalize()
            
        
            #Sustitución del campo
            Ex = Ex_target * np.exp(1j * phi_x)
            Ey = Ey_target * np.exp(1j * (phi_x+global_phase))
            #Ex = Ex / Ex.max()
            #Ey = Ey / Ey.max()



        else:
            Ex = np.fft.fft2(DOE_Ex)
            Ey = np.fft.fft2(DOE_Ey)

            result = Vector_source_XY(target.x,target.y,target.wavelength)
            result.Ex = Ex
            result.Ey = Ey
            result.normalize()
            
            #Sustitución del campo
            Ex = Ex_target * np.exp(1j * (phi_y-global_phase))
            Ey = Ey_target * np.exp(1j * phi_y)
            
        phi_x = np.angle(result.Ex)
        phi_y = np.angle(result.Ey)

        S0, S1, S2, S3 = result.get('stokes')
       
        
        #Calculamos el error con los parámetros de Stokes.
        e_s0 = (S0_t[S0_t>0.5]-S0[S0_t>0.5]).mean()
        e_s1 = (S1_t[S0_t>0.5]-S1[S0_t>0.5]).mean()
        e_s2 = (S2_t[S0_t>0.5]-S2[S0_t>0.5]).mean()
        e_s3 = (S3_t[S0_t>0.5]-S3[S0_t>0.5]).mean()

        error = np.sqrt(e_s0**2 + e_s1**2 + e_s2**2 + e_s3**2)
        errors[i] = error
        print("{}/{} - error = {}".format(i, num_steps, error), end='\r')

        if error < error_min:
            error_min = error
            DOE_final_x = DOE_Ex
            DOE_final_y = DOE_Ey
        

    field = Vector_source_XY(x,y,wavelength)
    field.Ex = np.fft.fftshift(Ex)    
    field.Ey = np.fft.fftshift(Ey)
   
    VDOE = Vector_source_XY(x,y,wavelength)
    VDOE.Ex = np.fft.fftshift(DOE_final_x)
    VDOE.Ey = np.fft.fftshift(DOE_final_y)
    
    if has_draw:
            plt.figure()
            plt.plot(errors)
            #plt.ylim(0,1)
            plt.title('errors')

    return VDOE,errors,field


def VIFTA_near(target,distance,num_steps,Ein,phi_0='random',has_draw=True):
    """""
    Vectorial Iterative Fourier Transform Algorithm - Near field

    target(Vector_source_XY): Objective.
    num_steps(int): Number of iterations.
    distance (float): Propagation distance of VRS approach.
    Ein (Vector_source_XY): Input light.
    phi_0 (string): Phase seed. Default: 'random'.
    has_draw (bool): If True, draws the errors. Default: True.

    Returns:
    VDOE (Vector_source_XY): Vectorial DOE.
    errors (np.ndarray): Errors of the algorithm.

    Ref: "Vectorial Holograms with a Dielectric Metasurface: Ultimate Polarization Pattern Generation". Ehsan Arbabi, et al. ACS Photnics. https://pubs.acs.org/doi/10.1021/acsphotonics.9b00678 
    """""
   
    x = target.x
    y = target.y
    wavelength = target.wavelength


    #Amplitud del target
    Ex_target = np.abs(target.Ex)
    Ey_target = np.abs(target.Ey)


    global_phase = np.angle(target.Ey) - np.angle(target.Ex)
    
    # De momento solo está implementado con fase inicial random
    phi_x = 0.5*np.random.rand(len(x), len(x))*2*np.pi
    phi_y = 0.5*np.random.rand(len(y),len(y))*2*np.pi
   

    Ex = Ex_target*np.exp(1j*phi_x)
    Ey = Ey_target*np.exp(1j*phi_y)
    E = Vector_source_XY(x,y,wavelength)
    E.Ex = Ex
    E.Ey = Ey

    S0_t, S1_t, S2_t, S3_t = target.get('stokes')


    errors = np.zeros(num_steps)
    error_min = 1e2
    for i in range(num_steps):

        DOE = E.VRS(z=-distance,new_field=True)

        #Restricción de fase en el espacio del DOE
        phase_x = np.angle(DOE.Ex)
        phase_y = np.angle(DOE.Ey)

        Ax_in  = np.abs(Ein.Ex)
        Ay_in = np.abs(Ein.Ey)
        
        DOE.Ex = Ax_in*np.exp(1j*phase_x)
        DOE.Ey = Ay_in*np.exp(1j*phase_y)

        I_DOE = np.sqrt(np.abs(DOE.Ex)**2 + np.abs(DOE.Ey)**2) #intensidad nula, el DOE es de fase.
        DOE.Ex = DOE.Ex / I_DOE
        DOE.Ey = DOE.Ey / I_DOE


        if i%2 == 0:

            E = DOE.VRS(z=distance,new_field=True)
        
            #Sustitución del campo
            E.Ex = Ex_target * np.exp(1j * phi_x)
            E.Ey= Ey_target * np.exp(1j * (phi_x+global_phase))
            E.normalize()
    

        else:
            E = DOE.VRS(z=distance,new_field=True)

            #Sustitución del campo
            E.Ex = Ex_target * np.exp(1j * (phi_y-global_phase))
            E.Ey = Ey_target * np.exp(1j * phi_y)
            E.normalize()
     
        phi_x = np.angle(E.Ex)
        phi_y = np.angle(E.Ey)

        S0, S1, S2, S3 = E.get('stokes')
       
        #error, factor = RMSE(I_z, I_target, fast=True)

        e_s0 = (S0_t[S0_t>0.5]-S0[S0_t>0.5]).mean()
        e_s1 = (S1_t[S0_t>0.5]-S1[S0_t>0.5]).mean()
        e_s2 = (S2_t[S0_t>0.5]-S2[S0_t>0.5]).mean()
        e_s3 = (S3_t[S0_t>0.5]-S3[S0_t>0.5]).mean()

        error = np.sqrt(e_s0**2 + e_s1**2 + e_s2**2 + e_s3**2)
        errors[i] = error
        print("{}/{} - error = {}".format(i, num_steps, error), end='\r')

        if error < error_min:
            error_min = error

            DOE_final_x = DOE.Ex
            DOE_final_y = DOE.Ey
    
        field = Vector_source_XY(x,y,wavelength)
        field.Ex = E.Ex
        field.Ey = E.Ey
        
   
    VDOE = Vector_source_XY(x,y,wavelength)
    VDOE.Ex = DOE_final_x
    VDOE.Ey = DOE_final_y
    
    if has_draw:
            plt.figure()
            plt.plot(errors)
            #plt.ylim(0,1)
            plt.title('errors')

    return VDOE,errors,field



def E_to_Jones_mask(Ein,Eout,draw=True,checks=True):

    A = np.array([[np.conjugate(Eout.Ex),np.conjugate(Eout.Ey)],[Ein.Ex,Ein.Ey]])
    B = np.array([np.conjugate(Ein.Ex),Eout.Ex])

    # Transponer A para que las últimas dos dimensiones sean las del Vector_mask 
    A = A.transpose(2, 3, 0, 1)
    A_inv = np.linalg.inv(A)

    # Transponer el resultado para que las dimensiones vuelvan a su lugar original
    A_inv = A_inv.transpose(2, 3, 0, 1)


    A_inv2 = A_inv.reshape((2,2,Ein.Ex.shape[0]*Ein.Ex.shape[1]))
    B2 = B.reshape((2,Ein.Ex.shape[0]*Ein.Ex.shape[1]))
    # print(A_inv2.shape,B2.shape)

    M1 = np.moveaxis(A_inv2, -1, 0)
    M2 = np.moveaxis(B2, -1, 0)

    if M2.ndim == 2:
        M2 = np.expand_dims(M2, 2)
        
    J = M1 @ M2
    J = np.squeeze(J,axis=-1)
    J = np.moveaxis(J, 0, -1)

    J00 = J[0,:]
    J10 = J[1,:]
    J01 = J10
    J11 = - np.exp(2j*np.angle(J10))*np.conjugate(J00)
    J00 = np.reshape(J00,newshape=(Ein.Ex.shape[0],Ein.Ex.shape[1]))
    J01 = np.reshape(J01,newshape=(Ein.Ex.shape[0],Ein.Ex.shape[1]))
    J10 = np.reshape(J10,newshape=(Ein.Ex.shape[0],Ein.Ex.shape[1]))
    J11 = np.reshape(J11,newshape=(Ein.Ex.shape[0],Ein.Ex.shape[1]))

    components = [J00,J01,J10,J11]
    J_DOE = Jones_matrix().from_components(components)

    if draw:
        plt.set_cmap('seismic')
        J_DOE.draw(verbose=False)

    if checks:
        J_DOE.checks.is_symmetric(draw=True);
        J_DOE.checks.is_retarder(draw=True);

    return J_DOE  


def fft_jones_matrix(J):
    
    J00,J01,J10,J11 = J.parameters.components()

    m00 = np.fft.fft2(J00)
    m01 = np.fft.fft2(J01)
    m10 = np.fft.fft2(J10)
    m11 = np.fft.fft2(J11)

    # m00 = np.fft.fftshift(m00)
    # m01 = np.fft.fftshift(m01)
    # m10 = np.fft.fftshift(m10)
    # m11 = np.fft.fftshift(m11)

    components = [m00,m01,m10,m11]
    J = Jones_matrix().from_components(components)
    
    #Normalization
    _,_,param = J.analysis.decompose_pure(all_info=True,verbose=False)
    p1 = param['p1']
    #print(p1.max())
    J.M = J.M/p1.max()

    return J


def fft_jones_vector(J):
    
    Ex,Ey = J.M[0],J.M[1]
    Ex = Ex.reshape((256,256))
    Ey = Ey.reshape((256,256))

    m_Ex = np.fft.fft2(Ex)
    m_Ey = np.fft.fft2(Ey)
   

    # m00 = np.fft.fftshift(m00)
    # m01 = np.fft.fftshift(m01)
    # m10 = np.fft.fftshift(m10)
    # m11 = np.fft.fftshift(m11)

    components = [m_Ex,m_Ey]
    J = Jones_vector().from_components(components)
    
    # Normalization
    I = J.parameters.intensity()
    J.M = J.M/np.sqrt(I.max())

    return J



def VIFTA_matrix_far(J_target,num_steps):

    """
    Vectorial Iterative Fourier Transform Algorithm using jones matrix.

    Ref: "Jones matrix holography with metasurfaces". Noah A. Rubine et al. Science advances. Vol 7, Issue 33. DOI: 10.1126/sciadv.abg7488

    Args:
        J_target (Jones_matrix): Jones matrix target.
        num_steps (int): Number of iterations of the algorithm.

    Returns:
        U_DOE (Jones_matrix): Jones matrix of the VDOE.
    """

    R = np.random.uniform(low=0,high=179.9*degrees,size=(J_target.shape[0],J_target.shape[1]))
    azimuth = np.random.uniform(low=0,high=89.9*degrees,size=(J_target.shape[0],J_target.shape[1]))
    global_phase = np.random.uniform(low=0,high=179.9*degrees,size=(J_target.shape[0],J_target.shape[1]))


    J = Jones_matrix().retarder_linear(R,azimuth,global_phase)
    check_unitary = J.checks.is_retarder(draw=False)
    check_symmetry = J.checks.is_symmetric(draw=False)

    if not (check_unitary.any() or check_symmetry.any()):
        print('Warning: Jones matrix seed is not unitary or symmetric')

    m00,m01,m10,m11 = J.parameters.components()

    for i in range(num_steps):

        m00 = np.fft.fft2(m00)
        m01 = np.fft.fft2(m01)
        m10 = np.fft.fft2(m10)
        m11 = np.fft.fft2(m11)
        
        
        # #Corrección orden 0 fft.
        # if i == 0:
        #     #print(np.real(m01)[0,0],np.real(m10)[0,0],np.imag(m00)[0,0],np.imag(m11)[0,0])       
        #     np.real(m01)[0,0] = np.real(m01)[1:,1:].mean()
        #     np.real(m10)[0,0] = np.real(m10)[1:,1:].mean()
        #     np.imag(m00)[0,0] = np.imag(m00)[1:,1:].mean()
        #     np.imag(m11)[0,0] = np.imag(m11)[1:,1:].mean()

        
        components = [m00,m01,m10,m11]
        J = Jones_matrix().from_components(components)
        
       #Normalizacion
        _,_,param= J.analysis.decompose_pure(all_info=True)
        p1 = param['p1'].max()
        J.M = J.M/p1
        
        U,_ = J.analysis.decompose_pure()
        
        global_phase = U.parameters.global_phase(draw=False)

        J_target.add_global_phase(global_phase);
        
        m00_desired,m01_desired,m10_desired,m11_desired = J_target.parameters.components()

        m00 = np.fft.ifft2(m00_desired)
        m01 = np.fft.ifft2(m01_desired)
        m10 = np.fft.ifft2(m10_desired)
        m11 = np.fft.ifft2(m11_desired)
        

        components = [m00,m01,m10,m11]
        J_DOE = Jones_matrix().from_components(components)
      
        _,_,param = J_DOE.analysis.decompose_pure(all_info=True,verbose=False)
        p1 = param['p1']
        #J_DOE.M = J_DOE.M/p1.max()
        
        U_DOE,H_DOE =  J_DOE.analysis.decompose_pure(verbose=False)
       

        m00,m01,m10,m11 = U_DOE.parameters.components()
        print("{}/{}".format(i, num_steps), end='\r')

    return U_DOE


def RS_jones_matrix(J,target_mask,distance,pupil=False):
     
    """
    Rayleigh-Sommeferld propagation for a Jones matrix (near field).


    Args:
        J (Jones_matrix): Jones matrix to propagate.
        target_mask (Scalar_mask_XY): Diffractio mask to obtain its dimensions and wavelength. 
        distance (float): Propagation distance.
        pupil (bool): If pupil, a circle-shaped mask is applied to jones matrix mask. Default: False.

    Returns:
        J (Jones_matrix): Propagated jones matrix.
    """

    J00,J01,J10,J11 = J.parameters.components()

    if pupil:
        mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
        mask.circle(r0=(0,0),radius=target_mask.x[-1])

        J00 = J00*mask.u
        J01 = J01*mask.u
        J10 = J10*mask.u
        J11 = J11*mask.u

        components = [J00,J01,J10,J11]
        J = Jones_matrix().from_components(components)
        #J.draw(verbose=False)

    m00_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m00_mask.u = J00
    m00 = m00_mask.RS(z=distance,new_field=True)
    
    m01_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m01_mask.u = J01
    m01 = m01_mask.RS(z=distance,new_field=True)
   
    m10_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m10_mask.u = J10
    m10 = m10_mask.RS(z=distance,new_field=True)

    m11_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m11_mask.u = J11
    m11 = m11_mask.RS(z=distance,new_field=True)


    components = [m00.u,m01.u,m10.u,m11.u]
    J = Jones_matrix().from_components(components)
    
    #Normalization
    _,_,param = J.analysis.decompose_pure(all_info=True,verbose=False)
    p1 = param['p1']
    #print(p1.max())
    J.M = J.M/p1.max()

    return J


def RS_jones_vector(J,target_mask,distance,pupil=False):
     
    """
    Rayleigh-Sommeferld propagation for a Jones matrix (near field).


    Args:
        J (Jones_matrix): Jones matrix to propagate.
        target_mask (Scalar_mask_XY): Diffractio mask to obtain its dimensions and wavelength. 
        distance (float): Propagation distance.
        pupil (bool): If pupil, a circle-shaped mask is applied to jones matrix mask. Default: False.

    Returns:
        J (Jones_matrix): Propagated jones matrix.
    """

    Ex = J.M[0]
    Ey = J.M[1]
    Ex = Ex.reshape((256,256))
    Ey = Ey.reshape((256,256))


    if pupil:
        mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
        mask.circle(r0=(0,0),radius=target_mask.x[-1])

        Ex = Ex*mask.u
        Ey = Ey*mask.u
       
        components = [Ex,Ey]
        J = Jones_vector().from_components(components)

    m00_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m00_mask.u = Ex
    m00 = m00_mask.RS(z=distance,new_field=True)
    
    m01_mask = Scalar_mask_XY(target_mask.x,target_mask.y,target_mask.wavelength)
    m01_mask.u = Ey
    m01 = m01_mask.RS(z=distance,new_field=True)

    components = [m00.u,m01.u]
    J = Jones_vector().from_components(components)
    
    # Normalization
    I = J.parameters.intensity()
    J.M = J.M/np.sqrt(I.max())

    return J


def VIFTA_matrix_near(J_target,target_mask,distance,num_steps):

    """
    Vectorial Iterative Fourier Transform Algorithm using jones matrix (near field).

    Args:
        J_target (Jones_matrix): Jones matrix target.
        target_mask (Scalar_mask_XY): Diffractio mask to obtain its dimensions and wavelength. 
        distance (float): Propagation distance.
        num_steps (int): Number of iterations of the algorithm.

    Returns:
        J (Jones_matrix): Jones matrix of the VDOE.
    """

    R = np.random.uniform(low=0,high=179.9*degrees,size=(J_target.shape[0],J_target.shape[1]))
    azimuth = np.random.uniform(low=0,high=89.9*degrees,size=(J_target.shape[0],J_target.shape[1]))
    global_phase = np.random.uniform(low=0,high=179.9*degrees,size=(J_target.shape[0],J_target.shape[1]))


    U_DOE = Jones_matrix().retarder_linear(R,azimuth,global_phase)
    U_DOE.checks.is_retarder(draw=False)
    U_DOE.checks.is_symmetric(draw=False)


    for i in range(num_steps):
        J_far = RS_jones_matrix(U_DOE,target_mask,distance)
        #J_far.draw(verbose=False)
        #J_far.checks.is_symmetric(draw=True)

        U,_ = J_far.analysis.decompose_pure()
        global_phase = U.parameters.global_phase(draw=False)

    
        J_target.add_global_phase(global_phase);
        #J_target.draw(verbose=False)

        J_DOE = RS_jones_matrix(J_target,target_mask,-distance)

        U_DOE,_ =  J_DOE.analysis.decompose_pure(verbose=False)
        print("{}/{}".format(i, num_steps), end='\r')

    return U_DOE


def DOE_to_metasuperfice(DOE,kind_sign='anticlockwise',check=True,draw=True):

    if check:
        DOE.checks.is_retarder(draw=True)
        DOE.checks.is_symmetric(draw=True)

    e1,e2 = DOE.parameters.eigenvalues()
    v1,v2 = DOE.parameters.eigenvectors()

    phi_x = np.angle(e1)
    phi_y = np.angle(e2)

    # if kind_sign in ('anticlockwise'):
    theta_1 = np.arctan2(np.real(v1.M[1]),np.real(v1.M[0]))#,np.imag(-v1.M[1]/v1.M[0]))
    theta_2 = np.arctan2(np.real(-v2.M[0]),np.real(v2.M[1]))#,np.imag(v2.M[0]/v2.M[1]))

    # elif kind_sign in ('clockwise'):
    #     theta_1 = np.arctan2(np.real(v1.M[1]),np.real(v1.M[0]))#,np.imag(v1.M[1]/v1.M[0]))
    #     theta_2 = np.arctan2(np.real(-v2.M[0]),np.real(v2.M[1]))#,np.imag(-v2.M[0]/v2.M[1])) 

    theta_1 = np.reshape(theta_1,newshape=(DOE.shape[0],DOE.shape[1]))
    theta_2 = np.reshape(theta_2,newshape=(DOE.shape[0],DOE.shape[1]))


    if draw:
        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(phi_x,origin='lower')
        plt.title('$\phi_x$')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.subplot(1,2,2)
        plt.imshow(phi_y,origin='lower')
        plt.title('$\phi_y$')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)

        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(theta_1,origin='lower')
        plt.title('$\\theta_1$')
        plt.colorbar()
        plt.subplot(1,2,2)
        plt.imshow(theta_2,origin='lower')
        plt.title('$\\theta_2$')
        plt.colorbar()

    return phi_x,phi_y,theta_1,theta_2



def GS_scalar_Fresnel(z,
                      source,
                      target,
                      kind,
                      is_binary,
                      num_steps,
                      wyroswsky_mask=1,
                      phase=np.pi,
                      has_draw=False):
    """Gerbech-Saxton algorithm for the near field.
    Arguments:
        z (float): distance of propagation
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        kind (str): 'phase' or 'amplitude'
        is_binary (bool): False or True
        num_steps (int): number of steps in the algorithm
        phase (float): phase for binary masks
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    x = target.x
    y = target.y
    wavelength = target.wavelength
    num_x = len(x)
    num_y = len(y)

    if source is None:
        source = Scalar_source_XY(x, y, wavelength)
        source.plane_wave()

    DOE = Scalar_mask_XY(x, y, wavelength)
    field_z = Scalar_mask_XY(x, y, wavelength)

    if isinstance(wyroswsky_mask, Scalar_mask_XY):
        wyro_mask = (wyroswsky_mask.u > 0)
        w_intensity = np.abs(wyroswsky_mask.u)**2

    else:
        w_intensity = 1

    u_target = np.abs(target.u)
    I_field = np.abs(target.u)**2

    field_z.u = u_target * np.exp(2j * phase * np.random.rand(num_y, num_x))

    for i in range(num_steps):

        DOEu = field_z.RS(z=-z, matrix=True)
        # DOEu = DOEu / DOEu.max()

        if kind == 'amplitude':
            mask = np.abs(DOEu)
            mask = mask / mask.max()

            if is_binary:
                mask = make_binary(mask, 0, 1, 0.5)

            DOE.u = mask

        elif kind == 'phase':
            mask = np.angle(DOEu)

            if is_binary:
                mask = make_binary(mask, -np.pi / 2, np.pi / 2, 0)

            DOE.u = np.exp(1j * mask)

        field_z = (source * DOE).RS(z=z, new_field=True)
        I_z = np.abs(field_z.u)**2
        # I_z = I_z * I_field_mean / I_z.mean()

        if isinstance(wyroswsky_mask, Scalar_mask_XY):
            field_z.u[wyro_mask] = u_target[wyro_mask] * \
                np.exp(1j * np.angle(field_z.u[wyro_mask]))
        else:
            field_z.u = u_target * np.exp(1j * np.angle(field_z.u))

        error, _ = RMSE(I_field, I_z, wyrowsky_mask=w_intensity)
        print("{}/{} - error {:2.6f} %".format(i, num_steps, error), end='\r')
        errors[i] = error

    if kind == 'phase':
        mask = (mask + np.pi) / (2 * np.pi)
        # mask = mask * target.u

    # elif kind == 'amplitude':
    #    mask = mask / mask.max()

    if has_draw:
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    return DOE, mask_final, errors


def GS_distances_far_deprecated(u_fab,
                                target,
                                num_steps=20,
                                has_draw=False,
                                verbose=False):
    """Gerbech-Saxton algorithm for the far field using distances measurement.

    Arguments:
        u_fab (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    x = target.x
    y = target.y
    num_x = len(x)
    num_y = len(y)
    wavelength = target.wavelength

    DOE = Scalar_mask_XY(x, y, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    I_target = target.intensity()
    u_target_abs = np.abs(np.fft.fftshift((target.u)))
    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_y, num_x))

    I_target_shifted = u_target_abs**2
    error_min = 100000
    for i in range(num_steps):
        DOEu = far_field.ifft(shift=False, matrix=True, new_field=False)
        DOEu = DOEu / DOEu.max()

        DOE_new, i_positions_i, distances = distances_scalar(DOEu, u_fab)

        DOEu = DOE_new
        mask = DOEu

        DOE.u = DOEu
        field_z = DOE.fft(shift=False, matrix=True)
        #field_z =  DOE.fft_proposal(z=None,shift=True, matrix=False,new_field=True)
        #test.draw()

        I_z = np.abs(field_z)**2
        factor = proportion_factor(I_z, I_target_shifted)
        I_z = factor * I_z
        error = RMSE(I_z, I_target_shifted, where='all')[0]

        far_field.u = u_target_abs * np.exp(1j * np.angle(field_z))

        print("{}/{} - error {:2.6f} %".format(i, num_steps, error), end='\r')
        errors[i] = error

        if error < error_min:
            error_min == error
            DOE_final = DOE
            i_positions = i_positions_i

    print("\n")
    mask = np.fft.fftshift(mask)
    mask = mask / mask.max()

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)
        plt.xlim(xmin=1, xmax=num_steps)

    if verbose:
        field_final = DOE_final.fft(new_field=True, remove0=False)
        field_final.draw()
        compute_parameters(field_final.intensity(), I_target)

    return DOE_final, mask_final, errors, i_positions


def GS_distances_far(u_fab,
                     target,
                     num_steps=20,
                     has_draw=False,
                     mask_wyrowsky=None,
                     verbose=False):
    """Gerbech-Saxton algorithm for the far field using distances measurement.

    Arguments:
        u_fab (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    #Dimensiones del target cuadrado (espacio de frecuencias/ espacio de ks)
    x = target.x
    y = target.y
    num_x = target.u.shape[0]#len(x)
    num_y = target.u.shape[1]#len(y)
    wavelength = target.wavelength

    #Dimensiones del DOE = Dimensiones del SLM (rectangular)
    pixel_size = 8
    size_x = num_x*pixel_size
    size_y = num_y*pixel_size
    x_DOE=np.linspace(-size_x/2, size_x/2,num_x) #256
    y_DOE=np.linspace(-size_y/2,size_y/2,num_y) #128

    DOE = Scalar_mask_XY(x_DOE,y_DOE, wavelength)
    DOE_final = Scalar_mask_XY(x_DOE,y_DOE, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    I_target = target.intensity()
    mean_target = (I_target.max() + I_target.min()) / 2

    u_target_abs = np.abs(np.fft.fftshift((target.u)))

    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_x, num_y))

    I_target_shifted = u_target_abs**2
    
    if isinstance(mask_wyrowsky, Scalar_mask_XY):
        wyrowsky_abs = np.fft.fftshift(np.abs(mask_wyrowsky.u))
        wyrowsky_abs = np.fft.fftshift((mask_wyrowsky.u))
        # wyrowsky_abs = np.abs(mask_wyrowsky)
    else:
        wyrowsky_abs = 1.

    error_min = 100000
    for i in range(num_steps):
        #El DOE tendrá dimensiones rectangulares si el target tiene dimensión cuadrada y píxeles rectangulares.
        DOE = far_field.ifft(z=None,
                                      shift=False,
                                      matrix=False,
                                      new_field=True,
                                      remove0=False)
        DOE.u = DOE.u / DOE.u.max()
        
        #plt.figure()
        #plt.polar(np.angle(DOE.u),np.abs(DOE.u),'k.')
        #plt.title('Before')
        #DOE.u = 2*np.abs(DOE.u)*np.exp(np.pi*2j*np.angle(DOE.u))
        #print(np.max(DOE.u))
        DOE.u = DOE.u / DOE.u.max()
        
        #plt.figure()
        #plt.polar(np.angle(DOE.u),np.abs(DOE.u),'k.')
        #plt.title('*1.2')
        DOE_new, i_positions_i, distances = distances_scalar(DOE.u, u_fab)

        #plt.figure()
        #plt.polar(np.angle(DOE_new),np.abs(DOE_new),'k.')
        
        DOE.u = DOE_new
        #print('DOE_alg',DOE.u)
        mask = DOE.u

        field_z = DOE.fft(z=None,shift=False,matrix=False,new_field=True,remove0=False)
        #field_z =  DOE.fft_proposal(z=None,shift=True, matrix=False,new_field=True)
        

        I_z = np.abs(field_z.u)**2
        factor = proportion_factor(I_z, I_target_shifted)
        I_z = factor * I_z
        error = RMSE(I_z, I_target_shifted, where='all')[0]

        i_wyrowsky = wyrowsky_abs > mean_target
        far_field.u[i_wyrowsky] = u_target_abs[i_wyrowsky] * \
            np.exp(1j * np.angle(field_z.u[i_wyrowsky]))

        #far_field.u = u_target_abs * np.exp(1j * np.angle(field_z.u))

        print("{}/{} - error {:2.6f} %".format(i, num_steps, error), end='\r')
        errors[i] = error

        if error < error_min:
            error_min == error
            DOE_final = DOE
            i_positions = i_positions_i
            #print(i_positions)

    print("\n")
    mask = np.fft.fftshift(mask)
    mask = mask / mask.max()

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)
        plt.xlim(xmin=1, xmax=num_steps)

    if verbose:
        field_final = DOE_final.fft(z=None,new_field=True, remove0=True)
        field_final.draw()
        #compute_parameters(field_final.intensity(), I_target)

    return DOE_final, mask_final, errors, i_positions


def GS_distances_far_backup(u_fab, target, num_steps=20, has_draw=False):
    """Gerbech-Saxton algorithm for the far field using distances measurement.

    Arguments:
        u_fab (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    x = target.x
    y = target.y
    num_x = len(x)
    num_y = len(y)
    wavelength = target.wavelength

    DOE = Scalar_mask_XY(x, y, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    u_target_abs = np.fft.fftshift(np.abs(target.u))
    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_y, num_x))
    I_field = u_target_abs**2
    I_field = I_field / I_field.mean()

    error_min = 100000
    for i in range(num_steps):
        DOEu = far_field.ifft(shift=False, matrix=True, new_field=False)
        DOEu = DOEu / DOEu.max()

        DOE_new, i_positions_i, distances = distances_scalar(DOEu, u_fab)

        DOEu = DOE_new
        mask = DOEu

        DOE.u = DOEu
        field_z = DOE.fft(shift=False, matrix=True)
        field_z = field_z / field_z.max()
        far_field.u = u_target_abs * np.exp(1j * np.angle(field_z))

        I_z = np.abs(field_z)**2
        # I_z = I_z / I_z.mean()

        error = RMSE(I_field, I_z)[0]
        print("{}/{} - error {:2.6f} %".format(i, num_steps, error), end='\r')
        errors[i] = error

        if error < error_min:
            error_min == error
            DOE_final = DOE
            i_positions = i_positions_i

    mask = np.fft.fftshift(mask)
    mask = mask / mask.max()

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)
        plt.xlim(xmin=1, xmax=num_steps)

    return DOE_final, mask_final, errors, i_positions,


def GS_far_algorithm_deprecated(source, target, num_steps, has_draw=False):
    """Gerbech-Saxton algorithm for the far field.

    Arguments:
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        has_draw (bool): If True, draws the errors.

    Returns:
        mask_final (Scalar_mask_XY): Result mask of algorithm.
        errors (np.array): Data of errors
    """

    x = target.x
    y = target.y
    wavelength = target.wavelength
    num_x = len(x)
    num_y = len(y)

    errors = np.zeros(num_steps)

    if source is None:
        source = Scalar_source_XY(x, y, wavelength)
        source.plane_wave()

    DOE = Scalar_mask_XY(x, y, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    u_target_abs = np.fft.fftshift(np.abs(target.u))
    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_y, num_x))
    I_field = u_target_abs**2
    # I_field_mean = I_field.mean()

    for i in range(num_steps):
        DOE = far_field.ifft(shift=False, new_field=True)
        mask = np.angle(DOE.u)
        DOE.u = np.exp(1j * mask)
        field_z = DOE.fft(shift=False, matrix=True)
        I_z = np.abs(field_z)**2
        far_field.u = u_target_abs * np.exp(1j * np.angle(field_z))

        error, _ = RMSE(I_z, I_field)
        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

    mask = np.fft.fftshift(mask)
    mask = (mask + np.pi) / (2 * np.pi)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)

    return DOE, mask_final, errors


def GS_Fresnel_algorithm_deprecated(z,
                                    source,
                                    target,
                                    num_steps,
                                    has_draw=False):
    """Gerbech-Saxton algorithm for the near field.

    Arguments:
        z (float): distance
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        has_draw (bool): If True, draws the errors.

    Returns:
        mask_final (Scalar_mask_XY): Result mask of algorithm.
        errors (np.array): Data of errors
    """

    errors = np.zeros(num_steps)

    x = target.x
    y = target.y
    wavelength = target.wavelength
    num_x = len(x)
    num_y = len(y)

    if source is None:
        source = Scalar_source_XY(x, y, wavelength)
        source.plane_wave()

    DOE = Scalar_mask_XY(x, y, wavelength)
    field_z = Scalar_mask_XY(x, y, wavelength)
    # DOE.u=np.abs(target.u)*np.exp(1j*2*np.pi*np.random.rand(num_x,num_y))

    u_target = np.abs(target.u)
    I_field = np.abs(target.u)**2
    I_field_mean = I_field.mean()

    field_z.u = u_target * np.exp(2j * np.pi * np.random.rand(num_y, num_x))

    for i in range(num_steps):
        DOE = field_z.RS(z=-z, new_field=True)
        mask = np.angle(DOE.u)
        DOE.u = np.exp(1j * mask)
        field_z = (source * DOE).RS(z=z, new_field=True)
        I_z = np.abs(field_z.u)**2
        I_z = I_z * I_field_mean / I_z.mean()

        field_z.u = u_target * np.exp(1j * np.angle(field_z.u))

        # if has_mask:
        #     DOE.u=np.exp(1j*np.pi*mask)*circle.u

        error, _ = RMSE(I_field, I_z)
        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

    mask = (mask + np.pi) / (2 * np.pi)

    if has_draw:
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')
        plt.title('optimization')
        plt.ylim(ymin=0)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    return DOE, mask_final, errors


def Wyrowski_algorithm(source, target, num_steps=11):
    """
    # TODO
    """

    errors = np.zeros(num_steps)

    z = 1 * mm
    x = target.x
    y = target.y
    num_x = len(x)
    num_y = len(y)
    wavelength = target.wavelength

    if source is None:
        source = Scalar_source_XY(x, y, wavelength)
        source.plane_wave()

    DOE = Scalar_mask_XY(x, y, wavelength)
    far_field = Scalar_mask_XY(x, y, wavelength)

    u_target_abs = np.fft.fftshift(np.abs(target.u))
    far_field.u = u_target_abs * np.exp(
        1j * 2 * np.pi * np.random.rand(num_y, num_x))
    I_field = u_target_abs**2
    I_field_mean = I_field.mean()

    for i in range(num_steps):
        print("{}/{}".format(i, num_steps), end='\r')
        DOE = far_field.ifft(z=z, shift=False)
        mask = np.angle(DOE.u)
        DOE.u = np.exp(1j * mask)
        field_z = DOE.fft(z=z, shift=False, matrix=True)
        I_z = np.abs(field_z)**2
        I_z = I_z * I_field_mean / I_z.mean()
        far_field.u = u_target_abs * np.exp(1j * np.angle(field_z))

        errors[i], _ = RMSE(I_field, I_z)

    if False:
        plt.figure()
        plt.imshow(I_field)
        plt.colorbar()
        plt.figure()
        plt.imshow(I_z)
        plt.colorbar()

    plt.plot(errors, 'k', lw=2)
    plt.title('errors')

    mask = np.fft.fftshift(mask)
    mask = (mask + np.pi) / (2 * np.pi)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    return mask_final, errors


def adaptative_near(source,
                    target,
                    z,
                    kind,
                    is_binary,
                    num_steps,
                    wyroswsky_mask,
                    phase=np.pi,
                    has_draw=False):
    """
    Adaptative algorithm for near field.
    Arguments:
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        z (float): distance of propagation
        kind (str): 'phase' or 'amplitude'
        is_binary (bool): False or True
        num_steps (int): number of steps in the algorithm.
        wyroswsky_mask (Scalar_mask_XY)
        phase (float): phase for binary masks
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors

    """
    errors = np.zeros(num_steps)
    lamda = np.linspace(1, 0, num_steps)
    x = target.x
    y = target.y
    wavelength = target.wavelength
    num_x = len(x)
    num_y = len(y)

    DOE = Scalar_mask_XY(x, y, wavelength)
    field_z = Scalar_mask_XY(x, y, wavelength)
    field_ant = Scalar_mask_XY(x, y, wavelength)

    if isinstance(wyroswsky_mask, Scalar_mask_XY):
        wyro_mask = (wyroswsky_mask.u > 0)
        w_intensity = np.abs(wyroswsky_mask.u)**2

    else:
        w_intensity = 1

    u_target = np.abs(target.u)
    I_target = np.abs(target.u)**2

    field_z.u = u_target * np.exp(2j * phase * np.random.rand(num_y, num_x))

    for i in range(num_steps):
        DOE = field_z.RS(z=-z, new_field=True)

        field_ant.u = np.abs(field_z.u)

        if kind == 'amplitude':
            mask = np.abs(DOE.u)
            mask = mask / mask.max()
            if is_binary:
                mask = make_binary(mask, 0, 1, 0.5)

            DOE.u = mask

        elif kind == 'phase':
            mask = np.angle(DOE.u)
            if is_binary:
                mask = make_binary(mask, -np.pi / 2, np.pi / 2, 0)

            DOE.u = np.exp(1j * mask)

        field_z = (source * DOE).RS(z=z, new_field=True)
        I_z = np.abs(field_z.u)**2

        if isinstance(wyroswsky_mask, Scalar_mask_XY):
            # field_z.u[i_target] = (((1-lamda[i])*u_target[i_target] + lamda[i]*field_ant.u[i_target]))*np.exp(1j * np.angle(field_z.u[i_target]))
            field_z.u[wyro_mask] = (
                (lamda[i] * u_target[wyro_mask] +
                 (1 - lamda[i]) * field_ant.u[wyro_mask])) * np.exp(
                     1j * np.angle(field_z.u[wyro_mask]))
        else:
            field_z.u = ((lamda[i] * u_target +
                          (1 - lamda[i]) * field_ant.u)) * np.exp(
                              1j * np.angle(field_z.u))

        error = RMSE(I_z,
                     I_target,
                     where='up',
                     wyrowsky_mask=w_intensity,
                     fast=False)[0]

        std = standard_deviation(I_z,
                                 I_target,
                                 where='all',
                                 wyrowsky_mask=w_intensity)
        print("{}/{} - error {:.6f} std {:.6f}".format(i, num_steps, error,
                                                       std),
              end='\r')
        errors[i] = error

    if kind == 'phase':
        mask = (mask + np.pi) / (2 * np.pi)
        mask = mask * target.u

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('#  step')
        plt.ylabel('error')
        plt.title('optimization')
        # plt.ylim(ymin=0)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    return DOE, mask_final, errors


def adaptative_far(source,
                   target,
                   kind,
                   is_binary,
                   num_steps,
                   wyroswsky_mask,
                   phase=np.pi,
                   has_draw=False):
    """
    Adaptative algorithm for far field.
    Arguments:
        source (None or Scalar_field_XY): Illumination.
        target (Scalar_mask_XY): Objective.
        kind (str): 'phase' or 'amplitude'
        is_binary (bool): False or True
        num_steps (int): number of steps in the algorithm.
        wyroswsky_mask (Scalar_mask_XY)
        phase (float): phase for binary masks
        has_draw (bool): If True, draws the errors.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors

    """

    lamda = np.linspace(1, 0, num_steps)
    errors = np.zeros(num_steps)

    x = target.x
    y = target.y
    wavelength = target.wavelength
    num_x = len(x)
    num_y = len(y)

    DOE = Scalar_mask_XY(x, y, wavelength)
    field_z = Scalar_mask_XY(x, y, wavelength)
    field_ant = Scalar_mask_XY(x, y, wavelength)

    if isinstance(wyroswsky_mask, Scalar_mask_XY):
        wyro_mask = (wyroswsky_mask.u > 0)
        w_intensity = np.abs(wyroswsky_mask.u)**2

    else:
        w_intensity = 1

    # u_target = np.fft.fftshift(np.abs(target.u))
    u_target = np.abs(target.u)
    I_target = u_target**2
    field_z.u = u_target * np.exp(2j * phase * np.random.rand(num_y, num_x))

    for i in range(num_steps):
        DOEu = field_z.ifft(shift=False, matrix=True, new_field=False)

        field_ant.u = np.abs(field_z.u)

        if kind == 'amplitude':
            mask = np.abs(DOEu)
            mask = mask / mask.max()
            if is_binary:
                mask = make_binary(mask, 0, 1, 0.5)

            DOEu = mask

        elif kind == 'phase':
            mask = np.angle(DOEu)
            if is_binary:
                mask = make_binary(mask, -np.pi / 2, np.pi / 2, 0)

            DOEu = np.exp(1j * mask)

        DOE.u = DOEu
        field_z.u = (source * DOE).fft(shift=False, matrix=True)
        I_z = np.abs(field_z.u)**2

        if isinstance(wyroswsky_mask, Scalar_mask_XY):
            # field_z.u[i_target] = (((1-lamda[i])*u_target[i_target] + lamda[i]*field_ant.u[i_target]))*np.exp(1j * np.angle(field_z.u[i_target]))
            field_z.u[wyro_mask] = (
                (lamda[i] * u_target[wyro_mask] +
                 (1 - lamda[i]) * field_ant.u[wyro_mask])) * np.exp(
                     1j * np.angle(field_z.u[wyro_mask]))
        else:
            field_z.u = ((lamda[i] * u_target +
                          (1 - lamda[i]) * field_ant.u)) * np.exp(
                              1j * np.angle(field_z.u))

        error = RMSE(I_z, I_target, where='up', wyrowsky_mask=w_intensity)[0]

        std = standard_deviation(I_z,
                                 I_target,
                                 where='all',
                                 wyrowsky_mask=w_intensity)

        print("{}/{} - error {:.6f} std {:.6f}".format(i, num_steps, error,
                                                       std),
              end='\r')
        errors[i] = error

    if kind == 'phase':
        mask = (mask + np.pi) / (2 * np.pi)
        mask = mask * target.u

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('RMSE')
        # plt.title('RMSE')
        # plt.ylim(ymin=0)

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = mask

    return DOE, mask_final, errors


def VGS_distances_far(target,
                  num_steps,
                  E_fab=None,
                  phase=np.pi,
                  rmse='fast',
                  has_draw=True,
                  hist=False):
    """Gerbech-Saxton algorithm for the far field.

    Arguments:
        target (Scalar_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        E_fab (Jones vector): Possible electric field.
        phase (float): Phase for binary masks. Default: np.pi
        rmse (str): Root mean square error: 'fast' or 'accurate'. Default: 'fast'
        has_draw (bool): If True, draws the errors. Default: True
        hist (bool): If True, draws a histogram with the lipss ratio. Default: False.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """
    x = target.x
    y = target.y
    wavelength = target.wavelength

    num_x = len(x)
    num_y = len(y)

    Ex_fab, Ey_fab = E_fab.parameters.components()
    
    target_abs = np.fft.fftshift(np.abs(target.u))
    I_target = target_abs**2

    # Inicialización del campo
    Ex = target_abs / np.sqrt(2) * np.exp(
          1j * 2 * phase * np.random.rand(num_y, num_x))
    Ey = target_abs / np.sqrt(2) * np.exp(
          1j * 2 * phase * np.random.rand(num_y, num_x))
    
    Ex = target_abs / np.sqrt(2) * np.exp(
        1j * np.ones((1024, 1024))*np.pi/3)
    Ey = target_abs / np.sqrt(2) * np.exp(
        1j * np.ones((1024, 1024))*np.pi/3)

    errors = np.zeros(num_steps)
    error_min = 1e2
    for i in range(num_steps):

        DOE_Ex = np.fft.ifft2(Ex)
        DOE_Ey = np.fft.ifft2(Ey)

        DOE_Ex = DOE_Ex  / DOE_Ex.max()
        DOE_Ey = DOE_Ey  / DOE_Ey.max()

        
        #if i==0:
        #    print(DOE_Ex.max(), DOE_Ey.max(), Ex_fab.max(), Ey_fab.max())
        #Distances
        DOE_Ex, DOE_Ey, i_positions_i, distances = distances_vector(
            DOE_Ex, DOE_Ey, Ex_fab, Ey_fab)

        # Propagación directa
        Ex = np.fft.fft2(DOE_Ex)  #field_z
        Ey = np.fft.fft2(DOE_Ey)  #field_z
    
        plt.figure()
        plt.imshow(np.angle(Ex))
        plt.colorbar()

        I_field = np.sqrt(np.abs(Ex)**2 + np.abs(Ey)**2)
        #I_field = I_field/I_field.max()
        #I_field = I_field * I_target.sum()/I_field.sum()

        # Cálculo del error
        if rmse == 'fast':
            error, factor = RMSE(I_field, I_target, fast=True)
        elif rmse == 'accurate':
            error, factor = RMSE(I_field, I_target, fast=False)

        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

        if error < error_min:
            error_min == error
            DOE_final_x = DOE_Ex
            DOE_final_y = DOE_Ey
            i_positions = i_positions_i

        # 10. substitución de los campos
        #Ex = Ex/I_field.max()
        Ix = np.abs(I_target) / 2
        Iy = np.abs(I_target - Ix)
        Ex = np.sqrt(Ix) * np.exp(1j * np.angle(Ex))
        Ey = np.sqrt(Iy) * np.exp(1j * np.angle(Ey))

    mask_final = Scalar_mask_XY(x, y, wavelength)
    mask_final.u = i_positions

    DOE = Vector_source_XY(x, y, wavelength)
    DOE.Ex = DOE_Ex
    DOE.Ey = DOE_Ey

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')

    if hist:
        plt.figure()
        plt.title('Histograma')
        plt.hist(mask_final.u.flatten(), bins=len(E_fab))

    return DOE, mask_final, errors


def VGS_far(target,
                  num_steps,
                  phi_0='random',
                  rmse='fast',
                  has_draw=True,
                  hist=False):
    """Gerbech-Saxton algorithm for the far field.

    Arguments:
        target (Vector_mask_XY): Objective.
        num_steps (int): number of steps in the algorithm
        phase (float): Phase for binary masks. Default: np.pi
        rmse (str): Root mean square error: 'fast' or 'accurate'. Default: 'fast'
        has_draw (bool): If True, draws the errors. Default: True
        hist (bool): If True, draws a histogram with the lipss ratio. Default: False.

    Returns:
        DOE (Scalar_mask_XY): DOE which produces the algoritm
        mask_final (Scalar_mask_XY): mask of algorithm (for fabrication: 0-1).
        errors (np.array): Data of errors
    """
    x = target.x
    y = target.y
    wavelength = target.wavelength

    num_x = len(x)
    num_y = len(y)

    E0x = np.abs(target.Ex)
    E0y = np.abs(target.Ey)
    target_abs = np.fft.fftshift(E0x**2 + E0y**2)
    I_target = target_abs**2

    global_phase = np.fft.fftshift(np.angle(target.Ey)-np.angle(target.Ex))

    if phi_0 == 'random':
        phi_x = np.random.rand(1024, 1024)*2*np.pi
        phi_y = np.random.rand(1024,1024)*2*np.pi
    elif phi_0 == 'plane':
        phi_x = np.ones((1024, 1024))*2*np.pi
        phi_y = np.ones((1024,1024))*2*np.pi
    elif phi_0 == 'paper':
         phi_x = 0
         phi_y = global_phase
    
    # Inicialización del campo
    Ex = E0x * np.exp(1j * phi_x)
    Ey = E0y * np.exp(1j * phi_y)

    errors = np.zeros(num_steps)
    error_min = 1e2
    for i in range(num_steps):

        DOE_Ex = np.fft.ifft2(Ex)
        DOE_Ey = np.fft.ifft2(Ey)


        DOE_Ex = DOE_Ex / DOE_Ex.max()
        DOE_Ey = DOE_Ey / DOE_Ey.max()


        # Propagación directa
        Ex = np.fft.fft2(DOE_Ex)  #field_z
        Ey = np.fft.fft2(DOE_Ey)  #field_z

        I_field = np.sqrt(np.abs(Ex)**2 + np.abs(Ey)**2)
        #I_field = I_field/I_field.max()
        #I_field = I_field * I_target.sum()/I_field.sum()

        # Cálclo del error
        if rmse == 'fast':
            error, factor = RMSE(I_field, I_target, fast=True)
        elif rmse == 'accurate':
            error, factor = RMSE(I_field, I_target, fast=False)

        print("{}/{} - error {:2.6f}".format(i, num_steps, error), end='\r')
        errors[i] = error

        if error < error_min:
            error_min == error
            DOE_final_x = DOE_Ex
            DOE_final_y = DOE_Ey

        # 10. substitución de los campos
        Ix = np.abs(I_target) / 2
        Iy = np.abs(I_target - Ix)
        Ex = np.sqrt(Ix) * np.exp(1j * np.angle(Ex))
        Ey = np.sqrt(Iy) * np.exp(1j * np.angle(Ey))


    DOE = Vector_source_XY(x, y, wavelength)
    DOE.Ex = DOE_final_x
    DOE.Ey = DOE_final_y

    if has_draw:
        plt.figure()
        plt.plot(errors, 'k', lw=2)
        plt.xlabel('# step')
        plt.ylabel('error')


    return DOE, errors
