# !/usr/bin/env python3

from copy import deepcopy
from scipy.optimize import minimize

from diffractio import np, plt


def __func_k_parameter__(k, I_field, I_target):
    """Se utiliza para obtener el mejor parámetro de ajuste entre intesndiades en la RMSE"""

    error = ((k * I_field - I_target)**2).mean()
    return error


def make_binary(mask, low_level=0, high_level=1, cut_level=0.5):
    """Convert a np.array to binary mask

    Arguments:
        mask (np.array): mask with real values.
        low_level (float): low level.
        high_level (float): high level.
        cut_level (float): value to divide in low and high levels."""

    mask_new = np.zeros_like(mask)
    mask_new[mask < cut_level] = low_level
    mask_new[mask >= cut_level] = high_level
    return mask_new


# def compute_error(I_field, I_target, normalize=False):
#     """
#     Computer the error as the average difference of the absolute value between of the intensity at target and the intensity at the result.
#
#     Parameters:
#         I_field (numpy.array): intensity produced by the algorithms
#         I_target (numpy.array): intentisty at target
#         is_balanced (bool): If True, perform the comparison using a proportion parameter to avoid different intensity levels
#
#
#     Returns:
#         error: Mean difference between result and target.
#
#     """
#
#     error = (np.abs(I_field - I_target)).mean()
#
#     return error


def proportion_factor(I_field,
                      I_target,
                      wyrowsky_mask=1,
                      fast=False,
                      verbose=False):
    """Compute parameter to fit u_field to target. The proportion factor is for intensity, not for amplitude

    Arguments:
        target (Scalar_mask_XY): target
        u_field  (Scalar_mask_XY): result in the DOE

    """

    mean_target = (I_target.max() + I_target.min()) / 2

    # I_not_normalized = u_field.intensity()
    # I_target.sum(), I_not_normalized.sum(
    # ), I_not_normalized.sum() / I_target.sum()
    # k = np.sqrt(I_target[I_target > 0.5].sum() /
    #             I_not_normalized[I_target > 0.5].sum())

    I_field = I_field * wyrowsky_mask
    I_target = I_target * wyrowsky_mask

    factor_0 = I_target[I_target > mean_target].sum() / I_field[
        I_target > mean_target].sum()

    if fast is False:
        res = minimize(__func_k_parameter__,
                       factor_0,
                       args=(I_field, I_target),
                       method='Nelder-Mead',
                       tol=1e-6)
        factor = res.x[0]
    else:
        factor = factor_0

    if verbose:
        print("k  = {:2.5f}".format(factor))

    return factor


def efficiency_total(I_field, I_target, DOE):
    '''
    Total efficiency of the designed DOE. The intensity at target position is compared to the incident light.

    Parameters:
        result (Scalar_mask XY): field or intensity distribution obtained with IFTA algorithm.
        target (Scalar_mask XY): field or intensity distribution of target.
        DOE (Scalar_mask_XY): transmission of the DOE.
        wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Returns:
        (float): Total efficiency of the DOE

    Reference:
        # 38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
    Jahn, K.,  https://doi.org/10.1016/j.optcom.2010.07.030
    '''

    transmision = DOE.average_intensity()
    diff_eff = diffraction_efficiency(I_field, I_target, wyrowsky_mask=1)

    return transmision * diff_eff


def normalize_to_target(u_field,
                        target,
                        wyrowsky_mask=1,
                        fast=False,
                        verbose=False):
    """Applies to u_field the proportion factor so that intensity fits better to target.

        Arguments:
            target (Scalar_mask_XY): target
            u_field  (Scalar_mask_XY): field distribution produced by DOE

        Returns:
            u_field  (Scalar_mask_XY): normalized field distribution produced by DOE
    """

    I_field = u_field.intensity()
    I_target = target.intensity()
    k = proportion_factor(I_field, I_target, wyrowsky_mask, fast, verbose)
    u_field.u = u_field.u * np.sqrt(k)
    return u_field


def diffraction_efficiency(I_field, I_target, wyrowsky_mask=1):
    '''
    Diffraction efficiency of the designed DOE.

    Parameters:
        result (Scalar_mask XY): field or intensity distribution obtained with IFTA algorithm.
        target (Scalar_mask XY): field or intensity distribution of target.
        wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Returns:
        (float): Diffraction efficiency of the DOE

    Reference:
        # 38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
    Jahn, K.,  https://doi.org/10.1016/j.optcom.2010.07.030
    '''

    I_field = I_field * wyrowsky_mask
    mean_target = (I_target.max() + I_target.min()) / 2

    I_region = I_field[I_target > mean_target].sum()
    I_field = I_field.sum()

    diff_eff = I_region / I_field

    return diff_eff


def standard_deviation(I_field,
                       I_target,
                       where='all',
                       wyrowsky_mask=1,
                       normalize=True):
    '''
    Standard deviation of the designed DOE.

    Parameters:
    result (Scalar_mask XY): Intensity distribution obtained with IFTA algorithm.
    target (Scalar_mask XY): Intensity of the target.
    where (string) ('up'.'down' or 'all'):  If 'up' we calculate the standard_deviation inside the mask. If 'down' we calculate the
     standard_deviation outside the mask. If 'all' we calculate the standard_deviation off the whole mask.  Default: 'up'.
    wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
    (float): Standard deviation of the DOE
    '''

    I_field = I_field * wyrowsky_mask
    # num_data = I_field.size

    mean_target = (I_target.max() + I_target.min()) / 2

    if normalize is True:
        factor_0 = proportion_factor(I_field, I_target)
    else:
        factor_0 = 1

    if where == 'up':
        std = np.std(factor_0 * I_field[I_target >= mean_target])

    elif where == 'down':
        std = np.std(factor_0 * I_field[I_target < mean_target])

    elif where == 'all':
        std = np.std(factor_0 * I_field)

    return std


# def mnse(I_field, I_target):
#     """
#     Computer the error as the average difference of the absolute value between of the intensity at target and the intensity at the result.
#
#     Parameters:
#         I_field (numpy.array): intensity produced by the algorithms
#         I_target (numpy.array): intentisty at target
#         is_balanced (bool): If True, perform the comparison using a proportion parameter to avoid different intensity levels
#
#     Reference:
#         K. Jahn and N. Bokor, “Intensity control of the focal spot by vectorial beam shaping,” Opt. Commun., vol. 283, no. 24, pp. 4859–4865, 2010, doi: 10.1016/j.optcom.2010.07.030.
#         Similar to Ec. 17
#
#     Returns:
#         error: Mean difference between result and target.
#
#     """
#
#     error = np.sqrt(
#         ((I_field / I_field.sum() - I_target / I_target.sum())**2).mean())
#
#     return error * 100


def parameters_IFTA(I_field,
                    I_target,
                    has_diffraction_efficiency=True,
                    has_std=True,
                    has_RMSE=True,
                    verbose=True):
    """
    Quality parameters of IFTA algorithm. Using this function we could calculate the diffraction efficiency, standard_deviation and root_mean_square_error of the designed DOE.

    Parameters:
    I_field (Scalar_mask XY): Intensity distribution obtained with IFTA algorithm.
    I_target (Scalar_mask XY): Target intesity distribution
    diffraction_eff (bool): True if you want to calculate the diffraction_eff of the designed DOE. Default: True
    std (bool): True if you want to calculate the standard deviation of the designed DOE. Default: True
    RMSE (bool): True if you want to calculate the root mean square error of the designed DOE. Default: True
    info_text (bool): If True the parameters appear with its name. If False the parameters appear as floats for use in a plot. Default: True


    Return:
    (list of strs or list of floats)  Quality parameters of the designed DOE.


    Reference (Diffraction Efficiency and Mean square error):
    # 38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
    Jahn, K., &
    https://doi.org/10.1016/j.optcom.2010.07.030

    """

    parameters = []

    if has_diffraction_efficiency:
        eff = diffraction_efficiency(I_field, I_target)
        parameters.append(eff)

        if verbose:
            print('Diffraction efficiency = {:.6f}'.format(eff))

    if has_std:
        std = standard_deviation(I_field, I_target, where='up')
        parameters.append(std)
        if verbose:
            print('Standard deviation = {:.6f}'.format(std))

    if has_RMSE:
        rmse = RMSE(I_field, I_target)
        parameters.append(rmse)

        if verbose:
            print('Root Mean Square error = {}'.format(rmse))

    return parameters


def verify_mask(mask,
                z,
                is_phase,
                is_binary,
                has_pupil=False,
                has_draw=False,
                has_axis=False,
                is_logarithm=False):
    """Computes the result of the algorithm in the far or near field. The mask is an amplitude mask.
    The mask can be one defined or one obtained from a
    If required it is converted to binary and/or phase

    Arguments:
        mask (Scalar_mask_XY): DOE to analyze
        z (None or float): If None computed at far field,else, distance for the near field
        is_phase (bool): If True, converts the mask to a phase mask
        is_binary (bool): If True, converts the mask to a binary mask
        has_pupil (bool): If True, implements a circular pupil
        has_draw (bool): If True, draw the result
        has_axis (bool): If True, implements axis, else, it is removed
        is_logarithm (bool): If True, performs a logarithm on intensity
    Returns:
        DOE_new (Scalar_mask_XY): Result mask of algorithm (binarized, phase, etc.)
        result (np.array): propagation of the mask
    """

    DOE_new = deepcopy(mask)

    if is_phase:
        if is_binary:
            DOE_new.u = np.exp(1j * np.pi * DOE_new.u)
        else:
            DOE_new.u = np.exp(2j * np.pi * DOE_new.u)

    if has_pupil:
        DOE_new.pupil()

    if z is None or z == 0:
        result = DOE_new.fft(new_field=True, shift=True, remove0=True)

    else:
        result = DOE_new.RS(z=z, new_field=True)

    if has_draw:
        result.draw(logarithm=is_logarithm)
        plt.axis('off')
        if has_axis is True:
            plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
        else:
            intensity = np.abs(result.u)**2
            if is_logarithm:
                intensity = np.log(intensity + is_logarithm)

    return DOE_new, result


def RMSE(I_field, I_target, where='all', wyrowsky_mask=1, fast=False):
    '''
    Root Mean Square error of the designed DOE.

    Parameters:
    I_target (np.array): Intesity distribution of target.
    I_field (array): Intensity distribution obtained with IFTA algorithm.
    where (string) ('up'.'down' or 'all'):  If 'up' we calculate the standard_deviation inside the mask. If 'down' we calculate the
     standard_deviation outside the mask. If 'all' we calculate the standard_deviation off the whole mask.  Default: 'up'.
    wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
    (float) Root Mean square error of the DOE.

    Reference:
    Pengcheng Zhou, Yong Bi, Minyuan Sun, Hao Wang, Fang Li, and Yan Qi,
    "Image quality enhancement and computation acceleration of 3D holographic display using a symmetrical 3D GS algorithm,"
    Appl. Opt. 53, G209-G213 (2014)

    '''

    I_field = I_field * wyrowsky_mask
    I_target = I_target * wyrowsky_mask

    mean_target = (I_target.max() + I_target.min()) / 2

    factor = proportion_factor(I_field, I_target, wyrowsky_mask, fast)
    I_field = I_field * factor
    factor = proportion_factor(I_field, I_target, wyrowsky_mask, fast)
    I_field = I_field * factor
    # factor_0 = I_target[I_target > mean_target].sum() / I_field[
    #     I_target > mean_target].sum()
    #
    # if fast is False:
    #     res = minimize(__func_k_parameter__,
    #                    factor_0,
    #                    args=(I_field, I_target),
    #                    method='Nelder-Mead',
    #                    tol=1e-6)
    #     factor = res.x[0]
    # else:
    #     factor = factor_0

    if where == 'up':
        i_target = I_target >= mean_target
        rmse = np.sqrt(((I_field[i_target] - I_target[i_target])**2).mean())

    elif where == 'down':
        i_target = I_target < mean_target
        rmse = np.sqrt(((I_field[i_target] - I_target[i_target])**2).mean())

    elif where == 'all':
        i_target = I_target > 0
        rmse = np.sqrt(((I_field - I_target)**2).mean())

    return rmse, factor


def to_amplitude(mask):
    """
    Convert a phase DOE to an amplitude mask.

    Parameters:
    mask (Scalar_mask_XY): Phase mask to be converted to amplitude.

    Return:
    mask (Scalar_mask_XY): Amplitude converted mask.

    """

    DOE = np.abs(mask.u)
    DOE = DOE / DOE.max()
    mask.u = DOE

    return mask


def RMSE_back_up(I_field, I_target, where='all', wyrowsky_mask=1):
    '''
    Root Mean Square error of the designed DOE.

    Parameters:
    I_target (np.array): field or intesity distribution of target.
    I_field (array): field or intensity distribution obtained with IFTA algorithm.
    where (string) ('up'.'down' or 'all'):  If 'up' we calculate the standard_deviation inside the mask. If 'down' we calculate the
     standard_deviation outside the mask. If 'all' we calculate the standard_deviation off the whole mask.  Default: 'up'.
    wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
    (float) Root Mean square error of the DOE.

    Reference:
    Pengcheng Zhou, Yong Bi, Minyuan Sun, Hao Wang, Fang Li, and Yan Qi,
    "Image quality enhancement and computation acceleration of 3D holographic display using a symmetrical 3D GS algorithm,"
    Appl. Opt. 53, G209-G213 (2014)

    '''

    I_field = I_field * wyrowsky_mask

    if where == 'up':
        diff = (
            (I_field[I_target >= 0.5] / I_field[I_target >= 0.5].mean()) -
            (I_target[I_target >= 0.5] / I_target[I_target >= 0.5].mean()))**2

    elif where == 'down':
        diff = (
            (I_field[I_target < 0.5] / I_field[I_target < 0.5].mean()) -
            (I_target[I_target < 0.5] / I_target[I_target < 0.5].mean()))**2

    elif where == 'all':
        diff = ((I_field / I_field.mean()) - (I_target / I_target.mean()))**2

    rmse = np.sqrt(diff.sum())

    return rmse, 0


def compute_parameters(I_field, I_target, DOE=None,wyrowsky=1, fast=False,verbose=False):
    """Compute and print parameters for result of DOE and it is compared to target.

    Arguments:
        target (Scalar_mask_XY): target
        u_far  (Scalar_mask_XY): result in the DOE
        DOE    (Scalar_mask:XY): DOE
        fast (bool): Optional method to calculate proportinal factor. Default: False
        verbose (bool): If True show the parameters. Default: False

    Returns:


    """

    factor = proportion_factor(I_field,
                               I_target,
                               wyrowsky_mask=1,
                               fast=fast,
                               verbose=False)


    I_field = factor * I_field

    #Check del factor de normalización
    factor_check = proportion_factor(I_field,
                               I_target,
                               wyrowsky_mask=1,
                               fast=fast,
                               verbose=False)
    I_field = factor_check * I_field

    std_all = standard_deviation(I_field,I_target,where='all',wyrowsky_mask=wyrowsky,normalize=True)
    std_up = standard_deviation(I_field,I_target,where='up',wyrowsky_mask=wyrowsky,normalize=True)
    std_down = standard_deviation(I_field,I_target,where='down',wyrowsky_mask=wyrowsky,normalize=True)

    rmse_all = RMSE(I_field, I_target, where='all', wyrowsky_mask=wyrowsky,fast=fast)
    rmse_up = RMSE(I_field, I_target, where='up', wyrowsky_mask=wyrowsky,fast=fast)
    rmse_down = RMSE(I_field, I_target, where='down', wyrowsky_mask=wyrowsky,fast=fast)
    
    diff_eff = diffraction_efficiency(I_field, I_target, wyrowsky_mask=wyrowsky)

    if verbose:
        print("k = {:2.5f}".format(factor))
        print("k_check = {}".format(factor_check))
        print("std   = {:2.5f} {:2.5f} {:2.5f}".format(std_all, std_up, std_down))
        print("rmse  = {:2.5f} {:2.5f} {:2.5f} ".format(rmse_all[0], rmse_up[0],rmse_down[0]))
        print("diffraction efficiency = {:2.5f}".format(diff_eff))


    if DOE is not None:
        transmision = DOE.average_intensity()
        print("transmision = {:2.5f}".format(transmision))

        eff_total = efficiency_total(I_field, I_target, DOE)
        print("efficiency_total = {:2.5f}".format(eff_total))

    return diff_eff, rmse_up[0], rmse_down[0]


# def mean_square_error(result, target):
#    '''
#    Mean Square error of the designed DOE.

#    Parameters:
#    result (Scalar_mask XY or intensity): field or intensity distribution obtained with IFTA algorithm.
#    target (Scalar_mask XY): field or intesity distribution of target

#    Return:
#    (float) Mean square error of the DOE.

#    Reference:
#    Jahn, K., &#38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
#    https://doi.org/10.1016/j.optcom.2010.07.030
#    '''

#    if isinstance(target, Scalar_source_XY) or isinstance(
#            target, Scalar_field_XY) or isinstance(target, Scalar_mask_XY):

#        max_result = np.abs(result.u).max()
#        max_target = np.abs(target.u).max()

#        result_normalize = np.abs(result.u) / max_result
#        target_normalize = np.abs(target.u) / max_target

#    else:
#        max_result = result.max()
#        max_target = target.max()

#        result_normalize = result / max_result
#        target_normalize = target / max_target

#    NMSE = 1 / 4 * (((result_normalize - target_normalize)**2).mean())
# print(NMSE)
#    return NMSE * 100

#
