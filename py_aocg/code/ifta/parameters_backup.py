# !/usr/bin/env python3

from copy import deepcopy

from diffractio import np, plt


def make_binary(mask, low_level=0, high_level=1, cut_level=0.5):
    """Convert a np.array to binary mask

    Arguments:
        mask (np.array): mask with real values.
        low_level (float): low level.
        high_level (float): high level.
        cut_level (float): value to divide in low and high levels."""

    mask_new = np.zeros_like(mask)
    mask_new[mask < cut_level] = low_level
    mask_new[mask >= cut_level] = high_level
    return mask_new


# def compute_error(I_field, I_target, normalize=False):
#     """
#     Computer the error as the average difference of the absolute value between of the intensity at target and the intensity at the result.
#
#     Parameters:
#         I_field (numpy.array): intensity produced by the algorithms
#         I_target (numpy.array): intentisty at target
#         is_balanced (bool): If True, perform the comparison using a proportion parameter to avoid different intensity levels
#
#
#     Returns:
#         error: Mean difference between result and target.
#
#     """
#
#     error = (np.abs(I_field - I_target)).mean()
#
#     return error


def diffraction_efficiency(I_field, I_target, wyrowsky_mask=1):
    '''
    Diffraction efficiency of the designed DOE.

    Parameters:
    result (Scalar_mask XY): field or intensity distribution obtained with IFTA algorithm.
    target (Scalar_mask XY): field or intensity distribution of target.
    wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
    (float): Diffraction efficiency of the DOE

    Reference:
    Jahn, K., &#38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
    https://doi.org/10.1016/j.optcom.2010.07.030
    '''

    I_field = I_field * wyrowsky_mask

    I_region = I_field[I_target > 0.5].mean()
    I_field = I_field.mean()

    diffraction_efficiency = I_region / I_field

    return diffraction_efficiency


def standard_deviation(I_field, I_target, where='all', wyrowsky_mask=1):
    '''
    Standard deviation of the designed DOE.

    Parameters:
    result (Scalar_mask XY): Intensity distribution obtained with IFTA algorithm.
    target (Scalzar_mask XY): Intensity of the target.
    where (string) ('up'.'down' or 'all'):  If 'up' we calculate the standard_deviation inside the mask. If 'down' we calculate the
     standard_deviation outside the mask. If 'all' we calculate the standard_deviation off the whole mask.  Default: 'up'.
    wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
    (float): Standard deviation of the DOE
    '''

    I_field = I_field * wyrowsky_mask

    if where == 'up':
        std = np.std(I_field[I_target >= 0.5])

    elif where == 'down':
        std = np.std(I_field[I_target < 0.5])

    elif where == 'all':
        std = np.std(I_field)

    return std


# def mnse(I_field, I_target):
#     """
#     Computer the error as the average difference of the absolute value between of the intensity at target and the intensity at the result.
#
#     Parameters:
#         I_field (numpy.array): intensity produced by the algorithms
#         I_target (numpy.array): intentisty at target
#         is_balanced (bool): If True, perform the comparison using a proportion parameter to avoid different intensity levels
#
#     Reference:
#         K. Jahn and N. Bokor, “Intensity control of the focal spot by vectorial beam shaping,” Opt. Commun., vol. 283, no. 24, pp. 4859–4865, 2010, doi: 10.1016/j.optcom.2010.07.030.
#         Similar to Ec. 17
#
#     Returns:
#         error: Mean difference between result and target.
#
#     """
#
#     error = np.sqrt(
#         ((I_field / I_field.sum() - I_target / I_target.sum())**2).mean())
#
#     return error * 100


def parameters_IFTA(I_field,
                    I_target,
                    diffraction_eff=True,
                    std=True,
                    RMSE=True,
                    verbose=True):
    """
    Quality parameters of IFTA algorithm. Using this function we could calculate the diffraction efficiency, standard_deviation and root_mean_square_error of the designed DOE.

    Parameters:
    I_field (Scalar_mask XY): Intensity distribution obtained with IFTA algorithm.
    I_target (Scalar_mask XY): Target intesity distribution
    diffraction_eff (bool): True if you want to calculate the diffraction_eff of the designed DOE. Default: True
    std (bool): True if you want to calculate the standard deviation of the designed DOE. Default: True
    RMSE (bool): True if you want to calculate the root mean square error of the designed DOE. Default: True
    info_text (bool): If True the parameters appear with its name. If False the parameters appear as floats for use in a plot. Default: True


    Return:
    (list of strs or list of floats)  Quality parameters of the designed DOE.


    Reference (Diffraction Efficiency and Mean square error):
    Jahn, K., &#38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
    https://doi.org/10.1016/j.optcom.2010.07.030

    """

    parameters = []

    if diffraction_eff:
        eff = diffraction_efficiency(I_field, I_target)
        parameters.append(eff)

        if verbose:
            print('Diffraction efficiency = {:.6f}'.format(eff))

    if std:
        std = standard_deviation(I_field, I_target)
        parameters.append(std)
        if verbose:
            print('Standard deviation = {:.6f}'.format(std))

    if RMSE:
        RMSE = RMSE(I_field, I_target)
        parameters.append(RMSE)

        if verbose:
            print('Root Mean Square error = {}'.format(RMSE))

    return parameters


def verify_mask(mask,
                z,
                is_phase,
                is_binary,
                has_pupil=False,
                has_draw=False,
                has_axis=False,
                is_logarithm=False):
    """Computes the result of the algorithm in the far or near field. The mask is an amplitude mask.
    The mask can be one defined or one obtained from a
    If required it is converted to binary and/or phase

    Arguments:
        mask (Scalar_mask_XY): DOE to analyze
        z (None or float): If None computed at far field,else, distance for the near field
        is_phase (bool): If True, converts the mask to a phase mask
        is_binary (bool): If True, converts the mask to a binary mask
        has_pupil (bool): If True, implements a circular pupil
        has_draw (bool): If True, draw the result
        has_axis (bool): If True, implements axis, else, it is removed
        is_logarithm (bool): If True, performs a logarithm on intensity
    Returns:
        DOE_new (Scalar_mask_XY): Result mask of algorithm (binarized, phase, etc.)
        result (np.array): propagation of the mask
    """

    DOE_new = deepcopy(mask)

    if is_phase:
        if is_binary:
            DOE_new.u = np.exp(1j * np.pi * DOE_new.u)
        else:
            DOE_new.u = np.exp(2j * np.pi * DOE_new.u)

    if has_pupil:
        DOE_new.pupil()

    if z is None or z == 0:
        result = DOE_new.fft(new_field=True, shift=True, remove0=True)

    else:
        result = DOE_new.RS(z=z, new_field=True)

    if has_draw:
        result.draw(logarithm=is_logarithm)
        plt.axis('off')
        if has_axis is True:
            plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
        else:
            intensity = np.abs(result.u)**2
            if is_logarithm:
                intensity = np.log(intensity + is_logarithm)

    return DOE_new, result


def RMSE(I_field, I_target, where='all', wyrowsky_mask=1):
    '''
    Root Mean Square error of the designed DOE.

    Parameters:
        I_target (Scalar_mask XY or intensity): field or intesity distribution of target.
        I_field (Scalar_mask XY or intensity): field or intensity distribution obtained with IFTA algorithm.
        where (string) ('up'.'down' or 'all'):  If 'up' we calculate the standard_deviation inside the mask. If 'down' we calculate the
         standard_deviation outside the mask. If 'all' we calculate the standard_deviation off the whole mask.  Default: 'up'.
        wyrowsky_mask (Scalar_mask XY): Square wyrowsky mask.

    Return:
        (float) Root Mean square error of the DOE.

    Reference:
        Pengcheng Zhou, Yong Bi, Minyuan Sun, Hao Wang, Fang Li, and Yan Qi, "Image quality enhancement and computation acceleration of 3D holographic display using a symmetrical 3D GS algorithm,"
    Appl. Opt. 53, G209-G213 (2014)

    '''

    I_field = I_field * wyrowsky_mask

    if where == 'up':
        diff = ((I_field[I_target >= 0.5] / I_field[I_target >= 0.5].mean()) -
                (I_target[I_target >= 0.5] / I_target[I_target >= 0.5].mean()))**2
        RMSE = np.sqrt(diff.mean())

    elif where == 'down':
        diff = ((I_field[I_target < 0.5] / I_field[I_target < 0.5].mean()) -
                (I_target[I_target < 0.5] / I_target[I_target < 0.5].mean()))**2
        RMSE = np.sqrt(diff.mean())

    elif where == 'all':
        diff = ((I_field / I_field.mean()) - (I_target / I_target.mean()))**2

    diff = diff.sum()
    RMSE = np.sqrt(diff) / len(I_field)**2

    return RMSE


# def mean_square_error(result, target):
#    '''
#    Mean Square error of the designed DOE.

#    Parameters:
#    result (Scalar_mask XY or intensity): field or intensity distribution obtained with IFTA algorithm.
#    target (Scalar_mask XY): field or intesity distribution of target

#    Return:
#    (float) Mean square error of the DOE.

#    Reference:
#    Jahn, K., &#38; Bokor, N. (2010). Intensity control of the focal spot by vectorial beam shaping. Optics Communications, 283 (24), 4859–4865.
#    https://doi.org/10.1016/j.optcom.2010.07.030
#    '''

#    if isinstance(target, Scalar_source_XY) or isinstance(
#            target, Scalar_field_XY) or isinstance(target, Scalar_mask_XY):

#        max_result = np.abs(result.u).max()
#        max_target = np.abs(target.u).max()

#        result_normalize = np.abs(result.u) / max_result
#        target_normalize = np.abs(target.u) / max_target

#    else:
#        max_result = result.max()
#        max_target = target.max()

#        result_normalize = result / max_result
#        target_normalize = target / max_target

#    NMSE = 1 / 4 * (((result_normalize - target_normalize)**2).mean())
    # print(NMSE)
#    return NMSE * 100
