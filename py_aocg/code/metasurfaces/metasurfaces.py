import numpy as np

from diffractio.vector_masks_XY import Vector_mask_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.vector_sources_XY import Vector_source_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio import *

from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

from py_aocg.ifta.parameters import *
from py_aocg.ifta.algorithms import *

plt.set_cmap('seismic')



def import_LUT(filename,draw=True):
    """Import the data of the look up table.

    Args:
        filename (string): filename of the look up table.
        draw (bool): If True the phase differences of the LUT are shown. Default: True

    Return:
        phix (np.ndarray): 2D array with LUT x-phase difference.  
        phi_y (np.ndarray): 2D array with LUT y-phase difference.
        T (np.ndarray): 2D array LUT transmittance. 
        w (np.ndarray): 2D array with the metasurface dimensions in y-direction.
        l (np.ndarray): 2D array with the metasurface dimensions in x-direction.
    """
   
    phix = np.load(filename)['phasex']
    phiy = np.load(filename)['phasey']
    T = np.load(filename)['transmittance']
    w = np.load(filename)['w']
    l = np.load(filename)['l']

    #phiy = phix.transpose()

    if draw:
        plt.figure(figsize=(15,6))
        plt.subplot(1,3,1)
        plt.imshow(phix,interpolation='bicubic',origin='lower',cmap='jet',extent=[l.min(),l.max(),w.min(),w.max()])
        plt.title('$\phi_x$')
        plt.colorbar()

        plt.subplot(1,3,2)
        plt.imshow(phiy,interpolation='bicubic',origin='lower',cmap='jet',extent=[l.min(),l.max(),w.min(),w.max()])
        plt.title('$\phi_y$')
        plt.colorbar()

        plt.subplot(1,3,3)
        plt.imshow(np.real(T),interpolation='bicubic',origin='lower',cmap='jet',extent=[l.min(),l.max(),w.min(),w.max()])
        plt.title('$T$')
        plt.colorbar()

    return phix,phiy,T,w,l



# def reshapedata_2_list(phix,phiy,T,w,l,draw=True):
#     """Convert the 2D data of the LUT to 1D array to calculate the metasurface pattern.

#     Args:
#         phix (np.ndarray): LUT x-phase difference  
#         phi_y (np.ndarray): LUT y-phase difference
#         T (np.ndarray): 2D array LUT transmittance 
#         w (np.ndarray): 2D array with the metasurface dimensions in y-direction.
#         l (np.ndarray): 2D array with the metasurface dimensions in x-direction.
#         draw (bool): If True phase differences with respect to the dimensions of the metasurfaces are shown. Default: True.

#         Return:
#         phix (np.ndarray): 1D array with LUT x-phase difference.
#         phi_y (np.ndarray): 1D array with LUT y-phase difference.
#         T (np.ndarray): 1D array LUT transmittance. 
#         w (np.ndarray): 1D array with the metasurface dimensions in y-direction.
#         l (np.ndarray): 1D array with the metasurface dimensions in x-direction.
#     """

#     T_list = T.reshape(T.shape[0]*T.shape[1])

#     phix_list = phix.reshape(phix.shape[0]*phix.shape[1])
#     phiy_list = phiy.reshape(phiy.shape[0]*phiy.shape[1])

#     l_list = l.reshape(l.shape[0]*l.shape[1])
#     w_list = w.reshape(w.shape[0]*w.shape[1])

#     phix_list -= np.pi
#     phiy_list -= np.pi

#     if draw:
#         plt.figure()
#         plt.tricontour(phix_list,phiy_list,l_list,levels=8000,cmap='jet')
#         plt.colorbar()   
#         plt.xlabel('$\phi_x$ (rad)')
#         plt.ylabel('$\phi_y$ (rad)')

#         plt.figure()
#         plt.tricontour(phix_list,phiy_list,w_list,levels=8000,cmap='jet')
#         plt.colorbar()   
#         plt.xlabel('$\phi_x$ (rad)')
#         plt.ylabel('$\phi_y$ (rad)')

#     return phix_list,phiy_list,T_list,w_list,l_list



def calculate_pattern(phix_design,phiy_design,phix,phiy,T,w,l,draw=True):
    """Calculate the differences between the desired phase obtained with VIFTA and the possible ones (LUT). Then, we obtain the 2D arrays with the desired dimensions of the metasurfaces as well as the desired phase differences and the desired transmittance. 

    Args:
        phix_design (np.ndarray): Phase_x of the design obtained using VIFTA.
        phiy_design (np.ndarray): Phase_y of the design obtained using VIFTA.
        phix (np.ndarray): 1D array with LUT phase_x difference.
        phi_y (np.ndarray): 1D array with LUT phase_y difference.
        T (np.ndarray): 1D array LUT transmittance. 
        w (np.ndarray): 1D array with the metasurface dimensions in y-direction.
        l (np.ndarray): 1D array with the metasurface dimensions in x-direction.
        draw (bool): If True the final patterns of phase, transmittance and metasurface dimensiones are shown. Default: True.

    Returns:
        phix_desired (np.ndarray): Desired phase_x map.
        phiy_desired (np.ndarray): Desired phase_y map.
        T_desired (np.ndarray):  Desired transmittance map.
        w_desired (np.ndarray):  2D array with the metasurface dimensions in y-direction for further manufacturing.
        l_desired (np.ndarray): 2D array with the metasurface dimensions in x-direction for further manufacturing.
        
    """

    T_list = T.reshape(T.shape[0]*T.shape[1])

    phix_list = phix.reshape(phix.shape[0]*phix.shape[1])
    phiy_list = phiy.reshape(phiy.shape[0]*phiy.shape[1])

    l_list = l.reshape(l.shape[0]*l.shape[1])
    w_list = w.reshape(w.shape[0]*w.shape[1])

    phix_list -= np.pi
    phiy_list -= np.pi


    #Calculamos la diferencia entre las fases de la look-up-table y las fases requeridas, las del diseño. 
    distancias = np.sqrt((phix_design - phix_list[:,np.newaxis,np.newaxis])**2 + (phiy_design - phiy_list[:,np.newaxis,np.newaxis])**2)
    #Obtenemos el índice donde la distancia es mínima.
    index = distancias.argmin(axis=0)
    #A partir de este índice, obtenemos las dimensiones en x/y de la metasuperficie. Las dimensiones x/y se obtienen de las listas extruidas.
    phix_desired=phix_list[index]
    phiy_desired=phiy_list[index]
    T_desired = T_list[index]
    l_desired=l_list[index]
    w_desired=w_list[index]

    #Desfase próximo a pi/2:
    # phix_list = phix_list % (2*np.pi)
    # phiy_list = phiy_list % (2*np.pi)
    diff = phiy_list - phix_list
    diff_pi_2 = np.abs(np.pi/2-diff)
    print(diff_pi_2[25])
    index_pi2 = diff_pi_2.argmin()
    print(index_pi2,phix_list[index_pi2],phiy_list[index_pi2])

    if draw:

        plt.figure()
        plt.tricontour(phix_list,phiy_list,l_list,levels=8000,cmap='jet')
        plt.colorbar()   
        plt.xlabel('$\phi_x$ (rad)')
        plt.ylabel('$\phi_y$ (rad)')

        plt.figure()
        plt.tricontour(phix_list,phiy_list,w_list,levels=8000,cmap='jet')
        plt.colorbar()
        plt.xlabel('$\phi_x$ (rad)')
        plt.ylabel('$\phi_y$ (rad)')

        # plt.figure()
        # plt.plot(diff,'o')
        # plt.grid()
        # plt.yticks(ticks=[-4,-2,0,1,1.5,2,4])

        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(phix_desired,origin='lower',cmap='seismic')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.title('phi_x (x)')
        plt.subplot(1,2,2)
        plt.imshow(phiy_desired,origin='lower',cmap='seismic')
        plt.title('phi_y (x)')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)


        plt.figure()
        plt.imshow(T_desired,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.title('T')

        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(l_desired,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.title('w_x')


        plt.subplot(1,2,2)
        plt.imshow(w_desired,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.title('w_y')


    return phix_desired,phiy_desired,T_desired,w_desired,l_desired


def check_design(phix,phiy,theta,target,z):

    """
    Checks if the metasurface generate the desired polarization distribution.

        phix (np.ndarray): 2D array with the phase_x obtained from the function 'calculate_pattern'
        phiy (np.ndarray): 2D array with the phase_y obtained from the function 'calculate_pattern'  
        theta (np.ndarray): 2D array with the rotation angle of the metasurfaces.
        target (Scalar_mask_XY): Target of the VIFTA.
        z (float): Propagation distance.


    Returns:
        J_f (Jones_matrix): Result of the propogation of the metasurface.
    """

    components = [np.exp(1j*phix),0,0,np.exp(1j*phiy)]
    J = Jones_matrix().from_components(components)
    J.rotate(angle=theta)
    J.draw()

    J_f = RS_jones_matrix(J,target,z,pupil=True)
    J_f.remove_global_phase()
    J_f.draw(kind='jones', verbose=False)

    return J_f


def calculate_error(phix_design,phix_desired,phiy_design,phiy_desired,draw=False):
    """Calculate the error of the metasurface design respect to the VIFTA one.

    Args:
        phix_design (np.ndarray): 2D array of the phase_x obtained with VIFTA.
        phix_desired (np.ndarray): 2D array of the phase_x obtained from the LUT.
        phiy_design (np.ndarray): 2D array of the phase_y obtained with VIFTA.
        phiy_desired (np.ndarray): 2D array of the phase_y obtained from the LUT.
        draw (bool): If True phases of VIFTA and LUT are shown. Default: False.

    """

    if draw: 
        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(phix_design,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.title('Fase VIFTA (x)')
        plt.subplot(1,2,2)
        plt.imshow(phix_desired,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.title('Fase LUT (x)')


        plt.figure(figsize=(10,6))
        plt.subplot(1,2,1)
        plt.imshow(phiy_design,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.title('Fase VIFTA (y)')
        plt.subplot(1,2,2)
        plt.imshow(phiy_desired,origin='lower',cmap='viridis')
        plt.colorbar()
        plt.clim(-np.pi/2,np.pi/2)
        plt.title('Fase LUT (y)')

    phix_design = phix_design % (2*np.pi)
    phiy_design = phiy_design % (2*np.pi)

    phix_desired = phix_desired % (2*np.pi)
    phiy_desired = phiy_desired % (2*np.pi)

    epsilon_x = phix_design - phix_desired
    eps_x = np.abs(epsilon_x).mean()

    epsilon_y = phiy_design - phiy_desired
    eps_y = np.abs(epsilon_y).mean()

    print('Error_x {:.4f} rad'.format(eps_x))
    print('Error_y {:.4f} rad'.format(eps_y))

    print('Error_x {:.4f}'.format(eps_x/(2*np.pi)))
    print('Error_y {:.4f}'.format(eps_y/(2*np.pi)))

