
import matplotlib.pyplot as plt
from py_pol.mueller import Mueller, Stokes, degrees, np
from py_lab.utils import PSA_states_2_angles, PSG_states_2_angles
from itertools import product
from utils_multiprocessing import auxiliar_multiprocessing

# Estados
phi = (1+np.sqrt(5))/2
r3 = np.sqrt(3)
raiz = np.sqrt(1 + phi**2)

Tetraedro = np.array([[1, 1, 0, 0], [1, -1/3, np.sqrt(8)/3, 0], [1, -1/3, -np.sqrt(2)/3, np.sqrt(6)/3], [1, -1/3, -np.sqrt(2)/3, -np.sqrt(6)/3]]).T
Octaedro = np.array([[1, 1, 0, 0], [1, -1, 0, 0], [1, 0, 1, 0], [1, 0, -1, 0], [1, 0, 0, 1], [1, 0, 0, -1] ]).T
Cubo = np.array([[r3, 1, 1, 1], [r3, 1, 1, -1], [r3, 1, -1, 1], [r3, -1, 1, 1], [r3, -1, -1, 1], [r3, -1, 1, -1], [r3, 1, -1, -1], [r3, -1, -1, -1]]) / r3
Icosaedro = np.array([[raiz, 0, 1, phi], [raiz, 0, -1, phi], [raiz, 0, 1, -phi], [raiz, 0, -1, -phi], 
    [raiz, 1, phi, 0], [raiz, -1, phi, 0], [raiz, 1, -phi, 0], [raiz, -1, -phi, 0],
    [raiz, phi, 0, 1], [raiz, -phi, 0, 1], [raiz, phi, 0, -1], [raiz, -phi, 0, -1],
]) / raiz
Dodecaedro = np.array([[r3, 1, 1, 1], [r3, 1, 1, -1], [r3, 1, -1, 1], [r3, -1, 1, 1], [r3, -1, -1, 1], [r3, -1, 1, -1], [r3, 1, -1, -1], [r3, -1, -1, -1], 
    [r3, 0, phi, 1/phi], [r3, 0, -phi, 1/phi], [r3, 0, phi, -1/phi], [r3, 0, -phi, -1/phi], 
    [r3, 1/phi, 0, phi], [r3, -1/phi, 0, phi], [r3, 1/phi, 0, -phi], [r3, -1/phi, 0, -phi], 
    [r3, phi, 1/phi, 0], [r3, -phi, 1/phi, 0], [r3, phi, -1/phi, 0], [r3, -phi, -1/phi, 0], ]) / r3

S_4 = Stokes().from_matrix(Tetraedro)
S_6 = Stokes().from_matrix(Octaedro)
S_8 = Stokes().from_matrix(Cubo)
S_12 = Stokes().from_matrix(Icosaedro)
S_20 = Stokes().from_matrix(Dodecaedro)
# lista = [S_4, S_8, S_12, S_20]
lista = [S_4, S_6, S_8, S_12, S_20]
lista2 = []
for i1 in range(5):
    for i2 in range(i1+1,5):
        lista2.append((lista[i1], lista[i2]))

# angle_1_S4, angle_2_S4 = PSG_states_2_angles(S=S_4)
# angle_1_S6, angle_2_S6 = PSG_states_2_angles(S=S_6)
# angle_1_S8, angle_2_S8 = PSG_states_2_angles(S=S_8)
# angle_1_S12, angle_2_S12 = PSG_states_2_angles(S=S_12)
# angle_1_S20, angle_2_S20 = PSG_states_2_angles(S=S_20)

# angle_4_S4, angle_3_S4 = PSA_states_2_angles(S=S_4)
# angle_4_S6, angle_3_S6 = PSA_states_2_angles(S=S_6)
# angle_4_S8, angle_3_S8 = PSA_states_2_angles(S=S_6)
# angle_4_S12, angle_3_S12 = PSA_states_2_angles(S=S_12)
# angle_4_S20, angle_3_S20 = PSA_states_2_angles(S=S_20)



###########################
## CALCULO DE ERRORES
###########################
def CALCULO_DE_ERRORES():
    pass

def several_samples(dict_param):
    """Funcion para calcular errores usando diferentes muestras

    slopes = np.zeros((Ncases, NtypesError, Nparam))
    """    
    # Options
    dict_param["MP_verbose"] = False
    dict_param["return"] = True
    filename = dict_param["filename"]
    dict_param["filename"] = None
    
    # Extract info
    if dict_param["angles"] in ("random", "spiral"):
        Ncases = dict_param["NmeasuresArray"][2]
    elif dict_param["angles"] == "perfect":
        Ncases = len(lista)
    elif dict_param["angles"] == "perfect_mixed":
        Ncases = len(lista2)
    NtypesError = len(dict_param["error_arrays"])
    Nparam = 9

    # Manage samples
    Msamples = dict_param["samples"]
    dict_param.pop("samples")
    Nsamples = Msamples.size
    errors = np.zeros((Ncases, NtypesError, Nparam, Nsamples))
    for ind, sample in enumerate(Msamples):
        print("----------------------------------------------")
        print("Starting sample {} of {}".format(ind+1, Nsamples))
        dict_param["sample"] = sample
        errors[:,:,:,ind] = calculate_errors(dict_param)
        print("----------------------------------------------")

    # Save info
    dict_param.pop("Msample")
    np.savez(filename, errors=errors, samples=Msamples.M, **dict_param)

    # Draw
    draw_global(filename)


def calculate_errors(dict_param):
    """Funcion que calcula todos los errores"""
    # Safety
    if len(dict_param["error_arrays"]) != len(dict_param["error_labels"]):
        return ValueError("Error arrays and error labels have different number of elements")
    NtypesError = len(dict_param["error_arrays"])

    # Basic info
    MP = auxiliar_multiprocessing(verbose=dict_param["MP_verbose"])
    if dict_param["angles"] == "random":
        if dict_param["random_increase"] == "log":
            Nmin = np.log10(dict_param["NmeasuresArray"][0])
            Nmax = np.log10(dict_param["NmeasuresArray"][1])
            Nnumber = dict_param["NmeasuresArray"][2]
            cases = np.logspace(Nmin, Nmax, Nnumber, dtype=int)
        elif dict_param["random_increase"] == "quadratic":
            cases = np.linspace(*dict_param["NmeasuresArray"], dtype=int)**2
        else:
            cases = np.linspace(*dict_param["NmeasuresArray"], dtype=int)
    elif dict_param["angles"] == "perfect":
        cases = lista
    elif dict_param["angles"] == "perfect_mixed":
        cases = lista2
    elif dict_param["angles"] == "spiral":
        cases = np.linspace(dict_param["NmeasuresArray"][0], dict_param["NmeasuresArray"][1], dict_param["NmeasuresArray"][2], dtype=int)
    error_list = []
    for elem in dict_param["error_arrays"]:
        error_list.append(np.linspace(elem[0], elem[1], dict_param["Nerrors"]))
    error_final = []
    for ind, elem in enumerate(error_list):
        for value in elem:
            error = np.zeros(len(dict_param["error_arrays"]))
            error[ind] = value
            error_final.append(error)

    # Sample
    if isinstance(dict_param["sample"], str):
        Msample = Mueller().vacuum()
    else:
        Msample = dict_param["sample"]
        dict_param["sample"] = Msample.M
    _,_,_, params = Msample.analysis.decompose_polar(tol=0, give_all=True, transmissions="field", angles="azimuth")
    dict_param["Msample_p1"] = params["p1"]
    dict_param["Msample_p2"] = params["p2"]
    dict_param["Msample_az_P"] = params["azimuth D"]
    dict_param["Msample_el_P"] = params["ellipticity D"]
    dict_param["Msample_pol"] = Msample.parameters.degree_polarizance()
    dict_param["Msample_ret"] = params["R"]
    dict_param["Msample_az_R"] = params["azimuth R"]
    dict_param["Msampleel_R1"] = params["ellipticity R"]
    dict_param["Msample"] = Msample

    # Loop in cases
    Ncases = len(cases)
    shape = (NtypesError, dict_param["Nerrors"], 9)
    error = np.zeros((Ncases, NtypesError, dict_param["Nerrors"], 9))
    dev = np.zeros((Ncases, NtypesError, dict_param["Nerrors"], 9))
    Nmeasurements = np.zeros(Ncases)
    for ind, case in enumerate(cases):
        # Set angles type
        if dict_param["angles"] == "random":
            dict_param["Nmeasures"] = case
            # draw_legend.append("Rand. N = {}".format(case))
            Nmeasurements[ind] = case
        elif dict_param["angles"] == "perfect":
            dict_param["angle_1"], dict_param["angle_2"] = PSG_states_2_angles(S=case)
            dict_param["angle_4"], dict_param["angle_3"] = PSA_states_2_angles(S=case)
            # draw_legend.append("Perfect N = {}".format(case.size**2))
            Nmeasurements[ind] = case.size**2
        elif dict_param["angles"] == "perfect_mixed":
            dict_param["angle_1"], dict_param["angle_2"] = PSG_states_2_angles(S=case[0])
            dict_param["angle_4"], dict_param["angle_3"] = PSA_states_2_angles(S=case[1])
            # draw_legend.append("Perfect N = {}".format(case[0].size * case[1].size))
            Nmeasurements[ind] = case[0].size * case[1].size
        elif dict_param["angles"] == "spiral":
            _,_,_,S = fill_sphere_fibonacci(num_samples=case, kind_exit='Stokes', has_draw=False)
            Nmeasurements[ind] = case**2
            dict_param["angle_1"], dict_param["angle_2"] = PSG_states_2_angles(S=S)
            dict_param["angle_4"], dict_param["angle_3"] = PSA_states_2_angles(S=S)
        dict_param["Nmeasurements"] = Nmeasurements
            
        # Loop in errors
        result = MP.execute_multiprocessing(
                                function=calculate_one_case,
                                var_iterable=error_final,
                                dict_constants=dict_param,
                                Ncores=dict_param["Ncores"])
        result = np.array(result)
        error_case, dev_case = result[:, 0, :], result[:, 1, :]
        error[ind, :, :] = error_case.reshape(shape)
        dev[ind, :, :] = dev_case.reshape(shape)

    # Calculate slopes
    slopes = extract_slope_from_error(dict_param, error)

    # Save result
    if isinstance(dict_param["filename"], str):
        dict_param["Msample"] = dict_param["Msample"].M  # Obects cannot be saved easily
        np.savez(dict_param["filename"], error=error, dev=dev, slopes=slopes, **dict_param)

        # Draw result
        draw_result(dict_param["filename"])

    if dict_param["return"]:
        return slopes


def calculate_one_case(error_values, dict_param):
    """Funcion que calcula un caso particular del experimento de polarimetria con errores"""
    # Remove trivial error = 0 case
    if np.all(error_values == 0):
        return np.zeros(9), np.zeros(9)

    # Calcular angulos    
    if dict_param["angles"] in ("perfect", "perfect_mixed", "spiral"):
        angles_13 = product(dict_param["angle_1"], dict_param["angle_3"])
        angles_24 = product(dict_param["angle_2"], dict_param["angle_4"])
        N = dict_param["angle_1"].size * dict_param["angle_3"].size
        angles_1 = np.zeros(N)
        angles_2 = np.zeros(N)
        angles_3 = np.zeros(N)
        angles_4 = np.zeros(N)
        for ind, elem in enumerate(angles_13):
            angles_1[ind] = elem[0]
            angles_3[ind] = elem[1]
        for ind, elem in enumerate(angles_24):
            angles_2[ind] = elem[0]
            angles_4[ind] = elem[1]
        angles = np.array([angles_1, angles_2, angles_3, angles_4])
    elif dict_param["angles"] != "random":
        angles = np.array([dict_param["angle_1"], dict_param["angle_2"], dict_param["angle_3"], dict_param["angle_4"]])

    # Sistema
    Silum = Stokes().general_azimuth_ellipticity(azimuth=dict_param["default_source_azimuth"], ellipticity=dict_param["default_source_ellipticity"], intensity=2)
    p1 = dict_param["default_pol_p1"]
    p2 = dict_param["default_pol_p2"]
    Mp1 = Mueller().diattenuator_linear(p1=p1[0], p2=p2[0])
    Mp2 = Mueller().diattenuator_linear(p1=p1[1], p2=p2[1])
    trans_ret = dict_param["default_ret_trans"]
    ret = dict_param["default_ret_retardance"]
    Mr1 = Mueller().diattenuator_retarder_linear(R=ret[0], p1=trans_ret[0], p2=trans_ret[1])
    Mr2 = Mueller().diattenuator_retarder_linear(R=ret[1], p1=trans_ret[2], p2=trans_ret[3])
    system = [Silum, Mp1, Mr1, Mr2, Mp2]

    # Muestra
    Msample = dict_param["Msample"]
    
    # Errores
    error_dict = errors_dict_empty(dict_param)
    for ind, label in enumerate(dict_param["error_labels"]):
        error_dict[label] = error_values[ind]

    # Promediar experimento
    error_param = np.zeros((dict_param["Naverage"], 9))
    for ind in range(dict_param["Naverage"]):
        # Experimento virtual con errores
        if dict_param["angles"] == "random":
            angles = np.random.rand(4, dict_param["Nmeasures"])
        I = calculate_intensities(error_dict, angles, Msample)
        # Medir muestra
        W = angles_to_W(angles, system)
        try:
            rank = np.linalg.matrix_rank(W)
        except:
            rank = 16
            # continue
        if rank == 16:
            Wi = W_inv(W)
            Mmeasured = Wi @ I
            # Medir error
            error_param[ind, 8] = np.linalg.norm(Mmeasured - Msample.M.flatten())
            try:
                Mmeasured = Mueller().from_matrix(Mmeasured)
                _,_,_, params = Mmeasured.analysis.decompose_polar(tol=0, give_all=True, transmissions="field", angles="azimuth")
                error_param[ind, 0] = np.abs(dict_param["Msample_p1"] - params["p1"])
                error_param[ind, 1] = np.abs(dict_param["Msample_p2"] - params["p2"])
                aux = dict_param["Msample_az_P"] - params["azimuth D"]
                error_param[ind, 2] = np.min([np.abs(aux), np.abs(np.pi + aux), np.abs(np.pi - aux)])
                error_param[ind, 3] = np.abs(dict_param["Msample_el_P"] - params["ellipticity D"])
                error_param[ind, 4] = np.abs(dict_param["Msample_pol"] - Mmeasured.parameters.degree_polarizance())
                error_param[ind, 5] = np.abs(dict_param["Msample_ret"] - params["R"])
                aux = dict_param["Msample_az_R"] - params["azimuth R"]
                error_param[ind, 6] = np.min([np.abs(aux), np.abs(np.pi + aux), np.abs(np.pi - aux)])
                error_param[ind, 7] = np.abs(dict_param["Msampleel_R1"] - params["ellipticity R"])
            except:
                pass
            
        
    # Limpiar casos en que la matriz no es invertible
    error = np.zeros(9)
    dev = np.zeros(9)
    for ind in range(9):
        aux = error_param[:,ind]
        aux = aux[aux > 0]
        error[ind] = np.mean(aux)
        dev[ind] = np.std(aux)

    # Resultado
    return error, dev

def calculate_intensities(dict_param, angles, Msample):
    """Funcion que nos devuelve las medidas de intensidad simuladas del experimento. Tiene en cuenta los posibles errores en todas las variables del experimento."""
    # Angulos
    angle_1 = angles[0]
    angle_2 = angles[1]
    angle_3 = angles[2]
    angle_4 = angles[3]
    if dict_param["error_angles"] > 0:
        N = angles[0].size
        angle_1 = angle_1 + dict_param["error_angles"] * np.random.randn(N)
        angle_2 = angle_2 + dict_param["error_angles"] * np.random.randn(N)
        angle_3 = angle_3 + dict_param["error_angles"] * np.random.randn(N)
        angle_4 = angle_4 + dict_param["error_angles"] * np.random.randn(N)

    # Errores del sistema
    az = dict_param["error_azimuth"] * np.random.randn(4) if dict_param["error_azimuth"] > 0 else np.zeros(4)
    az_source = dict_param["default_source_azimuth"]
    el_source = dict_param["default_source_ellipticity"]
    if dict_param["error_source"] > 0:
        az_source = az_source + dict_param["error_source"] * np.random.randn()
        el_source = el_source + dict_param["error_source"] * np.random.randn()
        el_source = 90*degrees - el_source if el_source > 45*degrees else el_source
    p1 = dict_param["default_pol_p1"]
    if dict_param["error_p1"] > 0:
        p1 = p1 + dict_param["error_p1"] * np.random.randn(2)
    p2 = dict_param["default_pol_p2"]
    if dict_param["error_p2"] > 0:
        p2 = np.abs(p2 + dict_param["error_p2"] * np.random.randn(2))
    ret = dict_param["default_ret_retardance"]
    if dict_param["error_ret"] > 0:
        ret = ret + dict_param["error_ret"] * np.random.randn(2)
    trans_ret = dict_param["default_ret_trans"]
    if dict_param["error_trans_ret"] > 0:
        trans_ret = trans_ret + dict_param["error_trans_ret"] * np.random.randn(2)
    # Crear sistema con errores
    Silum = Stokes().general_azimuth_ellipticity(azimuth=az_source, ellipticity=el_source, intensity=2)
    Mp1 = Mueller().diattenuator_linear(azimuth=az[0] + angle_1, p1=p1[0], p2=p2[0])
    Mp2 = Mueller().diattenuator_linear(azimuth=az[3] + angle_4, p1=p1[1], p2=p2[1])
    Mr1 = Mueller().diattenuator_retarder_linear(azimuth=az[1] + angle_2, R=ret[0], p1=trans_ret[0], p2=trans_ret[1])
    Mr2 = Mueller().diattenuator_retarder_linear(azimuth=az[2] + angle_3, R=ret[1], p1=trans_ret[2], p2=trans_ret[3])

    # Experimento simulado
    Sfinal = Mp2 * (Mr2 * (Msample * (Mr1 * (Mp1 * Silum))))
    I = Sfinal.parameters.intensity()
    if dict_param["error_Iprop"] > 0:
        I = I * (1 + dict_param["error_Iprop"] * np.random.randn(*I.shape))
    if dict_param["error_Inoise"] > 0:
        I = I + dict_param["error_Inoise"] * np.random.randn(*I.shape)


    return I


def errors_dict_empty(dict_param):
    """Crea un diccionario de errores vacio."""
    error_dict = {}
    error_dict["error_angles"] = 0
    error_dict["error_azimuth"] = 0
    error_dict["error_source"] = 0
    error_dict["error_p1"] = 0
    error_dict["error_p2"] = 0
    error_dict["error_ret"] = 0
    error_dict["error_trans_ret"] = 0
    error_dict["error_Iprop"] = 0
    error_dict["error_Inoise"] = 0
    for key in dict_param.keys():
        if key.find("default") >= 0:
            error_dict[key] = dict_param[key]

    return error_dict


def extract_slope_from_error(dict_param, error=None):
    """Funcion para coger el array de errores y calcular la pendiente. Asumo que siempre tengo el error global y el descompuesto.
    error = np.zeros((Ncases, NtypesError, dict_param["Nerrors"], 9))
    """
    # Load data
    if isinstance(dict_param, str):
        dict_param = np.load(dict_param)
        error = dict_param["error"]
    Ncases, NtypesError, _, Nparam = error.shape
    error_list = []
    for elem in dict_param["error_arrays"]:
        error_list.append(np.linspace(elem[0], elem[1], dict_param["Nerrors"]))

    # Fit
    slopes = np.zeros((Ncases, NtypesError, Nparam))
    for ind1 in range(NtypesError):
        x = error_list[ind1]
        x = x[:,np.newaxis]
        for ind0 in range(Ncases):
            for ind2 in range(Nparam):
                y = error[ind0, ind1, :, ind2]
                try:
                    slopes[ind0, ind1, ind2] = np.linalg.lstsq(x, y)[0]
                except:
                    slopes[ind0, ind1, ind2] = np.nan

    return slopes
        
    
###########################
## DRAW
###########################
def DRAW():
    pass

def draw_result(filename):
    """Funcion para representar los resultados de un archivo"""
    # Load data
    dict_param = np.load(filename)
    error = dict_param["error"]
    dev = dict_param["dev"]
    Ncases, NtypesError = error.shape[0], error.shape[1]
    error_list = []
    for elem in dict_param["error_arrays"]:
        error_list.append(np.linspace(elem[0], elem[1], dict_param["Nerrors"]))

    # Number of cases
    Nmeasurements = dict_param["Nmeasurements"]
    draw_legend = []
    for ind, case in enumerate(Nmeasurements):
        if dict_param["angles"] == "random":
            draw_legend.append("Rand. N = {:.0f}".format(case))
        else:
            draw_legend.append("Perfect N = {:.0f}".format(case))

    # Draw result
    Errores = ("Error Pol. P1", "Error Pol. P2", "Error Pol. AZ (deg)", "Error Pol. EL", 
                "Error Deg. Polarizance", "Error, Ret. R (deg)", "Error, Ret. AZ (deg)", "Error, Ret. EL (deg)")
    draw_y_const = [1, 1, degrees, degrees, 1, degrees, degrees, degrees]
    for ind in range(NtypesError):
        # Global error
        if error.shape[-1] == 9:
            plt.figure(figsize=(8, 6))
            for indC in range(Ncases):
                plt.errorbar(error_list[ind] / dict_param["draw_x_const"][ind], 
                                error[indC,ind, :, -1], yerr=dev[indC,ind, :, -1], capsize=5)
            plt.ylabel("Global error")
            plt.legend(draw_legend)
            plt.xlabel("Error amplitude" if dict_param["draw_x_const"][ind] == 1 else "Error amplitude (deg)")
        # Rest of errors
        plt.figure(figsize=(12, 6))
        plt.suptitle(dict_param["error_labels"][ind])
        for indE in range(8):
            plt.subplot(2,4,indE+1)
            for indC in range(Ncases):
                plt.errorbar(error_list[ind] / dict_param["draw_x_const"][ind], 
                            error[indC,ind, :, indE] / draw_y_const[indE], 
                            yerr=dev[indC,ind, :, indE] / draw_y_const[indE], capsize=5)
            plt.ylabel(Errores[indE])
            if indE == 7:
                plt.legend(draw_legend)
            if indE > 3:
                plt.xlabel("Error amplitude" if dict_param["draw_x_const"][ind] == 1 else "Error amplitude (deg)")

def draw_global(filename):
    """Funcion para dibujar los resultados cuando hay varias muestras
    
    errors = np.zeros((Ncases, NtypesError, Nparam, Nsamples))
    """
    # Data
    Param = ("Pol. P1", "Pol. P2", "Pol. AZ (deg)", "Pol. EL", 
                "Deg. Polarizance", "Ret. R (deg)", "Ret. AZ (deg)", "Ret. EL (deg)",
                "Total")

    # Load data
    dict_param = np.load(filename)
    errors = dict_param["errors"]
    Ncases, NtypesError, Nparam, Nsamples = errors.shape

    # Get some data
    TypeErrors = []
    for string in dict_param["error_labels"]:
        TypeErrors.append(string[7:])
    cmap = dict_param["cmap"] if  "cmap" in dict_param.files else "inferno"
    xlabel = dict_param["xlabel"] if  "xlabel" in dict_param.files else "Sample index"
    ylabels = []
    for case in dict_param["Nmeasurements"]:
        ylabels.append(str(case))
    
    # Plot
    plt.figure(figsize=(3*Nparam, 3*NtypesError))
    for indP in range(Nparam):
        for indT in range(NtypesError):
            ind = indP * NtypesError + indT
            plt.subplot(Nparam, NtypesError, ind+1)
            plt.title(Param[indP] + " / " + TypeErrors[indT])
            plt.imshow(errors[:, indT, indP, :], cmap=cmap)
            plt.colorbar()
            if indT == 0:
                plt.ylabel("N. of meas.")
            if indP == Nparam - 1:
                plt.xlabel(xlabel)






###########################
## FUNCIONES AUXILIARES
###########################

def FUNCIONES_AUXILIARES():
    pass

def W_from_S(S_PSG, S_PSA):
    """Funcion para calcular la matriz W a partir de los estados del PSG y del PSA.

    Args:
        S_PSG (Stokes): States of the PSG.
        S_PSA (Stokes): States of the PSA.

    Returns:
        W (np.ndarray): W matrix.
    """
    # Prealocate matrix
    N = S_PSA.size * S_PSG.size
    W = np.zeros((N,16))

    # Calculate matrix
    ind = 0
    for ind_G in range(S_PSG.size):
        for ind_A in range(S_PSA.size):
            # Calculate row
            MA = S_PSA.M[:,ind_A, np.newaxis]
            MG = S_PSG.M[np.newaxis,:,ind_G]
            M = MA @ MG

            # Assign row
            W[ind,:] = 0.5 * M.flatten()
            ind += 1

    return W

def W_from_S_with_error(S_PSG, S_PSA, error):
    """Funcion para calcular la matriz W a partir de los estados del PSG y del PSA.

    Args:
        S_PSG (Stokes): States of the PSG.
        S_PSA (Stokes): States of the PSA.
        error (float): Error amplitude.

    Returns:
        W (np.ndarray): W matrix.
    """
    # Prealocate matrix
    N = S_PSA.size * S_PSG.size
    W = np.zeros((N,16))

    # Calculate matrix
    ind = 0
    for ind_G in range(S_PSG.size):
        s_PSG = S_PSG[ind_G]
        for ind_A in range(S_PSA.size):
            s_PSA = S_PSG[ind_A]

            # Add error
            az, el = s_PSG.parameters.azimuth_ellipticity()
            if np.isnan(az):
                az = np.random.rand() * 180*degrees
            az = az + error * np.random.randn()
            el = el + error * np.random.randn()
            s_PSG = Stokes().general_azimuth_ellipticity(azimuth=az, ellipticity=el)

            az, el = s_PSA.parameters.azimuth_ellipticity()
            if np.isnan(az):
                az = np.random.rand() * 180*degrees
            az = az + error * np.random.randn()
            el = el + error * np.random.randn()
            s_PSA = Stokes().general_azimuth_ellipticity(azimuth=az, ellipticity=el)

            # Calculate row
            MA = s_PSA.M[:,0, np.newaxis]
            MG = s_PSG.M[np.newaxis,:,0]
            M = MA @ MG

            # Assign row
            W[ind,:] = 0.5 * M.flatten()
            ind += 1

    return W

def W_inv(W):
    """Calculate the inverse or pseudoinverse matrix of W.

    Args:
        W (np.ndarray): W matrix to invert.

    Returns:
        Wi (np.ndarray): Inverse matrix.
    """
    # Caso N = 16
    if W.shape[0] == W.shape[1]:
        if np.linalg.matrix_rank(W) == W.shape[0]:
            Wi = np.linalg.inv(W)
        else:
            print("WARNING: Non-invertible matrix")
            return W
    # Casos N > 16
    else:
        Wi = np.linalg.inv(W.T @ W) @ W.T

    return Wi

def norma_Frobenius(W):
    """Norma de Frobenius.

    Args:
        W (np.ndarray): Matriz W

    Returns:
        n (float): Norma de la matriz.
    """
    return np.linalg.norm(W)

def norma_Vect2(W):
    """Norma vectorial de orden dos.

    Args:
        W (np.ndarray): Matriz W

    Returns:
        n (float): Norma de la matriz.
    """
    _, singular, _ = np.linalg.svd(W)
    return singular.max()/singular.min()

def condition_number(W, f_norma):
    """Calcula el condition number de una matriz NxM.

    Args:
        W (np.ndarray): Matriz W

    Returns:
        kappa (float): Condition number
    """
    Wi = W_inv(W)
    return f_norma(W) * f_norma(Wi) # / (W.shape[0]/16)**(1)

def put_in_limits(az, el):
    """Funcion para colocar el acimut y la elipticidad dentro de sus limites teniendo en cuenta cambios conjuntos.

    Args:
        az (np.ndarray): Acimut
        el (np.ndarray): Angulo de elipticidad.

    Returns:
        az, el (np.ndarray())
    """
    # Elipticidad
    aux = np.tan(el)
    cond = np.abs(aux) > 1
    if len(cond) > 0:
        el[cond] = np.arctan(1/aux[cond])
        az[cond] = az[cond] + 90*degrees
    # Acimut
    az = az % (180*degrees)
    return az,el

def angles_to_W(angles, system):
    S, P1, R1, R2, P2 = system
    angles = np.reshape(angles, (4,int(angles.size/4)))
    Mp1_rot = P1.rotate(angle=angles[0,:], keep=True)
    Mr1_rot = R1.rotate(angle=angles[1,:], keep=True)
    Mr2_rot = R2.rotate(angle=angles[2,:], keep=True)
    Mp2_rot = P2.rotate(angle=angles[3,:], keep=True)

    PSG = Mr1_rot * (Mp1_rot * S)
    PSA = Mp2_rot * Mr2_rot
    a = PSA.M[0,:,:]
    a = a.T[:, :, np.newaxis]
    g = PSG.M.T[:, np.newaxis, :]
    W = a @ g # Similar to matmul
    W = W.reshape((W.shape[0], 16))
    
    return W

def fill_sphere_fibonacci(num_samples=100, kind_exit='list', has_draw=False):
    """Generate a quasi - uniform distribution around the poincare sphere.

    Arguments:
        num_samples(int): number of samples.
        kind_exit(str): ('list', 'numpy_array', 'Stokes', 'Jones')

    Reference:
         https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    """
    golden_angle = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    i = np.linspace(0, num_samples, num_samples)
    y = 1 - (i / (num_samples - 1)) * 2
    y[y > 1] = 1
    y[y < -1] = -1
    radius = np.sqrt(1 - y**2)  # radius at y
    theta = golden_angle * i  # golden angle increment
    x = np.cos(theta) * radius
    z = np.sin(theta) * radius

    if kind_exit == 'list':
        return (x, y, z)
    elif kind_exit == 'numpy_array':
        return np.vstack((x, y, z)).transpose()
    elif kind_exit == 'Stokes':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        return x, y, z, S
    else:
        print("No kind_exit parameter")
        return None