import matplotlib.pyplot as plt
import numpy as np
from diffractio import degrees
from py_aocg.ifta.algorithms import distances_scalar, distances_scalar_circle
from py_aocg.ifta.parameters import (RMSE, diffraction_efficiency,
                                     standard_deviation)
from py_pol.jones_matrix import Jones_matrix
from py_pol.stokes import Stokes
from py_pol.jones_vector import Jones_vector
from py_pol.mueller import Mueller
from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.utils.plotters import (plot_contour, plot_cost_history,
                                     plot_surface)
from pyswarms.utils.plotters.formatters import Mesher
from py_lab.setups.polarimeter_utils import Calculate_Mueller, Msystem_From_Dict

from foronoi import Voronoi, Polygon, Visualizer, VoronoiObserver



# array([4.25905262, 1.4903734 , 1.64862246, 1.5590109 ]) Phase configuration angles for Holoeye SLM


def cargar_Jones_SLM(type="Jones", filename="SLM_Jones_components.npz"):
    """Funcion para cargar la calibración del SLM y convertirlo a un objeto py_pol.

    Args:
        type (str): Tipo de objeto, Jones o Mueller.
        filename (str): Archivo npz con la matriz de Jones del SLM

    Returns:
        M (Jones_matrix o Mueller): Resultado"""
    data = np.load(filename)
    components = [
        data["J0"] * np.exp(1j * data["d0"]),
        data["J1"] * np.exp(1j * data["d1"]),
        data["J2"] * np.exp(1j * data["d2"]),
        data["J3"] * np.exp(1j * data["d3"])
    ]
    M = Jones_matrix().from_components(components)

    if type.lower() == "mueller":
        M2 = Mueller().from_Jones(M)
        M = M2

    return M


def generar_Jones_simulada(phase_max=4 * np.pi):
    """Genera unas matrices de Jones simuladas para 256 niveles de gris).

    Args:
        phase_max (float): ángulo máximo

    Returns:
        py_pol.jones_matrix (0-256)
    """

    num_gl = 256
    gl = np.arange(0, num_gl, 1, dtype=int)
    #m00 = np.ones(num_gl, dtype=float)
    m00 = np.linspace(1, 0.25, num_gl)
    m01 = np.linspace(0, 1, num_gl)
    m10 = np.linspace(0, 1, num_gl)
    m11 = np.linspace(1, 0.25, num_gl) * np.exp(1j * phase_max * gl / num_gl)

    Jslm_simulation = Jones_matrix(name='simulation')
    Jslm_simulation.from_components((m00, m01, m10, m11))

    return Jslm_simulation


def generar_Jones_paper(phase_max=2 * np.pi):
    """Genera unas matrices de Jones simuladas para 256 niveles de gris. La
    creamos a partir de los parámetros (campo,azimut,retardance...). En este
    caso solo variamos el campo p2 de forma lineal (0.25-1) y la retardancia
    (0 - phase_max).

    Args:
        phase_max (float): ángulo máximo


    Returns:
        py_pol.jones_matrix (0-256)

    """

    num_gl = 256
    p1 = 1
    p2 = np.linspace(1, 0.25, num_gl)
    R = np.linspace(0, phase_max, num_gl)

    Jslm_simulation = Jones_matrix(name='simulation')
    Jslm_simulation.diattenuator_retarder_linear(p1=p1, p2=p2, R=R)

    return Jslm_simulation


def generar_Jones_phase(phase_max=4 * np.pi):
    """Genera unas matrices de Jones simuladas para 256 niveles de gris).
    Es un retardador puro (1,0,0,e(1j*phi)), donde phi es la fase lineal entre 0 y phase_max

    Args:
        phase_max (float): ángulo máximo

    Returns:
        py_pol.jones_matrix (0-256)
    """

    num_gl = 256
    gl = np.arange(0, num_gl, 1, dtype=int)
    m00 = np.ones(num_gl, dtype=float)
    #m00 = np.linspace(1,0.25,num_gl)
    m01 = np.zeros(num_gl, dtype=float)
    m10 = np.zeros(num_gl, dtype=float)
    m11 = np.exp(1j * phase_max * gl / num_gl)

    Jslm_simulation = Jones_matrix(name='simulation')
    Jslm_simulation.from_components((m00, m01, m10, m11))

    return Jslm_simulation


def SLM_system_field(angles, Jslm, E0=None,method='ideal',filename='Polarimeter.npz',param=None):
    """Function that calculates the transmission (amplitude and phase) of the PSG + SLM + PSA system.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.
        states (bool): If True, angles is [PSG_az, PSG_el, PSA_az, PSA_el] instead of rotation angles. Default: False.
        E0 (Jones_vector or None). Source Jones vector. If None, circular polarization is used.

    Returns:
        amplitude (np.ndarray): Amplitude of final state.
        phase (np.ndarray): Global phase of final state.
        E_final (np.ndarray): Electric field of final state.
    """
    if method == 'ideal':
        # Create objects
        if E0 is None:
            E0 = Jones_vector().circular_light(intensity=2)
        if np.isnan(angles[0]) or angles[0] < 0:
            P1 = Jones_matrix().vacuum()
        else:
            P1 = Jones_matrix().diattenuator_perfect(azimuth=angles[0])
        if np.isnan(angles[1]) or angles[1] < 0:
            Q1 = Jones_matrix().vacuum()
        else:
            Q1 = Jones_matrix().quarter_waveplate(azimuth=angles[1])
        if np.isnan(angles[2]) or angles[2] < 0:
            Q2 = Jones_matrix().vacuum()
        else:
            Q2 = Jones_matrix().quarter_waveplate(azimuth=angles[2])
        if np.isnan(angles[3]) or angles[3] < 0:
            P2 = Jones_matrix().vacuum()
        else:
            P2 = Jones_matrix().diattenuator_linear(azimuth=angles[3])

        PSA = P2 * Q2
        PSG = Q1 * P1 * E0

        if Jslm._type.lower() == "mueller":
            PSG = Stokes().from_Jones(PSG)
            PSA = Mueller().from_Jones(PSA)

        Efinal = PSA * (Jslm * PSG)

    elif method == 'real':
        cal_dict = np.load(filename)

        if Jslm._type == 'Mueller':
        
            E0, P1, Q1, Q2, P2 = Msystem_From_Dict(cal_dict,initial_angles=False)
            
            P1.rotate(angles[0])
            Q1.rotate(angles[1])
            Q2.rotate(angles[2])
            P2.rotate(angles[3])
        

        elif Jslm._type == 'Jones_matrix':
            E0 = Jones_vector().general_azimuth_ellipticity(intensity=cal_dict["S0"],azimuth=cal_dict["illum_az"],ellipticity=cal_dict["illum_el"])
            P1 = Jones_matrix().diattenuator_retarder_linear(p1=cal_dict["P1_p1"],p2=cal_dict["P1_p2"],azimuth=angles[0])
            Q1 = Jones_matrix().diattenuator_retarder_linear(p1=cal_dict["R1_p1"],p2=cal_dict["R1_p2"],R=cal_dict["R1_Ret"],azimuth=angles[1])
            Q2 = Jones_matrix().diattenuator_retarder_linear(p1=cal_dict["R2_p1"],p2=cal_dict["R2_p2"],R=cal_dict["R2_Ret"],azimuth=angles[2])
            P2 = Jones_matrix().diattenuator_retarder_linear(p1=cal_dict["P2_p1"],p2=cal_dict["P2_p2"],azimuth=angles[3])
        
        # Calculate final state
        PSA = P2 * Q2
        PSG = Q1 * P1 * E0

        Efinal = PSA * (Jslm * PSG)


    # Extract phase and amplitude
    phase = Efinal.parameters.global_phase(out_number=False)
    phase -= phase[0]
    #phase = phase % (2 * np.pi)
    phase = np.unwrap(phase) #Dar más libertad en la fase

    intensity= Efinal.parameters.intensity()
    
    amplitude = np.sqrt(intensity)
    amplitude = amplitude / amplitude.max()  # La espiral llegue a amplitud 1.

    return amplitude, phase, Efinal, intensity


def cost_phase(angles, Jslm,method,filename=None,param=None,phase_space=None,region=None):
    """
    Cost function to obtain a properly SLM phase configuration.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.

    Returns:
        cost: (float): Value of merit function of a SLM phase configuration.

    """

    amplitude, phase, _,_ = SLM_system_field(angles, Jslm, E0=None,method=method,filename=filename,param=param)
    cost = -(phase.max() - phase.min()) / np.std(amplitude)

    return cost


def cost_amplitude(angles, Jslm,method,filename=None,param=None,phase_space=None,region=None):
    """
    Cost function to obtain a properly SLM amplitude configuration.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.

    Returns:
        cost: (float): Value of merit function of a SLM phase configuration.

    """
    amplitude, phase, _,_ = SLM_system_field(angles, Jslm, E0=None,method=method,filename=filename,param=param)
    # print(amplitude[0])
    cost = -(amplitude.max() - amplitude.min()) / np.std(phase)  # *(1/(phase.mean()-0.0))

    return cost


def cost_function_distances(angles, Jslm, S_incident, method,param,phase_space,region=None):
    """ Cost function to obtain a properly SLM phase configuration

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.
        S_incident (Jones_vector): Illumination

    Returns:
        cost: (float): Value of merit function of a SLM phase configuration

    """

    u_fab_amplitude, u_fab_phase, fields = SLM_system_field(angles,
                                                            Jslm,
                                                            E0=S_incident,method=method,param=param)

    #plt.polar(u_fab_phase,u_fab_amplitude, 'bx', ms=6)
    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)
    # Normalizar
    u_fab = u_fab / u_fab.max()
    #phase_space = phase_space/phase_space.max()
    DOE_new, i_positions, distances = distances_scalar_circle(
        phase_space, u_fab)

    cost = distances.mean()
    #cost = np.linalg.norm(distances)

    return cost


def cost_area(angles,Jslm,S_incident,method,param,phase_space,region):

    u_fab_amplitude, u_fab_phase, _ = SLM_system_field(angles,Jslm,E0=S_incident,method=method,param=param)

    E = u_fab_amplitude*np.exp(1j*u_fab_phase) 

    points = np.zeros((256,2))
    points[:,0] = np.real(E)
    points[:,1] = np.imag(E)

    polygon = Polygon(region)
    v = Voronoi(polygon)
    v.create_diagram(points=points)

    sites = v.sites
    
    areas = np.zeros(len(points))
    for i in range(len(points)):
        areas[i] = sites[i].area()
    
    cost = areas.std()

    return cost


def merit_function(angles,Jslm,method,filename,param,phase_space,region):

    u_fab_amplitude, u_fab_phase, _, intensity = SLM_system_field(angles,Jslm,method=method,filename=filename,param=param)
    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)
    intensity = intensity.max()
    #Nromalizamos los sumandos para que los parámetros de peso de la función de mérito sean más intuitivos.
    norm1 = 1#0.411 #0.294  #factor para igualar el sumando de las distancias
    norm2 = 0.040 #factor para igualar el sumando de las areas

    #Factores de peso de la función de mérito.
    w_I = 0.02
    w_d = 1
    w_a = 0

    if w_a>0:
        points = np.zeros((256,2))
        points[:,0] = np.real(u_fab)
        points[:,1] = np.imag(u_fab)
        polygon = Polygon(region)
        v = Voronoi(polygon)
        v.create_diagram(points=points)

        sites = v.sites
        
        areas = np.zeros(len(points))
        for i in range(len(points)):
            areas[i] = sites[i].area()

    
    DOE_new, i_positions, distances = distances_scalar_circle(phase_space, u_fab)
    #print(intensity,distances.mean())


    if w_a > 0:
        cost = w_I *(1/intensity) + w_d/norm1 *distances.mean() + w_a/norm2 *areas.std()
        #print(w_I *(1/intensity), w_d/norm1 *distances.mean(),w_a/norm2 *areas.std())
        
    else:
        #print(w_I *(1/intensity),w_d/norm1*distances.mean())
        cost = w_I *(1/intensity) + w_d/norm1*distances.mean()
        #cost = w_d/norm1*distances.mean()
        #cost = - (intensity.max() - intensity.min()) / distances.mean()
        #print(w_I *(1/intensity),w_d/norm1*distances.mean())

    return cost



def cost_function_distances_backup(angles, Jslm, S_incident, DOE):
    """ Cost function to obtain a properly SLM phase configuration

def optimization_loop(angles, Jslm, S_incident, phase_space, optimize_function):
    """

    u_fab_amplitude, u_fab_phase, fields = SLM_system_field(angles,
                                                            Jslm,
                                                            E0=S_incident)

    #plt.polar(u_fab_phase,u_fab_amplitude, 'bx', ms=6)
    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)
    # Normalizar
    #u_fab = u_fab / u_fab.max()
    #phase_space = phase_space/phase_space.max()
    DOE_new, i_positions, distances = distances_scalar(
        DOE, u_fab)

    cost = distances.mean()
    #cost = np.linalg.norm(distances)

    return cost

def optimization_loop(angles, Jslm, method,filename,param,phase_space,region,
                      optimize_function):
    """
    Optimization loop of the particules of PySwarms algorithm.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.
        optimize_function (function): Function to optimize.

    Returns:
        cost: (float): Value of merit function

    """
    num_particles = angles.shape[0]
    cost = [
        optimize_function(angles[i, :], Jslm,method,filename,param,phase_space,region)
        for i in range(num_particles)
    ]

    return cost


def optimization_loop_standard(angles, Jslm, optimize_function):
    """
    Optimization loop of the particules of PySwarms algorithm.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.
        optimize_function (function): Function to optimize.

    Returns:
        cost: (float): Value of merit function

    """

    num_particles = angles.shape[0]
    cost = [
        optimize_function(angles[i, :], Jslm) for i in range(num_particles)
    ]

    return cost


def circular_phase_space(size_x, size_y, has_draw=True):
    """
    Generate circular phase space, limiting the points whose amplitude is greater than 1.

    Parameters:
    size_x (int): X-dimension of phase space.
    size_y(int): Y-dimension of phase space.
    has_draw (bool): If True phase space is shown. Default: True

    Return:
    phase_space (np.array): Phase space points
    """
    space_real = np.linspace(-1, 1, size_x)
    space_imag = np.linspace(-1, 1, size_y)

    SPACE_real, SPACE_imag = np.meshgrid(space_real, space_imag)
    phase_space = SPACE_real + 1j * SPACE_imag

    amplitud = np.abs(phase_space)
    phase = np.angle(phase_space)

    amplitud = np.where(amplitud <= 1, amplitud, np.nan)

    phase_space = amplitud * np.exp(1j * phase)

    cond = np.logical_not(np.isnan(phase_space))
    # print(cond.shape)
    SPACE_real = np.real(phase_space[cond])
    SPACE_imag = np.imag(phase_space[cond])

    phase_space = SPACE_real + 1j * SPACE_imag

    if has_draw:
        plt.figure()
        plt.plot(SPACE_real, SPACE_imag, 'k.')

    return phase_space


def draw_phase_space(u_fab, title='', suptitle='', filename=''):
    """Dibuja el espacio de fases del SLM_system_field

    Arguments:
        u_fab (np.array): array con la amplitud u(gl)*exp[1j fase(gl)]

    """
    u_fab_phase = np.angle(u_fab)
    u_fab_amp = np.abs(u_fab)
    plt.figure()
    plt.polar(u_fab_phase, u_fab_amp, 'k.')
    plt.suptitle(suptitle, fontsize=16)
    plt.title(title, fontsize=16, x=0.95, y=0.975)

    if filename != '':
        plt.savefig(filename, bbox_inches='tight', pad_inches=0)


def SLM_amplitude_space(num_gl=256):
    """Genera modulación de un SLM en modo pura amplitud.

    Arguments:
        num_gl (float): número de niveles de gris del SLM.

    Returns:
        u_fab (np.array): array con la amplitud u(gl)*exp[1j fase(gl)]
        gls  (np.array): np.array([0, 1, ... num_gl])
        gls1 (np.array): np.array([0, ... 1])
    """

    gls = np.arange(0, num_gl)
    gls1 = gls / gls.max()
    u_fab_amplitude = gls1
    u_fab_phase = np.zeros_like(gls1)
    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)

    return u_fab, gls, gls1


def SLM_phase_space(num_gl=256):
    """Genera modulación de un SLM en modo pura fase.

    Arguments:
        num_gl (float): número de niveles de gris del SLM.

    Returns:
        u_fab (np.array): array con la amplitud u(gl)*exp[1j fase(gl)]
        gls  (np.array): np.array([0, 1, ... num_gl])
        gls1 (np.array): np.array([0, ... 1])
    """

    gls = np.arange(0, num_gl)
    gls1 = gls / gls.max()
    u_fab_amplitude = np.ones_like(gls1)
    u_fab_phase = 2 * np.pi * gls1
    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)

    return u_fab, gls, gls1


def SLM_spiral_space(phase_max, num_gl=256, is_sqrt=True):
    """Genera modulación de un SLM en modo espiral.

    Arguments:
        num_gl (float): número de niveles de gris del SLM.

    Returns:
        u_fab (np.array): array con la amplitud u(gl)*exp[1j fase(gl)]
        gls  (np.array): np.array([0, 1, ... num_gl])
        gls1 (np.array): np.array([0, ... 1])
    """

    gls = np.arange(0, num_gl)
    gls1 = gls / gls.max()

    if is_sqrt:
        u_fab_amplitude = np.sqrt(gls1)
        u_fab_phase = phase_max * np.sqrt(gls1)
    else:
        u_fab_amplitude = gls1
        u_fab_phase = phase_max * gls1

    u_fab = u_fab_amplitude * np.exp(1j * u_fab_phase)

    return u_fab, gls, gls1
