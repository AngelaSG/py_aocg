import numpy as np
import matplotlib.pyplot as plt


from diffractio import degrees
from py_pol.jones_matrix import Jones_matrix,Jones_vector
from py_pol.mueller import Mueller


from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.utils.plotters import (plot_cost_history, plot_contour, plot_surface)
from pyswarms.utils.plotters.formatters import Mesher

#array([4.25905262, 1.4903734 , 1.64862246, 1.5590109 ]) Phase configuration angles for Holoeye SLM.
#array([1.16034707, 1.53068947, 1.70460196, 1.57806774]) Other Phase configuration angles for Holoeye SLM.
#array([3.01195137, 2.74480665, 2.88036642, 0.79200517]) Amplitude configuration angles for Holoeye SLM.

def cargar_Jones_SLM(type="Jones"):
    """Funcion para cargar la calibración del SLM y convertirlo a un objeto py_pol.

    Args:
        type (str): Tipo de objeto, Jones o Mueller.

    Returns:
        M (Jones_matrix o Mueller): Resultado"""
    data = np.load("SLM_Jones_components.npz")
    components = [data["J0"] * np.exp(1j * data["d0"]),
                 data["J1"] * np.exp(1j * data["d1"]),
                 data["J2"] * np.exp(1j * data["d2"]),
                 data["J3"] * np.exp(1j * data["d3"])]
    M = Jones_matrix().from_components(components)

    if type.lower() == "mueller":
        M2 = Mueller().from_Jones(M)
        M = M2

    return M



def SLM_system_field(angles, Jslm, E0=None):
    """Function that calculates the transmission (amplitude and phase) of the PSG + SLM + PSA system.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.
        states (bool): If True, angles is [PSG_az, PSG_el, PSA_az, PSA_el] instead of rotation angles. Default: False.
        E0 (Jones_vector or None). Source Jones vector. If None, circular polarization is used.

    Returns:
        amplitude (np.ndarray): Amplitude of final state.
        phase (np.ndarray): Global phase of final state.
        E_final (np.ndarray): Electric field of final state.
    """
    # Create objects
    if E0 is None:
        E0 = Jones_vector().circular_light(intensity=2)

    if np.isnan(angles[0]) or angles[0] < 0:
        P1 = Jones_matrix().vacuum()
    else:
        P1 = Jones_matrix().diattenuator_perfect(azimuth=angles[0])
    if np.isnan(angles[1]) or angles[1] < 0:
        Q1 = Jones_matrix().vacuum()
    else:
        Q1 = Jones_matrix().quarter_waveplate(azimuth=angles[1])
    if np.isnan(angles[2]) or angles[2] < 0:
        Q2 = Jones_matrix().vacuum()
    else:
        Q2 = Jones_matrix().quarter_waveplate(azimuth=angles[2])
    if np.isnan(angles[3]) or angles[3] < 0:
        P2 = Jones_matrix().vacuum()
    else:
        P2 = Jones_matrix().diattenuator_perfect(azimuth=angles[3])
    # print(P1, Q1, Q2, P2, sep='\n')

    # Calculate final state
    Efinal = (P2 * Q2) * (Jslm * (Q1 * P1 * E0))

    # Extract phase and amplitude
    phase = Efinal.parameters.global_phase()
    phase -= phase[0]
    phase = phase % (2 * np.pi)
    phase = np.unwrap(phase)
    intensity = Efinal.parameters.intensity()
    amplitude = np.sqrt(intensity)

    return amplitude, phase, Efinal


def cost_phase(angles,Jslm):
    """
    Cost function to obtain a properly SLM phase configuration.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.

    Returns:
        cost: (float): Value of merit function of a SLM phase configuration.

    """

    amplitude,phase,_ = SLM_system_field(angles, Jslm, E0=None)
    cost = -(phase.max() - phase.min())/np.std(amplitude)

    return cost


def cost_amplitude(angles,Jslm):
    """
    Cost function to obtain a properly SLM amplitude configuration.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.

    Returns:
        cost: (float): Value of merit function of a SLM phase configuration.

    """
    amplitude,phase,_ = SLM_system_field(angles, Jslm, E0=None)
    cost = -(amplitude.max()-amplitude.min())/np.std(phase)
    return cost


def optimization_loop(angles,Jslm,optimize_function):
    """
    Optimization loop of the particules of PySwarms algorithm.

    Args:
        angles: (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm: (Jones_matrix): Jones matrix of the SLM.
        optimize_function (function): Function to optimize.

    Returns:
        cost: (float): Value of merit function

    """
    n_particles = angles.shape[0]
    cost = [cost_phase(angles[i,:],Jslm) for i in range(n_particles)]

    return cost



def optimize_SLM_system(merit_function,Jslm,iters,n_particles,dimensions,options={'c1': 0.5, 'c2': 0.6, 'w': 0.9},bounds,optimization_loop):

    """
    This function search the best angles for a certain configuration of a SLM system.

    Args:
        merit_function: (function): Merit function to obtain an amplitude or phase configuration.
        Jslm: (Jones_matrix): Jones matrix of the SLM.
        iters: (int): Number of iterations.
        n_particles: (int): Number of particles.
        dimensions: (int): Number of dimensions of our optimization.
        options: (dict): Dictionary with the options of PySwarms algorithm. Example: options = {'c1': 0.5, 'c2': 0.6, 'w': 0.9}
        bounds: (np.array,np.array): Maximum and minimum bounds of the angles. Example: min_bound = np.zeros(dimensions); max_bound = np.ones(dimensions); bounds = (min_bound, max_bound).
        optimize_loop (function): Necessary function to use PySwarms algorithm.

    Returns:
        cost: (float): Value of merit function
        angles: (np.array): Best angles for a certain SLM configuration.


    """
    optimizer = GlobalBestPSO(n_particles=n_particles, dimensions=dimensions, options=options, bounds=bounds)
    cost, angles = optimizer.optimize(optimization_loop,iters,Jslm=Jslm,optimize_function=merit_function)

    return cost,angles
