# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Se crea la simulación de un LCD_jones_matrices, según la tesis de Máster de Airel Burman
"Caracterización y control de un microdisplay como modulador espacial de luz"
Universidad de buenos Aires (2010).

Aquí se simula el comportamiento de un único pixel.
Esto nos servirá posteriormente para que, una vez caracterizado el LCD_jones_matrices,
podamos utilizarlo  para simular LCD_jones_matrices, con una matrix de píxeles.

Los parámetros son los parámetros que caracterizan el modulador
    * gl: levels de gris
    * alfa: angle de giro (twist) ángulo total de rotation de las moléculas
    * beta(V): phase producido definido como np.pi*(ne-no)*d/lambda
    * psi:  eje director de la cara de entrada
    * delta: phase
    * wavelength: longitud de onda a la que se trabaja
"""

import matplotlib.pyplot as plt
from numpy import cos, sin
from py_pol import degrees, np
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector
from scipy import interpolate
from scipy.interpolate.interpolate import interp1d

um = 1.

"""
MODELO PARA PAPER RARO DE AMPLITUD-FASE CON REDES
"""


gl256 = np.array(range(256))


def beta_burman(gl, betaMax=100 * degrees, betaMin=5 * degrees):
    """ kind de variación  de beta con el level de gris.
    Esta gráfica debería ser experimental, pero de momento me baso en la página 57 (Fig. 2.22)
    - betaMax: level máximo de ángulo
    - betaMin: level mínimo de ángulo
    """
    b = (betaMin + betaMax) / 2 + (betaMax - betaMin) * np.cos(np.pi * gl /
                                                               (gl.max())) / 2
    return b


def delta_burman(gl, deltaMax=20 * degrees, deltaMin=5 * degrees):
    """ kind de variación  de delta con el level de gris.
    Esta gráfica debería ser experimental, pero de momentome baso en la página 58 (Fig. 2.23)
    - deltaMax: level máximo de ángulo
    - deltaMin: level mínimo de ángulo
    """

    delta = deltaMin + (deltaMax - deltaMin) * \
        np.sin(np.pi * gl * (1.5 * gl.max()))**2

    return delta


"""
Modelo de la tesis de burman"""
Burman = {
    'gl': gl256,
    'alfa': -79.5 *
    degrees,
    'beta':
    beta_burman(
        gl=gl256, betaMax=100 * degrees, betaMin=5 * degrees),
    'psi': -1.8 *
    degrees,
    'delta':
    delta_burman(
        gl=gl256, deltaMax=20 * degrees,
        deltaMin=2.5 * degrees),
    'wavelength': 0.6328 * um,

}

"""modelo experimental de modulador de proyector SONY"""
beta_maxima = 134.540877676 * degrees
beta0 = np.array([
    -14.75115406, -15.71686778, -19.99936453, -24.75304376, -30.97099113,
    -40.01031685, -49.44003965, -62.81038974, -77.923332, -91.36725938,
    -105.11971535, -111.51588345, -117.02710469, -129.0265514, -131.70797705,
    -131.84139714
]) * degrees
psi = 32.5008547753 * degrees
delta0 = np.array([
    26.65533244, 41.85755408, 57.73401392, 64.96715132, 71.27024504,
    77.23831921, 85.12128394, 94.25451813, 105.61446543, 116.38728704,
    127.21404866, 132.87248308, 137.27601942, 149.1519894, 152.17715049,
    152.35101835
]) * degrees
error = [
    4.41090251, 0.28419206, 0.36585532, 0.33161884, 0.41375047, 0.5350761,
    0.64365905, 0.80031859, 0.87722312, 0.8363581, 0.77251774, 0.51642338,
    0.4330389, 0.58729751, 0.2515351, 0.2192197
]

gl0 = np.linspace(0, 255, 16)

f_beta_sony = interp1d(gl0, beta0, kind='linear')
beta_sony = f_beta_sony(gl256)

f_delta_sony = interp1d(gl0, delta0, kind='linear')
delta_sony = f_delta_sony(gl256)


Sony = {
    'gl': gl256,
    'alfa': -90.7832688192 * degrees,
    'beta': beta_sony,
    'psi': 32.50 * degrees,
    'delta': delta_sony,
    'wavelength': 0.6328 * um,
}


class LCD_model(object):

    def __init__(self):
        pass

    def load_theoretical_model(self, model):
        """Se crea la simulación de un LCD_jones_matrices, según la tesis de Máster de Airel Burman
        'Caracterización y control de un microdisplay como modulador espacial de luz'
        Universidad de buenos Aires (2010).

        Los parámetros son los parámetros que caracterizan el modulador
        * gl: levels de gris
        * alfa: angle de giro (twist) ángulo total de rotation de las moléculas a través del material
        * beta(V): phase producido definido como np.pi*(ne-no)*d/lambda
        * psi:  eje director de la cara de entrada
        * delta: phase
        * wavelength: longitud de onda a la que se trabaja
        """

        self.alfa = model['alfa']
        self.gl = model['gl']
        self.beta = model['beta']
        self.psi = model['psi']
        self.delta = model['delta']
        self.wavelength = model['wavelength']  # la longitud de onda
        self.Jones_matrices_gl = None

    def get_matrices(self):
        gl0 = np.array(range(256))

        alfa = self.alfa
        beta = self.beta[gl0]
        Gamma = np.sqrt(alfa**2 + beta**2)

        LC = np.matrix(
            np.array([[
                cos(Gamma) - 1.j * beta * sin(Gamma) / Gamma,
                alfa * sin(Gamma) / Gamma
            ],
                [
                -alfa * sin(Gamma) / Gamma,
                cos(Gamma) + 1.j * beta * sin(Gamma) / Gamma
            ]]),
            dtype=complex)

        DS = rotation(angle=-self.psi) * rotation(
            angle=-self.alfa) * self.LC(gl0) * rotation(angle=self.psi)

        delta = self.delta[gl0]

        m0 = Jones_matrix()

        S = (m0.retarder_linear(R=delta, azimuth=0 * degrees))

        dc = rotation(angle=-self.psi) * rotation(
            angle=-self.alfa) * S.M * LC * S.M * rotation(
                angle=self.psi)

        matrices = np.zeros((2, 2, len(self.gl)), dtype=complex)
        for i, gli in enumerate(self.gl):
            matrices[:, :, i] = self.DC0(gli)

        _, _, num_gl = matrices.shape

        m0 = Jones_matrix(name="SLM")
        m0.from_matrix(matrices)

        self.Jones_matrices_gl = m0

        return m0


def LC(self, gl0):
    """matrix LC según pág 39 de la tesis

        Arguments:
                gl0 (int): number of grey levels

        Returns:
                LC_matrix (np.matrix): 2x2 matrix
                """

    alfa = self.alfa
    beta = self.beta[gl0]
    Gamma = np.sqrt(alfa**2 + beta**2)

    return np.matrix(
        np.array([[
            cos(Gamma) - 1.j * beta * sin(Gamma) / Gamma,
            alfa * sin(Gamma) / Gamma
        ],
            [
            -alfa * sin(Gamma) / Gamma,
            cos(Gamma) + 1.j * beta * sin(Gamma) / Gamma
        ]]),
        dtype=complex)


def DS(self, gl0):
    """matrix del microDisplay D_s (LCD_jones_matrices) al aplicar voltaje model de saleh-LU, según ec. 2.42  pág 39
            Arguments:
                    gl0 (int): number of grey levels

            Returns:
                    DS_matrix (np.matrix): 2x2 matrix
            """

    return rotation(angle=-self.psi) * rotation(
        angle=-self.alfa) * self.LC(gl0) * rotation(angle=self.psi)


def DC0(self, gl0):
    """Simulacion del LCD_jones_matrices, a través de la matrix. Está definido en las ecuaciones A.33 (pag 112)
            Arguments:
                    gl0 (int): number of grey levels

            Returns:
                    DS_matrix (np.matrix): 2x2 matrix
            """

    delta = self.delta[gl0]

    m0 = Jones_matrix()

    S = (m0.retarder_linear(R=delta, azimuth=0 * degrees))
    dc = rotation(angle=-self.psi) * rotation(
        angle=-self.alfa) * S.M * self.LC(gl0) * S.M * rotation(
            angle=self.psi)

    return dc


def DC(self, filename=''):
    """Simulacion del LCD_jones_matrices, a través de la matrix. Está definido en las ecuaciones A.33 (pag 112).

            Arguments:
                    filename (str): filename to save data (if not '').

            Returns:
                    DS_matrix (np.matrix): 2x2 xnum_gl matrix
            """

    matrices = np.zeros((2, 2, len(self.gl)), dtype=complex)
    for i, gli in enumerate(self.gl):
        matrices[:, :, i] = self.DC0(gli)

    if filename != '':
        np.savez(filename, DC=matrices)

    return matrices


def DC_to_pypol(self, filename=''):
    """Simulacion del LCD_jones_matrices, a través de la matrix. Está definido en las ecuaciones A.33 (pag 112).

            Arguments:
                    filename (str): filename to save data (if not '').

            Returns:
                    m0 (py_pol.Jones_matrix): Single Jones_matrix element with all the simulations.
            """

    matrices = self.DC()

    _, _, num_gl = matrices.shape

    m0 = Jones_matrix(name="SLM")
    m0.from_matrix(matrices)

    self.Jones_matrices_gl = m0

    return m0

    def polarimeter(self, polarizers, angles, source, has_draw=False):
        """Develops the result from a polarimeter.

        Argumements:
            polarizers (array of 4 Jones vectors): Polaizers for PL1, WP2, WP3, WP4
            angles (np.array): 4x1 array with the angles of the polarizers. if an angle is np.nan, this polarizer is not used
            source (Jones_vector): Jones Vector of light source
        Returns:


        """

        pol1, pol2, pol3, pol4 = polarizers

        # if pol1 is not None:
        if np.isnan(angles[0]):
            pol1_r = P_unity
        else:
            pol1_r = pol1.rotate(angle=angles[0], keep=True)

        # if pol2 is not None:
        if np.isnan(angles[1]):
            pol2_r = P_unity
        else:
            pol2_r = pol2.rotate(angle=angles[1], keep=True)

        # if pol3 is not None:
        if np.isnan(angles[2]):
            pol3_r = P_unity
        else:
            pol3_r = pol3.rotate(angle=angles[2], keep=True)

        # if pol4 is not None:
        if np.isnan(angles[3]):
            pol4_r = P_unity
        else:
            pol4_r = pol4.rotate(angle=angles[3], keep=True)

        matrices_pypol = self.DC_to_pypol()

        fields_gl = []
        num_data = matrices_pypol.shape
        fases_array = np.zeros(num_data, dtype=float)
        amplitudes_array = np.zeros(num_data, dtype=float)
        intensities_array = np.zeros(num_data, dtype=float)

        # esto necesita una mejora
        for i, matriz_i in enumerate(matrices_pypol):
            salida = pol4_r * pol3_r * \
                matrices_pypol[i] * pol2_r * pol1_r * source
            fields_gl.append(salida)

            phases = salida.parameters.global_phase(draw=False, verbose=False)
            #phases = phases - phases.min()
            #phases = np.unwrap(phases - phases[0])
            #phases = phases - phases.min()
            intensities = salida.parameters.intensity(
                draw=False, verbose=False)
            amplitudes = np.sqrt(intensities)

            fases_array[i] = phases
            amplitudes_array[i] = amplitudes
            intensities_array[i] = intensities

        fases_array = fases_array - fases_array.min()
        fases_array = np.unwrap(fases_array - fases_array[0])
        fases_array = fases_array - fases_array.min()

        if has_draw:
            plt.figure(figsize=(14, 4))

            plt.subplot(1, 2, 1)
            plt.plot(self.gl, amplitudes_array, 'k', lw=2)
            plt.xlabel('g.l.')
            plt.ylabel('amplitude')
            plt.ylim(0, 1)
            plt.xlim(self.gl[0], self.gl[-1])

            plt.subplot(1, 2, 2)
            plt.plot(self.gl, fases_array / degrees, 'k', lw=2)
            plt.xlabel('g.l.')
            plt.ylabel('phase (degrees)')
            plt.ylim(0, 200)
            plt.xlim(self.gl[0], self.gl[-1])

        return fields_gl, amplitudes_array, fases_array


def get_LUT_amplitude(amplitudes, gl, amplitudes_new, has_draw=False):
    """
    """
    x = amplitudes
    y = gl
    f_amplitude = interpolate.interp1d(x, y)
    amplitudes_new = np.array(amplitudes_new)
    amplitudes_new[amplitudes_new < x.min()] = x.min()
    amplitudes_new[amplitudes_new > x.max()] = x.max()
    i_new = f_amplitude(
        amplitudes_new)  # use interpolation function returned by `interp1d`
    LUT_amplitudes = np.round(i_new).astype(int)
    if has_draw:
        plt.plot(x, y, 'o', amplitudes_new, LUT_amplitudes, '-')
        plt.show()
    return LUT_amplitudes


def get_LUT_phases(phases, gl, phases_new, has_draw=False):
    """
    """
    x = phases
    y = gl
    f_phase = interpolate.interp1d(x, y)
    phases_new = np.array(phases_new)
    phases_new[phases_new < x.min()] = x.min()
    phases_new[phases_new > x.max()] = x.max()
    i_new = f_phase(
        phases_new)  # use interpolation function returned by `interp1d`
    LUT_phases = np.round(i_new).astype(int)
    if has_draw:
        plt.plot(x, y, 'o', phases_new, LUT_phases, '-')
        plt.show()
    return LUT_phases


def rotation(angle=0):
    """Jones 2x2 rotation matrix.

        Parameters:
                angle (float): angle of rotation, in radians.

        Returns:
                numpy.matrix: 2x2 matrix
        """

    return np.matrix(
        np.array([[np.cos(angle), np.sin(angle)],
                  [-np.sin(angle), np.cos(angle)]]),
        dtype=float)
