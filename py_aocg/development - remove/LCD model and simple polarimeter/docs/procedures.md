# Procedimiento de uso del SLM (teoría y experimento)

1.  A. Simulación física ideal
1.  B. Simulación del comportamiento del LCD
1.  C. Optimización de los ángulos de los polarizadores
1.  D. Conversión de amplitudes y fases en niveles de gris (g.l)
1.  E. Simulación del modulador como $m_{SLM}(x,y)
1.  F. Simulación del experimento real
1.  G. Simulación del experimental de calibración del modulador
1.  H. Calibración experimental del modulador m_{experimental}(g.l.)
1.  I. Comparación entre las simulaciones reales y experimentales
1.  J. Imágenes experimentales difractométricas
1.  K. Desarrollo de experimentos de polarización (pol3, pol4)


## A. Simulación física ideal (terminado)

**Objetivo**: Conocer el comportamiento difractométrico de una máscara. En este sentido no es necesario el modulador, sino que este es el resultado ideal de propagación de una máscara.

**Herramientas**: Se realizará con Diffractio. Preferentemente con el modelo Scalar_field_XY,  Scalar_source_XY y  Scalar_mask_XY.

**Entradas**:
	1. Campo de iluminación u(x,y).
	2. Mácara $t(x,y)=A(x,y) e^{i \phi(x,y)}$.

**Salidas**:
	1. Campo de salida en función de la distancia.

**Ejemplos**:
	- ** procedure - A Simulacion física ideal.ipynb

**Observaciones**:
Ejemplo terminado
Simplemente para luego comparar con la parte experimental.

## B. Simulación del comportamiento del LCD  (terminado)

**Objetivo**: Dados unos ángulos de los polarizadores, se obtienen las curvas A(g.l.) y $\phi$(g.l)

**Herramientas**: py_pol

**Entradas**: source, LCD_model, polarizers, angles_polarizers

**Salidas**: SLM_Jones_phase, amplitudes, phases

**Funciones**: LCD_experiment.get_fields_gl

**Ejemplos**: Procedure - B Simulation behaviour LCD.ipynb
	- SLM_Jones_amplitude, amplitudes, phases=get_fields_gl(polarizers, LCD_jones, source, angles=best_amplitude_angles,has_draw=True)

**Observaciones**:
Procedimiento desarrollado e implementado en la función slm.LCD_jones_matrices.get_fields_gl

## C. Optimización de los ángulos de los polarizadores

**Objetivo**: Dado un SLM teórico, conocido a partir de las matrices polarimétricas y unos polarizadores, se obtienen los mejores ángulos de dichos polarizadores para hacer que el SLM funcione como amplitud o como fase.

**Herramientas**: py_pol, slm.optimizations

**Entradas**: source, LCD_model, polarizers, LCD_jones_matrices_gl, kind_optimization

**Salidas**: angles for optimization

**Funciones**:
	- Optimizaciones:optimization_amplitude, optimization_phase
	- Visualizar parámetros de optimziación: parameters_proposals_amplitude, parameters_proposals_phase
	- Fuciones de mérito: func_amplitude_optimization, func_phase_optimization

**Ejemplos**:
	- "development - C optimization behaviour SLM.ipynb"
	- "procedure - C optimization behaviour SLM.ipynb"

**Observaciones**:
	- Procedimiento desarrollado e implementado en 8 funciones dentro de slm.optimizations
	- Dividido entre amplitud y fase
	- Quizás se pueda pasar a una clase

## D. Conversión de amplitudes y fases en niveles de gris (g.l)

**Objetivo**: Dado un sistema polarimétrico optimizado, se obtiene la curva de calibración de los niveles de gris a implementar en el modulador en función de las amplitudes/fases deseadas

**Herramientas**: py_pol, slm

**Entradas**: amplitudes/phases, gl

**Salidas**: LUT_amplitudes/LUT_phases

**Funciones**: get_LUT_amplitude, get_LUT_phase

**Ejemplos**: development - D LUT amplitude_phase_to_gl.ipynb

**Observaciones**:
	- Falta implementar la función en el modulo, pero es fácil

## E. Simulación del modulador como $m_{SLM}(x,y)

**Objetivo**: Conocida una curva de calibración LUT, se carga dicha LUT y se obtiene la matriz de Jones polarimétrica m(x,y) de una máscara determinada

**Herramientas**: py_pol, diffractio

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**: development - E SLM to polarimetric image

**Observaciones**:

## F. Simulación del experimento real

**Objetivo**: Desarrollada una máscara polarimétrica, se implementa en un sistema de polarizadores completo y se determina el campo final en el plano imagen (z=0) u otros planos z

**Herramientas**:

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**:

**Observaciones**:

## G. Simulación del experimental de calibración del modulador

**Objetivo**: Asumimos que no tenemos conocimiento de las propiedades polarimétricas del LCD que genera un SLM. Por ello, utilizamos el algoritmo de  Moreno, I. et al. para calcular las matrices experimentales de polarización.

**Herramientas**:
	1. py_pol
	1. Moreno, I., Velásquez, P., Fernández-Pousa, C. R., Sánchez-López, M. M. & Mateos, F. Jones matrix method for predicting and optimizing the optical modulation properties of a liquid-crystal display. J. Appl. Phys. 94, 3697–3702 (2003).

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**:

**Observaciones**:
Son dos procedimientos.
	1. Cálculo de los ángulos de los polarizadores para obtener los estados de polarización deseados.
	2. Simulación de las intensidades en función del nivel de gris y se calculan la matrices de Jones.


## H. Calibración experimental del modulador m_{experimental}(g.l.)

**Objetivo**:

**Herramientas**:

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**: development - E SLM to polarimetric image

**Observaciones**:

## I. Comparación entre las simulaciones reales y experimentales

**Objetivo**:

**Herramientas**:

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**:

**Observaciones**:

## J. Imágenes experimentales difractométricas

**Objetivo**:

**Herramientas**:

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**:

**Observaciones**:

## K. Desarrollo de experimentos de polarización (pol3, pol4)

**Objetivo**:

**Herramientas**:

**Entradas**:

**Salidas**:

**Funciones**:

**Ejemplos**:

**Observaciones**:
