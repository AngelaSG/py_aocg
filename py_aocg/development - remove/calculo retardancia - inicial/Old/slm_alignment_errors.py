from scipy.optimize import least_squares
import matplotlib.pyplot as plt
import time

from py_pol.jones_matrix import Jones_matrix, Jones_vector, np, degrees


def cos2(par, x):
    """Funcion cos**2 para ajustar."""
    y = par[0] + par[1] * np.cos(x - par[2]) ** 2
    return y

def error_cos2(par, x, I):
    """Funcion error del cos**2 para ajustar."""
    dI = cos2(par, x) - I
    return dI


def cos2_2(par, x):
    """Funcion cos**2 de doble frecuencia para ajustar."""
    y = par[0] + par[1] * np.cos(2*(x - par[2])) ** 2
    return y

def error_cos2_2(par, x, I):
    """Funcion error del cos**2 con doble frecuencia para ajustar."""
    dI = cos2_2(par, x) - I
    return dI

def retardador(param, angulos, p1, p2):
    """Funcion de ajuste"""
    E0 = Jones_vector().linear_light(azimuth=0, intensity=2)
    Q = Jones_matrix().retarder_linear(R=param[0], azimuth=angulos-param[1])
    P = Jones_matrix().diattenuator_linear(p1=p1, p2=p2, azimuth=90*degrees + param[2])

    Efin = P * Q * E0
    return Efin.parameters.intensity()


def error_retardador(param, angulos, p1, p2, Iexp):
    """Funcion de error."""
    return retardador(param, angulos, p1, p2) - Iexp


def calcular_errores(N, Ne, Nm, Emax, tipo, verbose=True):
    """Funcion para calcular los errores."""
    start = time.time()
    E_array = np.linspace(0, Emax, Ne+1)
    # Si variamos el numero de medidas, vamos a usar recursividad
    if isinstance(Nm, np.ndarray):
        Na = Nm.size
        medias = np.zeros((Ne+1, 3, Na))
        stds = np.zeros((Ne+1, 3, Na))
        medias_abs = np.zeros((Ne+1, 3, Na))
        stds_abs = np.zeros((Ne+1, 3, Na))
        for ind, Nm_v in enumerate(Nm):
            medias[:,:,ind], stds[:,:,ind], medias_abs[:,:,ind], stds_abs[:,:,ind] = calcular_errores(N, Ne, Nm_v, Emax, tipo, verbose=False)

        if verbose:
            x = E_array / degrees if tipo == "angulos" else E_array
            deg_label = " (deg)" if tipo == "angulos" else ""
            plt.figure(figsize=(18, 6))
            plt.subplot(1,3,1)
            for ind in range(Na):
                plt.errorbar(x, medias_abs[:, 0, ind] / degrees, stds_abs[:, 0, ind] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Absoluto")
            plt.legend(Nm)

            plt.subplot(1,3,2)
            plt.plot(x, medias[:, 1, :] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Amplitud")
            plt.legend(Nm)

            plt.subplot(1,3,3)
            plt.plot(x, stds[:, 2, :] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Amplitud")
            plt.legend(Nm)

    else:
        # Param verdaderos
        R_true = 90*degrees
        az_true = 45*degrees
        offset_true = 0
        par_true = np.array([R_true, az_true, offset_true])
        p1_true = 0.9
        p2_true = 0.05
        angulos_true = np.linspace(0, 360*degrees, Nm)
        I_true = retardador(par_true, angulos_true, p1_true, p2_true)
        bounds = ((0, 0, -90*degrees), (180*degrees, 90*degrees, 90*degrees))

        # Param simulacion
        medias = np.zeros((Ne+1, 3))
        stds = np.zeros((Ne+1, 3))
        medias_abs = np.zeros((Ne+1, 3))
        stds_abs = np.zeros((Ne+1, 3))
        tol = 1e-10

        # Bucle en error
        for ind_E, E in enumerate(E_array[1:]):
            resultado = np.zeros((N, 3))
            resultado_abs = np.zeros((N, 3))

            # Bucle en repeticion de experimentos
            for ind in range(N):
                # Crear datos a ajustar
                error = np.random.standard_normal(Nm) * E
                if tipo == "intensidad":
                    I = I_true + error
                else:
                    p1 = p1_true + error[0] if tipo == "p1" else p1_true
                    p2 = p2_true + error[0] if tipo == "p2" else p2_true
                    angulos = angulos_true + error if tipo == "angulos" else angulos_true
                    I = retardador(par_true, angulos, p1, p2)

                # Ajuste
                args = (angulos_true, p1_true, p2_true, I)
                result = least_squares(x0=par_true, fun=error_retardador, bounds=bounds, args=args, ftol=tol, xtol=tol, gtol=tol)
                resultado[ind,:] = result.x - par_true
                resultado_abs[ind,:] = np.abs(result.x - par_true)

            # Calculo de media y error
            medias[ind_E+1, :] = np.mean(resultado, axis=0)
            stds[ind_E+1, :] = np.std(resultado, axis=0) #/ np.sqrt(N)
            medias_abs[ind_E+1, :] = np.mean(resultado_abs, axis=0)
            stds_abs[ind_E+1, :] = np.std(resultado_abs, axis=0) / np.sqrt(N)

        # Plot resultado
        if verbose:
            titulos = ("Retardancia", "Azimut", "Offset")
            leyendas = ("Absoluto", "Amplitud", "Media")
            x = E_array / degrees if tipo == "angulos" else E_array
            deg_label = " (deg)" if tipo == "angulos" else ""
            plt.figure(figsize=(18, 6))
            for ind in range(3):
                plt.subplot(1,3,ind+1)
                plt.errorbar(x, medias_abs[:, ind] / degrees, stds_abs[:, ind] / degrees)
                plt.plot(x, medias[:, ind] / degrees)
                plt.plot(x, stds[:, ind] / degrees)
                plt.xlabel("Error en " + tipo + deg_label)
                plt.ylabel("Error (deg)")
                plt.title(titulos[ind])
                plt.legend(leyendas[::-1])

            end_time = time.time()
            print("Tiempo de ejecucion: {:.0f} segundos".format(end_time - start))

    return medias, stds, medias_abs, stds_abs
