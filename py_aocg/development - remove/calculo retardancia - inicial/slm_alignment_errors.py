from scipy.optimize import least_squares
import matplotlib.pyplot as plt
import time
import os
import itertools
from numpy.random import standard_normal

from py_pol.jones_matrix import Jones_matrix, Jones_vector, np, degrees
from py_aocg.utils_multiprocessing import auxiliar_multiprocessing


####################
## Ajustes
####################
def _AJUSTES():
    pass

def cos2(par, x):
    """Funcion cos**2 para ajustar."""
    y = par[0] + par[1] * np.cos(x - par[2]) ** 2
    return y

def error_cos2(par, x, I):
    """Funcion error del cos**2 para ajustar."""
    dI = cos2(par, x) - I
    return dI


def cos2_2(par, x):
    """Funcion cos**2 de doble frecuencia para ajustar."""
    y = par[0] + par[1] * np.cos(2*(x - par[2])) ** 2
    return y

def error_cos2_2(par, x, I):
    """Funcion error del cos**2 con doble frecuencia para ajustar."""
    dI = cos2_2(par, x) - I
    return dI

def retardador(param, angulos, p1, p2, shift):
    """Funcion de ajuste"""
    E0 = Jones_vector().linear_light(azimuth=0, intensity=2)
    Q = Jones_matrix().retarder_linear(R=param[0], azimuth=angulos-param[1])
    P = Jones_matrix().diattenuator_linear(p1=p1, p2=p2, azimuth=90*degrees + shift + param[2])

    Efin = P * Q * E0
    return Efin.parameters.intensity()


def error_retardador(param, angulos, p1, p2, shift, Iexp):
    """Funcion de error."""
    return retardador(param, angulos, p1, p2, shift) - Iexp


#######################
## Landa cuartos
#######################
def _RETARDADOR():
    pass


def calcular_errores(N, Ne, Na, Emax, tipo, verbose=True, filename=False):
    """Funcion para calcular los errores."""
    start = time.time()
    E_array = np.linspace(0, Emax, Ne+1)
    # Param verdaderos
    R_true = 90*degrees
    az_true = 45*degrees
    offset_true = 0
    shift_true = 0
    par_true = np.array([R_true, az_true, offset_true, shift_true])
    p1_true = 0.9
    p2_true = 0.05
    angulos_true = np.linspace(0, 360*degrees, Na)
    I_true = retardador(par_true, angulos_true, p1_true, p2_true)
    bounds = ((0, 0, -90*degrees), (180*degrees, 90*degrees, 90*degrees))

    # Param simulacion
    medias = np.zeros((Ne+1, 3))
    stds = np.zeros((Ne+1, 3))
    medias_abs = np.zeros((Ne+1, 3))
    stds_abs = np.zeros((Ne+1, 3))
    tol = 1e-10

    # Bucle en error
    for ind_E, E in enumerate(E_array[1:]):
        resultado = np.zeros((N, 3))
        resultado_abs = np.zeros((N, 3))

        # Bucle en repeticion de experimentos
        for ind in range(N):
            # Crear datos a ajustar
            error = np.random.standard_normal(Na) * E
            if tipo == "intensidad":
                I = I_true + error
            else:
                p1 = p1_true + error[0] if tipo == "p1" else p1_true
                p2 = p2_true + error[0] if tipo == "p2" else p2_true
                angulos = angulos_true + error if tipo == "angulos" else angulos_true
                shift = shift_true + np.random.standard_normal() * E if tipo == "angulos" else shift_true
                I = retardador(par_true, angulos, p1, p2, shift)

            # Ajuste
            args = (angulos_true, p1_true, p2_true, I)
            result = least_squares(x0=par_true, fun=error_retardador, bounds=bounds, args=args, ftol=tol, xtol=tol, gtol=tol)
            resultado[ind,:] = result.x - par_true
            resultado_abs[ind,:] = np.abs(result.x - par_true)

        # Calculo de media y error
        medias[ind_E+1, :] = np.mean(resultado, axis=0)
        stds[ind_E+1, :] = np.std(resultado, axis=0) #/ np.sqrt(N)
        medias_abs[ind_E+1, :] = np.mean(resultado_abs, axis=0)
        stds_abs[ind_E+1, :] = np.std(resultado_abs, axis=0) / np.sqrt(N)

    # Plot resultado
    if verbose:
        titulos = ("Retardancia", "Azimut", "Offset")
        leyendas = ("Absoluto", "Amplitud", "Media")
        x = E_array / degrees if tipo == "angulos" else E_array
        deg_label = " (deg)" if tipo == "angulos" else ""
        plt.figure(figsize=(18, 6))
        for ind in range(3):
            plt.subplot(1,3,ind+1)
            plt.errorbar(x, medias_abs[:, ind] / degrees, stds_abs[:, ind] / degrees)
            plt.plot(x, medias[:, ind] / degrees)
            plt.plot(x, stds[:, ind] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title(titulos[ind])
            plt.legend(leyendas[::-1])

        end_time = time.time()
        print("Tiempo de ejecucion: {:.0f} segundos".format(end_time - start))

    if filename:
        np.savez(medias=medias, stds=stds, medias_abs=medias_abs, stds_abs=stds_abs)

    return medias, stds, medias_abs, stds_abs


def calcular_errores_multiple(N, Ne, Na, Emax, tipo, verbose=True):
    """Funcion para calcular los errores."""
    start = time.time()
    E_array = np.linspace(0, Emax, Ne+1)
    # Si variamos el numero de medidas, vamos a usar recursividad
    if isinstance(Na, np.ndarray):
        Na = Na.size
        medias = np.zeros((Ne+1, 3, Na))
        stds = np.zeros((Ne+1, 3, Na))
        medias_abs = np.zeros((Ne+1, 3, Na))
        stds_abs = np.zeros((Ne+1, 3, Na))
        for ind, Na_v in enumerate(Na):
            medias[:,:,ind], stds[:,:,ind], medias_abs[:,:,ind], stds_abs[:,:,ind] = calcular_errores(N, Ne, Na_v, Emax, tipo, verbose=False)

        if verbose:
            x = E_array / degrees if tipo == "angulos" else E_array
            deg_label = " (deg)" if tipo == "angulos" else ""
            plt.figure(figsize=(18, 6))
            plt.subplot(1,3,1)
            for ind in range(Na):
                plt.errorbar(x, medias_abs[:, 0, ind] / degrees, stds_abs[:, 0, ind] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Absoluto")
            plt.legend(Na)

            plt.subplot(1,3,2)
            plt.plot(x, medias[:, 1, :] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Amplitud")
            plt.legend(Na)

            plt.subplot(1,3,3)
            plt.plot(x, stds[:, 2, :] / degrees)
            plt.xlabel("Error en " + tipo + deg_label)
            plt.ylabel("Error (deg)")
            plt.title("Amplitud")
            plt.legend(Na)

            end_time = time.time()
            print("Tiempo de ejecucion: {:.0f} segundos".format(end_time - start))

        if filename:
            np.savez(filename, medias=medias, stds=stds, medias_abs=medias_abs, stds_abs=stds_abs)

        return medias, stds, medias_abs, stds_abs


def retardance_multiprocessing(iter, var_dict):
    """Calculo de un valor especifico de error."""
    # Extraer variables del iterable
    iter = np.array(iter, ndmin=1)
    for ind, key in enumerate(var_dict["iter_vars"]):
        if key == "E_amp":
            E_amp = iter[ind]
        if key == "N_angles":
            N_angles = iter[ind]
        else:
            N_angles = var_dict["N_angles"]
    angulos_true = np.linspace(0, 360*degrees, N_angles)

    # Bucle en repeticion de experimentos
    resultado = np.zeros((var_dict["N_repeat"], 3))
    for ind in range(var_dict["N_repeat"]):
        # Crear datos a ajustar
        p1 = var_dict["p1_true"] + standard_normal()*E_amp if "p1" in var_dict["tipo"] else  var_dict["p1_true"]
        p2 =  var_dict["p2_true"] + standard_normal()*E_amp if "p2" in var_dict["tipo"] else  var_dict["p2_true"]
        angulos = angulos_true + standard_normal(N_angles)*E_amp if "angulos" in var_dict["tipo"] else angulos_true
        shift = var_dict["shift_true"] + standard_normal()*E_amp if "angulos" in var_dict["tipo"] else var_dict["shift_true"]
        I = retardador(var_dict["par_true"], angulos, p1, p2, shift)
        I = I * (1 + standard_normal(N_angles)*E_amp) if "Iprop" in var_dict["tipo"] else I
        I = I  + standard_normal(N_angles)*E_amp if "Isuma" in var_dict["tipo"] else I

        # Ajuste
        args = (angulos_true, var_dict["p1_true"], var_dict["p2_true"], var_dict["shift_true"], I)
        result = least_squares(x0=var_dict["par_true"], fun=error_retardador, bounds=var_dict["bounds"], args=args, ftol=var_dict["tol"], xtol=var_dict["tol"], gtol=var_dict["tol"])
        resultado[ind,:] = result.x - var_dict["par_true"]

        # plt.figure(figsize=(8,4))
        # plt.plot(I)
        # plt.plot(var_dict["I_true"])
        # plt.plot(retardador(result.x, angulos, p1, p2, shift),'--')
        # plt.legend(("I", "I_true", "I_fit"))

    # Calculo de media y error
    medias = np.mean(resultado, axis=0)
    stds = np.std(resultado, axis=0) #/ np.sqrt(N)
    medias_abs = np.mean(np.abs(resultado), axis=0)
    stds_abs = np.std(np.abs(resultado), axis=0) / np.sqrt(var_dict["N_repeat"])

    return medias, stds, medias_abs, stds_abs


def retardance_1(N_repeat, N_error, E_max, N_angles, type, verbose=True, filename=None, folder=None):
    """Funcion que hace todos los calculos y luego representa los resultados.

    Estructura del resultado:
        ([errores], [medias, stds, medias_abs, stds_abs], [R, az, offset])

    """
    # Crear variables
    R = 90*degrees
    az = 45*degrees
    offset = 0
    angulos = np.linspace(0, 360*degrees, N_angles)

    var_dict = {}
    var_dict["iter_vars"] = ["E_amp"]
    var_dict["N_repeat"] = N_repeat
    var_dict["N_angles"] = N_angles
    var_dict["tipo"] = [type] if isinstance(type, str) else type
    var_dict["I_true"] = 0
    var_dict["p1_true"] = 0.9
    var_dict["p2_true"] = 0.05
    var_dict["shift_true"] = 0
    var_dict["par_true"] = [R, az, offset]
    var_dict["tol"] = 1e-10
    var_dict["bounds"] = ((0, 0, -90*degrees), (180*degrees, 90*degrees, 90*degrees))
    var_dict["I_true"] = retardador(var_dict["par_true"], angulos, var_dict["p1_true"], var_dict["p2_true"], var_dict["shift_true"])
    error = np.linspace(0, E_max, N_error)

    # Multiprocessing
    Multi = auxiliar_multiprocessing()
    result = Multi.execute_multiprocessing(function=retardance_multiprocessing,
                            var_iterable=error[1:],
                            dict_constants=var_dict,
                            Ncores=N_error-1)
                            # Ncores=1)
    result = np.array(result)
    # Add zeros
    result = np.insert(result, 0, values=0, axis=0)

    # Print
    if verbose:
        titulos = ("Retardancia", "Azimut", "Offset")
        leyendas = ("Absoluto", "Amplitud", "Media")
        x = error / degrees if type == "angulos" else error
        if isinstance(type, str):
            deg_label = " (deg)" if type == "angulos" else ""
            xlabel = "Error en " + type + deg_label
        else:
            xlabel = "Error en " + str(type)
        plt.figure(figsize=(18,6))
        for ind in range(3):
            plt.subplot(1,3,ind+1)
            plt.errorbar(x, result[:, 2, ind]/degrees, yerr=result[:, 3, ind]/degrees)
            plt.plot(x, result[:, 0, ind]/degrees)
            plt.plot(x, result[:, 1, ind]/degrees)
            plt.xlabel(xlabel)
            plt.ylabel("Error (deg)")
            plt.title(titulos[ind])
            plt.legend(leyendas[::-1])

    # Salvar datos
    if filename:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        np.savez(filename, N_error=N_error, E_max=E_max, result=result, retardance=R, azimuth=az, offset=offset, **var_dict)
        if folder:
            os.chdir(old_folder)



def retardance_2(N_repeat, N_error, E_max, array_N_angles, type, verbose=True, filename=None, folder=None):
    """Funcion que hace todos los calculos y luego representa los resultados.

    Estructura del resultado:
        ([errores], [N_angles], [medias, stds, medias_abs, stds_abs], [R, az, offset])

    """
    # Crear variables
    R = 90*degrees
    az = 45*degrees
    offset = 0

    var_dict = {}
    var_dict["iter_vars"] = ["E_amp"]
    var_dict["N_repeat"] = N_repeat
    var_dict["tipo"] = [type] if isinstance(type, str) else type
    var_dict["I_true"] = 0
    var_dict["p1_true"] = 0.9
    var_dict["p2_true"] = 0.05
    var_dict["shift_true"] = 0
    var_dict["par_true"] = [R, az, offset]
    var_dict["tol"] = 1e-10
    var_dict["bounds"] = ((0, 0, -90*degrees), (180*degrees, 90*degrees, 90*degrees))

    error = np.linspace(0, E_max, N_error)
    iterable = itertools.product(error[1:], array_N_angles)

    # Multiprocessing
    Multi = auxiliar_multiprocessing()
    result = Multi.execute_multiprocessing(function=retardance_multiprocessing,
                            var_iterable=iterable,
                            dict_constants=var_dict,
                            Ncores=N_error-1)
                            # Ncores=1)
    result = np.array(result)
    # Add zeros
    result = np.insert(result, 0, values=0, axis=0)

    # Print
    if verbose:
        titulos_1 = ("Retardancia", "Azimut", "Offset")
        titulos_2 = ("Absoluto ", "Amplitud ", "Media ")
        x = error / degrees if type == "angulos" else error
        if isinstance(type, str):
            deg_label = " (deg)" if type == "angulos" else ""
            xlabel = "Error en " + type + deg_label
        else:
            xlabel = "Error en " + str(type)
        plt.figure(figsize=(18,18))
        for ind in range(3):
            for ind2 in range(3):
                plt.subplot(3,3,ind2*3 + ind + 1)
                for ind3 in range(len(array_N_angles)):
                    if ind3 == 0:
                        plt.errorbar(x, result[:, ind2, 2, ind]/degrees, yerr=result[:, ind2, 3, ind]/degrees)
                    else:
                        plt.plot(x, result[:, ind2, ind3-1, ind]/degrees)
                        plt.plot(x, result[:, ind2, ind3-1, ind]/degrees)
                        # ([errores], [N_angles], [medias, stds, medias_abs, stds_abs], [R, az, offset])
                plt.xlabel(xlabel)
                plt.ylabel("Error (deg)")
                plt.title(titulos_1[ind] + titulos_2[ind2])
                plt.legend(leyendas[::-1])

    # Salvar datos
    if filename:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        np.savez(filename, N_error=N_error, E_max=E_max, array_N_angles=array_N_angles, result=result, retardance=R, azimuth=az, offset=offset, **var_dict)
        if folder:
            os.chdir(old_folder)

######################
## Polarizadores
######################
def _POLARIZADORES():
    pass

def polarizers_power(N_repeat, N_error, E_max, N_angles, type, verbose=True, filename=None, folder=None):
    """Gets the error of getting the maximum power."""
    # Crear variables
    Noutput = 2

    var_dict = {}
    var_dict["iter_vars"] = ["E_amp"]
    var_dict["N_repeat"] = N_repeat
    var_dict["N_angles"] = N_angles
    var_dict["tipo"] = [type] if isinstance(type, str) else type
    var_dict["azimuth"] = 45*degrees
    var_dict["p1"] = 0.9
    var_dict["p2"] = 0.05
    var_dict["Noutput"] = Noutput
    error = np.linspace(0, E_max, N_error)

    # Multiprocessing
    Multi = auxiliar_multiprocessing()
    result = Multi.execute_multiprocessing(function=polarizers_power_multiprocessing,
                            var_iterable=error[1:],
                            dict_constants=var_dict,
                            Ncores=N_error-1)
                            # Ncores=1)
    result = np.array(result)
    # Add zeros
    result = np.insert(result, 0, values=0, axis=0)

    # Print
    if verbose:
        titulos = ("Max", "Min")
        leyendas = ("Absoluto", "Amplitud", "Media")
        x = error / degrees if type == "angulos" else error
        if isinstance(type, str):
            deg_label = " (deg)" if type == "angulos" else ""
            xlabel = "Error en " + type + deg_label
        else:
            xlabel = "Error en " + str(type)
        plt.figure(figsize=(18,6))
        for ind in range(Noutput):
            plt.subplot(1,Noutput,ind+1)
            plt.errorbar(x, result[:, 2, ind]/degrees, yerr=result[:, 3, ind]/degrees)
            plt.plot(x, result[:, 0, ind]/degrees)
            plt.plot(x, result[:, 1, ind]/degrees)
            plt.xlabel(xlabel)
            plt.ylabel("Error (deg)")
            plt.title(titulos[ind])
            plt.legend(leyendas[::-1])

    # Salvar datos
    if filename:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        np.savez(filename, N_error=N_error, E_max=E_max, result=result, **var_dict)
        if folder:
            os.chdir(old_folder)

def polarizers_power_multiprocessing(iter, var_dict):
    """Calculo de un valor especifico de error para el angulo de potencia maxima."""
    # Extraer variables del iterable
    iter = np.array(iter, ndmin=1)
    for ind, key in enumerate(var_dict["iter_vars"]):
        if key == "E_amp":
            E_amp = iter[ind]
        if key == "N_angles":
            N_angles = iter[ind]
        else:
            N_angles = var_dict["N_angles"]
    angulos_true = np.linspace(0, 180*degrees, N_angles)
    E0 = Jones_matrix().diattenuator_linear(azimuth=-var_dict["azimuth"], p1=var_dict["p1"], p2=var_dict["p2"]) * Jones_vector().circular_light(azimuth=0, intensity=2)

    # Bucle en repeticion de experimentos
    resultado = np.zeros((var_dict["N_repeat"], var_dict["Noutput"]))
    for ind in range(var_dict["N_repeat"]):

        # Calculate intensity
        Jp = Jones_matrix().diattenuator_linear(p1=var_dict["p1"], p2=var_dict["p2"], azimuth=angulos_true - var_dict["azimuth"])
        Efin = Jp * E0
        I = Efin.parameters.intensity()

        # Añadir errores
        angulos = angulos_true + standard_normal(N_angles)*E_amp if "angulos" in var_dict["tipo"] else angulos_true
        I = I * (1 + standard_normal(N_angles)*E_amp) if "Iprop" in var_dict["tipo"] else I
        I = I  + standard_normal(N_angles)*E_amp if "Isuma" in var_dict["tipo"] else I

        # Obener resultado
        resultado[ind, 0] = (angulos[np.argmax(I)] - var_dict["azimuth"]) % np.pi
        resultado[ind, 1] = (angulos[np.argmin(I)] - var_dict["azimuth"]) % np.pi

        # plt.figure(figsize=(8,4))
        # plt.plot(I)
        # plt.plot(var_dict["I_true"])
        # plt.plot(retardador(result.x, angulos, p1, p2, shift),'--')
        # plt.legend(("I", "I_true", "I_fit"))

    # Calculo de media y error
    medias = np.mean(resultado, axis=0)
    stds = np.std(resultado, axis=0) #/ np.sqrt(N)
    medias_abs = np.mean(np.abs(resultado), axis=0)
    stds_abs = np.std(np.abs(resultado), axis=0) / np.sqrt(var_dict["N_repeat"])

    return medias, stds, medias_abs, stds_abs
