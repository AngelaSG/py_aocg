
# PSFunctionsComun.py

import numpy as np
from tqdm import trange # progress bar

from diffractio import degrees, mm, plt, sp, um, nm
from diffractio.scalar_masks_XY import Scalar_mask_XY

### Pruebecitas para ver si se carga el fichero

print('PSFunctionsComun imported')

def IntensityRegion_v2(HolesMask, f0 = 10, NFresnelMax = 9, plots = False, prints = False):
    """
    Figure of merit for Minimize. Return de Irradiance min (- Imax). 

    Parameters:
        HolesMask: Mask of pinholes whose propagation will be minimize in a region with size of Airy radius.
        f0: focal distance in mm, for default is 100 mm.
        lamb: scalar value of wavelength in um, for default is 0.633 um.
        N: maximun order of Fresnel Zone in the future Photon Sieve.
        npx: tuple (npx,npy) o scalar value npx .Number of pixels in x,y axis. For default is 512 in both axis.
        plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.
    
    Returns:
        I_min: Magnitude to minimize.'I_min = -I_max'.
        
    Request:
        import numpy as np
        from diffractio import degrees, mm, plt, sp, um, np, nm
        from diffractio.scalar_masks_XY import Scalar_mask_XY

    """    
    NFresnelMaxSup = NFresnelMax+1
    lamb = HolesMask.wavelength
    npx = len(HolesMask.x)
    npy = len(HolesMask.y) # realmente no hace falta porque de moemento van a ser cuadradas
    
    RAiry = 0.61*np.sqrt(f0*lamb/NFresnelMaxSup) # 0.305*np.sqrt(f0*lamb/NFresnelMaxSup) # um. El 0.305 surge de 1.22/(2*2)
    AiryMask = Scalar_mask_XY(HolesMask.x,HolesMask.y,lamb) 
    AiryMask.circle(r0=(0,0),radius = RAiry)
    
    PropHoleMask = HolesMask.CZT(z=f0, xout = HolesMask.x, yout=HolesMask.y, verbose=False)
    psOverlap = AiryMask.u * PropHoleMask.intensity()
    
    IntRegIrrad = psOverlap.sum()
    
    if prints == True:
        print('IntRegIrrad =', IntRegIrrad)
        
        
    if plots == True:
        AiryMask.draw(title="AiryMask", has_colorbar='vertical')
        HolesMask.draw(title="HolesMask", has_colorbar='vertical')
        PropHoleMask.draw(title='Propagation', logarithm = True)
        
    I_min = -IntRegIrrad
    
    if prints == True:
        print('I_min =', I_min)
        
    return I_min

def FZP(f0 = 100* mm, lamb = 0.633* um, NFresnelMax = 9, prints = False):
    """
    Returns the radius of Fresnel Zones and their centers.

    Parameters:
        f0 (float): focal distance in mm, by default is 100 mm.
        lamb (float): scalar value of wavelength in um, by default is 0.633 um.
        NFresnelMax (int): maximun order of Fresnel Zone in the future Photon Sieve.
        prints (bool): Show prints. By default is False.

    Returns:
        r_Zn (float): radii of Fresnel Zones.
        r_n (float): radii of Fresnel Zones in the middle.
  
    """
    r_Zn = []
    r_n = []
    Nsup = NFresnelMax+1

    for i in range(1,Nsup+2):
        r_Zn += [np.sqrt(i*lamb*f0)]

    r_n += [r_Zn[0]]

    for i in range(1,Nsup+1):
        r_n += [0.5*(r_Zn[i-1]+r_Zn[i])]


    if prints == True:
        print(r_Zn)
        print(r_n)

    return r_Zn, r_n

def AjParameter (DiffractioMask, f0, propagation = False, plots = False, prints = False):
    """
    Returns the parameter Aj, which give information about how similar is the mask respect to Fresnel Zone Plate.

    Parameters:
        DiffractioMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask
        f0 (float): focal distance in mm, by default is 100 mm.
        propagation (bool): if you want to get the intensity and field, set True, for only Aj parameter set False. By default is False.
        plots (bool): Show plots. By default is False.
        prints (bool): Show prints. By default is False.

    Returns:
        if propagation = True
            AjNorm (float): Aj parameter nomarlized.
            I_Aj (float): Intensity that contributes constructively in the focal plane.
            I_DiffractioMask (float): Intensity of mask in the focal plane.
  
    """
    
    lamb = DiffractioMask.wavelength
    xmax = np.max(DiffractioMask.x)
    NFresnelMax = int(np.floor(xmax**2/(lamb*f0)))

    FPZMask_ideal = Scalar_mask_XY(x = DiffractioMask.x, y =DiffractioMask.y, wavelength = lamb)
    FPZMask_ideal.fresnel_lens(r0 = (0,0), focal = f0, radius = xmax)

    
    if prints == True:
        print('FPZMask_ideal.u: ', FPZMask_ideal.u)


    if plots == True:
        FPZMask_ideal.draw()

    ComparationMask = Scalar_mask_XY(x = FPZMask_ideal.x, y = FPZMask_ideal.y, wavelength = lamb)
    
    ComparationMask.u = FPZMask_ideal.u * DiffractioMask.u
    ComparationMaskNorm = FPZMask_ideal.u *FPZMask_ideal.u


    if plots == True:
        ComparationMask.draw()
    
    Aj = np.sum(np.sum(ComparationMask.u, axis = 0), axis = 0) 

    Anorm = np.sum(np.sum(ComparationMaskNorm, axis = 0), axis = 0) 

    AjNorm = np.abs(Aj/Anorm)
    print(AjNorm)

    if propagation == True:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Aj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)

        return (AjNorm, I_Aj, I_DiffractioMask)
    else:
        return (AjNorm)

def BjParameter (DiffractioMask, f0, propagation = False, plots = False, prints = False):
    """
    Returns the parameter Bj, which give information about how different is the mask respect to Fresnel Zone Plate.

    Parameters:
        DiffractioMask (diffractio.scalar_masks_XY.Scalar_mask_XY object): mask
        f0 (float): focal distance in mm, by default is 100 mm.
        propagation (bool): if you want to get the intensity and field, set True, for only Bj parameter set False. By default is False.
        plots (bool): Show plots. By default is False.
        prints (bool): Show prints. By default is False.

    Returns:
        if propagation = True
            BjNorm (float): Bj parameter nomarlized.
            I_Bj (float): Intensity that contributes destructively in the focal plane.
            I_DiffractioMask (float): Intensity of mask in the focal plane.
  
    """
    lamb = DiffractioMask.wavelength
    xmax = np.max(DiffractioMask.x)
    NFresnelMax = int(np.floor(xmax**2/(lamb*f0)))

    FPZMask_ideal = Scalar_mask_XY(x = DiffractioMask.x, y =DiffractioMask.y, wavelength = lamb)
    FPZMask_ideal.fresnel_lens(r0 = (0,0), focal = f0, radius = xmax, phase = 2*np.pi )
    FPZMask_ideal.inverse_amplitude()
    FPZMask_ideal.u = np.abs(FPZMask_ideal.u)

    if prints == True:
        print('FPZMask_ideal.u: ', FPZMask_ideal.u)

    if plots == True:
        FPZMask_ideal.draw()

    ComparationMask = Scalar_mask_XY(x = FPZMask_ideal.x, y = FPZMask_ideal.y, wavelength = lamb)
    
    ComparationMask.u = FPZMask_ideal.u * DiffractioMask.u
    ComparationMaskNorm = FPZMask_ideal.u *FPZMask_ideal.u


    if plots == True:
        ComparationMask.draw()
    
    Bj = np.sum(np.sum(np.sqrt(ComparationMask.u*np.transpose(ComparationMask.u)), axis = 0), axis = 0) 

    Bnorm = np.sum(np.sum(ComparationMaskNorm, axis = 0), axis = 0) 
    
    BjNorm = Bj/Bnorm
    print(BjNorm)

    if propagation == True:

        I_DiffractioMask = - IntensityRegion_v2(HolesMask = DiffractioMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)
        I_Bj = - IntensityRegion_v2(HolesMask = ComparationMask, f0 = f0, NFresnelMax = NFresnelMax, plots = False)

        return (BjNorm, I_Bj, I_DiffractioMask)
    else:
        return(BjNorm)

def ideal_photon_sieve (maskEmpty, xcenter, ycenter, RHoles):
    """
    Return an ideal photon sieve. The holes are placed in the odd Fresnel Zones.

    inputs:

    xcenter (list): Each element is an array that contains x coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(x11, x12, ..., x1j1), nd.array_FZ3(x31, x32, ..., x1j3), ..., nd.array_FZN(xN1, xN2, ..., xNjN)]
    ycenter (list): Each element is an array that contains y coordinate of the holes belongs of differents odd Fresnel Zones. Example: [nd.array_FZ1(y11, y12, ..., y1j1), nd.array_FZ3(y31, y32, ..., y1j3), ..., nd.array_FZN(yN1, yN2, ..., yNjN)]
    RHoles (list): radii of Holes in each Fresnel Zone

    outputs:
    
    modify input maskEmpty ideal photon sieve mask.8
    """
    x0 = maskEmpty.x
    y0 = maskEmpty.y
    lamb = maskEmpty.wavelength

    #ps0=Scalar_mask_XY(x0,y0,lamb) # Hay que meter la longitud de onda en um
    t_circle=Scalar_mask_XY(x0,y0,lamb)
    #ps=Scalar_mask_XY(x0,y0,lamb)   

    #t = []

    for n, R in enumerate(RHoles):
        t = trange(len(xcenter[n]))
        for tm,i,j in zip(t, xcenter[n], ycenter[n]):
            #print(i)
            t_circle.circle(r0 = (i,j), radius = R)

            #t += [t_circle]
            #t_circle.draw()
            maskEmpty.u += t_circle.u
    
    return None