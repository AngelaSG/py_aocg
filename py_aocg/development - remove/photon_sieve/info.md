**Javier 210927**

Hola a ambos,

Os adjunto unos pocos ficheros en matlab (sorry, si lo hubiese hecho con python estaría todavía definiendo cosas);
La generación de la máscara tarda un tanto (unos 50 s para 2048x2048), pero la generación de los valores de radios y posiciones es casi inmediata (menos de 2 s).

agujeros_1.m es el código en matlab
valores_mascara.mat es una colección de valores para la posición de los centros de los agujeros (x e y) y el radio.
mascara_27sep2021.mat es la máscara en si junto con los valores xmax e ymask que son las coordenadas sobre la máscara. 

El tamaño de la máscara es de aprox. 3x3 mm2 y el tamaño del píxel es de 1.55 um (aprox.)
Con las condiciones sobre las zonas de salvaguardia entre agujeros, han salido 1984 agujeros con 20 valores de radio (incluyendo el central).

La fracción abierta que me sale es 0.18. Es decir, permitiría pasar en torno al 18% del fluido circulante. Este parámetro también lo debemos tener en cuenta para maximizarlo. Recordad que una lente de Fresnel convencional tendríamos un valor de 0.5.

La foto que sale es:

Ahora le toca a Verónica reproducir este resultado en Python, jeje. Sin prisa.
Ya verás que lo siguiente será jugar con factores de proporcionalidad para poder realizar una optimización del problema con una función de mérito que tendrá que ver con la cantidad de luz que le llega al foco.

Luismi, el dispositivo final trabajará en reflexión pero la optimización se puede hacer en transmisión (como te resulte mas sencillo).

En fin, que me ha cundido la tarde más de lo que esperaba.



**Javier 210928**
Ostras Pedrín!!

Qué bien queda.
Fíjate que lo de la convolución es una estrategía que pensaba iba a ser más costosa en tiempo que la generación de los agujeros individuales.

Respecto al cálculo de la irradiancia con diffractio, está es la parte más interesatnte para la optimización. En principio, un buen punto de partida sería considerar la distribución de una lente de Fresnel de amplitud clásica y calcular el valor de la irradiancia en el centro en el plano del foco. Con ese valor podemos generar una especie de razón de Strehl adaptada sencillamente obteniendo el ratio entre el máximo de la irradiancia para el photon sieve y el valor para la lente de Fresnel ideal.  Ese parámetro sería el que deberíamos maximizar y acercarlo todo lo posible a 1.

En este momento no quedan muchos parámetros para optimizar.
En el código sólo introduje un parámetro. Le llamé "factor" y le di el valor de 1. Este "factor" hace que los aguejros desborden (factor>1) o no (factor<1) el tamaño de la zona de Fresnel. Para jugar podemos representar el valor de esa función de Strehl adaptada en función de "factor". Imagino que dará lugar a un máximo para un valor (si factor es muy pequeño te quedas sin luz, si factor es muy grande te pasas de la zona de Fresnel).

Si vamos a una optimización por PSO u otro medio, podríamos poner como variables los propios radios de los centros de los agujeros, y también los radios de los agujeros. Si no somos muy ambiciosos podemos tener unas 40-50 variables. Para la función de mérito podemos pensar en una combinación de la función de Strehl adaptada (u otra cosa que se nos ocurra para medir la irradiancia total) y en el tamaño transversal de la región que tiene una proporción dada de la energía (encircled energy), por ejemplo, el 90% (o el radio del primer mínimo). Lo que buscamos es la mayor cantidad de energía en el menor espacio posible.

Os adjunto un artículo sobre PSO y sus variedades. Yo he tenido experiencia en la combinación de PSO y GA y la verdad es que funcionaba muy bien. A lo largo del día voy a buscar el tipo de parametrización que hice y os lo cuento.

Uno de los mayores problemas de PSO es la elección del número de agentes que se lanzan al espacio de variables. No hay un criterio general así que tendremos que ir probando. He leido en diagonal un trabajo en el que sugieren un valor mayor que la dimensión del espacio (por eso hay que tener cuidado de no ser muy ambicioso dejando muchas variables libres). Pero no me termina de convencer.

Otra cosa que me gustaría probar es una aproximación al phton sieve más sencilla e iterativa. Se trataría de optimizar por zonas, o por agujeros: poner el agujero central, y optimizas la posición y el tamaño del siguiente agujero (o grupo de agujeros). Una vez optimizado te vas al siguiente agujero (o grupo de agujeros). En fin, vamos a pensarlo y le damos una vuelta.

Verónica, no te agobies. Los resultados de ayer nos han dado un subidón y por eso estamos proponiendo tantas cosas.








