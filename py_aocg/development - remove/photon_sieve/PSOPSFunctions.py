
# PSOPSFunctions.py>

from IPython.display import Image
import datetime
import os
from tqdm import trange # progress bar

from diffractio import degrees, mm, plt, sp, um, nm
from diffractio.scalar_masks_XY import Scalar_mask_XY

import pyswarms as ps
from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.utils.plotters import (plot_cost_history, plot_contour, plot_surface)

### Pruebecitas para ver si se carga el fichero

print('PSOPSFunctions imported')

def PRUEBA1(name):
    print('Hola {}'.format(name))

# ¡EMPEZAMOS!

def Porlar2Cart(r_n_imp,alpha_i_n):
    """Return a mask of photon sieve. 

        Parameters:
            r_n: numpy.ndarray with shape (N,): r_n = array([r_1,r_2,...r_N]). It represent the radius of holes in each n zone.
            alpha_i_n: dictionary: alpha_i_n = {'n1': array([alpha_1_1,alpha_2_1,...alpha_(j_n1)_1]),'n2': array([alpha_1_2,alpha_2_2,...alpha_(j_n2)_2]), ..., nN: [alpha_1_N,alpha_2_N,...alpha_(j_nN)_N]).
            
        Returns:
            x_sieves: dictionary: x_sieves = {'n1': array([x1_n1, x2_n1, ..., xj1_n1]), 'n2': array([x1_n2, x2_n2, ..., xj2_n2]), ..., 'N': array([x1_N, x2_N, ..., xjN_N])}
            y_sieves: dictionary: y_sieves = {'n1': array([y1_n1, y2_n1, ..., yj1_n1]), 'n2': array([y1_n2, y2_n2, ..., yj2_n2]), ..., 'N': array([y1_N, y2_N, ..., yjN_N])}
            
        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """    
    # Lo primero será comprobar que existen el mismo número de zonas semiperiódicas:
    if len(alpha_i_n.keys()) != len(r_n_imp):
        raise ValueError("The number of semiperiodic zones not agree. Please, check it.")
        
    x_sieves = {'1': 0} 
    y_sieves = {'1': 0} 

    for i,j in enumerate(alpha_i_n.keys()):
        #print(j)
        x_sieves[j] = r_n_imp[i]*np.cos(alpha_i_n[j])
        y_sieves[j] = r_n_imp[i]*np.sin(alpha_i_n[j])
    
    return x_sieves, y_sieves

def J_Alpha_2D(var_PSO , g = 20*um):
    """Return a number, angular position and cartesian coordinates of pinholes in "n" zone. 

        Parameters:
            r_n: numpy.ndarray with shape (N,): r_n = array([r_1,r_2,...r_N]). It represent the radius of holes in each n zone.
            R_opt_n: numpy.ndarray with shape (N,). It contains the optimized size (radii) for the holes in each zone.
            g: scalar value of segurity radius in um, for default is 20 um.

        Returns:
            x_sieves: dictionary: x_sieves = {'n1': array([x1_n1, x2_n1, ..., xj1_n1]), 'n2': array([x1_n2, x2_n2, ..., xj2_n2]), ..., 'N': array([x1_N, x2_N, ..., xjN_N])}
            y_sieves: dictionary: y_sieves = {'n1': array([y1_n1, y2_n1, ..., yj1_n1]), 'n2': array([y1_n2, y2_n2, ..., yj2_n2]), ..., 'N': array([y1_N, y2_N, ..., yjN_N])}
            j_n: numpy.ndarray with shape (N,): j_n = array([j_1,j_2,...j_N]). It represent the number of holes in each n zone.
            alpha_i_n: dictionary: alpha_i_n = {'n1': array([alpha_1_1,alpha_2_1,...alpha_(j_n1)_1]),'n2': array([alpha_1_2,alpha_2_2,...alpha_(j_n2)_2]), ..., nN: [alpha_1_N,alpha_2_N,...alpha_(j_nN)_N]). The zones with more pinholes its expects that was those whose n is bigger (perhaps NOT happen...).
    
            Request:
            import numpy as np
            
    """
    D = len(var_PSO)
    DR = int(D/2)
    R_n_imp = var_PSO[:DR]
    r_n_imp = var_PSO[DR:]


    #print('var_PSO:', var_PSO)
    #print('r_n_imp: ',r_n_imp)
    #print('R_n_imp+g: ',R_n_imp+g)
    

    j_n = np.pi*r_n_imp/(R_n_imp+g)
    
    j_n_z = np.floor(j_n)
    N = len(j_n)

    alpha_i_n = {}

    for k in range(N):
        alpha_i_n[str(2*k+3)] = []
        Delta_alpha_n = 2*np.pi/j_n_z[k]
        for l in range(int(j_n_z[k])):
            alpha_i_n[str(2*k+3)] += [l*Delta_alpha_n]

    x_sieves, y_sieves = Porlar2Cart(r_n_imp,alpha_i_n)
    
    return x_sieves, y_sieves, j_n, alpha_i_n

def CounterMyDictEntries(Dict_simple):
    c = 1
    for i in Dict_simple:
        if type(Dict_simple[i]) == int:
            continue
        c += len(Dict_simple[i])    
    return c    

def CompareSize2MyDict(Dict1, Dict2):
    c1 = CounterMyDictEntries(Dict1)
    c2 = CounterMyDictEntries(Dict2)
    if c1 == c2:
        return True
    else:
        return False

def MaskPSGen_2D(x_sieves, y_sieves, var_PSO , lamb = 0.633, f0 = 100*mm, num_pixel = 512, margin_Draw = 15, verbose = False):
    """Return a mask of photon sieve. 

        Parameters:
            x_sieves: dictionary: x_sieves = {'n1': array([x1_n1, x2_n1, ..., xj1_n1]), 'n2': array([x1_n2, x2_n2, ..., xj2_n2]), ..., 'N': array([x1_N, x2_N, ..., xjN_N])}
            y_sieves: dictionary: y_sieves = {'n1': array([y1_n1, y2_n1, ..., yj1_n1]), 'n2': array([y1_n2, y2_n2, ..., yj2_n2]), ..., 'N': array([y1_N, y2_N, ..., yjN_N])}
            var_PSO: numpy.ndarray with shape (2N,). It contains the optimized size (radii) and the radial position for the holes in each zone.
            lamb: scalar value of wavelength in um, for default is 0.633 um.
            f0: focal distance in mm, for default is 100 mm.
            num_pixel: tuple (npx,npy) o scalar value np .Number of pixels in x,y axis. For default is 512 in both axis.
            margin_Draw: scalar value between 0 up to 100, For default 15. It indicate, in percent, the margin in function of the maximun value of r_N + R_max.
            verbose: bool. If True => print() info.
        
        Returns:
            ps: diffractio.scalar_masks_XY.Scalar_mask_XY. It's the mask of photon Sieve.
            
        Request:
            import numpy as np
            from diffractio import degrees, mm, plt, sp, um, np, nm
            from diffractio.scalar_masks_XY import Scalar_mask_XY

    """   
    D = len(var_PSO)
    DR = int(D/2)
    R_opt_n = var_PSO[:DR]

    max_long_x = 1
    index_x = 0

    for i,e in enumerate(x_sieves.values()):
        #print(i)
        if type(e) == int:
            continue

        if max_long_x < e.shape[0]:
            max_long_x = e.shape[0]
            index_x = i
        #print(max_long_x)
        #print('iteracion {i}: {e}'.format(i=i, e=e))

    max_long_x = list(x_sieves.keys())[index_x]

    # OJO, max_long_x debería ser el mayor valor dentro de todas las zonas. Lo que está ahora sirve en el caso de que la última zona contenga los pinholes más pequeños...
    # Quizás haya que crear otra función que tenga eso en cuenta y tome realmente el mayor valor de (x_sieve + Rmax)
    
    if CompareSize2MyDict(x_sieves, y_sieves) == True:

        max_long_y = max_long_x 
        index_y = index_x

    else: 
        print('size x_sieves no equal y_sieves')
        max_long_y = 1
        index_y = 0
        for i,e in enumerate(y_sieves.values()):
            print(i)
            if type(e) == int:
                continue

            if max_long_y < e.shape[0]:
                max_long_y = e.shape[0]
                index_y = i
            print(max_long_y)
            #print('iteracion {i}: {e}'.format(i=i, e=e))

            
            
    #x_max=x_sieves[max_long_x].max()+20*um
    
    #N_max_long_x = list(x_sieves.keys())[index_x]
    #x_max_l = x_sieves[N_max_long_x].max()+R_opt_n.max()
    #x_max = x_max_l*(1+0.01*margin_Draw)
   
    #y_max=y_sieves[max_long_y].max()+20*um
    #N_max_long_y = list(y_sieves.keys())[index_y]
    #y_max_l = y_sieves[N_max_long_y].max()+R_opt_n.max()
    #y_max = y_max_l*(1+0.01*margin_Draw)

    lxsk = list(x_sieves.keys())
    x_max = np.max(x_sieves[lxsk[-1]])*(1+0.1*margin_Draw)

    lysk = list(y_sieves.keys())
    y_max = np.max(y_sieves[lysk[-1]])*(1+0.1*margin_Draw)




    if verbose == True:
        
       #print('N_max_long_x: ', N_max_long_x)
       #print('N_max_long_y: ', N_max_long_y)

       #print('x_sieves[N_max_long_x].max(): ', x_sieves[N_max_long_x].max())
       #print('y_sieves[N_max_long_y].max() ', y_sieves[N_max_long_y].max())

        print('R_opt_n.max(): ', R_opt_n.max())
        print('20% de x_max: ', R_opt_n.max())

        print('x_max: ', x_max)
        print('y_max: ', y_max)

    if type(num_pixel) == tuple:
        npx = num_pixel[0]
        npy = num_pixel[1]
    else:
        npx = num_pixel
        npy = num_pixel

    x0=np.linspace(-x_max,x_max,npx)
    y0=np.linspace(-y_max,y_max,npy)

    if x_sieves.keys() != y_sieves.keys():
        raise ValueError("Coordinates of axies not agree.")

    ps0 = Scalar_mask_XY(x0,y0,lamb) 
    t_circle=Scalar_mask_XY(x0,y0,lamb)
    ps=Scalar_mask_XY(x0,y0,lamb)

    R_opt_plus = np.append([np.sqrt(f0*lamb)],R_opt_n)

    for i,j in enumerate(x_sieves.keys()):
        #print(type(int(i)))

        t_circle.clear_field()
        ps0.clear_field()
        #ps0.draw()
        
        t_circle.circle(r0=(0,0),radius = R_opt_plus[i])
        plt.plot(x_sieves[j],y_sieves[j],'.')
        ps0.photon_sieve(t1 = t_circle, r0 = np.vstack((x_sieves[j],y_sieves[j])))
        #print(j)

        ps=ps+ps0
        #t_circle.draw()
        #ps0.draw()
        #ps.draw()
    
    return ps

def PS_focus_intensity(var_PSO, Param, Draw = False, Propagation = False):
    """Generate a photon sieve mask and propagate it.

        Parameters:
            var_PSO: numpy.ndarray with shape (n_agents,PSO_var): var_PSO = array([R1,R2,...,RN]). It represent de variables (size of pinholes) in the space of search: R == radii of pinhole.
            Param: dictionary. It contains the parameters necessary to generate masks. Param = {'r_n': np.array([r1, r2, ..., rN]), 'lamb': scalar value of wavelength, 'f0': scalar value focal, 'g': scalar value of security distance, 'num_pixel': tuple (npx,npy) or scalar value of pixels}.

        Returns:
            I_min: 'I_min = -I_max' 
    """
    r_n = Param['r_n'] # Distancia radial al centro de las zonas semiperiódicas
    r_n_imp = r_n[2::2] # Distancia radial de las zonas semiperiódicas IMPARES (n = 3,5,7,9,...)/centros de agujeros
    g = Param['g'] # Distancia de seguridad en um
    lamb = Param['lamb'] # Longitud de onda en um
    f0 = Param['f0'] # distancia focal en mm
    num_pixel= Param['num_pixel'] # número de pixels, recomendados 2^n

    x_sieves, y_sieves, j_n, alpha_i_n = J_Alpha_2D(var_PSO , g)

    ps = MaskPSGen_2D(x_sieves, y_sieves, var_PSO , lamb, f0, num_pixel)
    
    if Draw == True:
        ps.draw()
    
    u_ps = ps.CZT(z=f0, xout=ps.x, yout=ps.y, verbose=False)
        #u_ps.draw(logarithm=True,filename='ps_python_focus_function.png')

    if Propagation == True:
        u_ps.draw()
    
    NFresnelMax = len(r_n)
    I_min = IntensityRegion_v2(u_ps, f0 = f0, NFresnelMax = NFresnelMax, plots = False, prints = False)
    #I_min =-np.max(u_ps.intensity())
    
    return I_min

def opt_func(X, **Param):
    n_particles = X.shape[0]  # number of particles
    #print('Param: ', Param)
  
    intensidad = [PS_focus_intensity(X[i], Param) for i in range(n_particles)]
    return intensidad

def WriteDataPSO(HolesN, tf, dt, DictHead, SolOptTOTAL, pos, NFresnelMax, j_n):  

    iters = np.arange(len(SolOptTOTAL.cost_history))
    
    pathcwd = os.getcwd()
    today = datetime.date.today()
    pathDirFolder = pathcwd + '\\PSO_{}_PS_{}Zones'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), NFresnelMax)
    
    #nameFolder = 'PSO_{}_PS_{}Zones'.format(today.strftime('%Y%m%d')+'-'+tf.strftime('%Hh%Mmin%Ss'), NFresnelMax)
    #pathNewFolder = pathDirFolder +'\\'+ nameFolder
    #nameFile = pathNewFolder+'\\PlaneTextData_{}_N{}.txt'.format(today.strftime('%Y%m%d'), HolesN)
    nameFile = pathDirFolder+'\\PlaneTextData_{}_N{}.txt'.format(today.strftime('%Y%m%d'), HolesN)

    #print('nameFolder: ',nameFolder)
    #print('pathNewFolder: ', pathNewFolder)
    #print('nameFile: ', nameFile)    
    
    # Escribimos txt

    file = open(nameFile, "w", encoding="utf-8") 

    #DictHead = {'lamb': lamb, 'f0': f0, 'npxx': 256, 'npxy': 256, 'lx': lx, 'ly': ly,'Rmax': Rmax[:N-1]}
    strHead = repr(DictHead)
    strHead = strHead.replace('array','')
    strHead = strHead.replace('(','')
    strHead = strHead.replace(')','')
    file.write("DATOS CÁLCULO " + str(tf) + ". NUMBER OF HOLES: {}. COMPUTATION TIME: {} \n".format(HolesN, dt))
    file.write(strHead + "\n")
    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Intensity (U. A.) [(including the first Fresnel zone hole)]\n")

    for i in iters:
        strI = repr(SolOptTOTAL.cost_history[i])
        file.write(strI+ "\n")
    
    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write("Final values for the optimized parameters\n")

    strP = repr(pos)
    strP = strP.replace('array','')
    strP = strP.replace('(','')
    strP = strP.replace(')','')

    file.write(strP+ "\n")

    file.write('---------------------------------------------------------------------------------------------------------------------------------------------' + "\n")
    file.write('Number of Holes for Zone ({} Fresnel Zones.{} Odd Fresnel Zones)\n'.format(NFresnelMax, len(j_n)+1))
    file.write("1 \n")

# No sé por qué pero no escribe lo anterior....
    for i in j_n:
        strI = repr(i)
        file.write(strI+ "\n")


    #for num in range(1,HolesN):
    #    strP = repr(pos)
    #    strP = strP.replace('array','')
    #    strP = strP.replace('(','')
    #    strP = strP.replace(')','')
    #   
    #    file.write(strP+ "\n")
      
        
    file.close()

    return pathDirFolder

def SavedWorkPSO(tf, AntMask, SolOptTOTAL, pos, dt, NFresnelMax, j_n, x_sieves, y_sieves, alpha_i_n, PathFolder):

    pathcwd = os.getcwd()
    today = datetime.date.today()
   
    # Imagen png máscara final
    
    AntMask.draw()[0].savefig(PathFolder+'\\MaskFinal.png') # agregado el 28 Marzo 22, comprobar si funciona...
    
    # guardamos ahí la información
    
    nameFile1 = 'VarOfComp_{}-{}_PSO.npz'.format(today.strftime('%Y%m%d'), tf.strftime('%Hh%Mmin%Ss'))
    np.savez(PathFolder+'\\'+ nameFile1, MaskPSSaved = AntMask, costHistorySaved = SolOptTOTAL.cost_history, posSaved = pos, posHistorySaved = SolOptTOTAL.pos_history, j_nSaved = j_n, x_sievesSaved = x_sieves, y_sievesSaved = y_sieves, alpha_i_nSaved = alpha_i_n)

def PS_OptimizationPSO(f0 = 100* mm, lamb = 0.633* um, NFresnelMax = 9, g = 20, Rmin = 2.5, pixels = (256,256), plots = False, prints = False, **PSOParameters):
    """
        Tiny modify of method widen include in Diffractio. Widens a mask using a convolution of a certain radius.

        Parameters:
            f0 (float): focal of diffractive element. By default is 100 mm.
            lamb: wavelength in um. By default is 0.633 um.
            NFresnelMax (int): It`s maximun number of Fresnel Zones expected for calculate of intensity in the Airy Region, it was calculated for the photon Sieve more simple and 9 zones were obtained. This by default is 9.
            g (float o int): value of "safety belt" of holes.v
            Rmin (float): minimun value manufacturable of holes.
            pixels (int, int): pixels x and pixels y for mask.
            plots = False, prints = False => **verbose (dictionary):  verbose = {plots = True, prints = True}. Show plots & prints. By default is False.
            **PSOParameters (dict): It's a parameters that PSO optimization needs. Example: PSOParameters = {'n_particles': n_particles, 'options': options, 'iters': iters}
                n_particles = 3
                options = {'c1': 0.5, 'c2': 0.6, 'w': 0.9}  
                iters = 30
            
            
            
        Returns:
        
            PS_optimization (GlobalBestPSO object): object of PySwarms module with all information of the optimization performed.
            cost (ndarray): Last value of figure of merit after optimization.
            pos (ndarray): results for the optimization.
            MaskPS (diffractio mask): 2-dimensional array that store x,y coordinates of each hole.
            j_n (list): 1-dimensional list that store the radial positon of each Fresnel zone (excluding the first Fresnel zone hole).
            x_sieves (dict): x cartesian position of each holes for zone. Example: x_sieves = {'n1': array([x1_n1, x2_n1, ..., xj1_n1]), 'n2': array([x1_n2, x2_n2, ..., xj2_n2]), ..., 'N': array([x1_N, x2_N, ..., xjN_N])}
            y_sieves (dict): y cartesian position of each holes for zone. Example: y_sieves = {'n1': array([y1_n1, y2_n1, ..., yj1_n1]), 'n2': array([y1_n2, y2_n2, ..., yj2_n2]), ..., 'N': array([y1_N, y2_N, ..., yjN_N])}

            
    """
    if not 'n_particles' in PSOParameters.keys():
        PSOParameters['n_particles'] = 10
    
    if not 'options' in PSOParameters.keys():
        PSOParameters['options'] = {'c1': 0.5, 'c2': 0.6, 'w': 0.9}
        
    if not 'iters' in PSOParameters.keys():
        PSOParameters['iters'] = 75

    n_particles = PSOParameters['n_particles']
    options = PSOParameters['options']
    iters = PSOParameters['iters']

    ####### PREPARO PARA ESCRIBIR LOGS ##############################
    
    pathcwd = os.getcwd()
    today = datetime.date.today()
    tname = datetime.datetime.now()
    
    nameFolder = 'PSO_{}_PS_{}Zones'.format(today.strftime('%Y%m%d')+'-'+tname.strftime('%Hh%Mmin%Ss'), NFresnelMax)
    pathNewFolder = pathcwd+'\\'+nameFolder
    #nameFile = pathNewFolder+'\\logsData_{}.txt'.format(today.strftime('%Y%m%d'))

    # creamos carpeta
    ArithmeticError
    try:
        os.mkdir(pathNewFolder)
    except OSError:
        print ("Creation of the directory %s failed" % pathNewFolder)
    else:
        print ("Successfully created the directory %s " % pathNewFolder)
    
    print('nameFolder: ',nameFolder)
    print('pathNewFolder: ', pathNewFolder)
    #print('nameFile: ', nameFile)
    
    ##################################################################
    
    t0 = datetime.datetime.now()
    
    # Obtenemos valores de los radios de las zonas semiperiódicas de Fresnel (r_ZFn) y de la posición radial de los agujeros por zona (r_n).
    r_ZFn, r_n = FZP(f0, lamb, NFresnelMax)
    npx = pixels[0]
    npy = pixels[1]
 
    Param = {'r_n': r_n, 'lamb': lamb, 'f0': f0, 'num_pixel': (npx,npy), 'g': g}

    N = len(r_ZFn)
    R_bounds_sup = [] # límite superior de las variables de los radios de los agujeros
    r_bounds_inf = [] # límite inferior de las variables de las posiciones radiales de los agujeros
    r_bounds_sup = [] # límite superior de las variables de las posiciones radiales de los agujeros

    for iter,i in enumerate(range(2,N+1)):
        #print('iter: ',iter+1)
        #print('i: ',i)
        #print('r_FZ(n={}): {}'.format(i,r_ZFn[i-1]))

        r_bounds_inf += [0.5*np.sqrt(lamb*f0)*(np.sqrt(i-2)+np.sqrt(i))+g]
        r_bounds_sup += [0.5*np.sqrt(lamb*f0)*(np.sqrt(i+1)+np.sqrt(i-1))-g]

        #print('r_bounds_inf(n={}): {}'.format(i,r_bounds_inf[i-2]))
        #print('r_bounds_sup(n={}): {}'.format(i,r_bounds_sup[i-2]))

    print('r_bounds_inf: ', r_bounds_inf)
    print('r_bounds_sup: ', r_bounds_sup)
    
    print('r_ZFn: ', r_ZFn)


    R_bounds_sup = 0.5*(np.array(r_ZFn[1:])-np.array(r_ZFn[:-1]))
    
    print('len(R_bounds_sup): ',len(R_bounds_sup))
    print('R_bounds_sup: ', R_bounds_sup)

    for i,j in enumerate(R_bounds_sup):
        if j <= Rmin:
            R_bounds_sup[i] = Rmin+1*um

    #print('R_bounds_sup_correct: ', R_bounds_sup)
    R_bounds_sup_imp = R_bounds_sup[2::2]
    r_bounds_inf_imp = r_bounds_inf[2::2]
    r_bounds_sup_imp = r_bounds_sup[2::2]

    print('R_bounds_sup_correct: ', len(R_bounds_sup))
    print('r_bounds_inf: ', r_bounds_inf_imp)
    print('r_bounds_inf: ', len(r_bounds_inf_imp))

    # Tenemos que definir los límites en los que cada variable variará

    min_bound = list(np.ones(len(R_bounds_sup_imp))*2.5*um) + r_bounds_inf_imp
    max_bound = list(R_bounds_sup_imp) + r_bounds_sup_imp

    dimensions = len(min_bound)
    bounds = (min_bound, max_bound)

    PS_optimization = GlobalBestPSO(n_particles=n_particles, dimensions=dimensions, options=options, bounds=bounds)
    cost, pos = PS_optimization.optimize(opt_func, iters=iters, n_processes=None, verbose=True, **Param)

    x_sieves, y_sieves, j_n_estimated, alpha_i_n = J_Alpha_2D(pos , g)
    MaskPS = MaskPSGen_2D(x_sieves, y_sieves, pos , lamb, f0, num_pixel = (npx,npy), margin_Draw = 1, verbose = False)
    
    tf = datetime.datetime.now()
    dt = tf-t0

    if plots == True:
        MaskPS.draw()
    
    lx = np.max(MaskPS.x) - np.min(MaskPS.x)
    ly = np.max(MaskPS.y) - np.min(MaskPS.y)

    DictHead = {'lamb': lamb, 'f0': f0, 'npxx': npx, 'npxy': npy, 'lx': lx, 'ly': ly}
    
    # número de holes que tiene el PS

    j_n = []
    for i,n in enumerate(x_sieves.values()):
        #print(n)
        if i>0:
            j_n += [len(n)]
    HolesN = sum(j_n)+1
    #print('j_n_estimated: ', j_n_estimated)
    #print('j_n: ', j_n)
    #print('HolesN: ', HolesN)

    PathFolder = WriteDataPSO(HolesN, tname, dt, DictHead, PS_optimization, pos, NFresnelMax, j_n) 

    SavedWorkPSO(tname, MaskPS, PS_optimization, pos, dt, NFresnelMax, j_n, x_sieves, y_sieves, alpha_i_n, PathFolder)  
    
    return  PS_optimization, cost, pos, MaskPS, j_n, x_sieves, y_sieves