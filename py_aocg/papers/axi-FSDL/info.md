# Info

En esta carpeta intentaremos desarrollar la lente axi-FSDL, es decir $f(r,\theta)=f_1(r)f_2(\theta)$, donde $f_1(r)$ se corresponde con un desarrollo polinómico: $f_1(r)=\sum a_j r^j $  y $f_2(\theta)=\sum b_j\cos(2\pi l \theta)$.

Creo que es un paper sencillo, pues sería una generalización de:

[1] Angela Soria-Garcia, Luis Miguel Sanchez-Brea, Jesus del Hoyo, Francisco Jose Torcal-Milla, Jose Antonio Gomez-Pedrero "Fourier series diffractive lens with extended depth of focus" Optics & Laser Technology, Volume 164,109491, 2023, ISSN 0030-3992, https://doi.org/10.1016/j.optlastec.2023.109491.

El efecto de $f_1(r)$ sería ayudar a aumentar la longitud del foco mediante aberración esférica.

*Anexo*: Podría ser también para un axicón + FSDL (podría entrar aquí?) Lo digo por lo mismo, porque la FDSL podría ayudar a ensanchar el haz, aunque bajaría la señal porque el efecto interferencial sería menor.


## Autores principales

* Luis Miguel Sánchez Brea
* Ángela Soria García


## Tareas

1. Definir lente y hacer un pequeño ejemplo en tutorial``
2. Recabar los archivos de optimización
3. Cambiarlos para incluir $f_1(r)$
4. Optimizar y comparar con [1]
5. Si mejora algo, paper
6. Incluir axicon
7. Experimentos

## Observaciones

* Las funciones se pueden meter en py_aocg.angular_lens
