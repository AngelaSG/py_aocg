# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from .utils_math import find_i_nearest


def load_file_data(filename,
                   xmin=None,
                   xmax=None,
                   header=None,
                   has_figure=False,
                   verbose=False):

    df = pd.read_csv(filename, delimiter='\t', comment='%', header=header)
    data = df.values
    x = data[:, 0]
    y = data[:, 1]

    if xmin is not None:
        i_min = find_i_nearest(x, xmin)
    else:
        i_min = 0

    if xmax is not None:
        i_max = find_i_nearest(x, xmax)
    else:
        i_max = -1

    if xmin is None or xmax is None:
        pass
    else:
        x = x[i_min:i_max]
        y = y[i_min:i_max]

    if has_figure:
        plt.figure()
        plt.plot(x, y, 'ko')
        plt.xlim(x[0], x[-1])
        plt.title(filename)
        # plt.savefig("{}.png".format(filename[:-4]))

    if verbose:
        print(data.shape, i_min, i_max, x[0], x[-1])
        print(data)

    return x, y


def spectral_reflectivity(w, rbk0, rbk0im, rbk1, rbk1im, rbk2, rbk2im, rbk3,
                          rbk3im, w0, f_real, f_imag, gamma):
    """_summary_

    Returns:
        _type_: _description_
    """
    f = f_real + 1j * f_imag
    rbk0 = rbk0 + 1j * rbk0im
    rbk1 = rbk1 + 1j * rbk1im
    rbk2 = 1 * rbk2 + 0j * rbk2im
    rbk3 = 0 * rbk3 + 0j * rbk3im

    r = rbk0 + rbk1 * (w - w0) + rbk2 * (w - w0)**2 + rbk3 * (
        w - w0)**3 + f * gamma / (1j * (w - w0) + gamma)

    return np.abs(r)**2