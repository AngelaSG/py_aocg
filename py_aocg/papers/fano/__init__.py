# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Femto-laser processed metasurface with Fano response: Applications to a high performance sensing device
    """

import numpy as np
from matplotlib import rcParams

kB = 1.38064852e-23
e = 1.6021766208e-19
h = 6.626070040e-34
hp = h / (2 * np.pi)
c = 3e8

alpha = 0.07  #eV/V

rcParams['figure.dpi'] = 150  #dpi
rcParams['axes.linewidth'] = 1.5
rcParams['axes.formatter.useoffset'] = False
rcParams['font.size'] = 14
rcParams['axes.labelsize'] = 14
