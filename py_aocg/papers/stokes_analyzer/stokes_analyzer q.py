# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Device based in a vectorial lens, to determine the Stokes parameters of a light beam.
@Authors:
    Javier Alda,
    Luis Miguel Sánchez Brea
    Jesús del Hoyo
"""

import os
import pathlib
from copy import deepcopy

from diffractio import degrees, mm, np, plt, um
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.vector_masks_XY import Vector_mask_XY
from diffractio.vector_sources_XY import Vector_source_XY
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector
from py_pol.stokes import Stokes

sq2 = 1 / np.sqrt(2)

# Matriz para calcular los parámetros de Stokes a partir de las medidas
vectors_standard = [(1, 0), (0, 1), (sq2, sq2), (sq2, -sq2), (sq2, 1j * sq2),
                    (sq2, -1j * sq2)]
Wi_standard = np.array([[1 / 3, 1 / 3, 1 / 3, 1 / 3, 1 / 3, 1 / 3],
                        [1, -1, 0, 0, 0, 0], [0, 0, 1, -1, 0, 0],
                        [0, 0, 0, 0, 1, -1]])


def matrices_standard():
    """Create several standard polarization matrices:
        - 0: linear 0
        - 1: linear 90
        - 2: linear 45
        - 3: linear 135
        - 4: diattenuator_azimuth_ellipticity 45º
        - 5: diattenuator_azimuth_ellipticity - 45º

    Returns:
        matrices(list): polarization matrices por the indices
    """
    # matrix0 = Jones_matrix(name='zero')
    #
    # matrix1 = Jones_matrix(name='one')
    # matrix1.vacuum()

    matrix1 = Jones_matrix(name='linear_0')
    matrix1.diattenuator_perfect(azimuth=0 * degrees)

    matrix2 = Jones_matrix(name='linear_90')
    matrix2.diattenuator_perfect(azimuth=90 * degrees)

    matrix3 = Jones_matrix(name='linear_p45')
    matrix3.diattenuator_perfect(azimuth=45 * degrees)

    matrix4 = Jones_matrix(name='linear_m45')
    matrix4.diattenuator_perfect(azimuth=135 * degrees)

    matrix5 = Jones_matrix(name='retarder_p')
    matrix5.diattenuator_azimuth_ellipticity(ellipticity=45 * degrees,
                                             azimuth=0 * degrees)

    matrix6 = Jones_matrix(name='retarder_m')
    matrix6.diattenuator_azimuth_ellipticity(ellipticity=-45 * degrees,
                                             azimuth=0 * degrees)

    matrices = [matrix1, matrix2, matrix3, matrix4, matrix5, matrix6]
    return matrices


jones_standard = matrices_standard()

# Matriz para eliminar la intensidad espuria que viene de otros detectores
diffraction_matrix_standard = np.matrix(
    np.eye(6))  # hay que ponerlo según num_sectors

# data = np.load("Optimizacion_SLM_polarimetria.npz")


class Detector(Scalar_field_XY):
    """Detector class

    Optimizacion_SLM_polarimetria.npz -> Load data from ...

    """

    def __init__(self, x, y, wavelength, num_sectors, r_shift, order):
        """equal than Scalar_field_XY"""
        super(self.__class__, self).__init__(x, y, wavelength, info="")

        self.num_sectors = num_sectors
        self.r_shift = r_shift
        self.set_order(order)

    def set_order(self, order):
        if order is None:
            order = range(1, self.num_sectors + 1)

        self.order = order

    def circular(self, detector_radius):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            has_draw (bool): If True drasw figures

        """

        r_shift = self.r_shift

        x0 = self.x
        y0 = self.y
        wavelength = self.wavelength

        angle_detector = 2 * np.pi / self.num_sectors

        theta = np.linspace(angle_detector / 2,
                            2 * np.pi + angle_detector / 2,
                            num=self.num_sectors,
                            endpoint=False)
        x_center_detectors = r_shift * np.cos(theta)
        y_center_detectors = r_shift * np.sin(theta)

        detector = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        detector_index = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        for i in range(self.num_sectors):
            detector.circle(r0=(x_center_detectors[i], y_center_detectors[i]),
                            radius=detector_radius,
                            angle=0 * degrees)
            detector_index.u = detector_index.u + self.order[i] * detector.u

        self.u = detector_index.u
        return self

    def square(self, detector_size=250 * um):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            has_draw (bool): If True drasw figures

        """
        r_shift = self.r_shift

        x0 = self.x
        y0 = self.y
        wavelength = self.wavelength

        angle_detector = 2 * np.pi / self.num_sectors

        theta = np.linspace(angle_detector / 2,
                            2 * np.pi + angle_detector / 2,
                            num=self.num_sectors,
                            endpoint=False)
        x_center_detectors = r_shift * np.cos(theta)
        y_center_detectors = r_shift * np.sin(theta)

        detector = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        detector_index = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        for i in range(self.num_sectors):
            detector.square(r0=(x_center_detectors[i], y_center_detectors[i]),
                            size=detector_size,
                            angle=0 * degrees)
            detector_index.u = detector_index.u + self.order[i] * detector.u

        self.u = detector_index.u
        return self

    def slices(self, fill_sectors=0.5, radius_size=2 * mm):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            has_draw (bool): If True drasw figures

        """
        r_shift = self.r_shift

        x0 = self.x
        y0 = self.y
        wavelength = self.wavelength

        R = np.sqrt((self.X)**2 + (self.Y)**2)
        Theta = np.arctan2(-self.Y, -self.X) + np.pi

        angle_detector = 2 * np.pi / self.num_sectors

        angle_size_maximum = 2 * np.pi / self.num_sectors
        angle_center = np.linspace(0,
                                   2 * np.pi,
                                   num=self.num_sectors,
                                   endpoint=False)

        detector = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        detector_index = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        for i in range(self.num_sectors):
            theta_min = angle_center[i] + angle_detector / \
                2 - angle_size_maximum / 2 * fill_sectors
            theta_max = angle_center[i] + angle_detector / \
                2 + angle_size_maximum / 2 * fill_sectors
            if theta_min < 0:
                theta_min = theta_min + 2 * np.pi
            if theta_max < 0:
                theta_max = theta_max + 2 * np.pi

            if theta_min < theta_max:
                i_pasa = np.bitwise_and(Theta > theta_min, Theta < theta_max)
            else:
                i_pasa = 1 - np.bitwise_and(Theta > theta_max,
                                            Theta < theta_min)
            detector.u[i_pasa] = 1
            detector_index.u = detector_index.u + self.order[i] * i_pasa

        detector_index.u[R < r_shift - radius_size / 2] = 0
        detector_index.u[R > r_shift + radius_size / 2] = 0
        detector_index.pupil()

        self.u = detector_index.u
        return self


class Stokes_analyzer(object):
    """stokes_analyzer_lens"""

    def __init__(self,
                 x,
                 y,
                 wavelength,
                 num_sectors,
                 focal,
                 r_shift,
                 radius=None,
                 jones_matrices=None,
                 trans_0=0,
                 info=""):
        """Arguments:
            x (np.array): x array
            y (np.array): y array
            wavelength (float): wavelength
            num_sectors (int): number of sectors
            focal (float): focal distance for the lens
            r_shift (float): shift of the lenses from center
            radius (float): radius of the lens
            jones_matrices (list): list with Jones_matrix of each of the sectors
            trans_0 (float): Transparency of the 0 level. Default: 0.
        """

        self.x = x
        self.y = y
        self.wavelength = wavelength
        self.info = info
        self.focal = focal
        self.num_sectors = num_sectors
        self.r_shift = r_shift
        if radius is None:
            self.radius = (x[-1] - x[0]) / 2
        else:
            self.radius = radius
        self.Wi = Wi_standard
        self.diffraction_matrix = None
        self.index_lens = None
        self.vector_lens = None
        self.jones_matrices = jones_matrices
        self.detector = None
        self.order = [1, 2, 3, 4, 5, 6]
        self.trans_0 = trans_0

    def circle_mask(self, has_draw=False):
        x0 = self.x
        y0 = self.y
        wavelength = self.wavelength
        num_sectors = self.num_sectors
        r_shift = self.r_shift

        apertura = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
        apertura_dummy = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)

        angle_apertura = 2 * np.pi / num_sectors
        theta = np.linspace(angle_apertura / 2,
                            2 * np.pi + angle_apertura / 2,
                            num=num_sectors,
                            endpoint=False)
        x_center_apertura = r_shift * np.cos(theta)
        y_center_apertura = r_shift * np.sin(theta)

        for jj in range(num_sectors):
            apertura_dummy.circle(r0=(x_center_apertura[jj],
                                      y_center_apertura[jj]),
                                  radius=r_shift / 2,
                                  angle=0 * degrees)
            apertura.u = apertura.u + apertura_dummy.u

        if has_draw is True:
            apertura.draw(kind='amplitude')

        self.index_lens = self.index_lens * apertura

    def lens_indexes(self, order=None, has_draw=False, return_sectors=True):
        """Generates Scalar lens for stokes analyzer analyzer.

        Arguments:
            order (list of ints)
            has_draw (bool): If True, shows the mask
        """

        num_sectors = self.num_sectors
        r_shift = self.r_shift
        focal = self.focal

        if order is None:
            order == self.order
        if order is None:
            order = range(1, self.num_sectors + 1)

        index_lens = Scalar_mask_XY(x=self.x,
                                    y=self.y,
                                    wavelength=self.wavelength)
        sectors = Scalar_mask_XY(x=self.x,
                                 y=self.y,
                                 wavelength=self.wavelength)

        num_pixels_x = len(self.x)
        num_pixels_y = len(self.y)
        length = self.x[-1] - self.x[0]

        mini_angle = 360 / (2 * num_sectors)
        radiolentellena = np.sqrt((length + r_shift)**2 + (length)**2)
        theta = np.linspace(
            mini_angle, 360 - mini_angle, num=num_sectors,
            endpoint=True) * degrees

        dd_x = r_shift * np.cos(theta)
        dd_y = r_shift * np.sin(theta)

        # obteniendo las máscaras de las lentes de Fresnel desplazadas
        campos = np.zeros((num_pixels_x, num_pixels_y, num_sectors))

        t_dummy = Scalar_mask_XY(self.x, self.y, self.wavelength)
        for jj in range(num_sectors):
            t_dummy.fresnel_lens(r0=(dd_x[jj], dd_y[jj]),
                                 radius=radiolentellena,
                                 focal=focal,
                                 angle=0 * degrees,
                                 kind='amplitude',
                                 phase=0,
                                 mask=True)
            if self.trans_0 > 0:
                t_dummy.u[t_dummy.u == 0] = 0.5
            campos[:, :, jj] = t_dummy.u

            # recortando las máscaras (aqui se calcula alfa en grados, desde 0 a 360,
            # estando el 0 en el semieje positivo de las X (horizontales))
            xx, yy = np.meshgrid(self.x, self.y, indexing='xy')
            alfa = np.arctan2(-yy, -xx) * (180 / np.pi) + 180

            ceros = np.zeros((num_pixels_x, num_pixels_y))
            unos = np.ones((num_pixels_x, num_pixels_y))

            thetadeg2 = np.linspace(0, 360, num=num_sectors + 1, endpoint=True)

            sectores = np.zeros((num_pixels_x, num_pixels_y))
            # mascaras = np.zeros((num_pixels_x, num_pixels_y, num_sectors))
            # sectores = np.zeros((num_pixels_x, num_pixels_y, num_sectors))
            mascara_total = np.zeros((num_pixels_x, num_pixels_y))
            # niveles = np.linspace(1,
            #                       num_sectors,
            #                       num=num_sectors,
            #                       endpoint=True,
            #                       dtype='int')

        for i in range(num_sectors):
            triangulo = np.where(
                np.logical_and(alfa > thetadeg2[i], alfa < thetadeg2[i + 1]),
                unos, ceros)
            mascara = triangulo * campos[:, :, i]
            sectores = sectores + triangulo * order[i]
            if self.trans_0 > 0:
                mascara[mascara == 1] = order[i]
                mascara[mascara == 0.5] = order[i] + num_sectors
                mascara_total = mascara_total + mascara
            else:
                mascara_total = mascara_total + mascara * (order[i])
        index_lens.u = mascara_total
        sectors.u = sectores

        # recorta la máscara a un círculo
        index_lens.pupil()
        sectors.pupil()

        self.index_lens = index_lens

        if has_draw is True:
            index_lens.draw(kind='amplitude',
                            has_colorbar='vertical',
                            colormap_kind='jet',
                            range_scale='mm')
            plt.clim(vmin=0)
            # sectors.draw(kind='amplitude',
            #              has_colorbar='vertical',
            #              colormap_kind='jet')
            # plt.clim(vmin=0)

        self.index_lens = index_lens
        if return_sectors:
            return index_lens, sectors
        else:
            return index_lens

    def lens_binary(self, filename="", has_draw=False):
        """Creates the binary lens, in order to print it in primter.
        Arguments:
            filename (str): If it is not "", then save mask (pdf, jpg, png)
            has_draw (bool): If True, shows the mask
        """
        num_pixels = len(self.x)

        lens_binary = self.index_lens.duplicate()
        lens_binary.u[lens_binary.u > 0] = 1
        if has_draw:
            lens_binary.draw()
        if filename != "":
            fn = os.path.splitext(filename)
            name = fn[0]
            extension = fn[1]
            new_name = "{} num_pixels_{} sectors_{} focal_{:0.0f} r_shift_{:0.1f}{}".format(
                name, num_pixels, self.num_sectors, self.focal / mm,
                self.r_shift / mm, extension)
            lens_binary.save_mask(new_name)

        return lens_binary, new_name

    def lens_vector(self,
                    jones_matrices,
                    has_draw=False,
                    jones0=None,
                    trans=None):
        """Creates the binary lens, in order to print it in printer.
        Arguments:
            jones_matrices (list): list with Jones matrix for each index
            order (list): order of the matrices to fill the index
            has_draw (bool): If True, shows the mask.
            jones0 (Jones_matrix or None): Jones matrix of the 0 level. If None, a neutral density filter is used. Default: None.
            trans (float): Field transmission of the 0 level. Default: None.
        """
        # Preparar lista de matrices de Jones
        if trans is None:
            trans = self.trans_0
        else:
            self.trans_0 = trans
        self.jones_matrices = jones_matrices
        jones_matrices = deepcopy(jones_matrices)
        if trans > 0:
            for ind in range(len(jones_matrices)):
                jones_matrices.append(jones_matrices[ind] * trans)
        if jones0 is None:
            jones0 = Jones_matrix(name='zero').filter_amplifier(D=trans)
        jones_matrices.insert(0, jones0)

        mask_vector = Vector_mask_XY(self.x, self.y, self.wavelength)
        mask_vector.multilevel_mask(mask=self.index_lens,
                                    states=jones_matrices,
                                    discretize=False,
                                    normalize=False)
        # mask_vector.pupil()
        if has_draw:
            mask_vector.draw('amplitude', range_scale='mm')
            mask_vector.draw('phase', range_scale='mm')
        self.vector_lens = mask_vector

    # def lens_vector(self, states, order=None, has_draw=False):
    #     """Creates the binary lens, in order to print it in printer.
    #     Arguments:
    #         states (list): list with Jones matrix for each index
    #         order (list): order of the states to fill the index
    #         has_draw (bool): If True, shows the mask
    #     """
    #
    #     if len(states) <= self.num_sectors:
    #         state0 = Jones_matrix(name='zero')
    #         states.insert(0, state0)
    #
    #     if order is None:
    #         order = self.order
    #
    #     if order is not None:
    #         states_new = (states[0], states[order[0]], states[order[1]],
    #                       states[order[2]], states[order[3]], states[order[4]],
    #                       states[order[5]])
    #
    #     # self.states = states
    #     mask_vector = Vector_mask_XY(self.x, self.y, self.wavelength)
    #     mask_vector.multilevel_mask(mask=self.index_lens,
    #                                 states=states_new,
    #                                 discretize=False,
    #                                 normalize=False)
    #     mask_vector.pupil()
    #     if has_draw:
    #         mask_vector.draw('amplitude', z_scale='mm')
    #         mask_vector.draw('phase', z_scale='mm')
    #     self.vector_lens = mask_vector

    def compute_intensity(self, j0, z_obs, lens_vector=None, u=1):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            j0 (jones_vector): Jones vector of the incoming light beam
            z_obs (float): observation plane, normally at focal length of lens
            u (Scalar_field_XY or float): Amplitude of the field (without polarization)

        Returns:
            intensidad_z (numpy.matrix):
            J_out_z (Vector_field_XY): Vector field at the focal plane
        """
        # stokes_input = Stokes().from_Jones(jones_vector_incident)

        # horizontal linear polarization

        if type(j0) in (tuple, list):
            j0 = Jones_vector().from_components(j0)

        J_in = Vector_source_XY(self.x, self.y, self.wavelength)
        J_in.constant_polarization(u=u,
                                   v=j0.M,
                                   has_normalization=True,
                                   radius=0.)

        if lens_vector is None:
            J_out = J_in * self.vector_lens
        else:
            J_out = J_in * lens_vector

        J_out_z = J_out.VRS(z=z_obs)
        intensidad_z = J_out_z.intensity()

        return intensidad_z, J_out_z

    def set_detector(self, detector, has_draw=False):
        """Establish a detector.

        Arguments:
            detector (Detector): Detector
        """

        self.detector = detector

        if has_draw:
            detector.draw(kind='amplitude',
                          range_scale='mm',
                          colormap_kind='jet')
            plt.clim(vmin=0)
            plt.xlim(-1.25 * self.r_shift / mm, 1.25 * self.r_shift / mm)
            plt.ylim(-1.25 * self.r_shift / mm, 1.25 * self.r_shift / mm)

    def compute_stokes(self,
                       intensidad_z,
                       order=None,
                       filter=False,
                       Wi=None,
                       diffraction_matrix=None,
                       j0=None):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            intensidad_z (Scalar_mask_XY(): Intensity distribution
            filter(bool): Filter Stokes physicall realizable conditions. Default: False (so it is the same as in old versions).
            j0(jones_vector): Jones vector of the incoming light beam

        Returns:
            J_out_z(Vector_field_XY): Vector field at the focal plane
            signals(np.array - num_sectors): Intensity at the sectorized - sectors
            stokes_result(Stokes): Computed Stokes vector.
            jones_result(Jones_vector): Computed Jones vector.
            error(float): Error between the incident polarization(jones_vector) and de computed jones vector

        """

        if order is None:
            order = self.order
        if Wi is None:
            Wi = self.Wi
        if diffraction_matrix is None:
            diffraction_matrix = self.diffraction_matrix

        # El estado 0 no tiene wi
        Wi_new = np.zeros_like(Wi)
        for i in range(6):
            Wi_new[:, i] = Wi[:, order[i] - 1]

        signals = np.zeros((self.num_sectors))
        for i in self.order:  #
            detector_area = self.detector.u == i
            signal = intensidad_z * detector_area
            signals[i - 1] = signal.mean()

        signals_new = diffraction_matrix @ signals
        stokes = Wi_new @ signals_new.transpose()
        stokes = stokes / stokes[0]

        stokes_result = Stokes('detectada').from_components(stokes)

        if j0 is not None:
            if type(j0) in (tuple, list):
                j0 = Jones_vector().from_components(j0)

            stokes_input = Stokes().from_Jones(j0)

            error = np.linalg.norm(stokes_result.normalize().M -
                                   stokes_input.normalize().M)
        else:
            error = None

        return stokes_result, signals_new, error

    def get_diffraction_matrix(self,
                               jones_vectors,
                               has_draw=False,
                               verbose=True,
                               save_fig=False,
                               return_signals=False,
                               all_signals=False):
        """    Procedimiento para eliminar los errores debidos a la interacción entre las lentes.
        La idea es activar uno a uno(no a la vez) cada uno de los sectores. Se mete luz polarizada por cada no de los estados y se calcula la potencia en cada uno de los detectores. Esto va a suponer una matriz 6x6 que va a servir para invertirla: se mete un estado de polarización y se obtiene otro. Mediante la matriz de inversión se resuelve

        Está en Stokes_lens_ideal_matriz_difraccion:
        """
        conversion_signals = (np.zeros((self.num_sectors, self.num_sectors)))
        if all_signals:
            signals_out = np.zeros((self.num_sectors, self.num_sectors))
        else:
            signals_out = np.zeros(self.num_sectors)

        Mascara_sector = Detector(self.x, self.y, self.wavelength,
                                  self.num_sectors, self.r_shift, self.order)
        Mascara_sector.slices(fill_sectors=1, radius_size=10000)

        Wi_new = np.zeros_like(self.Wi)
        for i in range(6):
            Wi_new[:, i] = self.Wi[:, self.order[i] - 1]

        for i in self.order:
            i = i - 1
            ji = jones_vectors[i]

            sector = Scalar_mask_XY(self.x, self.y, self.wavelength)
            # sector.u[Mascara_sector.u == i +
            #          1] = 1  # las detecciones son entre 1 y 6

            sector.u[Mascara_sector.u == i + 1] = 1

            mask_sector = self.vector_lens.duplicate()
            mask_sector.apply_scalar_mask(sector)

            jones_input = Jones_vector().from_components(ji)
            stokes_input = Stokes().from_Jones(jones_input)
            m = stokes_input.M.flatten()
            text = "[{:0.0f},{:0.0f},{:0.0f},{:0.0f}]".format(
                m[0], m[1], m[2], m[3])

            intensidad_z, J_z = self.compute_intensity(jones_input,
                                                       z_obs=self.focal,
                                                       lens_vector=mask_sector,
                                                       u=1,
                                                       has_draw=has_draw,
                                                       )

            stokes_result, signals, error = self.compute_stokes(
                intensidad_z,
                order=self.order,
                diffraction_matrix=diffraction_matrix_standard,
                filter=True)

            if return_signals and all_signals:
                signals_out[i, :] = signals
            elif return_signals:
                signals_out[i] = signals.max()
            conversion_signals[:, i] = signals / signals.max()

            if verbose:
                print(
                    conversion_signals[:,
                                       i])  # , '\n\n', signals/signals.max())

            if has_draw:
                plt.text(0.9 * self.x.min(),
                         0.85 * self.y.max(),
                         text,
                         fontsize=16,
                         color='white')
                if save_fig is True:
                    plt.savefig("detector_unitary_{}.pdf".format(i))

        diffraction_matrix = np.zeros_like(conversion_signals)

        for i in range(self.num_sectors):
            diffraction_matrix[:, i] = conversion_signals[:, self.order[i] - 1]

        conversion_matrix = np.matrix(diffraction_matrix)
        conversion_matrix_inverse = conversion_matrix.I

        self.diffraction_matrix = conversion_matrix_inverse
        # print(conversion_signals)
        # print([conversion_signals[i,i] for i in range(6)])

        if return_signals:
            return conversion_matrix_inverse, signals_out, signals_out / np.mean(
                signals_out)
        else:
            return conversion_matrix_inverse

    def compute_stokes_several(self,
                               jones_vectors,
                               z_obs,
                               u=1,
                               order=None,
                               Wi=None,
                               diffraction_matrix=None,
                               filter=False,
                               has_draw=False,
                               return_mean=False,
                               filename_figure='',
                               filename_data=''):
        """calcula la distribución de intensidad, y parámetros de polarización producida por una lente vectorial

        Arguments:
            jones_vectors (np.array): Array of matrices with Jones vector of the incoming light beam
            z_obs(float): distance at observation plane
            Wi(np.array(6x4)): Conversion factors between intensitys and Stokes polarization vector
            diffraction_matrix (6x6): diffraction matrix
            filter(bool): Filter Stokes physical realizable conditions. Default: False (so it is the same as in old versions).
            has_draw(bool): If True draws figures
            return_mean (bool): Return the mean signal
            filename_figures (str): If not '', then saves figures
            filename_data (str): If not '', then saves data

        Returns:
            stokes_result(Stokes): Computed Stokes vector.
            signals(np.array - num_sectors): Intensity at the sectorized - sectors
            error(float): Error between the incident polarization(jones_vector) and de computed jones vector
        """

        if filename_figure != '':
            has_draw = True
        if order is None:
            order = self.order
        if Wi is None:
            Wi = self.Wi
        if diffraction_matrix is None:
            diffraction_matrix = self.diffraction_matrix

        # Jz_outs = []
        # Intensidades_z = []
        Signals = []
        Stokes_results = []
        Errors = []

        for i, jones_i in enumerate(jones_vectors):
            if type(jones_i) in (tuple, list):
                jones_i = Jones_vector().from_components(jones_i)

            # stokes_input = Stokes().from_Jones(jones_i)

            intensidad_z, J_z = self.compute_intensity(j0=jones_i,
                                                       z_obs=z_obs,
                                                       u=u,
                                                       has_draw=False)

            stokes_result, signals, error = self.compute_stokes(
                intensidad_z,
                order=order,
                filter=filter,
                Wi=Wi,
                diffraction_matrix=diffraction_matrix,
                j0=jones_i)

            # Intensidades_z.append(intensidad_z)
            Signals.append(signals)
            Stokes_results.append(stokes_result)
            Errors.append(error)
            print("{}, error = {:2.2f} %".format(i, error * 100), end='\r')

        Errors = np.array(Errors)

        if return_mean:
            mean = np.mean(np.array(Signals))
            return Stokes_results, Signals, Errors, np.array(Signals) / mean
        else:
            return Stokes_results, Signals, Errors


def matrices_SLM(indices, filename="SLM_Jones_components.npz"):
    """Funcion que crea la lista de matrices de jones correspondiente a los niveles de gris del SLM introducidos en indices.

    Arguments:
        indices(np.array(int)): integer values for the indexes to extract
        filename(str): filename of the npz file with Jones data of SLM

    Returns:
        matrices(list): polarization matrices por the indices

    """
    # Cargar la calibración del SLM
    old_dir = os.getcwd()

    path_0 = pathlib.Path(__file__).parent.absolute()

    os.chdir(path_0)
    data = np.load(filename)
    J0 = data["J0"] * np.exp(1j * data["d0"])
    J1 = data["J1"] * np.exp(1j * data["d1"])
    J2 = data["J2"] * np.exp(1j * data["d2"])
    J3 = data["J3"] * np.exp(1j * data["d3"])
    os.chdir(old_dir)

    # Crear la matriz de Jones
    Jslm = Jones_matrix().from_components([J0, J1, J2,
                                           J3]).remove_global_phase()

    # Añadir el efecto del PSA si existe
    try:
        angulos = data["angles"]
        Jr = Jones_matrix().quarter_waveplate(azimuth=angulos[0])
        Jp = Jones_matrix().diattenuator_linear(azimuth=angulos[1])
        Jslm = Jp * Jr * Jslm
    except:
        pass

    # Crear la lista de matrices de Jones
    matrices = []
    for ind in indices:
        matrices.append(Jslm[ind])

    return matrices


def fill_sphere_flower(num_samples=100, kind_exit='list', has_draw=False):
    """
    https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    """
    indices = np.arange(0, num_samples, dtype=float) + 0.5

    phi = np.arccos(1 - 2 * indices / num_samples)
    theta = np.pi * (1 + 5**0.5) * indices

    x, y, z = np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(
        phi)

    if has_draw:
        plt.figure().add_subplot(111, projection='3d').scatter(x, y, z)
        plt.show()

    if kind_exit == 'list':
        return (x, y, z)
    elif kind_exit == 'numpy_array':
        return np.vstack((x, y, z)).transpose()
    elif kind_exit == 'Stokes':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        return x, y, z, S
    elif kind_exit == 'Jones':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        J = Jones_vector().from_Stokes(S)
        return x, y, z, J
    else:
        print("No kind_exit parameter")
        return None


def fill_sphere_standard(num_samples=100,
                         kind_exit='list',
                         is_flatten=True,
                         has_draw=False):

    N = int(np.sqrt(num_samples))
    theta = np.linspace(0, np.pi - 1e-6, N)
    phi = np.linspace(0, 2 * np.pi - 1e-6, N)
    theta, phi = np.meshgrid(theta, phi)

    # The Cartesian coordinates of the unit sphere
    x = np.sin(theta) * np.cos(phi)
    y = np.sin(theta) * np.sin(phi)
    z = np.cos(theta)

    if is_flatten:
        x = x.flatten()
        y = y.flatten()
        z = z.flatten()

    if has_draw:
        plt.figure().add_subplot(111, projection='3d').scatter(x, y, z)
        plt.show()

    if kind_exit == 'list':
        return (x, y, z)
    elif kind_exit == 'numpy_array':
        return np.vstack((x, y, z)).transpose()
    elif kind_exit == 'Stokes':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        return x, y, z, S
    elif kind_exit == 'Jones':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        J = Jones_vector().from_Stokes(S)
        return x, y, z, J
    else:
        print("No kind_exit parameter")
        return None


def fill_sphere_fibonacci(num_samples=100, kind_exit='list', has_draw=False):
    """Generate a quasi - uniform distribution around the poincare sphere.

    Arguments:
        num_samples(int): number of samples.
        kind_exit(str): ('list', 'numpy_array', 'Stokes', 'Jones')

    Reference:
         https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    """
    golden_angle = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    i = np.linspace(0, num_samples, num_samples)
    y = 1 - (i / (num_samples - 1)) * 2
    y[y > 1] = 1
    y[y < -1] = -1
    radius = np.sqrt(1 - y**2)  # radius at y
    theta = golden_angle * i  # golden angle increment
    x = np.cos(theta) * radius
    z = np.sin(theta) * radius

    if kind_exit == 'list':
        return (x, y, z)
    elif kind_exit == 'numpy_array':
        return np.vstack((x, y, z)).transpose()
    elif kind_exit == 'Stokes':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        return x, y, z, S
    elif kind_exit == 'Jones':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        J = Jones_vector().from_Stokes(S)
        return x, y, z, J
    else:
        print("No kind_exit parameter")
        return None
