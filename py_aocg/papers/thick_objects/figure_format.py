"""Formato del artículo, para que todas sean iguales
"""

from matplotlib import cm, rcParams

rcParams['figure.dpi'] = 300
rcParams['figure.figsize'] = (6, 8)
