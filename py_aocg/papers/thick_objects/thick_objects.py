# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module for thick wire.

"""

import itertools
from matplotlib import rcParams

from diffractio import np, sp, plt
from diffractio import um, degrees, mm

from diffractio.scalar_sources_X import Scalar_source_X
from diffractio.scalar_masks_X import Scalar_mask_X
from diffractio.scalar_masks_XZ import Scalar_mask_XZ
from diffractio.scalar_fields_Z import Scalar_field_Z
from diffractio.scalar_masks_XY import Scalar_mask_XY

from diffractio.utils_math import find_local_extrema, nearest, nearest2
from scipy.special import airy, ai_zeros, bi_zeros


def func_semicylinder2(x, z, wavelength, *args):
    """_summary_

    Args:
        x (_type_): _description_
        z (_type_): _description_
        wavelength (_type_): _description_
        refractive_index (_type_): _description_
        r0 (_type_): _description_
        radius (_type_): _description_

    Returns:
        _type_: _description_
    """

    u = Scalar_mask_XZ(x, z, wavelength, n_background=n_out)
    u.semi_sphere(r0=r0,
                  radius=diameter / 2,
                  refractive_index=refractive_index,
                  angle=180 * degrees)
    return np.squeeze(u.n)


def func_semicylinder(x, z, wavelength):
    """_summary_

    Args:
        x (_type_): _description_
        z (_type_): _description_
        wavelength (_type_): _description_
        refractive_index (_type_): _description_
        r0 (_type_): _description_
        radius (_type_): _description_

    Returns:
        _type_: _description_
    """

    global r0
    global diameter
    global n_out
    global refractive_index

    u = Scalar_mask_XZ(x, z, wavelength, n_background=n_out)
    u.semi_sphere(r0=r0,
                  radius=diameter / 2,
                  refractive_index=refractive_index,
                  angle=180 * degrees)
    return np.squeeze(u.n)


def func_cylinder(x, z, wavelength):
    """_summary_

    Args:
        x (_type_): _description_
        z (_type_): _description_
        wavelength (_type_): _description_
        refractive_index (_type_): _description_
        r0 (_type_): _description_
        radius (_type_): _description_

    Returns:
        _type_: _description_
    """
    global r0
    global diameter
    global n_out
    global refractive_index


# def func_cylinder_2(x, z, wavelength, **kwargs):
#     """_summary_

#     Args:
#         x (_type_): _description_
#         z (_type_): _description_
#         wavelength (_type_): _description_
#         refractive_index (_type_): _description_
#         r0 (_type_): _description_
#         radius (_type_): _description_

#     Returns:
#         _type_: _description_
#     """
#     # global r0
#     # global diameter
#     # global n_out
#     # global refractive_index

#     u = Scalar_mask_XZ(x, z, wavelength, n_background=n_out)
#     u.sphere(r0=r0,
#              radius=diameter / 2,
#              refractive_index=refractive_index,
#              angle=0)
#     return np.squeeze(u.n)


def func_vacuum(x, z, wavelength):
    """_summary_

    Args:
        x (_type_): _description_
        z (_type_): _description_
        wavelength (_type_): _description_
        refractive_index (_type_): _description_
        r0 (_type_): _description_
        radius (_type_): _description_

    Returns:
        _type_: _description_
    """

    u = Scalar_mask_XZ(x, z, wavelength)

    return np.squeeze(u.n)


def func_thick_strip(x, z, wavelength):
    """_summary_

    Args:
        x (_type_): _description_
        z (_type_): _description_
        wavelength (_type_): _description_
        refractive_index (_type_): _description_
        r0 (_type_): _description_
        radius (_type_): _description_

    Returns:
        _type_: _description_
    """

    global diameter
    global n_out
    global refractive_index
    global depth

    u = Scalar_mask_XZ(x, z, wavelength)
    u.rectangle(r0=(0 * um, 1.25 * diameter / 2 + depth / 2),
                size=(diameter, depth),
                angle=0,
                refractive_index=refractive_index)
    return np.squeeze(u.n)


def func_biprism(x, z, wavelength):
    """_summary_

        Args:
            x (_type_): _description_
            z (_type_): _description_
            wavelength (_type_): _description_
            refractive_index (_type_): _description_
            r0 (_type_): _description_
            radius (_type_): _description_

        Returns:
            _type_: _description_
        """

    global diameter
    global n_out
    global refractive_index
    global depth

    u = Scalar_mask_XZ(x, z, wavelength)
    u.biprism(r0=(0 * um, diameter / 2),
              length=diameter,
              height=depth,
              refractive_index=refractive_index,
              angle=0)

    return np.squeeze(u.n)


def func_biprism_inverted(x, z, wavelength):
    """_summary_

        Args:
            x (_type_): _description_
            z (_type_): _description_
            wavelength (_type_): _description_
            refractive_index (_type_): _description_
            r0 (_type_): _description_
            radius (_type_): _description_

        Returns:
            _type_: _description_
        """

    global diameter
    global n_out
    global refractive_index
    global depth

    u = Scalar_mask_XZ(x, z, wavelength)
    u.biprism(r0=(0 * um, diameter / 2),
              length=diameter,
              height=depth,
              refractive_index=refractive_index,
              angle=180 * degrees)

    return np.squeeze(u.n)


def func_hyperellipse(x, z, wavelength):
    global diameter
    global refractive_index
    global n_shape

    radius = diameter / 2

    txy = Scalar_mask_XY(x, x, wavelength, n_background=n_out)
    txy.super_ellipse(r0=(0, 0), radius=radius, n=n_shape)
    txy.u[txy.X < 100] = 0

    txz = Scalar_mask_XZ(x, z, wavelength, n_background=n_out)
    txz.n = np.zeros_like(txy.u, dtype=complex)
    txz.n[txy.u > 0.5] = refractive_index
    txz.n[txy.u < 0.5] = 1

    return np.squeeze(txz.n)


class Thick_object(object):
    """Diffractio by thick objects and comparison to TEA.

    Parameters:
        wavelength (float): wavelength of the incident field


    Attributes:

        self.wavelength (float): Wavelength of the incident field.

    """

    def __init__(self, u0, fn, diameter, z0, n_in, n_out=1, parameters=None):
        """_summary_

        Args:
            u0 (_type_): _description_
            fn (function): _description_
            diameter (_type_): _description_
            z0 (_type_): _description_
            n_in (_type_): _description_
            n_out (int, optional): _description_. Defaults to 1.
            parameters (_type_, optional): _description_. Defaults to None.
        """

        self.u0 = u0
        self.z0 = z0

        self.wavelength = u0.wavelength
        self.refractive_index = n_in
        self.n_background = n_out

        self.fn = fn
        self.diameter_nominal = diameter

        self.parameters = parameters

        self.fields = dict(u_final=None, u_out_roi=None, u_axis_z=None)
        WPM = dict(I=None, angles=None, i_angles=None, diameters=None)
        TEA = dict(I=None, angles=None, i_angles=None, diameters=None)

        self.results = dict(WPM=WPM, TEA=TEA)

        self.format()

    def __str__(self):
        """Represents main data of the atributes."""
        print(" - wavelength:  {:2.2f} um".format(self.wavelength))
        print(" - diameter:    {:2.2f} um".format(self.diameter_nominal))
        print(" - fn:          {}".format(self.fn.__name__))

        print("\nPARAMETERS:")
        for name, value in self.parameters.items():
            if value is None:
                print(" - {}:        {}".format(name, value))
            elif isinstance(value, (tuple, list)):
                print(" - {}:        {}".format(name, len(value)))
            else:
                print(" - {}:        {}".format(name, value.shape))

        print("\nFIELDS:")
        for name, value in self.fields.items():
            if value is None:
                print(" - {}:        {}".format(name, "None"))
            elif isinstance(value,
                            (Scalar_source_X, Scalar_mask_XZ, Scalar_field_Z)):
                print(value)
            elif isinstance(value, (tuple, list)):
                print(" - {}:        {}".format(name, value))
            else:
                print(" - {}:        {}".format(name, value.shape))

        return ("")

    def format(self):

        rcParams['figure.dpi'] = 300
        rcParams['figure.figsize'] = (6, 3)
        rcParams['font.size'] = 14
        rcParams['figure.titlesize'] = 18
        rcParams['axes.grid'] = True

        return rcParams

    def compute_intensity_TEA(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        u0 = self.u0
        angles = self.results['TEA']['angles']
        # keep = self.parameters['keep']

        t_TEA = Scalar_mask_X(u0.x, u0.wavelength)
        t_TEA.slit(x0=0, size=self.diameter_nominal)
        u1_tea = u0 * t_TEA

        u_far_tea = u1_tea.fft(new_field=True, shift=True, remove0=False)
        i_max = np.argmax(u_far_tea.intensity())
        u_far_tea.u[i_max] = u_far_tea.u[i_max + 1]
        u_far_tea.normalize()
        angles = u_far_tea.x

        I_TEA = u_far_tea.intensity()

        self.results['TEA']['angles'] = angles
        self.results['TEA']['I'] = I_TEA

        return angles, I_TEA

    def compute_field_WPM(self):
        """_summary_
        """
        diameter = self.diameter_nominal
        refractive_index = self.refractive_index
        u_final, _, u_out_roi, u_axis_z, _, _, _ = self.u0.WPM(
            self.fn,
            self.z0,
            num_sampling=self.parameters['num_sampling'],
            ROI=self.parameters['ROI'],
            x_pos=0,
            has_edges=False)

        self.fields['u_final'] = u_final
        self.fields['u_axis_z'] = u_axis_z
        self.fields['u_out_roi'] = u_out_roi

    def compute_field_vacuum(self):
        """_summary_
            """
        diameter = self.diameter_nominal
        refractive_index = np.ones_like(self.refractive_index)
        u_final, _, u_out_roi, u_axis_z, _, _, _ = self.u0.WPM(
            func_vacuum,
            self.z0,
            num_sampling=self.parameters['num_sampling'],
            ROI=self.parameters['ROI'],
            x_pos=0,
            has_edges=False)

        self.fields['u_vacuum_final'] = u_final
        self.fields['u_vacuum_axis_z'] = u_axis_z
        self.fields['u_vacuum_out_roi'] = u_out_roi

    def compute_intensity_WPM(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        u_final = self.fields['u_final']

        u_far_fft = u_final.fft(new_field=True, shift=True, remove0=False)
        i_max = np.argmax(u_far_fft.intensity())
        u_far_fft.u[i_max] = u_far_fft.u[i_max + 1]
        u_far_fft.normalize()
        u_far_fft.u = u_far_fft.u - np.abs(u_far_fft.u).min()
        angles = u_far_fft.x

        I_WPM = u_far_fft.intensity()

        self.results['WPM']['angles'] = angles
        self.results['WPM']['I'] = I_WPM

        return angles, I_WPM

    def find_minima_diameters(self, angles, I, kind):
        """_summary_

        Args:
            angles (_type_): _description_
            I (_type_): _description_
            kind (_type_): _description_

        Returns:
            _type_: _description_
        """

        angles_minima, y_minima, i_angles = find_local_extrema(
            'minima', I, angles, 2)

        diameters = self.wavelength / np.sin(np.diff(angles_minima))

        if kind in ('WPM', 'TEA'):
            self.results[kind]['angles_minima'] = angles_minima
            self.results[kind]['i_angles'] = i_angles
            self.results[kind]['diameters'] = diameters

        return angles_minima, i_angles, diameters

    def get_error(self, kind, comparison):
        """_summary_

        Args:
            kind (_type_): _description_
            comparison (_type_): _description_

        Returns:
            _type_: _description_
        """

        diameters_WPM = self.results[kind]['diameters']
        diameters_TEA = self.results[comparison]['diameters']

        num_minima = min(len(diameters_WPM), len(diameters_TEA))
        error = (diameters_WPM[0:num_minima] -
                 diameters_TEA[0:num_minima]) / diameters_TEA[0:num_minima]
        self.results['error'] = error
        return error

    def remove_angles(self, angles, I, angles_lim, kind):
        """_summary_

        Args:
            angles (_type_): _description_
            I (_type_): _description_
            angles_lim (_type_): _description_
            kind (_type_): _description_

        Returns:
            _type_: _description_
        """
        angles_min, angles_max = angles_lim
        keep = np.bitwise_and(angles > angles_min, angles < angles_max)

        self.parameters['keep'] = keep

        I = I[keep]
        angles = angles[keep]

        if kind in ('WPM', 'TEA'):
            self.results[kind]['angles'] = angles
            self.results[kind]['I'] = I

        return angles, I

    def draw_final_field(self, kind='intensity', logarithm=0, size=None):
        """_summary_

        Args:
            kind (str, optional): _description_. Defaults to 'intensity'.
            logarithm (int, optional): _description_. Defaults to 0.
            size (_type_, optional): _description_. Defaults to None.
        """

        if size is None:
            size = 1.5 * self.diameter_nominal

        u_final = self.fields['u_final']
        u_final.draw(kind=kind, logarithm=logarithm)
        plt.xlim(-size, size)

    def draw_ROI(self, kind='intensity', logarithm=0, draw_borders=True):
        """_summary_

        Args:
            kind (str, optional): _description_. Defaults to 'intensity'.
            logarithm (int, optional): _description_. Defaults to 1.
            draw_borders (bool, optional): _description_. Defaults to True.
        """
        u_out_roi = self.fields['u_out_roi']

        u_out_roi.draw(kind=kind,
                       logarithm=logarithm,
                       draw_borders=draw_borders)
        plt.gca().set_aspect('equal', adjustable='box')

    def draw_ROI_vacuum(self,
                        kind='intensity',
                        logarithm=1,
                        draw_borders=True):
        """_summary_

        Args:
            kind (str, optional): _description_. Defaults to 'intensity'.
            logarithm (int, optional): _description_. Defaults to 1.
            draw_borders (bool, optional): _description_. Defaults to True.
        """
        u_out_roi = self.fields['u_out_roi'] - self.fields['u_vacuum_out_roi']

        u_out_roi.draw(kind=kind,
                       logarithm=logarithm,
                       draw_borders=draw_borders)
        plt.axis('scaled')

    def draw_diameters(self, kinds=('WPM', 'TEA')):
        """_summary_

        Args:
            kinds (tuple, optional): _description_. Defaults to ('WPM', 'TEA').
        """

        plt.figure()
        colors = ('r.', 'b.', 'k.', 'g.', 'c.')

        for i, kind in enumerate(kinds):
            angles = self.results[kind]['angles_minima']
            diameters = self.results[kind]['diameters']

            plt.plot(angles[1:] / degrees, diameters, colors[i], label=kind)
            plt.xlabel("$\phi$ ($^o$)")
            plt.ylabel("D ($\mu$m)")
            plt.legend()

    def draw_error(self):
        """_summary_
            """
        error = self.results['error']
        angles_minima_TEA = self.results['TEA']['angles_minima']

        num_minima = len(error)

        delta_angle = angles_minima_TEA[1] - angles_minima_TEA[0]
        plt.figure()
        plt.plot((angles_minima_TEA[0:num_minima] + delta_angle / 2) / degrees,
                 error * 100,
                 'r.',
                 label='WPM')
        plt.xlabel("$\phi$ (º)")
        plt.ylabel("error (%)")
        plt.xlim(angles_minima_TEA[0] / degrees,
                 angles_minima_TEA[-1] / degrees)
        plt.grid()

    def draw_diffraction_pattern(self, kinds):

        plt.figure()
        marker = itertools.cycle((',', '+', '.', 'o', '*'))
        colors = itertools.cycle(('r', 'b', 'g', 'k'))
        linestyle = itertools.cycle(('-', '-.', '--'))
        
        
        for kind in kinds:
            angles = self.results[kind]['angles']
            I = self.results[kind]['I']
            angles_minima = self.results[kind]['angles_minima']
            i_angles = self.results[kind]['i_angles']
            color = next(colors)

            plt.semilogy(angles / degrees, I, color=color, label=kind)
            plt.semilogy(angles[i_angles] / degrees,
                         I[i_angles],
                         color=color,
                         linestyle='',
                         marker='o')
        plt.legend()

        plt.xlabel(r'$\theta$ ($^{o}$)')
        plt.ylabel(r"I($\theta$)")
        plt.xlim(angles_minima[0] / degrees, angles_minima[-1] / degrees)
        plt.grid()


def format_linea(linea, num_data):
    """
    """
    if len(linea) == num_data:
        pass
    elif len(linea) < num_data:
        num_add = num_data - len(linea)
        linea = np.concatenate((linea, np.zeros(num_add)))
    elif len(linea) > num_data:
        linea = linea[0:num_data]

    return linea


"""
def compute_intensity_rigorous(self,
                                angles,
                                num_terms=150,
                                has_draw=False):

    diameter = self.diameter_nominal
    wavelength = self.wavelength

    k = 2 * np.pi / wavelength
    radius = diameter / 2

    airy_f, airy_p, airy_i, airy_ip = ai_zeros(num_terms)

    C_p = np.pi**(3 / 2) / (3 * 6**(1 / 3) * airy_ip**2)
    C_s = np.pi**(3 / 2) / (3 * 6**(1 / 3) * airy_f * airy_i**2)

    delta_p = 1 / (2 * k**(2 / 3)) * (radius / 6)**(1 / 3) * airy_f
    delta_s = 1 / (2 * k**(2 / 3)) * (radius / 6)**(1 / 3) * airy_p
    kappa_p = (np.sqrt(3) / 2) * (k * radius / 6)**(1 / 3) * airy_f
    kappa_s = (np.sqrt(3) / 2) * (k * radius / 6)**(1 / 3) * airy_p

    v_p = 1j * k * (radius + delta_p) - kappa_p
    v_s = 1j * k * (radius + delta_s) - kappa_s
    angles2 = angles        plt.xlabel('$\phi$ (º)')

    angles = np.remainder(angles, 2 * np.pi) - np.pi

    R = 1000 * mm
    gamma_plus = np.pi + angles
    gamma_minus = np.pi - angles

    k1 = np.exp(1j * k * R) / np.sqrt(R)
    k1 = 1
    u_geom = k1 * np.sqrt(
        radius / 2 * np.abs(np.cos(angles / 2))) * np.exp(
            -2j * k * radius * np.abs(np.cos(angles / 2)))

    k2 = np.exp(1j * k * R) / np.sqrt(2 * k * R)
    k2 = 1
    t1_diffractive = k2 * (k * radius)**(1 / 3) * np.exp(1j * np.pi / 12)

    u_p = np.zeros_like(angles)
    u_s = np.zeros_like(angles)

    for i in range(num_terms):
        print("{}/{}".format(i, num_terms), end="\r")
        u_p = u_p + C_p[i] / (1 - np.exp(2 * np.pi * 1j * v_p[i])) * (
            np.exp(1j * v_p[i] * gamma_plus) +
            np.exp(1j * v_p[i] * gamma_minus))

        u_s = u_s + C_s[i] / (1 - np.exp(2 * np.pi * 1j * v_s[i])) * (
            np.exp(1j * v_s[i] * gamma_plus) +
            np.exp(1j * v_s[i] * gamma_minus))

    u_p = u_p * t1_diffractive
    u_s = u_s * t1_diffractive

    u_final_p = u_geom + u_p
    u_final_s = u_geom + u_s

    I_final_p = np.abs(u_final_p)**2
    I_final_s = np.abs(u_final_s)**2

    I_final_p = I_final_p / I_final_p.max()
    I_final_s = I_final_s / I_final_s.max()

    if has_draw is True:
        I_final_p = I_final_p
        I_final_s = I_final_s
        plt.figure()
        plt.semilogy(angles2 / degrees, I_final_s, 'r', label='s')
        plt.semilogy(angles2 / degrees, I_final_p, 'b', label='p')
        plt.grid('on')
        plt.xlim()
        plt.legend()

    self.results['rigorous_p']['angles'] = angles
    self.results['rigorous_p']['I'] = I_final_p
    self.results['rigorous_s']['angles'] = angles
    self.results['rigorous_s']['I'] = I_final_s

    return I_final_p, I_final_s
"""