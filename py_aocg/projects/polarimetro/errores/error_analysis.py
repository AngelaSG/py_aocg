
import numpy as np
import matplotlib.pyplot as plt

from py_pol.jones_matrix import Jones_matrix, create_Jones_matrices
from py_pol.jones_vector import Jones_vector, degrees
from py_pol.mueller import Mueller
from py_pol.stokes import Stokes

from py_lab.setups.slm_calibration import *
from py_lab.config import optics_angles





def generate_intensities(sigma,J,has_draw=True):

    """
    Generate random angular positions of polarizers and retarders with a certain
    Jones matrix. This function also compare the random result with theoretical one.

    Parameters:
    sigma (float): Width of normal distribution which is related to the variation of
    the angular positions of polarizers and retarders.
    J (Jones_matrix): Jones matrix to study
    has_draw(bool): If True the theoretical and random intensities are shown. Default: True

    Return:
    I (np.ndarray): Theoretical intensities.
    I_rand (np.ndarray): Intensities calculated with random angles.
    """


    s = np.random.normal(0, sigma, (16,4))
    angles_variation = optics_angles + s
    angles_variation[angles_variation<0] = 2*np.pi + angles_variation[angles_variation<0]

    I_rand,I = np.zeros((16,256)),np.zeros((16,256))
    levels = np.arange(0,256)
    colors = ('b','k','k+')
    plt.figure(figsize=(18, 14))


    for ind in range(16):
        #print(optics_angles[ind])
        amp,phase = Calculate_Transmission(optics_angles[ind],J,E0=None)
        amp_rand,phase_rand = Calculate_Transmission(angles_variation[ind],J,E0=None)
        I_rand[ind] = amp_rand
        I[ind] = amp

        if has_draw:
            plt.subplot(4,4,ind+1)
            plt.plot(levels,amp,colors[0],label='Theoretical')
            plt.plot(levels,amp_rand,colors[1],label='Rand')
            plt.title('I = {}'.format(ind))
            plt.legend(loc='best')
            plt.ylabel('Intensity (a.u)')

        else:
             plt.close()


    return I, I_rand


def compare_Jones(I,J,has_draw=True):
    """
    Compare theoretical Jones matrix with the one calculated with random angles.

    Parameters:
    I (np.ndarray): Intensities calculated with random angles.
    J (Jones_matrix): Theoretical Jones matrix.
    has_draw (bool): If True the theoretical and random Jones matrix are shown. Default: True

    Return:
    comp (list): List of J0, J1, J2, J3, det, d1, d2, d3 of Jones matrix calculated
    with random angles.

    """

    levels = np.arange(0,256)
    #Las dimensiones del aray comp son: (# de elementos que devuelve el método
    #Hoyo,niveles de gris,# pasos)
    comp = np.zeros((8,len(levels),len(I[0,0])))

    for i in range(len(I[0,0])):
        print(i,end='\r',sep='\r')
        comp[:,:,i] = Metodo_Hoyo_Ext(I[:,:,i],None)


    mean_comp = np.mean(comp,axis=2)
    std_comp = np.std(comp,axis=2)


    J00,J01,J10,J11 = J.parameters.components(out_number=False,draw=False,verbose=False);
    det = J.parameters.det(draw=False)
    global_phase=J.parameters.global_phase(draw=False)
    theorical_components = [np.abs(J00),np.abs(J01),np.abs(J10),np.abs(J11),np.abs(det),np.angle(J01)-np.angle(J00),np.angle(J10)-np.angle(J00),np.angle(J11)-np.angle(J00)]

    colors = ('g','k')
    titles = ('|J0|', '|J1|', '|J2|', '|J3|', 'det', 'd1', 'd2', 'd3')
    plt.figure(figsize=(18, 10))
    if has_draw:
        for ind in range(8):
                plt.subplot(2,4,ind+1)
                plt.plot(levels,theorical_components[ind],colors[0],label='Theoretical')
                plt.plot(levels,mean_comp[ind],colors[1],markersize=3,label='mean')
                plt.fill_between(levels,mean_comp[ind,:]+std_comp[ind,:],mean_comp[ind,:] - std_comp[ind,:],color='mediumpurple',label='$\pm$ std',alpha=0.5)
                plt.fill_between(levels,mean_comp[ind,:] + 2*std_comp[ind,:],mean_comp[ind,:] - 2*std_comp[ind,:],color='purple',alpha=0.2,label='$\pm$ 2std')
                plt.title(titles[ind])
                plt.legend(loc='best')
                if ind<5:
                    plt.ylim(-0.01,1)

    return comp,mean_comp,std_comp



def check_intensities(comp,J,has_draw=True):
    """
    Check the variation of the theoretical and random intensities after calculating
    the Jones matrix.

    Parameters:
    comp (list): List of J0, J1, J2, J3, det, d1, d2, d3 of Jones matrix calculated
    with random angles.
    angles_rand (np.ndarray): Random angles obtained with normal distribution.
    J (Jones_matrix): Theoretical Jones matrix.
    has_draw (bool): If True the theoretical and random intensities are shown. Default: True

    """


    J_hoyo_ext = Components_To_Matrix(comp)
    E0 = Jones_vector().circular_light(intensity=2)

    #Fase global de la matriz de Jones?
    #print(J_hoyo_ext.parameters.global_phase(draw=False))
    colors = ('b','k')
    levels = np.arange(0,256)
    plt.figure(figsize=(18, 14))

    for ind in range(16):
        for ort in range(2):

            angles = Calculate_Angles(
                    ind,
                    ort,
                    mirrors=[False,False],
                    check_repeat=False,
                    return_angles=True,
                    add_zero=False)


            I_hoyo, P_hoyo = Calculate_Transmission(angles, J_hoyo_ext, E0=E0)
            amp, phase = Calculate_Transmission(angles, J, E0=E0)

            if ort == 0:
                if has_draw:
                    plt.subplot(4,4,ind+1)
                    plt.plot(levels,amp,colors[0],label='Theoretical')
                    #plt.errorbar(levels,amp,xerr=0.01,yerr=0.01,alpha=0.3)
                    plt.plot(levels,I_hoyo,colors[1],label='Rand')

                    plt.title('M = {}, O = {}'.format(ind,ort))
                    plt.ylabel('Intensity (a.u)')
                    plt.legend(loc='best')



def errors(sigma,steps,J,has_draw=True):

    """
    Calculate the mean and std of the resulting intensities with different angles of the
    polarizers and retarders.

    Parameters:
    sigma (float): Width of normal distribution which is related to the variation of
    the angular positions of polarizers and retarders.
    steps (int): Number of iterations of the loop which generate random angles.
    J (Jones_matrix): Theoretical Jones matrix.
    has_draw (bool): If True the mean and the standard deviation of each intensity is shown. Default: True


    Return:
    mean (np.ndarray): Mean of the intensities calculated in the loop.
    std (np.ndarray): Standard deviation of the caluclated intensities.
    I (np.ndarray): Theoretical intensities.
    """

    intensities = np.zeros((16,256,steps))
    levels = np.arange(0,256)
    colors = ('g','k')

    for i in range(steps):
        print(i,end='\r',sep='\r')
        I, I_rand = generate_intensities(sigma,J,has_draw=False)
        intensities[:,:,i] = I_rand

    mean_rand = intensities.mean(axis=2)
    std_rand = np.std(intensities,axis=2)
    plt.figure(figsize=(18, 14))

    if has_draw:
        for i in range(16):
            plt.subplot(4,4,i+1)
            plt.plot(levels,mean_rand[i,:],colors[1],label='mean')
            plt.fill_between(levels,mean_rand[i,:]+std_rand[i,:],mean_rand[i,:] - std_rand[i,:],color='mediumpurple',label='$\pm$ std',alpha=0.5)
            plt.fill_between(levels,mean_rand[i,:] + 2*std_rand[i,:],mean_rand[i,:] - 2*std_rand[i,:],color='purple',alpha=0.2,label='$\pm$ 2std')
            plt.ylim(0,1.1)
            plt.plot(levels,I[i],colors[0],label='Theoretical')

            plt.legend(loc='best')
    else:
        plt.close()

    return intensities,mean_rand,std_rand


def parameters_Jones(comp,J,compare=True,has_draw=True):
    #print(len(comp))
    param = Analyze_Matrix(comp)
    levels = np.arange(0,256)
    R = [param["R"] / degrees]
    az_R = [param["azimuth R"] / degrees]
    el_R = [param["ellipticity R"] / degrees]
    p1 = [param["p1"]]
    p2 = [param["p2"]]
    az_D = [param["azimuth D"] / degrees]
    el_D = [param["ellipticity D"] / degrees]
    elements = [R, az_R, el_R, p1, p2, az_D, el_D]

    if compare:
        J00,J01,J10,J11 = J.parameters.components(out_number=False,draw=False,verbose=False);
        det = J.parameters.det(draw=False)
        global_phase=J.parameters.global_phase(draw=False)
        theorical_components =[np.abs(J00),np.abs(J01),np.abs(J10),np.abs(J11),np.abs(det),np.angle(J01)-np.angle(J00),np.angle(J10)-np.angle(J00),np.angle(J11)-np.angle(J00)]
        #print(theorical_components)

        param_t = Analyze_Matrix(theorical_components)

        R_t = [param_t["R"] / degrees]
        az_Rt = [param_t["azimuth R"] / degrees]
        el_Rt = [param_t["ellipticity R"] / degrees]
        p1t = [param_t["p1"]]
        p2t = [param_t["p2"]]
        az_Dt = [param_t["azimuth D"] / degrees]
        el_Dt = [param_t["ellipticity D"] / degrees]
        elements_t = [R_t, az_Rt, el_Rt, p1t, p2t, az_Dt, el_Dt]

    if has_draw:
        #print(np.max(param["p1"]))
        titles = ('Retardance', 'Azimuth R', 'Ellipticity R',
                  'Max. field trans.', 'Min. field trans.', 'Azimuth D',
                  'Ellipticity D')
        ylabels = ('Retardance (deg)', 'Azimuth  (deg)',
                   'Ellipticity (deg)', 'Field trans.', 'Field trans.',
                   'Azimuth (deg)', 'Ellipticity (deg)')
        plt.figure(figsize=(20, 10))
        for ind, elem in enumerate(elements):
            if elem is not None:
                elem = np.transpose(np.array(elem))
                plt.subplot(2, 4, ind + 1)
                if compare:
                    plt.plot(levels,elements_t)
                plt.plot(levels, elem)
                plt.title(titles[ind])
                plt.xlabel('SLM level')
                plt.ylabel(ylabels[ind])

    else:
        plt.close()



def check_all_intensities(comp,J,has_draw=True):
    """
    Check the variation of the theoretical and random intensities after calculating
    the Jones matrix.

    Parameters:
    comp (list): List of J0, J1, J2, J3, det, d1, d2, d3 of Jones matrix calculated
    with random angles.
    angles_rand (np.ndarray): Random angles obtained with normal distribution.
    J (Jones_matrix): Theoretical Jones matrix.
    has_draw (bool): If True the theoretical and random intensities are shown. Default: True

    """

    I = np.zeros((16,256,len(comp[0,0])))
    I_teoric = np.zeros((16,256))

    colors = ('g','k')
    levels = np.arange(0,256)
    plt.figure(figsize=(18, 14))

    for i in range(len(comp[0,0])):
      print(i,end='\r',sep='\r')
      J_hoyo_ext = Components_To_Matrix(comp[:,:,i])
      E0 = Jones_vector().circular_light(intensity=2)

      for ind in range(16):

              I_hoyo, P_hoyo = Calculate_Transmission(optics_angles[ind], J_hoyo_ext, E0=E0)
              amp, phase = Calculate_Transmission(optics_angles[ind], J, E0=E0)
              I[ind,:,i] = I_hoyo
              I_teoric[ind,:] = amp


    I_mean = np.mean(I,axis=2)
    I_std = np.std(I,axis=2)


    if has_draw:
        for j in range(16):
            std = 2*np.mean(I_std[j,:])
            plt.subplot(4,4,j+1)
            plt.plot(levels,I_teoric[j],colors[0],label='Theoretical')

            plt.plot(levels,I_mean[j],colors[1],label='mean')
            plt.fill_between(levels,I_mean[j,:]+I_std[j,:],I_mean[j,:] - I_std[j,:],color='mediumpurple',label='$\pm$ std',alpha=0.5)
            plt.fill_between(levels,I_mean[j,:] + 2*I_std[j,:],I_mean[j,:] - 2*I_std[j,:],color='purple',alpha=0.2,label='$\pm$ 2std')

            plt.title('I = {}, std = {:.3f}'.format(j,std))
            plt.ylabel('Intensity (a.u)')
            plt.legend(loc='best')
            plt.ylim(0,1)


    #Fase global de la matriz de Jones?

    return I,I_mean,I_std
