# !/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
    Proyecto Discrepo

DISCREPO: Dispositivo para la caracterización robusta de elementos fotónicos de polarización 
PDC2023-145843-I00  
Ministerio de Ciencia, Innovación y Universidades. Plan Estatal de Investigación Científica y Técnica y de Innovación 2021-2023. Subprograma Estatal de Transferencia de Conocimiento. Proyectos de «Prueba de Concepto» 2023 (PERTE CHIP). Subvención con cargo a NextGeneration EU. 
01/01/2024 - 31/12/2025
I.P.: Luis Miguel Sánchez Brea y Javier Alda Serrano
E.I.: Javier del Hoyo, Joaquín Andrés Porras
117.781,40 €


Este proyecto se basa en el desarrollo de un artículo científico y de una patente:
- J. del Hoyo, J. Andres-Porras, A. Soria-Garcia, L.M. Sanchez-Brea, V. Pastor-Villarrubia, M.H. Elshorbagy , J. Alda "Interferometric method for simultaneous characterization of retardance and fast axis of a retarder" Optics and Lasers in Engineering 179 108262 (2024) https://doi.org/10.1016/j.optlaseng.2024.108262 
- J. del Hoyo, J. Andrés-Porras, A. Soria-García, L.M. Sánchez-Brea, J. Alda "Dispositivo optoelectrónico para determinar de forma simultánea la retardancia absoluta y el ángulo de giro de un retardador óptico" P202330488 Concesión ES2948491 B2 09/06/2023 Fecha de concesión 22/02/2024 https://consultas2.oepm.es/InvenesWeb/detalle?referencia=P202330488


Después de los experimentos para la patente y el artículo científico, se desarrollo el TFM:
- Armando David González García "Dispositivo optoelectrónico basado en un interferómetro de Young para caracterizar un retardador óptico" Máster en Nuevas Tecnologías Electrónicas y Fotónicas. Universidad Complutense de Madrid 2024

Los datos a transferir aquí están en:
../profesion/docencia/docencia TFM/2024 Armando Gonzalez/
"""