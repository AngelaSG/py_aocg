================================================
Python py_aocg module
================================================


.. image:: https://img.shields.io/pypi/v/py_aocg.svg
        :target: https://pypi.org/project/py_aocg/

.. image:: https://img.shields.io/travis/angsoria/py_aocg.svg
        :target: https://bitbucket.org/angsoria/py_aocg/src/master/

.. image:: https://readthedocs.org/projects/py_aocg/badge/?version=latest
        :target: https://py_aocg.readthedocs.io/en/latest/
        :alt: Documentation Status


* Free software: MIT license

* Documentation: https://py_aocg.readthedocs.io/en/latest/


.. image:: logo.png
   :width: 75
   :align: right


Features
----------------------



Credits
---------------------------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
