.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Python py_aocg , run this command in your terminal:

.. code-block:: console

	# Linux:
	$ pip3 install py_aocg

	# Windows:
	$ pip install py_aocg


This is the preferred method to install Python py_aocg , as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


Additional packages
------------------------

py_aocg uses also the following non-standard modules:

* py-pol

In some schemes, the following modules are also required:

* screeninfo
* PIL
* mayavi, traits, tvtk
* python-opencv

They should previously be installed before py_aocg module.


From sources
------------

The sources for Python py_aocg  can be downloaded from the `Bitbucket repo`_.

You can either clone the public repository:

.. code-block:: console

	$ git clone git@bitbucket.org:angsoria/py_aocg.git
	$ git clone https://angsoria@bitbucket.org/angsoria/py_aocg.git



Once you have a copy of the source, you can install it with:

.. code-block:: console

	# Linux:
	$ python3 setup.py install

	# Windows:
	$ python setup.py install



.. _Bitbucket repo: https://bitbucket.org/angsoria/py_aocg/src/master/
