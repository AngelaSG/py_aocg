import numpy as np
from diffractio import mm


def generate_rings_mask(transitions, t_mask, has_draw=False):
    num_rings=len(transitions)

    inner_radii = np.zeros(int(num_rings/2))
    outer_radii = np.zeros(int(num_rings/2))

    inner_radii[:]=transitions[0::2]  #Primer argumento: Todas las partículas
    outer_radii[:]=transitions[1::2]

    t_mask.rings(r0=(0,0), inner_radius=inner_radii, outer_radius=outer_radii, mask=True)
    t_mask.u = np.exp(1j*np.pi*t_mask.u)
    t_mask.pupil()

    if has_draw:
        t_mask.draw('field')

    return t_mask


def focus_intensity_rings(transitions, t_mask, has_draw=False):

    transitions.sort()

    t_mask=generate_rings_mask(transitions, t_mask, has_draw=False)

    focal=5*mm
    u2=t_mask.RS(z=focal,verbose=False)

    if has_draw:
        u2.draw(has_colorbar='vertical', logarithm=False)


    return u2.intensity()

def cost_function_1(intensity):

    return -intensity.max()

def optimization_loop_1(transitions, t_mask, has_draw=False):

    intensity = focus_intensity_rings(transitions, t_mask, has_draw=False)
    cost1 = cost_function_1(intensity)
    return cost1


def opt_func_1(Transitions, t_mask):
    num_particles = Transitions.shape[0]  # number of particles
    intensidad = [optimization_loop_1(Transitions[i,:],t_mask) for i in range(num_particles)]
    return intensidad
