py\_aocg package
================

Submodules
----------

py\_aocg.IFTA module
--------------------

.. automodule:: py_aocg.IFTA
   :members:
   :undoc-members:
   :show-inheritance:

py\_aocg.angular\_lens module
-----------------------------

.. automodule:: py_aocg.angular_lens
   :members:
   :undoc-members:
   :show-inheritance:

py\_aocg.optimizacion module
----------------------------

.. automodule:: py_aocg.optimizacion
   :members:
   :undoc-members:
   :show-inheritance:

py\_aocg.utils\_multiprocessing module
--------------------------------------

.. automodule:: py_aocg.utils_multiprocessing
   :members:
   :undoc-members:
   :show-inheritance:

py\_aocg.utils\_tests module
----------------------------

.. automodule:: py_aocg.utils_tests
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py_aocg
   :members:
   :undoc-members:
   :show-inheritance:
