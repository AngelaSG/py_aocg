===========
Credits
===========

Development Lead
---------------------------

* Angela Soria García <angsoria@ucm.es>
* Luis Miguel Sánchez Brea <optbrea@ucm.es>

    **Universidad Complutense de Madrid**,
    *Applied Optics Complutense Group*
    Faculty of Physical Sciences,
    Department of Optics
    Plaza de las ciencias 1,
    ES-28040 Madrid (Spain)

Contributors
--------------

* Javier Alda Serrano <javier.alda@ucm.es>
* Jesus del Hoyo Muñoz <jhoyo@ucm.es>a
* Mahmoud Elshorbagy <mahmouha@ucm.es>
* Verónica Pastor Villarrubia <mapast09@ucm.es>
